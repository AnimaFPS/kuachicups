FROM rustlang/rust:nightly@sha256:9e61b4db550ca53ab4adfc3287e49afb22180578161ef195773c85858723be08 AS server-builder
    WORKDIR /server
    RUN USER=root cargo init && cargo install movine --version=^0.8
    COPY ./server/Cargo.toml ./server/Cargo.lock ./
    RUN cargo build --release && \
        rm src/main.rs && \
        rm ./target/release/deps/kuachicups*
    COPY ./server/sqlx-data.json ./sqlx-data.json
    COPY ./server/src ./src
    RUN SQLX_OFFLINE=true cargo build --release

FROM node:buster-slim AS client-builder
    WORKDIR /client
    COPY ./client/package.json ./client/.babelrc ./client/.sassrc ./client/tsconfig.json ./client/yarn.lock ./client/webpack.config.js ./
    RUN yarn
    COPY ./client/src-simplestatic ./src-simplestatic
    COPY ./client/src ./src
    COPY ./branding/logo_text.png ./src/logo_text.png
    COPY ./branding/favicon.png ./src/favicon.png
    RUN NODE_ENV=production yarn webpack --output-path /var/www

FROM debian:buster-slim
    RUN apt-get update && apt-get -y install curl sqlite3 && apt-get clean && rm -rf /var/lib/apt/lists/*
    COPY --from=server-builder /usr/local/cargo/bin/movine /bin/
    COPY --from=server-builder /server/target/release/kuachicups-server /bin/
    COPY --from=client-builder /var/www /var/www
    COPY ./server/db/migrations /db/migrations
    COPY ./server/db/movine-server.toml /db/movine.toml
