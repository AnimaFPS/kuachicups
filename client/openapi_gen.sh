#!/usr/bin/env bash
set -e
wget localhost:8000/api/openapi.json -O src/openapi.json
rm -r src/api/generated
yarn run openapi-generator \
  generate -i src/openapi.json \
  -g typescript-fetch -c openapi.cfg.json \
  -o src/api/generated

