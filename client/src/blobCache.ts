import { useAsync } from "./components/async/Awaitable";

let blobCache: undefined | Map<string, Promise<string | undefined>>;

export default function useBlobCache(props: () => { id: string; src: string }) {
  if (blobCache === undefined) {
    blobCache = new Map();
  }
  return useAsync(async () => {
    const { id, src } = props();
    if (src !== "") {
      const cached = blobCache!.get(id);
      if (cached === undefined) {
        const getter = fetch(src)
          .then((f) => f.blob())
          .then((blob) => {
            if (blob.size === 0 || blob.type === "") {
              return undefined;
            }
            return URL.createObjectURL(blob);
          })
          .catch(() => undefined);
        blobCache!.set(id, getter);
        return getter;
      } else {
        return cached;
      }
    }
    return undefined;
  });
}
