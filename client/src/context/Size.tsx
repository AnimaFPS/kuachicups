import {
  useContext,
  createContext,
  createEffect,
  createSignal,
  onCleanup,
  createMemo,
  JSX,
} from "solid-js";
import log from "loglevel";
import { debounce } from "lodash";

interface Size {
  width: number;
  height: number;
  small: boolean;
}

const computeSize = (
  width: number = document.documentElement.clientWidth,
  height: number = document.documentElement.clientHeight
) => {
  return {
    width,
    height,
    small: width <= 1024,
    mobile: width <= 768,
  };
};

const Context = createContext<{
  size(): Size;
  small(): boolean;
  mobile(): boolean;
}>();

export function Provider<T extends { children: any }>(props: T) {
  let lastSize = computeSize();
  const [size, setSize] = createSignal(lastSize);
  const [small, setSmall] = createSignal(lastSize.small, (a, b) => a === b);
  const [mobile, setMobile] = createSignal(lastSize.mobile, (a, b) => a === b);

  function update() {
    const width = innerWidth;
    const height = innerHeight;
    log.info("resize", { width, height });
    if (lastSize.width !== width || lastSize.height !== height) {
      const s = computeSize(width, height);
      setSmall(s.small);
      setMobile(s.mobile);
      setSize(s);
    }
  }

  const listener = debounce(update, 50, {
    leading: false,
    trailing: true,
    maxWait: 150,
  });

  update();

  addEventListener("resize", listener);
  onCleanup(() => {
    log.info("cleaned up resize");
    removeEventListener("resize", listener);
  });

  return (
    <Context.Provider value={{ size, small, mobile }}>
      {props.children}
    </Context.Provider>
  );
}

export const useSize = () => useContext(Context).size;
export const useSmall = () => useContext(Context).small;
export const useMobile = () => useContext(Context).mobile;

const ContentSizeContext = createContext<{
  contentWidth: () => number;
}>({
  contentWidth: () => 800,
});

export function ContentSizeProvider(props: {
  id?: string;
  children: JSX.Element;
}) {
  const size = useSize();
  const width = createMemo(() => size().width);
  const [contentWidth, setContentWidth] = createSignal(800);
  const div = document.getElementById("measure")!;
  createEffect(() => {
    if (div === undefined) return 800;
    width();
    setContentWidth(div.offsetWidth);
  });
  return (
    <div id={props.id} class="container">
      <ContentSizeContext.Provider value={{ contentWidth }}>
        {props.children}
      </ContentSizeContext.Provider>
    </div>
  );
}

export const useContentWidth = () =>
  useContext(ContentSizeContext).contentWidth;

export function ClampY(props: { children: JSX.Element }) {
  // TODO
  return props.children;
}
