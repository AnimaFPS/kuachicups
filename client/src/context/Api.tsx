import { createContext, useContext } from "solid-js";
import createApi, { Api } from "../api";

const Context = createContext<Api>();

const useApi = () => useContext(Context);
export default useApi;

export function Provider<T extends { children: any }>(props: T) {
  const value = createApi();
  return <Context.Provider value={value}>{props.children}</Context.Provider>;
}
