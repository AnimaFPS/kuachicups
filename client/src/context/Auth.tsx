import {
  createContext,
  useContext,
  createSignal,
  untrack,
  JSX,
} from "solid-js";
import { Player, api } from "../api";
import { CaseOf } from "../components/Case";

export enum LoginState {
  Unknown,
  LoggedIn,
  NotLoggedIn,
}

export type AuthCtx = AuthState & {
  then: Promise<AuthState>;
};

export type AuthState =
  | {
      state: LoginState.Unknown | LoginState.NotLoggedIn;
      login: null;
      admin: false;
    }
  | {
      state: LoginState.LoggedIn;
      login: Player;
      admin: boolean;
    };

export type Auth = () => AuthCtx;

export const Context = createContext<Auth>();

export function Provider<T extends { children: any }>(props: T) {
  const getAuth: () => Promise<AuthState> = async () => {
    try {
      const login = await api.playerCurrentSession();
      const admin = (await api.playerCurrentSessionIsAdmin()).isAdmin;
      return { state: LoginState.LoggedIn, login, admin };
    } catch (_e) {
      return { state: LoginState.NotLoggedIn, login: null, admin: false };
    }
  };

  const [auth, setAuth] = createSignal<AuthCtx>({
    state: LoginState.Unknown,
    login: null,
    admin: false,
    then: getAuth().then((s) => {
      untrack(() =>
        setAuth({
          ...s,
          then: Promise.resolve(s),
        } as AuthCtx)
      );
      return s;
    }),
  });

  return <Context.Provider value={auth}>{props.children}</Context.Provider>;
}

const useAuth = () => useContext(Context);
export default useAuth;

export function WithLogin(props: {
  children: (player: Player) => JSX.Element;
  fallback?: JSX.Element;
}) {
  const auth = useAuth();
  return (
    <CaseOf data={auth()} key="state" fallback={props.fallback}>
      {{
        [LoginState.LoggedIn]: (s) => props.children(s.login),
      }}
    </CaseOf>
  );
}

export function RequireLogin(props: {
  children: JSX.Element;
  fallback?: JSX.Element;
}) {
  const auth = useAuth();
  return (
    <CaseOf data={auth()} key="state" fallback={props.fallback}>
      {{
        [LoginState.LoggedIn]: () => props.children,
      }}
    </CaseOf>
  );
}
