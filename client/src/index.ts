import "solid-js";
import { setLevel } from "loglevel";
import { render } from "solid-js/dom";
import App from "./components/App";
import "./theme.scss";

setLevel(0);
render(App, document.getElementById("root") as Node);
