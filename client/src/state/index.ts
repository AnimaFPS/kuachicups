import { createContext, createResourceState, JSX } from "solid-js";
import * as cups from "./slice/cups";
import * as players from "./slice/players";
import { MkAppState } from "./types";

export type IAppState = MkAppState<
  [
    ["cups", cups.IState, cups.IActions],
    ["players", players.IState, players.IActions]
  ]
>;

export const AppStateContext = createContext<IAppState>();

export function AppStateProvider(props: { children: JSX.Element }) {
  return undefined;
}
