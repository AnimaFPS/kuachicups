import log from "loglevel";
import { createResourceState, State } from "solid-js";
import {
  Cup,
  CupMatch2p,
  CupSignup,
  CupStage,
  CupStageScoring,
} from "../../api";
import useApi from "../../context/Api";

export interface IState {
  [cupId: string]: {
    cup?: Cup;
    signups?: CupSignup[];
    stages?: {
      [stageId: string]: {
        stage?: CupStage;
        matches?: CupMatch2p[];
        scorings?: { [scoringId: string]: CupStageScoring };
      };
    };
  };
}

export interface IActions {}
export const initialState: IState = {};

export function create() {
  const api = useApi();
  const [state, load] = createResourceState<IState>({});
  return {
    state,
    getCup(cupId: string) {
      async function getter() {
        return { cup: await api.cups.byId(cupId) };
      }
      // has no effect
      load({ [cupId]: getter });
    },
  };
}
