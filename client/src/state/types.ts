export type MkAppState<
  A extends [string, any, any][],
  StateAcc = {},
  ActionsAcc = {}
> = A extends [[infer Name, infer State, infer Actions], ...infer XS]
  ? MkAppState<
      Extract<XS, [string, any, any][]>,
      StateAcc & Record<Extract<Name, string>, State>,
      ActionsAcc & Actions
    >
  : { state: StateAcc; actions: ActionsAcc };
