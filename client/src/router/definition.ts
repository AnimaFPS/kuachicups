const routes = [
  { name: "index", path: "/" },

  {
    name: "announcements",
    path: "/ann?page",
    children: [
      { name: "unpublished", path: "/unpublished" },
      { name: "new", path: "/new" },
      {
        name: "announcement",
        path: "/:id",
        children: [{ name: "edit", path: "/edit" }],
      },
    ],
  },

  { name: "logged_in", path: "/logged_in" },

  { name: "privacy_policy", path: "/privacy_policy" },

  { name: "media", path: "/media?page" },

  {
    name: "teams",
    path: "/teams?page&query",
    children: [
      { name: "invites", path: "/invites" },
      { name: "mine", path: "/mine" },
      { name: "new", path: "/new" },
      {
        name: "profile",
        path: "/:id",
        children: [
          {
            name: "membership",
            path: "/membership",
          },
          {
            name: "edit",
            path: "/edit",
          },
          {
            name: "invite",
            path: "/invite",
          },
        ],
      },
    ],
  },

  {
    name: "cups",
    path: "/cups?page&active",
    children: [
      { name: "new", path: "/new" },
      {
        name: "cup",
        path: "/:id",
        children: [
          { name: "manage", path: "/manage" },
          { name: "edit", path: "/edit" },
          { name: "schedule", path: "/schedule" },
          {
            name: "stage",
            path: "/stage/:stageNo",
            children: [
              { name: "bracket", path: "/bracket" },
              { name: "play", path: "/play" },
            ],
          },
        ],
      },
    ],
  },

  {
    name: "players",
    path: "/players?page",
    children: [{ name: "profile", path: "/:id" }],
  },
] as const;

export default routes;
