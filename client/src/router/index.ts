import createSolidRouter, { Descend, ReadRoutes } from "solid-typefu-router5";
import createRouter, { State as RouteState } from "router5";
import browserPluginFactory from "router5-plugin-browser";
import routes from "./definition";
import { ElementOf } from "ts-essentials";
import { RSM } from "solid-typefu-router5/dist/components/Router";
import { RouteMeta } from "solid-typefu-router5/dist/types";

export { useRoute, useIsActive } from "solid-typefu-router5";

export const LOGIN_DEST_KEY = "login_dest";

export type { RouteState };

type Routes = ReadRoutes<typeof routes>;

export const { Router, Provider, Link, router, navigate } = createSolidRouter<
  typeof routes,
  Routes
>({
  routes,

  navActiveClass: "is-active",

  createRouter5: (routes) => {
    const router = createRouter(routes, { allowNotFound: true });
    router.usePlugin(browserPluginFactory({ useHash: false }));
    return router;
  },

  onStart: (router) => {
    // Perform login redirect
    const here = router.getState();
    switch (here.name) {
      case "logged_in": {
        const dest = localStorage.getItem(LOGIN_DEST_KEY);
        const state = here.params.state;
        if (dest !== null) {
          const redirect: { state: string; dest: RouteState } = JSON.parse(
            dest
          );
          if (redirect.state === state) {
            router.navigate(redirect.dest.name, redirect.dest.params);
          }
          localStorage.removeItem(LOGIN_DEST_KEY);
        }
        break;
      }
    }
  },
});
