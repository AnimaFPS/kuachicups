import jscookie from "js-cookie";
import log from "loglevel";

export const CSRF_HEADER = "X-CSRF-Token";
export const CSRF_COOKIE = "csrf";

export interface Csrf {
  header: string;
}

export let getCsrfHeader: () => string = () => {
  log.error("using invalid csrf");
  return "_invalid";
};

const header = jscookie.get(CSRF_HEADER);
const cookie = jscookie.get(CSRF_COOKIE);

if (typeof header === "string" && typeof cookie === "string") {
  getCsrfHeader = () => {
    jscookie.set(CSRF_COOKIE, cookie);
    return header;
  };
}
