import {
  cacheByParentIdWithIdCache,
  cacheById,
  cachePaginated,
  cacheByIds,
} from "./helpers";
import {
  Cup,
  CupStage,
  CupMatch2p,
  CupCreateForm,
  CupSignup,
  CupStageScoring,
  DefaultApi,
  Standings,
} from "../generated";

export default function createCupCache(api: DefaultApi) {
  const byId = cacheByIds<Cup, "id">("id")({
    name: "cups",
    byIds: (ids: string[]) => api.cupsCups({ ids }),
  });

  return cachePaginated<Cup, "id">("id")({
    ...byId,

    page: (page: number) => api.cupsCupsPage({ page }),

    create: async (form: CupCreateForm): Promise<string> => {
      const { cup } = await api.cupsCupCreate({ cupCreateForm: form });
      byId.cache[cup.id] = Promise.resolve(cup);
      return cup.id;
    },

    scorings: cacheById<CupStageScoring, "id">("id")({
      name: "cups.cup.scorings",
      byId: (id: number) => api.cupsCupStageScoring({ scoringId: id }),
    }),

    signups: cacheByParentIdWithIdCache<CupSignup, "cupId", "id">(
      "cupId",
      "id"
    )({
      name: "cups.cup.signups",
      byParentId: async (id) =>
        (await api.cupsCupSignups({ id })).sort((a, b) => {
          const seedA = a.seedValue;
          const seedB = b.seedValue;
          if (typeof seedA === "number" && typeof seedB === "number") {
            return seedA > seedB ? 1 : -1;
          }
          return a.id > b.id ? 1 : -1;
        }),
    }),

    stages: cacheByParentIdWithIdCache<CupStage, "cupId", "stageNo">(
      "cupId",
      "stageNo"
    )({
      name: "cups.stages",
      byParentId: (id: string) => api.cupsCupStages({ id }),
      standings: cacheById<{ id: string; standings: Standings }, "id">("id")({
        name: "cups.stages.standings",
        byId: async (stageId: string) => ({
          id: stageId,
          standings: await api.cupsCupStageStandings({ stageId }),
        }),
      }),

      matches: cacheByParentIdWithIdCache<CupMatch2p, "cupStageId", "id">(
        "cupStageId",
        "id"
      )({
        name: "cups.stages.matches",
        byParentId: (stageId: string) => api.cupsCupStageMatches2p({ stageId }),
      }),
    }),
  });
}
