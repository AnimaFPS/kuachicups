import { DefaultApi } from "../generated";
import createAnnouncementsCache from "./announcements";
import createCupCache from "./cups";
import createGamesCache from "./games";
import createMediaCache from "./media";
import createPlayersCache from "./players";
import createTeamsCache from "./teams";

export default function createCache(api: DefaultApi) {
  return {
    players: createPlayersCache(api),
    teams: createTeamsCache(api),
    cups: createCupCache(api),
    games: createGamesCache(api),
    announcements: createAnnouncementsCache(api),
    media: createMediaCache(api),
  };
}

export type ApiCache = ReturnType<typeof createCache>;
