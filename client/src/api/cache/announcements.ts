import { DefaultApi, PostAnn } from "../generated";
import { cachePaginatedLazy, cacheByIds } from "./helpers";

export default function createAnnouncementsCache(api: DefaultApi) {
  const paginated = cachePaginatedLazy<PostAnn, "id">("id")({
    ...cacheByIds<PostAnn, "id">("id")({
      name: "announcements",
      byIds: (ids: string[]) => api.postsAnns({ ids }),
    }),
    page: (page: number) => api.postsAnnsPage({ page }),
  });

  return {
    ...paginated,
    unpublished: cachePaginatedLazy<PostAnn, "id">("id")({
      name: "announcements.unpublished",
      cache: paginated.cache,
      byId: paginated.byId,
      byIds: paginated.byIds,
      page: (page: number) => api.postsAnnsUnpublishedPage({ page }),
    }),
  };
}
