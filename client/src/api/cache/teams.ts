import { DefaultApi, Team } from "../generated";
import { cacheByParentId, cacheByIds, cachePaginated } from "./helpers";
import { PT } from "./players";

export default function createTeamsCache(api: DefaultApi) {
  return cachePaginated<Team, "id">(
    "id",
    true
  )({
    ...cacheByIds<Team, "id">("id")({
      name: "teams",
      byIds: (ids: string[]) => api.teamsTeams({ ids }),
    }),

    page: (page: number, terms?: string) => api.teamsTeamsPage({ page, terms }),

    // teams->players
    players: cacheByParentId<PT, "teamId">("teamId")({
      name: "team.players",
      byParentId: async (teamId: string) =>
        (await api.teamsPlayers({ id: teamId })).map((pt) => ({
          playerId: pt.playerId,
          teamId,
          role: pt.role,
        })),
    }),
  } as const);
}
