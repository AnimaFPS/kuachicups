import { Builder } from "./builder";
import log from "loglevel";

export const cacheById = <
  T extends Record<K, Id>,
  K extends string | number | symbol = keyof T,
  Id extends string | number | symbol = T[K]
>(
  idKey: K
): Builder<
  {
    name: string;
    byId: (id: Id) => Promise<T>;
  },
  {
    byId(ids: Id, refresh?: boolean): Promise<T>;
    byIds(ids: Id[], refresh?: boolean): Promise<T[]>;
    cache: { [id in Id]?: Promise<T> };
  }
> => (props) => {
  const { byId: __byId, name } = props;

  const cache: { [id in Id]?: Promise<T> } = {};
  async function byId(id: Id, refresh: boolean = false): Promise<T> {
    const cached: Promise<T> | undefined = cache[id];
    if (!refresh) {
      if (cached !== undefined) {
        log.info(name, "used cached value for", id);
        return cached;
      }
    }
    log.info(name, "retrieving", id);
    return (cache[id] = __byId(id));
  }
  return {
    ...props,
    idKey,
    cache,
    byId,
    byIds: (ids: Id[], refresh: boolean = false): Promise<T[]> =>
      Promise.all(ids.map((id) => byId(id, refresh))),
  };
};

export const cacheByIds = <
  T extends Record<K, Id>,
  K extends string | number | symbol = keyof T,
  Id extends string | number | symbol = T[K]
>(
  idKey: K
): Builder<
  {
    name: string;
    byIds: (id: Id[]) => Promise<T[]>;
  },
  {
    byId(ids: Id, refresh?: boolean): Promise<T>;
    byIds(ids: Id[], refresh?: boolean): Promise<T[]>;
    cache: { [id in Id]?: Promise<T> };
  }
> => (props) => {
  const { name, byIds: __byIds } = props;
  const cache: { [id in Id]?: Promise<T> } = {};

  async function byIds(ids: Id[], refresh: boolean = false): Promise<T[]> {
    log.info(name, "need", ids);
    if (ids.length === 0) {
      log.warn(name, "queried nothing");
      return [];
    }
    const ids2ix: Record<string | number, [number, number]> = {};
    const required: Id[] = [];
    const result: Promise<T>[] = new Array(ids.length);
    for (let i = 0; i < ids.length; i++) {
      const id: Id = ids[i];
      const g: Promise<T> | undefined = cache[id];
      if (g !== undefined && !refresh) {
        result[i] = g;
      } else {
        ids2ix[id as any] = [i, required.length];
        required.push(id);
      }
    }
    if (required.length > 0) {
      const gets = __byIds(required);
      for (const id of required) {
        const [resultIx, reqIx] = ids2ix[id as any];
        const g = gets.then((g) => g[reqIx]);
        cache[id] = g;
        result[resultIx] = g;
      }
    }
    return Promise.all(result);
  }

  const buffer: Id[] = [];
  const resolves: ((obj: { [id in Id]?: T }) => void)[] = [];

  function awaitBuffer(buffer: Id[]): Promise<{ [id in Id]?: T }> {
    return new Promise((resolve) => {
      const len0 = buffer.length;
      setTimeout(() => {
        // wait for the length to stabilise at 100ms intervals
        if (len0 === buffer.length) {
          const ids = Array.from(new Set(buffer.splice(0, buffer.length)));
          const ress = resolves.splice(0, resolves.length);
          const r: { [id in Id]?: T } = {};
          byIds(ids, true).then((vals) => {
            for (const val of vals) {
              r[val[idKey]] = val;
            }
            resolve(r);
            for (const res of ress) {
              res(r);
            }
          });
        } else {
          resolves.push(resolve);
        }
      }, 100);
    });
  }

  return {
    ...props,
    idKey,
    cache,
    byId: async (id: Id, refresh: boolean = false): Promise<T> => {
      const getter: Promise<T> | undefined = cache[id];
      if (!refresh && getter !== undefined) {
        return getter;
      }
      buffer.push(id);
      return (await awaitBuffer(buffer))[id]!;
    },
    byIds,
  };
};
