import { Builder } from "./builder";
import { PagedForUuid as Paged } from "../../generated";

// it's all just type overloads

function prepareTerms(terms?: string): undefined | string {
  if (terms === undefined) return undefined;
  return terms
    .split(" ")
    .map((w) => w + ":*")
    .join(" ");
}

export function cachePaginated<
  T extends Record<K, string> & { updatedAt: Date },
  K extends string | number
>(
  idKey: K,
  textSearch?: false
): Builder<
  {
    cache?: { [id in string]?: Promise<T> };
    byIds(ids: string[], refresh?: boolean): Promise<T[]>;
    page(begin: number): Promise<Paged>;
  },
  {
    cache: { [id in string]?: Promise<T> };
    byIds(ids: string[], refresh?: boolean): Promise<T[]>;
    page(begin: number): Promise<[Paged, T[]]>;
  }
>;

export function cachePaginated<
  T extends Record<K, string> & { updatedAt: Date },
  K extends string | number
>(
  idKey: K,
  textSearch: true
): Builder<
  {
    cache?: { [id in string]?: Promise<T> };
    byIds(ids: string[], refresh?: boolean): Promise<T[]>;
    page(begin: number, terms?: string): Promise<Paged>;
  },
  {
    cache: { [id in string]?: Promise<T> };
    byIds(ids: string[], refresh?: boolean): Promise<T[]>;
    page(begin: number, terms?: string): Promise<[Paged, T[]]>;
  }
>;

export function cachePaginated<
  T extends Record<K, string> & { updatedAt: Date },
  K extends string | number
>(
  idKey: K,
  textSearch?: boolean
): Builder<
  {
    cache?: { [id in string]?: Promise<T> };
    byIds(ids: string[], refresh?: boolean): Promise<T[]>;
    page(begin: number): Promise<Paged>;
    page(begin: number, terms?: string): Promise<Paged>;
  },
  {
    cache: { [id in string]?: Promise<T> };
    byIds(ids: string[], refresh?: boolean): Promise<T[]>;
    page(begin: number, terms?: string): Promise<[Paged, T[]]>;
    page(begin: number): Promise<[Paged, T[]]>;
  }
> {
  return (props) => {
    const { cache = {}, page, byIds } = props;
    return {
      ...props,
      cache,
      page: async (begin: number, terms?: string): Promise<[Paged, T[]]> => {
        begin = begin !== undefined && isNaN(begin) ? 0 : begin ?? 0;
        const paged: Paged = await (textSearch
          ? page(begin, prepareTerms(terms))
          : page(begin));
        const items = paged.items;
        const required: string[] = [];
        const id2ix: { [k: string]: number } = {};
        const result: T[] = new Array(items.length);
        for (let i = 0; i < items.length; i++) {
          const { id, updatedAt } = items[i];
          const cached = cache[id];
          const val = cached !== undefined ? await cached : undefined;
          if (
            cached === undefined ||
            (val !== undefined && new Date(updatedAt) > new Date(val.updatedAt))
          ) {
            id2ix[id] = i;
            required.push(id);
          } else {
            result[i] = val!; // cached !== undefined => val !== undefined
          }
        }
        const vals = await byIds(required, true);
        for (const val of vals) {
          const id = val[idKey];
          result[id2ix[id]] = val;
        }
        return [paged, result];
      },
    };
  };
}

export const cachePaginatedLazy = <
  T extends Record<K, string> & { updatedAt: Date },
  K extends string | number
>(
  _idKey: K
): Builder<
  {
    cache?: { [id in string]?: Promise<T> };
    byId(ids: string, refresh?: boolean): Promise<T>;
    page(begin?: number): Promise<Paged>;
  },
  {
    cache: { [id in string]?: Promise<T> };
    byId(ids: string, refresh?: boolean): Promise<T>;
    page(begin?: number): Promise<[Paged, (() => Promise<T>)[]]>;
  }
> => (props) => {
  const { cache = {}, page, byId } = props;
  return {
    ...props,
    cache,
    page: async (begin?: number): Promise<[Paged, (() => Promise<T>)[]]> => {
      begin = begin !== undefined && isNaN(begin) ? 0 : begin ?? 0;
      const paged: Paged = await page(begin);
      const items = paged.items;
      const required: [number, string][] = [];
      const result: (() => Promise<T>)[] = new Array(items.length);
      for (let i = 0; i < items.length; i++) {
        const { id, updatedAt } = items[i];
        const cached = cache[id];
        const val = cached !== undefined ? await cached : undefined;
        if (
          cached === undefined ||
          (val !== undefined && new Date(updatedAt) > new Date(val.updatedAt))
        ) {
          required.push([i, id]);
        } else {
          result[i] = () => Promise.resolve(cached);
        }
      }
      for (const [i, id] of required) {
        result[i] = () => byId(id, true);
      }
      return [paged, result];
    },
  };
};
