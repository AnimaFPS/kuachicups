import { Builder } from "./builder";
import { cacheById } from "./byId";
import log from "loglevel";

export const cacheByParentId = <
  Child extends Record<K, Id>,
  K extends string | number | symbol = keyof Child,
  Id extends string | number | symbol = Child[K]
>(
  _idKey: K
): Builder<
  {
    name: string;
    byParentId(pid: Id): Promise<Child[]>;
  },
  {
    name: string;
    byParentId(pid: Id, refresh?: boolean): Promise<Child[]>;
    parentCache: { [id in Id]?: Promise<Child[]> };
  }
> => props => {
  const { name, byParentId } = props;
  const parentCache: { [pid in Id]?: Promise<Child[]> } = {};
  return {
    ...props,
    parentCache,
    async byParentId(id: Id, refresh: boolean = false): Promise<Child[]> {
      if (!refresh) {
        const cur: Promise<Child[]> | undefined = parentCache[id];
        if (cur !== undefined) {
          return await cur;
        }
      }
      if (refresh) {
        log.info(name, "not using cache", id);
      }
      return await (parentCache[id] = byParentId(id));
    }
  };
};

export const cacheByParentIdWithIdCache = <
  Child extends Record<K, Id> & Record<IK, IId>,
  K extends string | number | symbol = keyof Child,
  IK extends keyof Child = keyof Child,
  IId extends string | number | symbol = Child[IK],
  Id extends string | number | symbol = Child[K]
>(
  idKey: K,
  childId: IK
): Builder<
  {
    name: string;
    byParentId(pid: Id): Promise<Child[]>;
  },
  {
    name: string;
    byParentId(pid: Id, refresh?: boolean): Promise<Child[]>;
    byId(pid: Id, id: IId, refresh?: boolean): Promise<Child>;
    parentCache: { [id in Id]?: Promise<Child[]> };
  }
> => props => {
  const name = props.name;
  const { byParentId, parentCache } = cacheByParentId<Child, K, Id>(idKey)({
    name,
    byParentId: props.byParentId
  });
  const childCache: { [id in Id]?: Promise<{ [id in IId]?: Child }> } = {};
  return {
    ...props,
    parentCache,
    byParentId,
    async byId(pid: Id, id: IId, refresh: boolean = false): Promise<Child> {
      log.info(name, "ask", pid, "for", id);
      if (childCache[pid] === undefined || refresh) {
        childCache[pid] = byParentId(pid, refresh).then(children => {
          const childrenById: { [id in IId]?: Child } = {};
          for (const child of children) {
            childrenById[child[childId]] = child;
          }
          return childrenById;
        });
      }
      const parent = await (childCache[pid]! as Promise<
        { [id in IId]?: Child }
      >);
      if (parent[id] !== undefined) return parent[id]!;
      throw new Error(`${props.name} child ${id} not in parent ${pid}`);
    }
  };
};
