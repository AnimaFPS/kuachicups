export type Builder<Builder, Opts> = <Props extends Builder>(
  props: Props
) => Omit<Props, keyof Builder | keyof Opts> & Opts;
