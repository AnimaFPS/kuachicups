import { DefaultApi, Game, GameMode, PagedForUuid } from "../generated";
import {
  cacheByParentId,
  cacheById,
  cacheByIds,
  cachePaginated,
} from "./helpers";

export default function createGamesCache(api: DefaultApi) {
  const byId = cacheByIds<Game, "id">("id")({
    name: "game",
    byIds: (ids: string[]) => api.gameGames({ ids }),
  });

  const paginated = cachePaginated<Game, "id">("id")({
    ...byId,
    page: (page: number) => api.gameGamesPage({ page }),
  });

  const modes = cacheById<GameMode, "id">("id")({
    ...cacheByParentId<GameMode, "gameId">("gameId")({
      name: "game.modes",
      byParentId: (id: string) => api.gameGameModes({ id }),
    }),
    byId: (id: string) => api.gameGameModeById({ id }),
  });

  return {
    ...paginated,
    modes: {
      ...modes,
      dropdownOptions: async (page: number) => {
        const [paged, games] = await paginated.page(page);
        const options = [];
        for (let i = 0; i < games.length; i++) {
          const game = games[i];
          options.push(game.name); // appears as a title
          for (const mode of await modes.byParentId(game.id)) {
            options.push({
              title: mode.name,
              key: mode.id,
              extra: { game, mode },
            });
          }
        }
        return [paged, options] as [PagedForUuid, typeof options];
      },
    },
  };
}
