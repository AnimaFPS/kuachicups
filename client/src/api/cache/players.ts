import { DefaultApi, Player, Role } from "../generated";
import { cacheByParentId, cacheByIds, cachePaginated } from "./helpers";

export interface PT {
  playerId: string;
  teamId: string;
  role: Role;
}

export default function createPlayersCache(api: DefaultApi) {
  const byId = cacheByIds<Player, "id">("id")({
    name: "players",
    byIds: (ids) => api.playerProfile({ ids }),
  });

  const teamRoles = cacheByParentId<PT, "playerId">("playerId")({
    name: "players.teamRoles",

    roleWithin: async (
      playerId: string,
      teamId: string,
      refresh?: boolean
    ): Promise<undefined | Role> => {
      const pts = await teamRoles.byParentId(playerId, refresh);
      for (const pt of pts) {
        if (pt.teamId === teamId) return pt.role;
      }
    },

    byParentId: async (playerId: string) =>
      (await api.playerTeams({ id: playerId })).map((pt) => ({
        playerId,
        teamId: pt.teamId,
        role: pt.role,
      })),
  });

  return cachePaginated<Player, "id">(
    "id",
    true
  )({
    ...byId,
    page: (page, terms) => api.playerPlayersPage({ page, terms }),
    teamRoles,
  });
}
