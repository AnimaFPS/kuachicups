import { DefaultApi, PostMedia } from "../generated";
import { cacheByIds, cachePaginatedLazy } from "./helpers";

export default function createMediaCache(api: DefaultApi) {
  return cachePaginatedLazy<PostMedia, "id">("id")({
    ...cacheByIds<PostMedia, "id">("id")({
      name: "medias",
      byIds: (ids: string[]) => api.postsMedias({ ids }),
    }),
    page: (page: number) => api.postsMediasPage({ page }),
  });
}
