import createCache, { ApiCache } from "./cache";
import { CSRF_HEADER, getCsrfHeader } from "./csrf";
import {
  Configuration,
  DefaultApi,
  HTTPHeaders,
  PagedForUuid,
} from "./generated";
export * from "./generated";

export type Paged = PagedForUuid;

class Cfg extends Configuration {
  get headers(): HTTPHeaders {
    return { [CSRF_HEADER]: getCsrfHeader() };
  }
}

const basePath = process.env.basePath;
export const api = new DefaultApi(
  new Cfg({
    credentials: "include",
    fetchApi: window.fetch.bind(window),
    basePath: basePath ?? `${location.origin}/api`,
  })
);
(window as any).api = api;

export type Api = DefaultApi & ApiCache;

export default function createApi(): Api {
  return Object.assign(api, createCache(api));
}

/*
 *

  announcements = cachePaginatedLazy<PostAnn, "id">("id")({
    ...cacheByIds<PostAnn, "id">("id")({
      name: "announcements",
      byIds: (ids: string[]) => this.postsAnns({ ids }),
    }),
    page: (page: number) => this.postsAnnsPage({ page }),
  });

  announcementsUnpublished = cachePaginatedLazy<PostAnn, "id">("id")({
    name: "announcements.unpublished",
    cache: this.announcements.cache,
    byId: this.announcements.byId,
    byIds: this.announcements.byIds,
    page: (page: number) => this.postsAnnsUnpublishedPage({ page }),
  });

  */
