/* tslint:disable */
/* eslint-disable */
/**
 * kuachicups-server
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    Cup,
    CupFromJSON,
    CupFromJSONTyped,
    CupToJSON,
    CupStage,
    CupStageFromJSON,
    CupStageFromJSONTyped,
    CupStageToJSON,
} from './';

/**
 * 
 * @export
 * @interface CupCreate
 */
export interface CupCreate {
    /**
     * 
     * @type {Cup}
     * @memberof CupCreate
     */
    cup: Cup;
    /**
     * 
     * @type {Array<CupStage>}
     * @memberof CupCreate
     */
    stages: Array<CupStage>;
}

export function CupCreateFromJSON(json: any): CupCreate {
    return CupCreateFromJSONTyped(json, false);
}

export function CupCreateFromJSONTyped(json: any, ignoreDiscriminator: boolean): CupCreate {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'cup': CupFromJSON(json['cup']),
        'stages': ((json['stages'] as Array<any>).map(CupStageFromJSON)),
    };
}

export function CupCreateToJSON(value?: CupCreate | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'cup': CupToJSON(value.cup),
        'stages': ((value.stages as Array<any>).map(CupStageToJSON)),
    };
}


