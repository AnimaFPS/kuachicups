/* tslint:disable */
/* eslint-disable */
/**
 * kuachicups-server
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface IsAdmin
 */
export interface IsAdmin {
    /**
     * 
     * @type {boolean}
     * @memberof IsAdmin
     */
    isAdmin: boolean;
}

export function IsAdminFromJSON(json: any): IsAdmin {
    return IsAdminFromJSONTyped(json, false);
}

export function IsAdminFromJSONTyped(json: any, ignoreDiscriminator: boolean): IsAdmin {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'isAdmin': json['is_admin'],
    };
}

export function IsAdminToJSON(value?: IsAdmin | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'is_admin': value.isAdmin,
    };
}


