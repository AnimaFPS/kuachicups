/* tslint:disable */
/* eslint-disable */
/**
 * kuachicups-server
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface UpdatePostAnn
 */
export interface UpdatePostAnn {
    /**
     * 
     * @type {string}
     * @memberof UpdatePostAnn
     */
    authorId: string;
    /**
     * 
     * @type {string}
     * @memberof UpdatePostAnn
     */
    contentMd?: string | null;
    /**
     * 
     * @type {string}
     * @memberof UpdatePostAnn
     */
    id: string;
    /**
     * 
     * @type {string}
     * @memberof UpdatePostAnn
     */
    titleMd?: string | null;
}

export function UpdatePostAnnFromJSON(json: any): UpdatePostAnn {
    return UpdatePostAnnFromJSONTyped(json, false);
}

export function UpdatePostAnnFromJSONTyped(json: any, ignoreDiscriminator: boolean): UpdatePostAnn {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'authorId': json['author_id'],
        'contentMd': !exists(json, 'content_md') ? undefined : json['content_md'],
        'id': json['id'],
        'titleMd': !exists(json, 'title_md') ? undefined : json['title_md'],
    };
}

export function UpdatePostAnnToJSON(value?: UpdatePostAnn | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'author_id': value.authorId,
        'content_md': value.contentMd,
        'id': value.id,
        'title_md': value.titleMd,
    };
}


