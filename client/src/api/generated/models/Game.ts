/* tslint:disable */
/* eslint-disable */
/**
 * kuachicups-server
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface Game
 */
export interface Game {
    /**
     * 
     * @type {string}
     * @memberof Game
     */
    avatar: string;
    /**
     * 
     * @type {Date}
     * @memberof Game
     */
    createdAt: Date;
    /**
     * 
     * @type {string}
     * @memberof Game
     */
    descriptionHtml: string;
    /**
     * 
     * @type {string}
     * @memberof Game
     */
    descriptionMd: string;
    /**
     * 
     * @type {string}
     * @memberof Game
     */
    id: string;
    /**
     * 
     * @type {string}
     * @memberof Game
     */
    name: string;
    /**
     * 
     * @type {string}
     * @memberof Game
     */
    rulesHtml: string;
    /**
     * 
     * @type {string}
     * @memberof Game
     */
    rulesMd: string;
    /**
     * 
     * @type {string}
     * @memberof Game
     */
    slug: string;
    /**
     * 
     * @type {Date}
     * @memberof Game
     */
    updatedAt: Date;
}

export function GameFromJSON(json: any): Game {
    return GameFromJSONTyped(json, false);
}

export function GameFromJSONTyped(json: any, ignoreDiscriminator: boolean): Game {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'avatar': json['avatar'],
        'createdAt': (new Date(json['created_at'])),
        'descriptionHtml': json['description_html'],
        'descriptionMd': json['description_md'],
        'id': json['id'],
        'name': json['name'],
        'rulesHtml': json['rules_html'],
        'rulesMd': json['rules_md'],
        'slug': json['slug'],
        'updatedAt': (new Date(json['updated_at'])),
    };
}

export function GameToJSON(value?: Game | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'avatar': value.avatar,
        'created_at': (value.createdAt.toISOString()),
        'description_html': value.descriptionHtml,
        'description_md': value.descriptionMd,
        'id': value.id,
        'name': value.name,
        'rules_html': value.rulesHtml,
        'rules_md': value.rulesMd,
        'slug': value.slug,
        'updated_at': (value.updatedAt.toISOString()),
    };
}


