import { define, Validator } from "superstruct";
import { v4 as isUuid4 } from "is-uuid";

export const Uuid = define("Uuid", isUuid4 as Validator<string, any>);
