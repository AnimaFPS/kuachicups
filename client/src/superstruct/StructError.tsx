import { StructError } from "superstruct";

export default function ShowStructError<T>(props: {
  value: StructError | T | undefined;
}) {
  return (
    <p class="help">
      {props.value === undefined || props.value instanceof StructError
        ? [<b>Error: </b>, (props.value as StructError)?.message]
        : undefined}
    </p>
  );
}
