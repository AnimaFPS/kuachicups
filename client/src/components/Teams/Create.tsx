import log from "loglevel";
import { createState, Show } from "solid-js";
import { Player, Team } from "../../api";
import { navigate } from "../../router";
import useApi from "../../context/Api";
import { WithLogin } from "../../context/Auth";
import Form, { MarkdownField, text, visual } from "../ui/Form";
import Icon from "../ui/Icon";
import { TeamLink } from ".";
import ConfirmButton from "../ui/ConfirmButton";
import { format, add, startOfWeek } from "date-fns";
import { failure } from "../Toast";

export default (props: { team?: Team }) => {
  return (
    <WithLogin>
      {(self) => <TeamCreate self={self} team={props.team} />}
    </WithLogin>
  );
};

interface S {
  name: string;
  blurb: string;
}

function insertTimetable(
  insertText: (text: string) => void,
  _textarea: HTMLTextAreaElement
) {
  const week1 = startOfWeek(new Date());
  const week2 = add(week1, { weeks: 1 });
  const week3 = add(week2, { weeks: 1 });
  const week4 = add(week3, { weeks: 1 });
  const weeks = [week1, week2, week3, week4].map((w) =>
    format(w, "MMM d").padStart(8, " ")
  );
  insertText(`
# Scrim timetable
|    Week | Sun     | Mon     | Tue     | Wed     | Thu     | Fri     | Sat     |
|--------:|---------|---------|---------|---------|---------|---------|---------|
${weeks
  .map(
    (w) =>
      `|${w} |         |         |         |         |         |         |         |`
  )
  .join("\n")}`);
}

function TeamCreate(props: { self: Player; team?: Team }) {
  const api = useApi();
  const [state, setState] = createState<S>({
    name: props.team?.name ?? "",
    blurb: props.team?.blurbMd ?? "",
  });
  return (
    <>
      <h2 class="title is-4">
        {props.team !== undefined ? (
          <>
            Editing <TeamLink team={props.team} />
          </>
        ) : (
          "Create team..."
        )}
      </h2>
      <hr />
      <Form<S>
        state={[() => state, setState]}
        form={[
          text(["name"], {
            label: "Name",
          }),
          visual(() => (
            <MarkdownField
              label="Blurb"
              get={() => state.blurb}
              set={(b: string) => setState("blurb", b)}
              controls={(props) => (
                <div class="level">
                  <div class="level-left">
                    <button
                      class="button is-info is-light is-small mb-0"
                      onClick={() =>
                        insertTimetable(props.insertText, props.textarea)
                      }
                    >
                      <Icon type="calendar" />
                      <span>Add timetable</span>
                    </button>
                  </div>
                  <small class="pl-2 help">
                    Don't worry, tables does not need to be aligned (or even
                    have the correct number of columns) to work. See{" "}
                    <a href="https://www.markdownguide.org/extended-syntax/#tables">
                      here
                    </a>{" "}
                    for information about tables in Markdown.
                  </small>
                </div>
              )}
            />
          )),
        ]}
      />
      <div class="buttons">
        <button
          class="button is-primary"
          onClick={() => {
            const name = state.name.trim();
            const blurb = state.blurb.trim();
            if (name === "") {
              failure("Team name must be provided");
              return;
            }
            if (props.team !== undefined) {
              api
                .teamsUpdate({
                  teamUpdateForm: {
                    id: props.team.id,
                    name: name,
                    blurbMd: blurb,
                  },
                })
                .then((t) => {
                  log.info("update team", t);
                  api.teams.cache[t.id] = Promise.resolve(t);
                  navigate({ to: "teams.profile", params: { id: t.id } });
                });
            } else {
              api
                .teamsCreate({
                  teamCreateForm: {
                    name: name,
                    blurbMd: blurb,
                    ownerId: props.self.id,
                  },
                })
                .then((t) => {
                  log.info("create team", t);
                  api.teams.cache[t.id] = Promise.resolve(t);
                  navigate({ to: "teams.profile", params: { id: t.id } });
                });
            }
          }}
        >
          {props.team !== undefined ? "Update" : "Create"}
        </button>
        <Show when={props.team !== undefined}>
          <button
            class="button"
            onClick={() => {
              navigate({ to: "teams.profile", params: { id: props.team!.id } });
            }}
          >
            Cancel
          </button>
        </Show>
      </div>
      <Show when={props.team !== undefined}>
        <hr />
        <ConfirmButton
          class="is-danger"
          onConfirm={() => {}}
          confirming={() => (
            <>
              <Icon type="trash" />
              <span>Really delete team?</span>
            </>
          )}
        >
          <Icon type="trash" />
          <span>Delete team</span>
        </ConfirmButton>
      </Show>
    </>
  );
}

// <b>CREATE</b>;
