import { TeamLink } from ".";
import { Team } from "../../api";
import useApi from "../../context/Api";
import { useRoute } from "../../router";
import { useAsync } from "../async/Awaitable";
import { DiscordServerAvatarImg } from "../discord/Avatar";
import { Lazily, LazyProvider } from "../Lazy";
import Pages from "../Paginate";
import PlayerLink from "../Player/Link";

export default function List() {
  const api = useApi();
  const route = useRoute();

  return (
    <Pages
      route="teams"
      source={(page) => api.teams.page(page, route()?.params?.query)}
      renderItem={(t: Team) => (
        <Lazily>
          <TeamPreview team={t} />
        </Lazily>
      )}
      renderItems={(t) => (
        <LazyProvider monotonic length={() => t.items.length}>
          {t.children}
        </LazyProvider>
      )}
    />
  );
}

function TeamPreview(p: { team: Team }) {
  const api = useApi();
  const members = useAsync(() => api.teams.players.byParentId(p.team.id));
  return (
    <div class="box">
      <div
        class="hero team-preview"
        data-has-avatar={
          typeof p.team.discordServerAvatar === "string" &&
          typeof p.team.discordServerId === "string"
        }
      >
        {typeof p.team.discordServerAvatar === "string" &&
          typeof p.team.discordServerId === "string" && (
            <div class="team-avatar">
              <DiscordServerAvatarImg
                discordId={p.team.discordServerId}
                discordAvatar={p.team.discordServerAvatar}
              />
            </div>
          )}
        <div class="team-blurb">
          <h1 class="title mb-2">
            <TeamLink team={p.team} />
          </h1>
          <div class="content">
            {typeof p.team.blurbHtml === "string" && (
              <div innerHTML={p.team.blurbHtml} class="summary" />
            )}
          </div>
        </div>
        <div class="team-player-grid">
          <h2 class="heading">Members</h2>
          <div class="team-player-list">
            <members.For>
              {(pt) => <PlayerLink playerId={pt.playerId} />}
            </members.For>
          </div>
        </div>
      </div>
    </div>
  );
}
