import { debounce } from "lodash";
import { Team } from "../../api";
import { RequireLogin } from "../../context/Auth";
import { Link, navigate, Router, useRoute } from "../../router";
import Icon from "../ui/Icon";
import Layout from "../ui/Layout";
import Create from "./Create";
import List from "./List";
import MyInvites from "./MyInvites";
import MyTeams from "./MyTeams";
import Profile, { TeamProfileMenu } from "./Profile";

export function TeamLink(props: { team: Team }) {
  return (
    <Link to="teams.profile" params={{ id: props.team.id }}>
      {props.team.name}
    </Link>
  );
}

function TeamsPanel() {
  function onSearch(s: string) {
    if (typeof s === "string" && s !== "") {
      navigate({ to: "teams", params: { query: s } });
    } else {
      navigate({ to: "teams" });
    }
  }

  return (
    <aside class="menu">
      <Router assume="teams">
        {{
          fallback: () => (
            <>
              <div class="control has-icons-right">
                <input
                  class="input"
                  type="email"
                  placeholder="Search"
                  onInput={debounce((ev) => onSearch(ev.target.value), 300)}
                />
                <Icon class="is-right" type="search" />
              </div>
              <RequireLogin>
                <br />
                <p class="menu-label">Your teams</p>
                <ul class="menu-list">
                  <li>
                    <Link nav to="teams.mine">
                      <Icon type="contact" />
                      <span>Your teams</span>
                    </Link>
                  </li>
                  <li>
                    <Link nav to="teams.invites">
                      <Icon type="add" />
                      <span>Invites</span>
                    </Link>
                  </li>
                  <li>
                    <Link nav to="teams.new">
                      <Icon type="create" />
                      <span>Create a team</span>
                    </Link>
                  </li>
                </ul>
              </RequireLogin>
            </>
          ),
          profile: { render: () => <TeamProfileMenu /> },
        }}
      </Router>
    </aside>
  );
}

export default () => {
  const route = useRoute();
  return (
    <Layout panel={TeamsPanel}>
      <Router assume="teams">
        {{
          fallback: List,
          new: { render: () => <Create /> },
          profile: { render: () => <Profile id={route().params.id} /> },
          invites: { render: MyInvites },
          mine: { render: MyTeams },
        }}
      </Router>
    </Layout>
  );
};
