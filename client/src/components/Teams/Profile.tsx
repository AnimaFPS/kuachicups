import { format } from "date-fns";
import { createState, For, Show } from "solid-js";
import { TeamLink } from ".";
import { Player, Role, Team, TeamPlayer } from "../../api";
import useApi from "../../context/Api";
import useAuth, { LoginState, WithLogin } from "../../context/Auth";
import { Link, navigate, Router, useRoute } from "../../router";
import { useAsync } from "../async/Awaitable";
import { createRefreshable } from "../async/Refreshable";
import Resource from "../async/Resource";
import { CaseOf } from "../Case";
import { DiscordServerAvatarImg } from "../discord/Avatar";
import PlayerDropdown from "../Player/Dropdown";
import PlayerLink from "../Player/Link";
import { failure, success } from "../Toast";
import Icon from "../ui/Icon";
import TeamUpdate from "./Create";

export const View = (p: { team: Team }) => {
  const api = useApi();

  const getMembers = createRefreshable(async () => {
    const roles: { [k: string]: Role } = {};
    const tps: TeamPlayer[] = await api.teamsPlayers({
      id: p.team.id,
    });
    const ids = [];
    for (const tp of tps) {
      ids.push(tp.playerId);
      roles[tp.playerId] = tp.role;
    }
    return api.players.byIds(ids);
  }, []);

  return (
    <div class="columns">
      <Show
        when={
          typeof p.team.discordServerId === "string" &&
          typeof p.team.discordServerAvatar === "string"
        }
      >
        <div class="column is-left-panel">
          <DiscordServerAvatarImg
            class="discord-avatar is-pulled-right image is-full"
            discordId={p.team.discordServerId!}
            discordAvatar={p.team.discordServerAvatar!}
            size="fill"
          />
        </div>
      </Show>
      <div class="column">
        <h2 class="title is-2">
          <Link to="teams.profile" params={{ id: p.team.id }}>
            {p.team.name}
          </Link>
        </h2>
        <h2 class="heading">Members</h2>
        <div class="team-player-list">
          <For each={getMembers()}>{(p) => <PlayerLink player={p} />}</For>
        </div>
        <hr />
        <dl>
          <dt>Created</dt>
          <dd>{format(p.team.createdAt, "PPPPpppp")}</dd>
        </dl>
        <dl>
          <dt>Updated</dt>
          <dd>{format(p.team.updatedAt, "PPPPpppp")}</dd>
        </dl>
        <hr />
        <div class="content blurb" innerHTML={p.team.blurbHtml ?? ""} />
      </div>
    </div>
  );
};

export function TeamProfileMenu() {
  const route = useRoute();
  const api = useApi();
  function AuthItems(self: Player) {
    const getRole = useAsync(() =>
      api.players.teamRoles.roleWithin(self.id, route().params.id)
    );
    return (
      <CaseOf
        data={{ role: getRole() ?? ("none" as "none" | Role) }}
        key="role"
      >
        {{
          none: () => undefined,
          player: () => <PlayerItems />,
          captain: () => [<PlayerItems />, <CaptainItems />],
          admin: () => [<PlayerItems />, <CaptainItems />, <AdminItems />],
        }}
      </CaseOf>
    );
  }
  function PlayerItems() {
    return (
      <ul class="menu-list">
        <li>
          <Link
            nav
            to="teams.profile.membership"
            params={{ id: route().params.id }}
          >
            <Icon type="settings" />
            <span>Your membership</span>
          </Link>
        </li>
      </ul>
    );
  }
  function CaptainItems() {
    return undefined;
  }
  function AdminItems() {
    return (
      <>
        <p class="menu-label">Administration</p>
        <ul class="menu-list">
          <li>
            <Link
              nav
              to="teams.profile.edit"
              params={{ id: route().params.id }}
            >
              <Icon type="create" />
              <span>Edit</span>
            </Link>
          </li>
          <li>
            <Link
              nav
              to="teams.profile.invite"
              params={{ id: route().params.id }}
            >
              <Icon type="people" />
              <span>Members</span>
            </Link>
          </li>
        </ul>
      </>
    );
  }
  return <WithLogin fallback="No actions available">{AuthItems}</WithLogin>;
}

/**
   undefined => we are still loading
   null => no role
 */
function useRole(teamId: string): () => Role | undefined | null {
  const api = useApi();
  const auth = useAuth();
  return useAsync(
    async (): Promise<Role | undefined | null> => {
      const self = auth();
      switch (self.state) {
        case LoginState.LoggedIn:
          return (
            (await api.players.teamRoles.roleWithin(self.login.id, teamId)) ??
            null
          );
        case LoginState.NotLoggedIn:
          return null;
        default:
          return undefined;
      }
    }
  );
}

export function Membership(props: { team: Team }) {
  const api = useApi();
  const role = useRole(props.team.id);
  function onLeaveTeam() {
    api.teamsLeave({ teamId: props.team.id }).then(() => {
      navigate({ to: "teams.profile", params: { id: props.team.id } });
    });
  }
  return (
    <>
      <h2 class="title is-4">
        Membership of <TeamLink team={props.team} />
      </h2>
      <hr />
      <dl>
        <dt>Role</dt>
        <dd>{role()}</dd>
      </dl>
      <hr />
      <button class="button is-danger" onClick={onLeaveTeam}>
        <Icon type="exit" />
        <span>Leave team</span>
      </button>
    </>
  );
}

function ManageMember(p: {
  team: Team;
  playerId: string;
  role: Role;
  canManage: boolean;
  refresh: () => void;
}) {
  const api = useApi();
  const [state, setState] = createState({
    expanded: false,
  });
  function onExpand() {
    setState("expanded", (e) => !e);
  }
  const onMakeRole = (role: Role | null) => () => {
    api
      .teamsSetMember({
        setTeamMember: {
          playerId: p.playerId,
          teamId: p.team.id,
          playerRole: role,
        },
      })
      .then(() => p.refresh());
  };
  return (
    <div class="box px-2 py-2">
      <div
        class="level is-mobile"
        style={{ "margin-bottom": state.expanded ? "0.5em" : 0 }}
      >
        <div class="level-left">
          <div class="level-item">
            <PlayerLink playerId={p.playerId} />
          </div>
          <div class="level-item">
            <small>{p.role}</small>
          </div>
        </div>
        <div class="level-right">
          <div class="level-item">
            <div class="buttons">
              <button
                class="button is-rounded is-light is-small"
                onClick={onExpand}
                disabled={!p.canManage}
              >
                <Icon type={state.expanded ? "close" : "menu"} />
              </button>
            </div>
          </div>
        </div>
      </div>
      <Show when={state.expanded && p.canManage}>
        <div class="buttons">
          <button
            class="button is-small is-outlined"
            onClick={onMakeRole(Role.Admin)}
          >
            Make admin
          </button>
          <button
            class="button is-small is-outlined"
            onClick={onMakeRole(Role.Captain)}
          >
            Make captain
          </button>
          <button
            class="button is-small is-outlined"
            onClick={onMakeRole(Role.Player)}
          >
            Make player
          </button>
          <button class="button is-small is-danger" onClick={onMakeRole(null)}>
            Remove from team
          </button>
        </div>
      </Show>
    </div>
  );
}

export function Members(props: { team: Team }) {
  const api = useApi();
  const auth = useAuth();
  const role = useRole(props.team.id);
  const players = createRefreshable(async (refresh) => {
    const ps = await api.teams.players.byParentId(props.team.id, refresh);
    ps.sort((a, b) => (a.playerId > b.playerId ? 1 : -1));
    return ps;
  });

  const [state, setState] = createState({
    invite: undefined as string | undefined,
  });

  async function onInvite() {
    const { invite } = state;
    if (invite === undefined) return;
    let errs: any = undefined;
    try {
      const invs = await api.teamsMakeInvite({
        inviteTeamMember: {
          playerId: invite,
          teamId: props.team.id,
          // bit of a wart that this field is here at all, but it is checked
          inviteByPlayerId: auth().login!.id,
        },
      });
      if (invs <= 0) errs = "No invite made";
    } catch (e) {
      errs = await e?.text?.();
    }

    if (errs !== undefined) {
      failure(
        `Cannot invite that player. They may already be in this team or have a
        pending invite to it.`,
        4000
      );
    } else {
      success("Invite sent");
    }

    setState({ invite: undefined });
  }

  return (
    <>
      <h2 class="title is-4">
        Members of <TeamLink team={props.team} />
      </h2>
      <hr />
      <h3 class="title is-5">
        <div class="level is-mobile">
          <div class="level-left">Current members</div>
          <div class="level-right">
            <players.RefreshButton />
          </div>
        </div>
      </h3>
      <ul>
        <For each={players() ?? []}>
          {(p) => (
            <li class="mb-2">
              <ManageMember
                team={props.team}
                playerId={p.playerId}
                role={p.role}
                canManage={role() === "admin"}
                refresh={() => players.update()}
              />
            </li>
          )}
        </For>
      </ul>
      <hr />
      <h3 class="title is-5">Invite</h3>
      <div class="buttons">
        <PlayerDropdown
          value={state.invite}
          onChange={(player) => setState({ invite: player })}
        />
        <button
          class="button is-primary is-outlined"
          onClick={onInvite}
          disabled={state.invite === undefined}
        >
          Invite
        </button>
      </div>
    </>
  );
}

export function Edit(props: { team: Team }) {
  const role = useRole(props.team.id);
  return (
    <Show when={role() === "admin"}>
      <TeamUpdate team={props.team} />
    </Show>
  );
}

export default (props: { id: string }) => {
  const api = useApi();
  return (
    <Resource data={() => api.teams.byId(props.id)}>
      {(team) => (
        <Router assume="teams.profile">
          {{
            fallback: () => <View team={team} />,
            invite: { render: () => <Members team={team} /> },
            edit: { render: () => <Edit team={team} /> },
            membership: { render: () => <Membership team={team} /> },
          }}
        </Router>
      )}
    </Resource>
  );
};
