import { compareDesc, format } from "date-fns";
import { For } from "solid-js";
import { TeamLink } from ".";
import { Team, TeamMemberInvite } from "../../api";
import useApi from "../../context/Api";
import useAuth from "../../context/Auth";
import PlayerLink from "../Player/Link";
import Icon from "../ui/Icon";
import { createRefreshable } from "../async/Refreshable";

export default function MyInvites() {
  const api = useApi();
  const auth = useAuth();
  const getMyInvites = createRefreshable(async () => {
    const self = auth().login;
    if (self === null) return [];
    const invites = await api.playerCurrentSessionTeamInvites();
    const nice = await Promise.all(
      invites.map(async (inv) => ({
        team: await api.teams.byId(inv.teamId),
        invite: inv,
      }))
    );
    return nice.sort((a, b) =>
      compareDesc(a.invite.createdAt, b.invite.createdAt)
    );
  });
  getMyInvites.startInterval(10);
  return (
    <>
      <div class="level is-mobile">
        <div class="level-left">
          <h2 class="title is-4">Invites</h2>
        </div>
        <div class="level-right">
          <getMyInvites.RefreshButton />
        </div>
      </div>
      <hr />
      <For
        each={getMyInvites() ?? []}
        fallback={() => <p class="px-3 py-3">No invites</p>}
      >
        {(t) => (
          <InviteLink
            team={t.team}
            invite={t.invite}
            update={getMyInvites.update}
          />
        )}
      </For>
    </>
  );
}

function InviteLink(props: {
  team: Team;
  invite: TeamMemberInvite;
  update: () => void;
}) {
  const api = useApi();
  const auth = useAuth();
  function onAccept() {
    api
      .teamsAcceptInvite({
        acceptInviteTeamMember: {
          teamId: props.team.id,
          playerId: auth().login!.id,
        },
      })
      .then(props.update);
  }
  function onReject() {
    api
      .teamsRejectInvite({
        acceptInviteTeamMember: {
          teamId: props.team.id,
          playerId: auth().login!.id,
        },
      })
      .then(props.update);
  }
  return (
    <div class="level box px-2 py-2 mb-2">
      <div class="level-left">
        <div class="level-item">
          Invite to &nbsp;
          <TeamLink team={props.team} />
          &nbsp; from &nbsp;
          <PlayerLink playerId={props.invite.inviteByPlayerId} />
          &nbsp; at &nbsp;
          {format(props.invite.createdAt, "PPPppp")}
        </div>
      </div>
      <div class="level-right">
        <div class="level-item">
          <div class="buttons">
            <button
              class="button hover-hide is-rounded is-small is-success is-light"
              onClick={onAccept}
            >
              <Icon type="checkmark" />
              <span>Accept</span>
            </button>
            <button
              class="button hover-hide is-rounded is-small is-danger is-light"
              onClick={onReject}
            >
              <Icon type="close" />
              <span>Reject</span>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
