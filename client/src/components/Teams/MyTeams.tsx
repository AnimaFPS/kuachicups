import { For } from "solid-js";
import { createRefreshable } from "../async/Refreshable";
import useApi from "../../context/Api";
import useAuth from "../../context/Auth";
import { TeamLink } from ".";

export function useTeamsOf() {
  const api = useApi();
  return async (playerId: string, refresh = false) => {
    const pts = await api.players.teamRoles.byParentId(playerId, refresh);
    const r = await Promise.all(pts.map((pt) => api.teams.byId(pt.teamId)));
    return r;
  };
}

export function useTeamsOfAuth() {
  const auth = useAuth();
  const teamsOf = useTeamsOf();
  return async (refresh = false) => {
    const self = await auth().then;
    if (self.login === null) return [];
    return teamsOf(self.login.id, refresh);
  };
}

export default function MyTeams() {
  const getMyTeams = createRefreshable(useTeamsOfAuth());
  return (
    <>
      <div class="level is-mobile">
        <div class="level-left">
          <h2 class="title is-4">Your teams</h2>
        </div>
        <div class="level-right">
          <getMyTeams.RefreshButton />
        </div>
      </div>
      <hr />
      <ul class="menu-list">
        <For
          each={getMyTeams() ?? []}
          fallback={() => (
            <li>
              <p>No teams</p>
            </li>
          )}
        >
          {(t) => (
            <li>
              <TeamLink team={t} />
            </li>
          )}
        </For>
      </ul>
    </>
  );
}
