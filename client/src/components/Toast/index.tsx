import {
  createContext,
  createSignal,
  createState,
  For,
  onCleanup,
  produce,
  splitProps,
  untrack,
  JSX,
} from "solid-js";

function mkToast(
  { push, pop }: ToastCtx,
  props: {
    timeout?: number;
    children?: JSX.Element;
  } & JSX.HTMLAttributes<HTMLDivElement>
): JSX.Element {
  const self = push((id: number) => {
    const [popping, setPopping] = createSignal(false);
    function onPop() {
      setPopping(true);
      setTimeout(() => {
        pop(id);
      }, 500);
    }
    onCleanup(onPop);
    const timeout = untrack(() => props.timeout) ?? 2000;
    if (timeout > 0) {
      setTimeout(onPop, timeout);
    }
    const [p, dp] = splitProps(props, ["classList", "class", "children"]);
    return (
      <div
        classList={{ "toast-pop": popping(), ...p.classList }}
        class={"toast notification " + (p.class ?? "")}
        {...dp}
      >
        <button class="delete" onClick={onPop} />
        {p.children}
      </div>
    );
  });
  onCleanup(() => pop(self));
  return undefined;
}

export function Toast(
  props: {
    children?: JSX.Element;
    timeout?: number;
  } & JSX.HTMLAttributes<HTMLDivElement>
) {
  return mkToast(globalCtx, props);
}

export function success(children: JSX.Element, timeout: number = 2000) {
  return untrack(() => mkToast(globalCtx, { children, timeout }));
}

export function failure(children: JSX.Element, timeout: number = -1) {
  return untrack(() =>
    mkToast(globalCtx, {
      children,
      class: "is-danger",
      timeout,
    })
  );
}

export interface ToastCtx {
  push(toast: (id: number) => JSX.Element): number;
  pop(toastId: number): void;
}

const globalCtx: ToastCtx = {
  push() {
    return 0;
  },
  pop() {},
};

const Ctx = createContext<ToastCtx>(globalCtx);

export function Provider(props: { children: JSX.Element }) {
  let nextId: number = 0;
  const [state, setState] = createState<{
    toasts: { id: number; toast: JSX.Element }[];
  }>({
    toasts: [],
  });

  function push(toast: (id: number) => JSX.Element) {
    const id = nextId++;
    setState(
      "toasts",
      produce((ts: { id: number; toast: JSX.Element }[]) => {
        ts.push({ id, toast: untrack(() => toast(id)) });
        return ts;
      })
    );
    return id;
  }

  function pop(toastId: number) {
    setState(
      "toasts",
      produce((ts: { id: number; toast: JSX.Element }[]) => {
        const ix = ts.findIndex((v) => v.id === toastId);
        if (ix >= 0) {
          ts.splice(ix, 1);
        }
        return ts;
      })
    );
  }

  globalCtx.push = push;
  globalCtx.pop = pop;
  const toasts = { push, pop };

  return (
    <Ctx.Provider value={toasts}>
      {props.children}
      <div id="toasts">
        <For each={state.toasts}>{(t) => t.toast}</For>
      </div>
    </Ctx.Provider>
  );
}
