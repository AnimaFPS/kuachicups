import { createSignal, onCleanup, createMemo, JSX } from "solid-js";
import { formatDistance, format } from "date-fns";

const fromMs: Record<"h" | "m" | "s", number> = {
  h: 1000 * 60 * 60,
  m: 1000 * 60,
  s: 1000,
};

export function createTicker(tick: "m" | "s" | "h" | number): () => Date {
  const [getTime, setTime] = createSignal(new Date());
  const interval = setInterval(
    () => {
      setTime(new Date());
    },
    typeof tick === "number" ? tick * 1000 : fromMs[tick]
  );
  onCleanup(() => clearInterval(interval));
  return getTime;
}

export function Countdown(props: {
  target: Date;
  tick: "m" | "s" | "h" | number;
}): JSX.Element {
  const ticker = createTicker(props.tick);
  const result = createMemo(() =>
    formatDistance(props.target.getTime(), ticker(), {
      includeSeconds: true,
      addSuffix: true,
    })
  );
  return <span class="countdown">{result}</span>;
}

export function DateTime(props: { time: string | Date }) {
  return () =>
    format(
      typeof props.time === "string" ? new Date(props.time) : props.time,
      "PPpp"
    );
}

export default function Now(props: {
  precision: "m" | "s" | "h" | number;
}): JSX.Element {
  const ticker = createTicker(props.precision);
  return () => format(ticker(), "PPpp");
}
