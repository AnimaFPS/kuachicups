import { format } from "date-fns";
import { createEffect, createResource, lazy } from "solid-js";
import { Player, PlayerTeam } from "../../api";
import useApi from "../../context/Api";
import useAuth from "../../context/Auth";
import { Link } from "../../router";
import Await from "../async/Await";
import { useAsync } from "../async/Awaitable";
import { DiscordAvatarImg } from "../discord/Avatar";
import { Loading } from "../Loading";

export { default as Self } from "./Self";

const Egg = lazy(() => import("./Egg"));

const TeamList = (p: { teams: PlayerTeam[] }) => {
  const api = useApi();
  const ids = p.teams.map((pt) => pt.teamId);
  const teams = useAsync(() => api.teams.byIds(ids));
  return (
    <teams.For>
      {(t) => (
        <li>
          <Link to="teams.profile" params={{ id: t.id }}>
            {t.name}
          </Link>
        </li>
      )}
    </teams.For>
  );
};

export function DiscordTagLink(props: { id: string; tag: string }) {
  return <a href={`https://discord.com/users/${props.id}`}>{props.tag}</a>;
}

export const View = (p: { self: Player; teams: undefined | PlayerTeam[] }) => (
  <div class="columns">
    <div class="column is-left-panel">
      <DiscordAvatarImg
        class="discord-avatar is-pulled-right image is-full"
        discordId={p.self.discordId}
        discordAvatar={p.self.discordAvatar}
        size="256"
      />
    </div>
    <div class="column">
      <p class="title is-4">
        <Link to="players.profile" params={{ id: p.self.id }}>
          {p.self.discordUsername}
        </Link>
      </p>
      <p class="subtitle is-family-monospace is-6">
        <DiscordTagLink id={p.self.discordId} tag={p.self.discordTag} />
      </p>
      <hr />
      <dl>
        <dt>Created</dt>
        <dd>{format(p.self.createdAt, "PPPPpppp")}</dd>
        <dt>Updated</dt>
        <dd>{format(p.self.updatedAt, "PPPPpppp")}</dd>
      </dl>
      <hr />
      <dl>
        <dt>Teams</dt>
        <dd>
          <ul>
            {p.teams === undefined ? <Loading /> : <TeamList teams={p.teams} />}
          </ul>
        </dd>
      </dl>
      {p.self.discordId === "257811893773795328" ? <Egg /> : undefined}
    </div>
  </div>
);

export interface Props {
  self: undefined | boolean;
  id: undefined | string;
}

export default (props: Props) => {
  const auth = useAuth();
  const api = useApi();

  const [player, loadPlayer] = createResource<Player>();
  const [teams, loadTeams] = createResource<PlayerTeam[]>();

  createEffect(() => {
    const { self, id } = props;
    const authUser: null | false | Player = auth().login;
    if (
      (self === true && authUser) ||
      (id !== undefined && authUser && authUser.id === id)
    ) {
      loadPlayer(() => authUser);
    } else if (id !== undefined) {
      loadPlayer(() => api.players.byId(id));
    }
  });

  createEffect(() => {
    const id = player()?.id;
    if (id !== undefined) {
      loadTeams(() => api.playerTeams({ id }));
    }
  });

  return (
    <Await data={player}>{(p) => <View self={p} teams={teams()} />}</Await>
  );
};
