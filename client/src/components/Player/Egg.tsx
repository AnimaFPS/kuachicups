import YT from "youtube-player";

const videoId = "Kb0ooQKbflM";
export default function Egg(_props: {}) {
  return (
    <>
      <hr />
      <div
        style={{ width: "100%" }}
        ref={(div: HTMLDivElement) => {
          const player = YT(div);
          player.loadVideoById({
            videoId,
            startSeconds: 73
          });
          setTimeout(() => {
            player.playVideo();
          }, 2000);
        }}
      />
    </>
  );
}
