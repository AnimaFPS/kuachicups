import { CaseOf } from "../Case";
import useAuth, { LoginState } from "../../context/Auth";
import { Loading } from "../Loading";
import { View } from "./index";

export default () => {
  const auth = useAuth();
  return (
    <CaseOf
      key="state"
      data={auth()}
      options={{
        [LoginState.LoggedIn]: (auth) => (
          <View self={auth.login} teams={undefined} />
        ),
        [LoginState.Unknown]: () => <Loading />,
      }}
    />
  );
};
