import { splitProps } from "solid-js";
import useApi from "../../context/Api";
import Dropdown, { Props } from "../ui/Dropdown";

export default function PlayerDropdown(
  p0: {
    value: undefined | string;
    onChange: (playerId: string) => void;
  } & Omit<
    Props<string>,
    | "value"
    | "onChange"
    | "options"
    | "kind"
    | "textSearchRequireTerms"
    | "title"
  >
) {
  const api = useApi();
  const [props, outerProps] = splitProps(p0, ["value", "onChange"]);
  return (
    <Dropdown
      title="Player"
      value={props.value}
      onChange={(p) => props.onChange(p.key!)}
      kind="search"
      textSearchRequireTerms
      options={(p, terms) =>
        api.players.page(p, terms).then(([pg, players]) => [
          pg,
          players.map((p) => ({
            key: p.id,
            title: p.discordTag.slice(1),
          })),
        ])
      }
      {...outerProps}
    />
  );
}
