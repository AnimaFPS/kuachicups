import { Player, Team } from "../../../api";
import useApi from "../../../context/Api";
import { Link } from "../../../router";
import Resource from "../../async/Resource";
import Avatar, { Size } from "../../discord/Avatar";

export type Props = (
  | {
      playerId: string;
      teamId?: string;
    }
  | {
      player: Player | undefined;
      playerId?: undefined;
      team?: Team | undefined;
    }
) & {
  size?: Size;
  align?: "left" | "right";
  nameOnly?: boolean;
};

export default function PlayerLink(props: Props) {
  if (props.playerId !== undefined) {
    const api = useApi();
    return (
      <Resource
        data={async () => {
          const { playerId, teamId } = props;
          const [player, team] = await Promise.all([
            api.players.byId(playerId),
            teamId !== undefined ? api.teams.byId(teamId) : undefined,
          ]);
          return { player, team };
        }}
      >
        {(p) => (
          <View
            player={p.player}
            team={p.team}
            size={props.size}
            align={props.align}
          />
        )}
      </Resource>
    );
  }
  return (
    <View
      size={props.size}
      align={props.align}
      player={props.player}
      team={props.team}
    />
  );
}

const View = (props: {
  size?: Size;
  align?: "left" | "right";
  nameOnly?: boolean;
  player: Player | undefined;
  team: Team | undefined;
}) => (
  <span
    class="player-link"
    classList={{ "player-link-right": props.align === "right" }}
  >
    {props.nameOnly || props.player === undefined ? undefined : (
      <Avatar player={props.player} size={props.size} />
    )}
    {props.team === undefined ? undefined : (
      <Link to="teams.profile" params={{ id: props.team.id }}>
        {props.team.name}
      </Link>
    )}
    {props.player === undefined || props.team !== undefined ? undefined : (
      <Link
        to="players.profile"
        params={{ id: props.player.id }}
        class="discord-avatar-username-link"
      >
        {props.player.discordUsername}
      </Link>
    )}
  </span>
);
