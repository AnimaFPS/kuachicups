import { debounce } from "lodash";
import {
  createContext,
  createEffect,
  createSignal,
  JSX,
  onCleanup,
  untrack,
  useContext,
} from "solid-js";

function isElementInViewport(el: HTMLElement): boolean {
  const rect = el.getBoundingClientRect();
  return (
    rect.top >= 0 &&
    rect.left >= 0 &&
    rect.bottom <=
      (window.innerHeight ||
        document.documentElement.clientHeight) /* or $(window).height() */ &&
    rect.right <=
      (window.innerWidth ||
        document.documentElement.clientWidth) /* or $(window).width() */
  );
}

type LazyEl =
  | {
      exists: true;
      div: HTMLDivElement;
      setInViewport: (inView: boolean) => void;
    }
  | {
      exists: false;
      div?: undefined;
      setInViewport?: undefined;
    };

interface LazyContext {
  monotonic: boolean;
  divs: LazyEl[];
  nextId(): number;
  checkDivs(): void;
  length?: () => number;
}

function mkLazyContext(monotonic: boolean, length?: () => number) {
  const divs: LazyContext["divs"] = [];
  return {
    monotonic,
    length,
    divs,
    nextId: () => divs.length,
    checkDivs: debounce(
      () => {
        if (divs.length === 0) return;
        let added = 0;
        for (let i = 0; i < divs.length; i++) {
          const d = divs[i];
          if (d.exists) {
            const { div, setInViewport } = d;
            const inViewport = isElementInViewport(div);
            if (inViewport) {
              divs[i] = done;
              added++;
              setInViewport(true);
            } else if (monotonic && added > 0) {
              break;
            }
          } else {
            added++;
          }
        }
      },
      300,
      {
        leading: false,
        trailing: true,
        maxWait: 1000,
      }
    ) as () => void,
  };
}

export function LazyProvider(props: {
  children: JSX.Element;
  length?: () => number;
  monotonic?: boolean;
}) {
  const ctx = untrack(() =>
    mkLazyContext(props.monotonic === true, props.length)
  );
  const checkDivs = ctx.checkDivs;
  createEffect(() => {
    props?.length?.();
    checkDivs();
  });
  addEventListener("resize", checkDivs);
  addEventListener("scroll", checkDivs);
  onCleanup(() => {
    removeEventListener("resize", checkDivs);
    removeEventListener("scroll", checkDivs);
    ctx.divs = noDivs;
    ctx.checkDivs = noCheck;
  });
  return (
    <LazyContext.Provider value={ctx}>{props.children}</LazyContext.Provider>
  );
}

export function Lazily(props: { children: JSX.Element }) {
  /* - Create a wrapper div with a loading icon in it
     - Add it to a global array of divs to check if in viewport (and if so then
       trigger load)
     - On cleanup check if global array still has our wrapper div, if so delete
   */
  const ctx = useContext(LazyContext);
  const [inViewport, setInViewport] = createSignal(false);
  let self: LazyEl = { exists: false };
  onCleanup(() => {
    if (self.exists) {
      Object.assign(self, {
        exists: false,
        setInViewport: undefined,
        div: undefined,
      });
    }
  });
  return () =>
    inViewport() ? (
      props.children
    ) : (
      <div
        class="lazy-loader"
        ref={(div: HTMLDivElement) => {
          setTimeout(() => {
            ctx.divs.push({ exists: true, div, setInViewport });
            ctx.checkDivs();
          }, 0);
        }}
      />
    );
}

const done: LazyEl = { exists: false } as const;
const noDivs = [] as any[];
const noCheck = () => {};

const globalLazy = mkLazyContext(false);
addEventListener("resize", globalLazy.checkDivs);
addEventListener("scroll", globalLazy.checkDivs);

const LazyContext = createContext<LazyContext>(globalLazy);
