import {
  createEffect,
  createMemo,
  createSignal,
  JSX,
  onCleanup,
  Show,
  untrack,
} from "solid-js";
import { useMobile } from "../../context/Size";
import Icon from "./Icon";

export let Portal: (props: { children: JSX.Element }) => JSX.Element = () =>
  undefined;

export default function Layout(props: {
  children: JSX.Element;
  panel?: JSX.Element;
}) {
  const mobile = useMobile();
  const [isOpen, setOpen] = createSignal(true);
  const [hasItems, setHasItems] = createSignal(true);
  createEffect(() => setOpen(!mobile()));
  const openMobile = createMemo(() => mobile() && isOpen());

  function listen(div: HTMLDivElement) {
    function observe() {
      if (div.childElementCount === 0) {
        setHasItems(false);
        return;
      }
      for (const c of Array.from(div.childNodes)) {
        if (c.hasChildNodes()) {
          setHasItems(true);
          return;
        }
      }
      setHasItems(false);
    }
    const observer = new MutationObserver(observe);
    observer.observe(div, { childList: true, subtree: true });
    createEffect(() => untrack(observe));
    onCleanup(() => {
      observer.disconnect();
    });
  }

  return (
    <div class="columns mobile-reverse mobile-no-pad tablet-no-pad is-mobile">
      <div class="column mobile-no-pad layout-content">{props.children}</div>
      <div
        class="column is-3-tablet is-narrow layout-panel"
        data-open={isOpen()}
        classList={{
          box: openMobile(),
          "mb-2": mobile(),
          "mobile-no-pad": mobile() && !isOpen(),
          "has-background-white-bis": openMobile(),
          "is-hidden": !hasItems(),
        }}
      >
        <Show when={props.panel !== undefined}>
          <div
            ref={listen}
            class="layout-panel-content"
            classList={{ "is-closed": mobile() && !isOpen() }}
          >
            {props.panel}
          </div>
          <Show when={mobile()}>
            <button
              data-ismobilemenu={true}
              class="button is-full-width is-small is-light"
              classList={{ "mt-2": openMobile() }}
              onClick={() => {
                setOpen(!untrack(isOpen));
              }}
            >
              <Icon type="menu" />
              <span>{isOpen() ? "Contract" : "Expand"} context menu</span>
            </button>
          </Show>
        </Show>
      </div>
    </div>
  );
}
