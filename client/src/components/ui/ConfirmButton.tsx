import { createSignal, splitProps, JSX } from "solid-js";

enum Confirm {
  Unclicked,
  Clicked,
  Confirmed,
}

export default function ConfirmButton(
  props: {
    onConfirm: () => void;
    confirmed?: JSX.Element;
    confirming?: JSX.Element;
  } & JSX.IntrinsicElements["button"]
) {
  const [getState, setState] = createSignal(Confirm.Unclicked);
  const [p0, p1] = splitProps(props, [
    "class",
    "children",
    "confirming",
    "confirmed",
    "onConfirm",
  ]);
  function onClick() {
    switch (getState()) {
      case Confirm.Unclicked:
        setState(Confirm.Clicked);
        break;
      case Confirm.Clicked:
        setState(Confirm.Confirmed);
        p0.onConfirm();
        break;
    }
  }
  return (
    <button
      class={"button " + (p0.class ?? "")}
      onClick={onClick}
      disabled={getState() === Confirm.Confirmed}
      {...p1}
    >
      {() => {
        switch (getState()) {
          case Confirm.Unclicked:
            return p0.children;
          case Confirm.Clicked:
            return p0.confirming ?? "Confirm?";
          case Confirm.Confirmed:
            return p0.confirmed ?? "Ok";
        }
      }}
    </button>
  );
}
