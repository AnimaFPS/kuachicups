import Icon from "./Icon";

export default function RefreshButton(props: {
  update: () => void;
  isLoading: () => boolean;
}) {
  return (
    <button
      classList={{ "is-loading": props.isLoading() }}
      class="button is-small is-text"
      onClick={() => props.update()}
    >
      <Icon type="refresh" />
    </button>
  );
}
