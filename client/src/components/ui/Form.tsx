import { format, isMatch, isValid, parseISO } from "date-fns";
import {
  createMemo,
  createSignal,
  Show,
  For,
  SetStateFunction,
  State,
  untrack,
  JSX,
} from "solid-js";
import { CaseOf, EnumFromTo } from "../Case";
import Dropdown, {
  Option as DropdownOption,
  Options as DropdownOptions,
} from "./Dropdown";
import Icon from "./Icon";
import debounce from "lodash/debounce";
import { ElementOf } from "ts-essentials";
import { Paged } from "../../api";
import Markdown from "./Markdown";

export enum FieldType {
  Text,
  MultilineText,
  Number,
  Checkbox,
  Radio,
  Select,
  Date,
  Time,
  DateTime,
  Duration,
  Subform,
  ShowIf,
  StatePipe,
  Visual,
}

export type Fields = {
  [FieldType.Text]: TextField & { type: FieldType.Text };
  [FieldType.MultilineText]: MultilineTextField & {
    type: FieldType.MultilineText;
  };
  [FieldType.Number]: NumberField & { type: FieldType.Number };
  [FieldType.Checkbox]: CheckboxField & { type: FieldType.Checkbox };
  [FieldType.Radio]: RadioField & { type: FieldType.Radio };
  [FieldType.Select]: SelectField<string | number> & { type: FieldType.Select };
  [FieldType.Date]: DateField & { type: FieldType.Date };
  [FieldType.Time]: TimeField & { type: FieldType.Time };
  [FieldType.DateTime]: DateTimeField & { type: FieldType.DateTime };
  [FieldType.Duration]: DurationField & { type: FieldType.Duration };
  [FieldType.Subform]: SubformField<any> & { type: FieldType.Subform };
  [FieldType.ShowIf]: ShowIfField<any> & { type: FieldType.ShowIf };
  [FieldType.StatePipe]: StatePipeField<any> & { type: FieldType.StatePipe };
  [FieldType.Visual]: VisualField<any> & { type: FieldType.Visual };
};

export type SomeField = Fields[keyof Fields];

export type FieldProps<T> = T extends keyof Fields
  ? Fields[T] extends infer S
    ? S
    : never
  : never;

export type FieldState<T> = FieldProps<T> extends BaseField<infer S>
  ? S
  : never;

type FieldConstr<Type> = <S>(
  target: PathsMatching<S, FieldState<Type>>,
  props: Omit<FieldProps<Type>, "get" | "set" | "type">
) => (
  getState: () => S,
  setState: (...args: any[]) => void
) => FieldProps<Type>;

export type SomeFieldConstr<S> = keyof Fields extends infer Type
  ? (getState: () => S, setState: (...args: any[]) => void) => FieldProps<Type>
  : never;

function follow(obj: any, path: (number | string)[]): unknown {
  for (const seg of path) {
    obj = obj[seg];
  }
  return obj;
}

function constr<Type extends FieldType>(type: Type): FieldConstr<Type> {
  return (target, props) => (getState, setState) => {
    return {
      ...props,
      type,
      get: createMemo(() => follow(getState(), target)),
      set: (...args: any[]) => {
        setState(...(target as any[]), ...args);
      },
    } as any;
  };
}

interface BaseField<S> {
  type: FieldType;
  label?: JSX.Element;
  description?: JSX.Element;
  tooltip?: string;
  defaultValue?: S;
  get: () => State<S>;
  set: SetStateFunction<S>;
  innerProps?: any;
}

interface ValidatedField<Raw, Validated>
  extends BaseField<[Validated | undefined, Raw]> {}

export const text = constr(FieldType.Text);
export const textarea = constr(FieldType.MultilineText);
export const number = constr(FieldType.Number);
export const checkbox = constr(FieldType.Checkbox);
export const radio = constr(FieldType.Radio);
export const date = constr(FieldType.Date);
export const time = constr(FieldType.Time);
export const datetime = constr(FieldType.DateTime);
export const duration = constr(FieldType.Duration);
export const select = constr(FieldType.Select);

export function visual<S>(
  render: (state: State<S>, setState: SetStateFunction<S>) => JSX.Element
): (
  getState: () => State<S>,
  setState: (...args: any[]) => void
) => FieldProps<FieldType.Visual> {
  return (getState, setState) => ({
    type: FieldType.Visual,
    render: render as any,
    get: getState,
    set: setState,
  });
}

export function subform<
  S,
  Target extends PathsMatching<S, FieldState<FieldType.Subform>>,
  InnerS extends FollowPath<S, [...Target, number]>
>(
  target: Target,
  props: Omit<SubformField<InnerS>, "get" | "set" | "type">
): (
  getState: () => State<S>,
  setState: (...args: any[]) => void
) => SubformField<InnerS> & { type: FieldType.Subform } {
  return (constr as any)(FieldType.Subform)(target, props);
}

type ShowIfConstr = <S>(
  when: (s: S, setState: SetStateFunction<S>) => boolean,
  props: ShowIfField<S>["form"]
) => (
  getState: () => S,
  setState: SetStateFunction<S>
) => FieldProps<FieldType.ShowIf>;

export const showif: ShowIfConstr = (cond, form) => (getState, setState) => {
  return {
    form,
    type: FieldType.ShowIf,
    when: () => cond(getState(), setState),
    get: getState,
    set: setState,
  };
};

// TODO combine with `visual`, it's pretty much the same thing

type StatePipeConstr = <S>(
  props: StatePipeField<S>["form"]
) => (
  getState: () => S,
  setState: SetStateFunction<S>
) => FieldProps<FieldType.StatePipe>;

export const pipe: StatePipeConstr = (form) => (getState, setState) => {
  return {
    form,
    type: FieldType.StatePipe,
    get: getState,
    set: setState,
  };
};

export interface TextField extends BaseField<string> {
  placeholder?: string;
}

export function TextField(props: TextField) {
  // note https://github.com/ryansolid/solid/issues/203
  const ref = controlledValue(props);
  return (
    <input
      ref={ref}
      class="input"
      type="text"
      placeholder={props.placeholder ?? ""}
      onInput={debounce((ev) => props.set(ev.target.value), 100)}
      {...(props.innerProps ?? {})}
    />
  );
}

export interface MultilineTextField extends BaseField<string> {
  placeholder?: string;
  lines?: number;
  expand?: boolean;
}

export function MultilineTextField(props: MultilineTextField) {
  const control = controlledValue(props);
  return (
    <div class="textarea-sizer" data-value={props.get()}>
      <textarea
        ref={control}
        class="input"
        classList={{ expand: props.expand === true }}
        placeholder={props.placeholder ?? ""}
        onInput={debounce((ev) => props.set(ev.target.value), 100)}
        style={
          props.expand
            ? {
                "overflow-y": "hidden",
              }
            : undefined
        }
        {...(props.innerProps ?? {})}
      />
    </div>
  );
}

export function MarkdownField(
  props: Omit<MultilineTextField, "type"> & {
    controls?: (props: {
      insertText: (text: string) => void;
      textarea: HTMLTextAreaElement;
    }) => JSX.Element;
    charLimit?: number;
    lines?: number;
  }
) {
  const [isPreview, setPreview] = createSignal(false);
  let ta: HTMLTextAreaElement | undefined = undefined;
  return (
    <div class="md-field">
      <label class="label mb-0">{props.label}</label>
      <p>
        <small>
          <a href="https://www.markdownguide.org/cheat-sheet/">
            Markdown cheatsheet
          </a>
        </small>
      </p>
      <div class="md-field-content is-marginless px-0">
        {isPreview() ? (
          <div class="md-preview">
            <Markdown class="content" text={props.get()} />
          </div>
        ) : (
          <MultilineTextField
            type={FieldType.MultilineText}
            innerProps={{
              class: "input expand is-family-monospace",
              ref: (ta_: HTMLTextAreaElement) => {
                ta = ta_;
              },
            }}
            expand={typeof props.lines !== "number"}
            lines={props.lines}
            {...props}
          />
        )}
        <div class="md-controls px-1 py-1 has-background-white-bis">
          <div class="level is-mobile">
            <div>
              {props.controls === undefined || ta === undefined ? undefined : (
                <props.controls
                  textarea={ta}
                  insertText={(text: string) => {
                    untrack(() => {
                      const t = props.get();
                      const v = t + text;
                      if (ta !== undefined) {
                        ta.value = v;
                      }
                      props.set(v);
                    });
                  }}
                />
              )}
            </div>
            <div class="level-right">
              <Show when={props.charLimit !== undefined}>
                <div class="level-item">
                  <small class="is-family-monospace">
                    {props.get().length} / {props.charLimit}
                  </small>
                </div>
              </Show>
              <div class="level-item">
                <div class="buttons has-addons">
                  <button
                    class="button is-small"
                    onClick={() => setPreview(false)}
                    classList={{
                      "is-selected": !isPreview(),
                      "is-link": !isPreview(),
                    }}
                  >
                    Edit
                  </button>
                  <button
                    class="button is-small"
                    onClick={() => setPreview(true)}
                    classList={{
                      "is-selected": isPreview(),
                      "is-link": isPreview(),
                    }}
                  >
                    Preview
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export interface NumberField extends BaseField<string | number> {
  precision?: number;
  min?: number;
  max?: number;
  step?: number;
  constraint?: (val: number) => boolean;
}

export function NumberField(props: NumberField) {
  const [getIsValid, setIsValid] = createSignal(false);
  return (
    <>
      <input
        ref={(ref: HTMLInputElement) => {
          ref.value = String(props.get() ?? props.defaultValue);
          setIsValid(ref.validity.valid);
        }}
        class="input"
        type="number"
        min={props.min}
        max={props.max}
        step={props.step ?? 1}
        onInput={debounce((ev) => {
          const val = ev.target.value;
          const n = Number.parseInt(val);
          let valid = ev.target.validity.valid;
          if (typeof props.constraint === "function") {
            valid = valid && props.constraint(n);
          }
          setIsValid(valid);
          props.set(
            valid && Number.isFinite(n) && Number.isSafeInteger(n) && n >= 0
              ? n
              : val
          );
        }, 100)}
        {...(props.innerProps ?? {})}
      />
      {getIsValid() ? undefined : <p class="help is-danger">Invalid number</p>}
    </>
  );
}

export interface CheckboxField extends BaseField<boolean> {}

export function CheckboxField(props: CheckboxField) {
  const ref = controlledChecked(props);
  return (
    <label class="checkbox">
      <input
        ref={ref}
        type="checkbox"
        onChange={(ev) => props.set(ev.target.checked)}
        {...(props.innerProps ?? {})}
      />{" "}
      {props.label}
      <p class="help">{props.description}</p>
    </label>
  );
}

export interface DateTimeField
  extends ValidatedField<{ date: string; time: string }, Date> {}

function computeDateTime(date: string, time: string): undefined | Date {
  const str = `${date} ${time}`;
  const obj = parseISO(str);
  return isValid(obj) ? obj : undefined;
}

export function DateTimeField(props: DateTimeField) {
  const defaultDate = formatDateMaybe(props.defaultValue);
  const defaultTime = formatTimeMaybe(props.defaultValue?.[1]?.time);
  return (
    <>
      <div class="control columns">
        <div class="column">
          <DateField
            type={FieldType.Date}
            defaultValue={defaultDate}
            set={(datestr: string) =>
              props.set(([_, strs]) => [
                computeDateTime(datestr, strs.time),
                { date: datestr, time: strs.time },
              ])
            }
            get={() => props.get()[1].date}
          />
        </div>
        <div class="column">
          <TimeField
            type={FieldType.Time}
            defaultValue={defaultTime}
            set={(timestr: string) =>
              props.set(([_, strs]) => [
                computeDateTime(strs.date, timestr),
                { date: strs.date, time: timestr },
              ])
            }
            get={() => props.get()[1].time}
          />
        </div>
      </div>
    </>
  );
}

export interface RadioField extends BaseField<boolean> {
  trueLabel?: string;
  falseLabel?: string;
}

function RadioField(props: RadioField) {
  const ref = controlledChecked(props);
  return (
    <input
      ref={ref}
      class="input"
      type="checkbox"
      onChange={(ev) => props.set(ev.target.checked)}
    />
  );
}

export interface DateField extends BaseField<string> {}
export function DateField(props: DateField) {
  const selfIsValid = createMemo(() => {
    const val = props.get();
    return val === "" || isMatch(props.get(), "yyyy-MM-dd");
  });
  const ref = controlledValue(props);
  return (
    <>
      <input
        ref={ref}
        class="input"
        type="date"
        pattern="\d{4}-\d{2}-\d{2}"
        onChange={(ev) => props.set(ev.target.value)}
      >
        Date
      </input>
      {selfIsValid() ? undefined : <p class="help is-danger">Invalid date</p>}
    </>
  );
}

export interface TimeField extends BaseField<string> {}
export function TimeField(props: TimeField) {
  const selfIsValid = createMemo(() => {
    const val = props.get();
    return val === "" || isMatch(props.get(), "HH:mm");
  });
  const ref = controlledValue(props);
  return (
    <>
      <input
        ref={ref}
        class="input"
        type="time"
        pattern="\d{2}:\d{2}"
        onChange={(ev) => props.set(ev.target.value)}
      >
        Date
      </input>
      {selfIsValid() ? undefined : <p class="help is-danger">Invalid time</p>}
    </>
  );
}

export type SelectField<K extends string | number | symbol> = {
  title: JSX.Element;
  value?: null | K;
  onChange?: (option: DropdownOption<K>) => void;
  render?: (option: DropdownOption<K>) => JSX.Element;
  hoverable?: boolean;
} & BaseField<{ active: undefined | K }> &
  (
    | {
        kind?: "simple";
        options: DropdownOptions<K>;
      }
    | {
        kind: "fetch";
        options: (
          page: number
        ) => Promise<undefined | [Paged, DropdownOptions<K>]>;
      }
  );

export function SelectField<K extends string | number | symbol>(
  props: SelectField<K>
) {
  return (
    <Dropdown
      title={(props.title as any) ?? "Select"}
      value={
        (props.get().active as undefined | K) ??
        (props.defaultValue?.active as undefined | K)
      }
      kind={props.kind as any}
      options={props.options as any}
      render={props.render}
      hoverable={props.hoverable}
      onChange={(opt) => props.set({ active: opt.key })}
    />
  );
}

export interface DurationField extends BaseField<string | number> {
  precision: "s" | "m" | "h";
}

const durationPlaceholder = {
  m: "Duration in minutes",
  s: "Duration in seconds",
  h: "Duration in hours",
} as const;

export function DurationField(props: DurationField): JSX.Element {
  const [getIsValid, setIsValid] = createSignal(false);
  return (
    <>
      <input
        ref={(ref: HTMLInputElement) => {
          ref.value = String(props.get() ?? props.defaultValue);
          setIsValid(ref.validity.valid);
        }}
        class="input"
        type="number"
        placeholder={durationPlaceholder[props.precision]}
        min={0}
        pattern="\d+"
        onInput={debounce((ev) => {
          const val = ev.target.value;
          const n = Number.parseInt(val);
          const valid = ev.target.validity.valid;
          setIsValid(ev.target.validity.valid);
          props.set(
            valid && Number.isFinite(n) && Number.isSafeInteger(n) && n >= 0
              ? n
              : val
          );
        }, 100)}
      />
      {getIsValid() ? undefined : (
        <p class="help is-danger">Invalid duration</p>
      )}
    </>
  );
}

export interface SubformField<S> extends BaseField<S[]> {
  form: SomeFieldConstr<S>[] | ((s: () => S) => SomeFieldConstr<S>[]);
  allowAddRemove?: boolean;
  create: (index: number) => S;
}

export function SubformField<S>(props: SubformField<S>): JSX.Element {
  const { form } = props;
  function add() {
    const instances = props.get();
    const subdata = props.create(instances.length);
    props.set([...instances, subdata] as S[]);
  }
  function drop(index: number) {
    const instances = [...props.get()] as S[];
    instances.splice(index, 1);
    props.set(instances);
  }
  function state(index: number): [() => State<S>, SetStateFunction<S>] {
    return [
      () => (props.get() as any)[index] as State<S>,
      (...args: any[]) => (props.set as any)(index, ...args),
    ];
  }
  return (
    <>
      <EnumFromTo from={0} to={props.get().length}>
        {(index) => (
          <div class="container box">
            <Form
              state={state(index)}
              form={
                typeof form === "function"
                  ? form(() => (props.get() as any)[index])
                  : form
              }
            />
            {!props.allowAddRemove ? undefined : (
              <div class="field">
                <div class="control buttons is-right">
                  <button class="button is-light" onClick={() => drop(index)}>
                    <Icon type="close" />
                    <span>Cancel</span>
                  </button>
                </div>
              </div>
            )}
          </div>
        )}
      </EnumFromTo>
      <div class="field">
        <div class="control">
          {!props.allowAddRemove ? undefined : (
            <button class="button" onClick={add}>
              <Icon type="add" />
              <span>Add {props.label}</span>
            </button>
          )}
          {props.description}
        </div>
      </div>
    </>
  );
}

export interface ShowIfField<S> extends BaseField<S> {
  when: () => boolean;
  form: SomeFieldConstr<S>[];
}

export function ShowIfField<S>(props: ShowIfField<S>): JSX.Element {
  return (
    <Show when={props.when()}>
      <Form state={[props.get, props.set]} form={props.form} />
    </Show>
  );
}

export interface StatePipeField<S> extends BaseField<S> {
  form: (getState: () => S, set: SetStateFunction<S>) => SomeFieldConstr<S>[];
}

export function StatePipeField<S>(props: StatePipeField<S>): JSX.Element {
  const innerForm = props.form(props.get as () => S, props.set);
  return <Form form={innerForm} state={[props.get, props.set]} />;
}

export interface VisualField<S> extends BaseField<S> {
  render: (state: State<S>, setState: SetStateFunction<S>) => JSX.Element;
}

function VisualField<S>(props: VisualField<S>): JSX.Element {
  return props.render(props.get(), props.set);
}

export default function Form<S>(props: {
  state: [() => State<S>, SetStateFunction<S>];
  form: SomeFieldConstr<S>[];
}): JSX.Element {
  const { form, state } = props;
  const fields = form.map((constr) => constr(state[0] as () => S, state[1]));
  return <For each={fields}>{Field}</For>;
}

function Field<Props extends SomeField>(props: Props): JSX.Element {
  function field<P>(
    Child: (innerProps: P) => JSX.Element
  ): (innerProps: P) => JSX.Element {
    return (innerProps) => (
      <div class="field">
        <Child {...innerProps} />
      </div>
    );
  }

  function label<P>(
    Child: (innerProps: P) => JSX.Element
  ): (innerProps: P) => JSX.Element {
    return (innerProps) => (
      <div class="control">
        {typeof props.label !== "string" ? undefined : (
          <label class="label">
            {props.label}
            {props.description === undefined ? undefined : (
              <p>
                <small class="has-text-weight-normal">
                  {props.description}
                </small>
              </p>
            )}
          </label>
        )}
        <Child {...innerProps} />
      </div>
    );
  }

  return (
    <CaseOf
      key="type"
      data={props as SomeField}
      options={{
        [FieldType.Text]: field(label(TextField)),
        [FieldType.MultilineText]: field(label(MultilineTextField)),
        [FieldType.Number]: field(label(NumberField)),
        [FieldType.Checkbox]: field(CheckboxField),
        [FieldType.Radio]: field(RadioField),
        [FieldType.Select]: field(label(SelectField)),
        [FieldType.Date]: field(label(DateField)),
        [FieldType.Time]: field(label(TimeField)),
        [FieldType.DateTime]: field(label(DateTimeField)),
        [FieldType.Duration]: field(label(DurationField)),
        [FieldType.Subform]: SubformField,
        [FieldType.ShowIf]: ShowIfField,
        [FieldType.StatePipe]: StatePipeField,
        [FieldType.Visual]: VisualField,
      }}
    />
  );
}

// This is what requires typescript 4.0+
type Flatten<Arg, Acc extends any[]> = Arg extends []
  ? Acc
  : Arg extends [infer X]
  ? [...Acc, X]
  : Arg extends [infer X, infer XS]
  ? Defer<Flatten<XS, [...Acc, X]>>
  : never;

// Same trick as in https://github.com/microsoft/TypeScript/pull/21613
interface Defer<X> {
  self: Undefer<X>;
}
type Undefer<X> = X extends { self: infer U } ? U : X;

type StatePathNested<T> =
  | []
  | (T extends any[]
      ? StatePathArr<T>
      : T extends object
      ? StatePathObj<T>
      : []);

type StatePathArr<T extends any[]> = [number, StatePathNested<ElementOf<T>>];

type StatePathObj<T> = {
  [Key in keyof T]: [Key, StatePathNested<T[Key]>];
}[keyof T];

type StatePath<T> = Undefer<Flatten<StatePathNested<T>, []>>;

type AvailablePathsD<Path extends any[], T, P> = P extends [
  infer Key,
  // prettier-ignore
  ...infer Next
]
  ? Key extends keyof T
    ? Defer<AvailablePathsD<[...Path, Key], T[Key], Next>>
    : never
  : [Path, T];

type AvailablePaths<T> = Undefer<AvailablePathsD<[], T, StatePath<T>>>;

export type PathsMatching<T, Target> = AvailablePaths<T> extends infer FP
  ? FP extends [infer Path, infer Obj]
    ? Obj extends Target
      ? Path
      : never
    : never
  : never;

// prettier-ignore
export type FollowPathD<T, Target> = Target extends [infer X, ...infer XS]
  ? X extends keyof T
    ? Defer<FollowPathD<T[X], XS>>
    : never
  : T;

type FollowPath<T, Target> = Undefer<FollowPathD<T, Target>>;

function controlledValue<Ref extends { value: string }>(
  props: BaseField<string>
): (ref: Ref) => void {
  return (ref) => {
    ref.value = props.get() ?? props.defaultValue;
    if (typeof props?.innerProps?.ref === "function") {
      props.innerProps.ref(ref);
    }
  };
}

function controlledChecked<Ref extends { checked: boolean }>(
  props: BaseField<boolean>
): (ref: Ref) => void {
  return (ref) => {
    ref.checked = props.get() ?? props.defaultValue;
  };
}

function formatDateMaybe(
  date?: string | number | Date | [Date | undefined, { date: string } | string]
): undefined | string {
  if (Array.isArray(date)) {
    const [obj, str] = date;
    return (
      formatDateMaybe(obj) ??
      (typeof str === "string"
        ? formatDateMaybe(str)
        : formatDateMaybe(str.date))
    );
  }
  if (typeof date === "string") return date;
  return date instanceof Date || typeof date === "number"
    ? format(date, "yyyy-MM-dd")
    : undefined;
}

function formatTimeMaybe(date?: string | number | Date): undefined | string {
  if (typeof date === "string") return date;
  return date instanceof Date || typeof date === "number"
    ? format(date, "HH:mm")
    : undefined;
}
