import { createEffect, createSignal, JSX, untrack } from "solid-js";

export default function Keyed(p: {
  key: any;
  children: JSX.Element;
}): JSX.Element {
  const [children, setChildren] = createSignal(p.children, () => false);
  createEffect(() => {
    p.key;
    untrack(() => {
      setChildren(p.children);
    });
  });
  return children;
}
