import { Marked, Renderer } from "@ts-stack/markdown";
import log from "loglevel";

log.info("init markdown worker");

const md = Marked.setOptions({
  renderer: new Renderer(),
  sanitize: true,
  smartLists: true,
});

const ctx: Worker = self as any;

function render(markdown: string) {
  return md.parse(markdown);
}

ctx.onmessage = (ev: MessageEvent<{ id: number; markdown: string }>) => {
  const { id, markdown } = ev.data;
  const html = render(markdown);
  ctx.postMessage({ id, html });
};
