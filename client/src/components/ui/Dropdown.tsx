import {
  createEffect,
  createResource,
  createSignal,
  JSX,
  onCleanup,
  Show,
  splitProps,
  untrack,
} from "solid-js";
import { Paged } from "../../api";
import { ClampY } from "../../context/Size";
import Await from "../async/Await";
import { SimplePaged } from "../Paginate";
import Icon from "./Icon";

function processOptions<K extends string | number | symbol>(
  options: Options<K>
) {
  const optionTitlesByKey: { [k in K]?: Option<K> } = {};
  for (let i = 0; i < options.length; i++) {
    const opt = options[i];
    if (typeof opt === "string") continue;
    if (opt.key === undefined) opt.key = i as any;
    optionTitlesByKey[opt.key as K] = opt;
  }
  return optionTitlesByKey;
}

export default function Comp<K extends string | number | symbol>(
  outerProps: Props<K>
) {
  const [props, divProps] = splitProps(outerProps, [
    "title",
    "value",
    "onChange",
    "disabled",
    "renderTrigger",
    "render",
    "isRight",
    "hoverable",
    "kind",
    "options",
    "textSearchRequireTerms" as any,
    "class",
    "buttonClass",
  ]) as [DropdownCoreProps<K>, DropdownDivProps<K>];

  const { hoverable, onChange, render: defaultRender, renderTrigger } = props;

  const [getPage, setPage] = createSignal(0);
  const [getTerms, setTerms] = createSignal<string | undefined>(undefined);
  const [isOpen, setOpen] = createSignal(false);
  const [activeKey, setActiveKey] = createSignal<null | K>(
    untrack(() => props.value) ?? null
  );

  const [getOptions, setOptions] = createResource<{
    items: Options<K>;
    byKey: { [k in K]?: Option<K> };
    page: Paged;
  }>();

  createEffect(() => {
    const options = props.options;
    const page = getPage();
    const terms = getTerms();
    untrack(async () => {
      if (typeof options === "function") {
        if (
          props.kind === "search" &&
          props.textSearchRequireTerms &&
          (terms === "" || terms === undefined)
        ) {
          // i.e. require a search
          setOptions(() => ({
            items: [] as string[],
            byKey: {},
            page: unpaged,
          }));
          return;
        }
        const r = await options(page, terms ?? "");
        if (r === undefined) return;
        setOptions(() => ({
          items: r[1],
          page: r[0],
          byKey: processOptions(r[1]),
        }));
      } else {
        setOptions(() => ({
          items: options as any,
          page: {
            page: 0,
            hasMore: false,
            items: [],
            totalCount: options.length,
          },
          byKey: processOptions(options as any),
        }));
      }
    });
  });

  const render: (option: Option<K>) => JSX.Element =
    defaultRender !== undefined ? defaultRender : (option) => option.title;

  createEffect(() => {
    const value = props.value;
    if (value !== untrack(activeKey)) {
      setActiveKey(value ?? null);
    }
  });

  const onClick = (option: Option<K>) => (event: MouseEvent) => {
    if (option.onClick !== undefined) {
      option.onClick(event);
    }
    if (onChange !== undefined) {
      onChange(option);
    }
    if (props.value === undefined) {
      setActiveKey(option.key as K);
    }
    setOpen(false);
  };

  createEffect(() => {
    if (props.disabled) setOpen(false);
  });

  function onCloseMenu() {
    setOpen(false);
  }
  return (
    <div
      class={"dropdown " + (props.class ?? "")}
      classList={{
        "is-right": props.isRight,
        "is-active": isOpen(),
      }}
      onMouseLeave={() => {
        if (hoverable) setOpen(false);
      }}
      onMouseEnter={() => {
        if (hoverable) setOpen(true);
      }}
      {...divProps}
    >
      <button
        disabled={props.disabled}
        class={"button " + (props.buttonClass ?? "")}
        onClick={() => {
          if (props.disabled) return;
          setOpen(!isOpen());
        }}
        aria-haspopup="true"
      >
        <div class="dropdown-trigger">
          <span>
            <Await
              data={getOptions}
              fallback={props.title}
              children={(self) => {
                const key = activeKey();
                const bykey = self.byKey;
                const got: Option<K> | undefined = bykey[key as K];
                if (renderTrigger !== undefined) {
                  return renderTrigger({
                    ...(got ?? { title: props.title }),
                    isActive: got !== undefined,
                  });
                }
                if (key === null || bykey[key] === undefined) {
                  return props.title;
                }
                if (got !== undefined) {
                  return render({ ...got, isActive: true });
                }
                return "Unknown";
              }}
            />
          </span>
          <Icon type="arrow-dropdown" />
        </div>
      </button>
      <ClampY>
        <div
          class="dropdown-menu"
          ref={(div: HTMLDivElement) => {
            function onClick(ev: MouseEvent) {
              const target = ev.target;
              if (target instanceof Node) {
                if (!div.contains(target)) {
                  onCloseMenu();
                }
              }
            }
            document.body.addEventListener("click", onClick);
            onCleanup(() =>
              document.body.removeEventListener("click", onClick)
            );
          }}
        >
          <div class="dropdown-content">
            <Show when={props.kind === "search"}>
              <div class="dropdown-item">
                <div class="control has-icons-right">
                  <input
                    class="input is-small"
                    placeholder="Search..."
                    onInput={(ev) => {
                      const val = ev.target.value.trim();
                      setTerms(val === "" ? undefined : val);
                    }}
                  />
                  <Icon class="is-right" type="search" />
                </div>
              </div>
              <hr />
            </Show>
            <Await
              data={getOptions}
              children={(opts) => (
                <SimplePaged
                  source={() => Promise.resolve([opts.page, opts.items])}
                  getPage={getPage}
                  renderContent={(c) => c}
                  renderItems={(c) => c.children}
                  renderItem={(item) => {
                    if (typeof item === "string") {
                      return (
                        <div class="dropdown-item dropdown-heading">{item}</div>
                      );
                    }
                    if (item.type === "content") {
                      return <div class="dropdown-item">{render(item)}</div>;
                    }
                    return (
                      <a
                        onClick={onClick(item)}
                        class="dropdown-item"
                        classList={{
                          "is-active":
                            item.isActive !== false && activeKey() === item.key,
                        }}
                      >
                        {render(item)}
                      </a>
                    );
                  }}
                  renderPageButtons={(props) => (
                    <Show
                      when={
                        props.prev !== undefined || props.next !== undefined
                      }
                    >
                      <hr />
                      <div class="buttons">
                        <button
                          class="button is-small"
                          disabled={props.prev === undefined}
                          onClick={() => setPage(props.prev as number)}
                        >
                          Previous
                        </button>
                        <button
                          class="button is-small"
                          disabled={props.next === undefined}
                          onClick={() => setPage(props.next as number)}
                        >
                          Next
                        </button>
                      </div>
                    </Show>
                  )}
                />
              )}
            />
          </div>
        </div>
      </ClampY>
    </div>
  );
}

export interface Option<K> {
  key?: K;
  title: JSX.Element;
  type?: "content" | "button";
  onClick?: (event: MouseEvent) => void;
  isActive?: boolean;
  extra?: any;
}

export type Options<K> = (Option<K> | string)[];

export type DropdownCoreProps<K = number> = {
  title: JSX.Element;
  class?: string;
  buttonClass?: string;
  value?: null | K;
  onChange?: (option: Option<K>) => void;
  disabled?: boolean;
  renderTrigger?: (option: Option<K>) => JSX.Element;
  render?: (option: Option<K>) => JSX.Element;
  isRight?: boolean;
  hoverable?: boolean;
} & (
  | {
      kind?: undefined;
      options: Options<K>;
    }
  | {
      kind: "fetch";
      options: (page: number) => Promise<undefined | [Paged, Options<K>]>;
    }
  | {
      kind: "search";
      options: (
        page: number,
        terms: string
      ) => Promise<undefined | [Paged, Options<K>]>;
      textSearchRequireTerms?: boolean;
    }
);

export type DropdownDivProps<K = number> = Omit<
  JSX.IntrinsicElements["div"],
  keyof DropdownCoreProps<K> | "ref"
>;

export type Props<K = number> = DropdownCoreProps<K> & DropdownDivProps<K>;

// TODO clean up this so the types are a bit less horrific

export const unpaged: Paged = {
  totalCount: Number.MAX_SAFE_INTEGER,
  items: [],
  hasMore: false,
  page: 0,
};

type SimpleItem = {
  id: string | number;
} & (
  | {
      name: string;
      title?: undefined;
    }
  | {
      name?: undefined;
      title: string;
    }
);

export function simpleItems<T extends SimpleItem>(
  items: T[]
): [Paged, Option<T["id"]>[]] {
  return [
    { totalCount: items.length, items: [], hasMore: false, page: 0 },
    items.map((i) => ({
      key: i.id,
      title: i.name ?? i.title,
      extra: i,
    })),
  ];
}
