import { For, createMemo, JSX } from "solid-js";
import { Paged } from "../../api";
import { Link } from "../../router";
import { useRoute } from "solid-typefu-router5";
import { useAsync } from "../async/Awaitable";

export interface PagedProps<T> {
  source: (page: number) => undefined | [Paged, T[]] | Promise<[Paged, T[]]>;
  getPage: () => number;
  renderItem: (item: T) => JSX.Element;
  renderItems: (p: { children: JSX.Element; items: T[] }) => JSX.Element;
  renderContent: (children: JSX.Element) => JSX.Element;
  renderPageButtons: (props: {
    prev: number | undefined;
    next: number | undefined;
  }) => JSX.Element;
}

export interface Props<T> {
  route: "players" | "teams" | "announcements";
  source: (page: number) => Promise<[Paged, T[]]>;
  render?: (item: T) => JSX.Element;
  renderItem?: (item: T) => JSX.Element;
  renderItems?: (p: { children: JSX.Element; items: T[] }) => JSX.Element;
  renderContent?: (children: JSX.Element) => JSX.Element;
  renderPageButtons?: (props: {
    prev: number | undefined;
    next: number | undefined;
  }) => JSX.Element;
}

const emptyPages: { paged: Paged; items: any[] } = {
  paged: {
    page: 0,
    items: [],
    hasMore: false,
    totalCount: 0,
  },
  items: [],
};

export function SimplePaged<T>(props: PagedProps<T>): JSX.Element {
  const {
    getPage,
    renderItem,
    renderItems: RenderItems,
    renderContent,
    source,
    renderPageButtons: PageButtons,
  } = props;

  const getPaged = useAsync(async () => {
    const cur = getPage();
    const r = await source(cur);
    if (r !== undefined) {
      const [paged, items] = r;
      return { paged, items };
    }
    return undefined;
  }, emptyPages);

  const nextPage = createMemo(() => {
    const cur = getPage();
    const hasMore = getPaged().paged.hasMore;
    const dest = typeof cur === "number" ? cur + 1 : 1;
    return hasMore ? dest : undefined;
  });

  const prevPage = createMemo(() => {
    const cur = getPage();
    return cur > 0 ? cur - 1 : undefined;
  });

  return renderContent(() => (
    <>
      <RenderItems items={getPaged().items as T[]}>
        {() => <For each={getPaged().items as T[]}>{renderItem}</For>}
      </RenderItems>
      <PageButtons prev={prevPage()} next={nextPage()} />
    </>
  ));
}

export default function <T>(props: Props<T>): JSX.Element {
  const here = useRoute();
  const {
    route,
    render,
    source,
    renderItem,
    renderContent,
    renderItems,
    renderPageButtons,
  } = props;
  return (
    <div class="pages">
      <SimplePaged
        source={source}
        getPage={() => here().params.page}
        renderItem={
          renderItem ??
          (render === undefined
            ? () => "item" // TODO make this a type error
            : (item) => <li>{render(item)}</li>)
        }
        renderItems={
          renderItems ??
          ((p) => (
            <aside class="menu">
              <ul class="menu-list">{p.children}</ul>
            </aside>
          ))
        }
        renderContent={
          renderContent ??
          ((content) => <div class="pages-items">{content}</div>)
        }
        renderPageButtons={
          renderPageButtons ??
          ((props) => (
            <nav class="pagination" aria-label="pagination">
              <Link
                class="pagination-previous"
                to={route as Dummy}
                params={{ page: props.prev + "" }}
                disabled={props.prev === undefined}
              >
                Previous
              </Link>
              <Link
                class="pagination-next"
                to={route as Dummy}
                params={{ page: props.next + "" }}
                disabled={props.next === undefined}
              >
                Next
              </Link>
            </nav>
          ))
        }
      />
    </div>
  );
}

type Dummy = "teams";
