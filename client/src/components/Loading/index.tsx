export const LoadingFull = (
  <div class="loading loading-full">
    <div class="button is-loading is-white" />
  </div>
);

export const Loading = () => (
  <div class="loading">
    <div class="button is-loading is-white" />
  </div>
);

export const TransparentLoading = () => (
  <div class="button is-loading is-text" />
);
