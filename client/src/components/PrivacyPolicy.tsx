import Markdown from "./ui/Markdown";

const pp = `
# Privacy policy

## Data we collect
For each user that logs in, we store the following data:
- Their Discord username and tag
- Their Discord id
- Their Discord avatar
- Their email address tied to their Discord account (if any)

No user data is sold or distributed to any third parties.
`;

export default function PrivacyPolicy() {
  return (
    <div class="content">
      <Markdown text={pp} />
    </div>
  );
}
