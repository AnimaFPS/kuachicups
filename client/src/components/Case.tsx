import { createMemo, JSX } from "solid-js";
import { UnionToIntersection } from "ts-essentials";

export function Case<Key extends string | number | symbol>(props: {
  key: Key;
  fallback?: JSX.Element;
  options: { [K in Key]?: JSX.Element };
}): JSX.Element {
  const options = props.options;
  const getter = createMemo<[Key, undefined | JSX.Element]>(
    () => {
      const key = props.key;
      const elem = options[key];
      return [key, elem];
    },
    undefined,
    (a, b) => {
      const eq = a !== undefined && a[0] === b[0];
      return eq;
    }
  );
  return () => {
    return getter()[1] ?? props.fallback;
  };
}

type CaseOpts<Data, Key extends keyof Data, Result> = Partial<
  UnionToIntersection<
    Data[Key] extends infer Value
      ? Value extends string | symbol | number
        ? Record<Value, (data: Data & Record<Key, Value>) => Result>
        : never
      : never
  >
>;

export function CaseOf<
  Data extends { [Val in Key]: string | number | symbol },
  Key extends keyof Data,
  R = JSX.Element
>(
  props:
    | {
        data: Data;
        key: Key;
        fallback?: R;
        options: CaseOpts<Data, Key, JSX.Element>;
        children?: undefined;
      }
    | {
        data: Data;
        key: Key;
        fallback?: R;
        options?: undefined;
        children: CaseOpts<Data, Key, JSX.Element>;
      }
): () => undefined | R {
  const getter = createMemo<[Data, undefined | ((data: Data) => R)]>(
    () => {
      const { key, data } = props;
      const options = props.options ?? props.children;
      const render: undefined | ((data: Data) => R) = (options as any)[
        data[key]
      ];
      return [data, render];
    },
    undefined,
    (a, b) => a !== undefined && a[0] === b[0] && a[1] === b[1]
  );
  return () => {
    const [data, render] = getter();
    return typeof render === "function" ? render(data) : props.fallback;
  };
}

export function EnumFromTo(props: {
  from: number;
  to: number;
  step?: number;
  children: (index: number) => JSX.Element;
}): JSX.Element {
  return () => {
    const { from, step = 1, to, children: render } = props;
    const r: JSX.Element = [];
    for (let i = from; i < to; i += step) {
      r.push(render(i));
    }
    return r;
  };
}
