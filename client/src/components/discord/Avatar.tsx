import { Show, splitProps, JSX } from "solid-js";
import { Link } from "../../router";
import { Player } from "../../api";
import useBlobCache from "../../blobCache";

export type Size = 16 | 24 | 32 | 48 | 64 | 96 | 128 | "fill" | string;

export interface DiscordAvatarLinkProps {
  player: Player;
  size?: Size;
}

export function discordAvatarLink(
  id: string,
  avatar: undefined | null | string,
  wantSize: Size
) {
  if (avatar == null) return "";
  const size =
    typeof wantSize !== "number"
      ? 256
      : wantSize <= 100
      ? 80
      : wantSize <= 128
      ? 128
      : 256;
  return `https://cdn.discordapp.com/avatars/${id}/${avatar}.png?size=${size}`;
}

function sizeClass(size: Size = 24): string {
  switch (size) {
    case 16:
      return "is-16x16";
    case 24:
      return "is-24x24";
    case 32:
      return "is-32x32";
    case 48:
      return "is-48x48";
    case 64:
      return "is-64x64";
    case 96:
      return "is-96x96";
    case 128:
      return "is-128x128";
    case "fill":
      return "is-full";
    default:
      return "";
  }
}

/*

Don't bother with this because foreign objects in SVG in Chrome won't appear
correctly.

const claims: (() => void)[] = [];

let claimer: NodeJS.Timeout | undefined;

function startClaimer() {
  const maxIdle = 10;
  let idle = 10;
  claimer = setInterval(() => {
    if (claims.length > 0) {
      for (let i = 0; i < claims.length && i < 4; i++) {
        claims.pop()!();
      }
    } else if (idle > maxIdle) {
      clearInterval(claimer!);
      claimer = undefined;
    } else {
      log.info("winding down avatar loader");
      idle++;
    }
  }, 50);
}

function waitForClaim() {
  if (claimer === undefined) startClaimer();
  return new Promise(resolve => {
    claims.push(resolve);
  });
}
*/

export function useDiscordAvatar(
  getData: () => {
    discordId: string;
    discordAvatar?: null | string;
    size?: Size;
  }
) {
  return useBlobCache(() => {
    const { discordId, discordAvatar, size = 24 } = getData();
    return {
      id: `avatar-${discordId}-${size}`,
      src: discordAvatarLink(discordId, discordAvatar, size),
    };
  });
}

export function DiscordAvatarImg(
  p: {
    discordId: string;
    discordAvatar?: null | string;
    size?: Size;
  } & JSX.ImgHTMLAttributes<any>
) {
  const [discordProps, imgProps] = splitProps(p, [
    "discordId",
    "discordAvatar",
    "size",
  ]);
  const img = useDiscordAvatar(() => discordProps);
  return <img class="discord-avatar" src={img()!} {...imgProps} />;
}

export function DiscordServerAvatarImg(
  p: {
    discordId: string;
    discordAvatar: string;
    size?: Size;
  } & JSX.ImgHTMLAttributes<any>
) {
  const [dp, imgProps] = splitProps(p, ["discordId", "discordAvatar", "size"]);
  const img = useBlobCache(() => ({
    src: `https://cdn.discordapp.com/icons/${dp.discordId}/${
      dp.discordAvatar
    }.png?size=${dp.size ?? 256}`,
    id: `team-${dp.discordId}-${dp.discordAvatar}`,
  }));
  return <img class="discord-avatar" src={img()!} {...imgProps} />;
}

export default function PlayerAvatar(p: DiscordAvatarLinkProps) {
  const img = useDiscordAvatar(() => ({
    discordId: p.player.discordId,
    discordAvatar: p.player.discordAvatar,
    size: p.size,
  }));
  return (
    <Show when={img() !== undefined}>
      <figure
        class="discord-avatar image is-inline-block"
        style={{
          height:
            typeof p.size === "string" && p.size !== "fill"
              ? p.size
              : undefined,
          width:
            typeof p.size === "string" && p.size !== "fill"
              ? p.size
              : undefined,
        }}
        classList={{ [sizeClass(p.size)]: true }}
      >
        <Link to="players.profile" params={{ id: p.player.id }}>
          <img src={img()!} />
        </Link>
      </figure>
    </Show>
  );
}
