import { JSX } from "solid-js";
import { Provider as Api } from "../../context/Api";
import { Provider as Auth } from "../../context/Auth";
import { ContentSizeProvider, Provider as Size } from "../../context/Size";
import { Provider as Router } from "../../router";
import Footer from "./Footer";
import NavBar from "./NavBar";
import Routes from "../Routes";
import Title from "./Title";
import { Provider as Toast } from "../Toast";

export default function () {
  return (
    <Providers>
      <Title />
      <NavBar />
      <section id="main">
        <div id="content-wrapper">
          <ContentSizeProvider id="content">
            <Routes />
          </ContentSizeProvider>
        </div>
      </section>
      <Footer />
    </Providers>
  );
}

function Providers(props: { children: JSX.Element }) {
  return (
    <Size>
      <Router>
        <Auth>
          <Api>
            <Toast>{props.children}</Toast>
          </Api>
        </Auth>
      </Router>
    </Size>
  );
}
