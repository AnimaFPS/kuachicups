import { createEffect, untrack } from "solid-js";
import useApi from "../../context/Api";
import { useRoute } from "../../router";

const titleEl = document.getElementById("title")!;

export default () => {
  const getRoute = useRoute();
  const api = useApi();
  createEffect(() => {
    const route = getRoute();
    untrack(() => {
      async function mkTitle() {
        const r: string[] = ["kuachi cups"];
        switch (route.name) {
          case "announcements":
            r.push("announcements");
            break;
          case "media":
            r.push("media");
            break;
          case "privacy_policy":
            r.push("privacy policy");
            break;
          case "players.self":
            r.push("self");
            break;
          case "players.other":
            r.push((await api.players.byId(route.params.id)).discordUsername);
            break;
          case "cups":
            r.push("cups");
            break;
          case "cups.new":
            r.push("create a cup");
            break;
          case "cups.cup.manage":
          case "cups.cup.edit":
          case "cups.cup.schedule":
          case "cups.cup.stage":
          case "cups.cup.stage.bracket":
          case "cups.cup.stage.play":
          case "cups.cup":
            switch (route.name) {
              case "cups.cup.manage":
                r.push("manage");
                break;
              case "cups.cup.edit":
                r.push("edit");
                break;
              case "cups.cup.stage":
                r.push(
                  (
                    await api.cups.stages.byId(
                      route.params.id,
                      route.params.stageNo
                    )
                  ).title
                );
                break;
            }
            r.push((await api.cups.byId(route.params.id)).title);
            break;

          case "teams":
            r.push("all teams");
            break;
          case "teams.new":
            r.push("create a team");
            break;
          case "teams.mine":
            r.push("your teams");
            break;
          case "teams.invites":
            r.push("your invites");
            break;
          case "teams.profile.membership":
          case "teams.profile.edit":
          case "teams.profile.invite":
          case "teams.profile":
            r.push((await api.teams.byId(route.params.id)).name);
            switch (route.name) {
              case "teams.profile.membership":
                r.push("membership");
                break;
              case "teams.profile.edit":
                r.push("edit");
                break;
              case "teams.profile.invite":
                r.push("invite");
                break;
            }
            break;
        }
        return r.reverse().join(" · ");
      }
      mkTitle().then((t) => {
        titleEl.textContent = t;
      });
    });
  });

  return undefined;
};

/* <title>asdf</title> */
