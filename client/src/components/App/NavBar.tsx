import { createMemo, createSignal, onCleanup } from "solid-js";
import { Player } from "../../api";
import { loginRedirBack, logout } from "../../api/oauth2";
import useAuth, { LoginState } from "../../context/Auth";
import { useMobile } from "../../context/Size";
import logo from "../../logo_text.png";
import { Link, useRoute } from "../../router";
import { CaseOf } from "../Case";
import Avatar from "../discord/Avatar";
import Icon from "../ui/Icon";
import Breadcrumbs from "./Breadcrumbs";

export default function () {
  const here = useRoute();
  const [menu, setMenu] = createSignal(false);
  const mobile = useMobile();
  const auth = useAuth();

  const cupsIcon = createMemo((v) => {
    if (here().name.startsWith("cups")) {
      return (v + 1) % cupsIcons.length;
    }
    return v;
  }, 0);

  let mouseEnteredMenu: boolean = false;

  function onMouseEnterMenu() {
    mouseEnteredMenu = true;
  }

  function onMouseLeaveMenu() {
    mouseEnteredMenu = true;
  }

  function onCloseMenu() {
    setMenu(false);
  }

  function onClickMenuBurger() {
    setMenu(!menu());
  }

  function onBlurMenu() {
    if (!mouseEnteredMenu) onCloseMenu();
  }

  function menuCloseListener() {
    if (mouseEnteredMenu) return;
    if (menu()) setMenu(false);
  }

  addEventListener("click", menuCloseListener);
  onCleanup(() => removeEventListener("click", menuCloseListener));

  return (
    <nav
      id="nav"
      class="navbar is-black is-fixed-top is-z11"
      aria-role="navigation"
      aria-label="main navigation"
    >
      <div class="container">
        <div class="navbar-brand is-transparent">
          <Link id="navbar-logo" class="navbar-item" to="announcements">
            <img src={logo} />
          </Link>
          <button
            class="navbar-burger burger reset"
            aria-label="menu"
            onMouseEnter={onMouseEnterMenu}
            onMouseLeave={onMouseLeaveMenu}
            onClick={onClickMenuBurger}
            onBlur={onBlurMenu}
          >
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </button>
        </div>
        <div
          class="navbar-menu"
          classList={{ ["is-active"]: menu() }}
          onMouseEnter={() => {
            mouseEnteredMenu = true;
          }}
          onMouseLeave={() => {
            mouseEnteredMenu = false;
          }}
        >
          <div class="navbar-start">
            <Link
              class="navbar-item"
              to="announcements"
              onClick={onCloseMenu}
              nav
              navIgnoreParams
            >
              <Icon type="calendar" />
              ann
            </Link>
            <Link
              class="navbar-item"
              to="media"
              onClick={onCloseMenu}
              nav
              navIgnoreParams
            >
              <Icon type="videocam" />
              media
            </Link>
            <Link
              class="navbar-item"
              to="cups"
              params={{ page: "0", active: "true" }}
              onClick={onCloseMenu}
              nav
              navIgnoreParams
            >
              <Icon type={cupsIcons[cupsIcon()]} />
              cups
            </Link>
            <Link
              class="navbar-item"
              to="teams"
              onClick={onCloseMenu}
              nav
              navIgnoreParams
            >
              <Icon type="contacts" />
              teams
            </Link>
            <a
              id="nav-discord"
              class="navbar-item"
              href="https://discord.gg/v4KkkRQ"
              onClick={onCloseMenu}
            >
              <Icon type="chatboxes" />
              discord
            </a>
          </div>
          <div class="navbar-end">
            {!mobile() ? <Breadcrumbs /> : undefined}
            <CaseOf
              data={auth()}
              key="state"
              options={{
                [LoginState.LoggedIn]: (auth) => (
                  <AuthMenu player={auth.login} onClick={onCloseMenu} />
                ),
                [LoginState.NotLoggedIn]: () => (
                  <div class="navbar-item">
                    <div class="buttons">
                      <a class="button is-dark" onClick={loginRedirBack}>
                        Login
                      </a>
                    </div>
                  </div>
                ),
              }}
            />
          </div>
        </div>
      </div>
    </nav>
  );
}

function AuthMenu(p: { player: Player; onClick: () => void }) {
  return (
    <div class="navbar-item has-dropdown is-hoverable">
      <a class="navbar-item navbar-link">
        <span class="discord-avatar-username-link">
          <Avatar player={p.player} size={24} />
          <span>{p.player.discordUsername}</span>
        </span>
      </a>
      <div class="navbar-dropdown is-boxed">
        <Link
          class="navbar-item"
          to="players.profile"
          params={{ id: "self" }}
          onClick={p.onClick}
        >
          Profile
        </Link>
        <a class="navbar-item" onClick={logout}>
          Logout
        </a>
      </div>
    </div>
  );
}

const cupsIcons = ["trophy", "beer", "cafe"] as const;
