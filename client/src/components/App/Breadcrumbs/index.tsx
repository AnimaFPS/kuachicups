import { isEqual } from "lodash";
import { createMemo, JSX } from "solid-js";
import { Link, useRoute } from "../../../router";

export default (props: { center?: boolean }) => (
  <nav id="app-breadcrumbs" class="navbar">
    <div class="container">
      <nav class="breadcrumb" classList={{ "is-centered": props.center }}>
        <ul>
          <Crumbs />
        </ul>
      </nav>
    </div>
  </nav>
);

function Crumbs(): JSX.Element {
  const route = useRoute();
  return () => {
    const r = [];
    const path = [];
    const n = route();
    for (const seg of n.nameArray) {
      path.push(seg);
      r.push(<CrumbLi to={[...path].join(".")} fragment={seg} />);
    }
    return r;
  };
}

interface CrumbProps {
  to: string;
  fragment: string;
}

function CrumbLi(props: CrumbProps): JSX.Element {
  const getRouteName = useRoute();
  const active = createMemo(() => isEqual(getRouteName().name, props.to));
  return (
    <li classList={{ "is-active": active() }}>
      <Crumb fragment={props.fragment} to={props.to} />
    </li>
  );
}

function Crumb(props: CrumbProps): JSX.Element {
  const route = useRoute();
  return props.to !== undefined ? (
    <Link to={props.to as any} params={route().params}>
      {props.fragment}
    </Link>
  ) : (
    props.fragment
  );
}
