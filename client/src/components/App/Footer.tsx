import { Link } from "../../router";
import Icon from "../ui/Icon";

export default function Footer() {
  return (
    <footer id="footer" class="footer">
      <div class="container">
        <div class="level is-mobile">
          <div class="level-left">
            <div class="level-item help">
              <div>
                <p>
                  © 2020 <span class="is-family-monospace">kuachi cups</span>
                </p>
                <p>
                  <Link to="privacy_policy">Privacy policy</Link>
                </p>
              </div>
            </div>
          </div>
          <div class="level-center">
            <div class="level-item help poetic">
              <p>
                <span class="is-family-monospace">kuachi cups</span> is the home
                of competitive AFPS, by and for passionate Diabotical, Quake
                Champions, and Quake Live players, across Australia, New
                Zealand, and Oceania.
              </p>
            </div>
          </div>
          <div class="level-right">
            <div class="level-item help">
              <div>
                <p>
                  <a
                    class="button is-small is-text ml-2"
                    href="https://gitlab.com/_mike/kuachicups"
                  >
                    <Icon type="code" />
                    <span>Source</span>
                  </a>
                </p>
                <p>
                  website by <a href="mailto:mike@quasimal.com">mike</a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}
