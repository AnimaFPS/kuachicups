import log from "loglevel";
import { For, createEffect } from "solid-js";
import { Ranking } from "../../../api";
import { SignupLoader } from "./CupSignup";
import { useCupStore } from "./Store";

export default () => {
  const get = useCupStore();
  createEffect(() => {
    log.info("rankings", get.standings().rankings);
  });
  return (
    <div class="box">
      <div class="cup-standings">
        <table class="table cup-standings is-fullwidth">
          <colgroup>
            <col span="1" />
            <col span="1" style="width:25%" />
          </colgroup>
          <thead>
            <tr>
              <th>Entrant</th>
              <th class="is-3">
                {get.standings().rankings.length > 0 ? "Score" : ""}
              </th>
            </tr>
          </thead>
          <tbody>
            <For
              each={get.standings().rankings}
              fallback={() => (
                <get.signups.For
                  fallback={() => (
                    <tr>
                      <td colSpan="2" class="help">
                        No standings yet
                      </td>
                    </tr>
                  )}
                >
                  {(signup) => (
                    <tr>
                      <td colSpan="2">
                        <div class="level">
                          <SignupLoader
                            cupId={get.props.id}
                            signup={signup}
                            showCheckin
                          />
                        </div>
                      </td>
                    </tr>
                  )}
                </get.signups.For>
              )}
            >
              {(rank: Ranking) => (
                <tr>
                  <td>
                    <div class="level">
                      <SignupLoader cupId={get.props.id} signup={rank.signup} />
                    </div>
                  </td>
                  <td class="is-3">{rank.score}</td>
                </tr>
              )}
            </For>
          </tbody>
        </table>
      </div>
    </div>
  );
};
