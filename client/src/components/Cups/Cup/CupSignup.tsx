import { JSX } from "solid-js";
import { CupSignup, Player, Team } from "../../../api";
import Icon from "../../ui/Icon";
import useApi from "../../../context/Api";
import PlayerLink from "../../Player/Link";
import { createRefreshable } from "../../async/Refreshable";
import { Size } from "../../discord/Avatar";

export interface CupSignupProps {
  signup: CupSignup;
  player?: Player;
  team?: Team;
  showCheckin?: boolean;
  nameOnly?: boolean;
  size?: Size;
}

export default function SignupView(props: CupSignupProps): JSX.Element {
  return (
    <span class="cup-signup">
      <PlayerLink
        size={props.size}
        player={props.player}
        team={props.team}
        nameOnly={props.nameOnly}
      />
      {props.showCheckin && props.signup.checkedIn ? (
        <Icon type="done-all" />
      ) : undefined}
    </span>
  );
}

export function SignupLoader(props: {
  cupId: string;
  signup: CupSignup | string | undefined;
  showCheckin?: boolean;
  nameOnly?: boolean;
  size?: Size;
  render?: (props: CupSignupProps) => JSX.Element;
}) {
  const api = useApi();

  const get = createRefreshable(async () => {
    const ps = props.signup;
    if (ps === undefined) return undefined;
    const signup =
      typeof ps === "string"
        ? await api.cups.signups.byId(props.cupId, ps)
        : ps;
    const [player, team] = await Promise.all([
      signup.playerId != null ? api.players.byId(signup.playerId) : undefined,
      signup.teamId != null ? api.teams.byId(signup.teamId) : undefined,
    ]);
    return {
      signup,
      team,
      player,
      showCheckin: props.showCheckin,
      nameOnly: props.nameOnly,
      size: props.size,
    };
  });

  return <get.Await>{props.render ?? SignupView}</get.Await>;
}
