import {
  createContext,
  createEffect,
  createMemo,
  JSX,
  splitProps,
  useContext,
} from "solid-js";
import {
  Cup,
  CupSignup,
  CupStage,
  Game,
  GameMode,
  Standings,
  Team,
} from "../../../api";
import useApi from "../../../context/Api";
import { useRoute } from "../../../router";
import { Awaitable, AwaitableI, useAsync } from "../../async/Awaitable";

export interface CupStore {
  props: { id: string };
  cup: Awaitable<Cup>;
  stage: Awaitable<CupStage>;
  stages: Awaitable<CupStage[]>;
  standings: AwaitableI<Standings>;
  signups: Awaitable<CupSignup[]>;
  gameMode: Awaitable<{ game: Game; mode: GameMode }>;
  ownerTeam: Awaitable<Team>;
  loading: () => boolean;
  update: () => void;
}

const CupContext = createContext<CupStore>();

export const useCupStore = () => useContext(CupContext);

export function CupStore(props: {
  id: string;
  children: JSX.Element;
}): JSX.Element {
  const api = useApi();
  const getRoute = useRoute();
  const cup = useAsync((ref) => api.cups.byId(props.id, ref));

  const stages = useAsync((ref) => api.cups.stages.byParentId(props.id, ref));

  const stageNo = createMemo(
    () => {
      let no = getRoute().params.stageNo;
      const cupStage = cup()?.currentStage;
      if (typeof no === "string") {
        no = Number.parseInt(no);
      }
      if (Number.isSafeInteger(no) && no >= 0) return no;
      if (typeof cupStage === "number") return cupStage;
      return 0;
    },
    (a: unknown, b: unknown) => a === b
  );

  const stage = useAsync((ref) =>
    api.cups.stages.byId(props.id, stageNo(), ref)
  );

  const signups = useAsync((ref) => api.cups.signups.byParentId(props.id, ref));

  const gameMode = useAsync(async () => {
    const c = await cup.then();
    const mode = await api.games.modes.byId(c.gameModeId);
    const game = await api.games.byId(mode.gameId);
    return { game, mode };
  });

  const ownerTeam = useAsync(async () => {
    const c = await cup.then();
    return api.teams.byId(c.ownerTeamId);
  });

  const standings = useAsync(
    async (ref) => {
      const s = await stage.then();
      return (await api.cups.stages.standings.byId(s.id, ref)).standings;
    },
    { rankings: [], isComplete: false } as Standings
  );

  const isComplete = useAsync(
    async () => standings()?.isComplete === true,
    true
  );

  createEffect(() => {
    if (!isComplete()) {
      standings.startInterval(30);
    } else {
      standings.stopInterval();
    }
  });

  const loading = createMemo(() =>
    [cup, stages, signups, standings, gameMode, ownerTeam]
      .map((f) => f.loading())
      .some((a) => a)
  );

  const provProps = splitProps(props, ["id"])[0];

  return (
    <CupContext.Provider
      value={{
        props: provProps,
        cup,
        stage,
        stages,
        signups,
        standings,
        gameMode,
        ownerTeam,
        loading,
        update: () => {},
        // update: ref.update,
      }}
    >
      {props.children}
    </CupContext.Provider>
  );
}
