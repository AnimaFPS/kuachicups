import log from "loglevel";
import { For } from "solid-js";
import { Cup, CupStage } from "../../../../api";
import useApi from "../../../../context/Api";
import { Link, navigate } from "../../../../router";
import { createTicker } from "../../../Time";
import { stageFormats, StageTime } from "../Stage";

export interface Props {
  cup: Cup;
  stages: CupStage[];
}

export default function (props: Props) {
  return (
    <For each={props.stages}>
      {(stage) => <CupStage cup={props.cup} stage={stage} />}
    </For>
  );
}

const CupStage = (props: { cup: Cup; stage: CupStage }) => {
  const api = useApi();
  const now = createTicker("m");
  return (
    <div class="box">
      <div class="media is-marginless">
        <div class="media-content">
          <h2 class="subtitle">
            <Link
              to="cups.cup.stage"
              params={{ id: props.cup.id, stageNo: props.stage.stageNo + "" }}
            >
              {props.stage.title} - {stageFormats[props.stage.format]}
            </Link>
          </h2>
          <StageTime stage={props.stage} />
        </div>
        <div class="media-right">
          <button
            disabled={
              props.stage.isStarted ||
              !(
                props.stage.starttime instanceof Date &&
                now() >= props.stage.starttime
              )
            }
            onClick={() => {
              api
                .cupsCupStageInitialise({
                  cupId: props.cup.id,
                  stageId: props.stage.id,
                })
                .then(
                  () => {
                    delete api.cups.cache[props.cup.id];
                    delete api.cups.stages.parentCache[props.cup.id];
                    navigate({
                      to: "cups.cup.stage",
                      params: {
                        id: props.cup.id,
                        stageNo: props.stage.stageNo + "",
                      },
                    });
                  },
                  () => {
                    log.error(
                      "could not initialise cup!",
                      JSON.stringify(props, null, 2)
                    );
                  }
                );
            }}
            class="button is-primary"
          >
            {props.stage.isStarted ? "Already started" : "Start"}
          </button>

          <button
            class="button is-warning"
            onClick={() => {
              api
                .cupsCupStageRestart({
                  cupId: props.cup.id,
                  stageId: props.stage.id,
                })
                .then(() => {
                  delete api.cups.cache[props.cup.id];
                  delete api.cups.stages.parentCache[props.cup.id];
                  navigate({
                    to: "cups.cup.stage",
                    params: {
                      id: props.cup.id,
                      stageNo: props.stage.stageNo + "",
                    },
                  });
                });
            }}
          >
            Restart stage
          </button>
        </div>
      </div>
    </div>
  );
};
