import { debounce } from "lodash";
import log from "loglevel";
import { createEffect, createState, For, JSX } from "solid-js";
import { Cup, CupSignup, Player, SetSeed, Team } from "../../../../api";
import useApi from "../../../../context/Api";
import { useAsync } from "../../../async/Awaitable";
import { Refreshable } from "../../../async/Refreshable";
import { Case } from "../../../Case";
import PlayerLink from "../../../Player/Link";
import { failure } from "../../../Toast";
import Dropdown, { unpaged } from "../../../ui/Dropdown";
import Icon from "../../../ui/Icon";
import SignupView from "../CupSignup";

export interface State {
  seeds: Record<string, number>;
  extra: {
    playerId: string;
    teamId: string | undefined;
    seedValue: number | undefined;
    status: "signup" | "checkin" | "drop";
  }[];
  changes: Record<string, "checkin" | "checkout" | "drop" | undefined>;
}

export default function SignupsControls(props: {
  cup: Cup;
  getSignups: Refreshable<CupSignup[]>;
}): JSX.Element {
  const api = useApi();

  const isEditable = useAsync(async () => {
    return true;
    /*
    if (
      typeof props.cup.currentStage === "number" &&
      props.cup.currentStage > 0
    ) {
      log.info(
        "cannot edit signups since already at stage ",
        props.cup.currentStage
      );
      return false;
    }
    const stages = await api.cups.stages.byParentId(props.cup.id);
    if (stages.length > 0) {
      const stage0 = stages[0];
      if (stage0.isStarted) return false;
    }
    return true;
    */
  });

  const [state, setState] = createState<State>({
    seeds: {},
    extra: [],
    changes: {},
  });

  const update = () => {
    setState({ seeds: {}, extra: [], changes: {} });
    props.getSignups.update();
  };

  const getPlayers = useAsync(async () => {
    const signups = props.getSignups() ?? [];
    const players: Record<string, Player> = {};
    const playerIds = signups
      .map((s) => s.playerId)
      .filter((pid) => typeof pid === "string") as string[];
    for (const r of await api.players.byIds(playerIds)) {
      players[r.id] = r;
    }
    return players;
  });

  const getTeams = useAsync(async () => {
    const signups = props.getSignups();
    if (signups === undefined) return undefined;
    const teams: Record<string, Team> = {};
    const teamIds = signups
      .map((s) => s.teamId)
      .filter((tid) => typeof tid === "string") as string[];
    for (const r of await api.teams.byIds(teamIds)) {
      teams[r.id] = r;
    }
    return teams;
  });

  createEffect(async () => {
    const signups = props.getSignups();
    if (signups === undefined) return;
    const initialSeeds: Record<string, number> = {};
    for (const signup of signups) {
      if (typeof signup.seedValue === "number") {
        initialSeeds[signup.id] = signup.seedValue;
      }
    }
    setState({ seeds: initialSeeds });
  });

  async function onSaveChanges() {
    const visited: Record<string, true> = {};
    const setSeed: SetSeed[] = [];
    const { seeds, changes, extra } = state;
    for (const signupId in seeds) {
      const seedValue = state.seeds[signupId];
      const change = state.changes[signupId];
      visited[signupId] = true;
      setSeed.push({
        seedValue,
        signupId,
        drop: change === "drop",
        checkin:
          change === "checkin" || change === "checkout"
            ? change === "checkin"
            : undefined,
      });
    }

    for (const signupId in changes) {
      if (visited[signupId]) continue;
      const change = changes[signupId];
      setSeed.push({
        drop: change === "drop",
        checkin: change === "checkin",
        signupId,
      });
    }

    try {
      for (const add of extra) {
        const status = add.status;
        if (status === "drop") continue;
        await api.cupsCupAddSignup({
          id: props.cup.id,
          addSignup: {
            playerId: add.playerId,
            seedValue: add.seedValue,
            teamId: add.teamId,
            checkin: status === "checkin",
          },
        });
      }
      await api.cupsCupSetSeeds({ id: props.cup.id, setSeed });
    } catch (e) {
      failure(
        <>
          <p class="content">Cannot save</p>
          <pre>
            <code>{JSON.stringify(e, null, 2)}</code>
          </pre>
        </>
      );
    }
    update();
  }

  const SignupAdderRow = () => {
    const initialState = {
      seedValue: undefined,
      status: "signup",
      teamId: null,
      playerId: null,
    } as {
      seedValue: undefined | number;
      teamId: null | string;
      playerId: null | string;
      status: "signup" | "checkin";
    };
    const [another, setAnother] = createState({ ...initialState });
    return (
      <tr>
        <td>
          <input
            class="input is-small"
            type="number"
            placeholder="Seed"
            tabIndex="1"
            value={another.seedValue}
            onInput={debounce((ev) => {
              const value = Number.parseInt(ev.target.value);
              if (another.seedValue === value) return;
              setAnother("seedValue", another.seedValue);
            }, 50)}
          />
        </td>
        <td>
          <div class="buttons">
            <Dropdown
              title="Team"
              renderTrigger={(opt) =>
                typeof opt.key === "string" ? opt.title : "Team"
              }
              buttonClass="is-small"
              onChange={(option) => {
                const teamId = option.key;
                if (typeof teamId === "string" && teamId !== "_empty") {
                  setAnother({ teamId, playerId: null });
                }
              }}
              value={another.teamId ?? "_empty"}
              kind="search"
              options={async (pg: number, terms) => {
                const noTeamOpt = {
                  key: "_empty",
                  title: "No team",
                  onClick: () => setAnother("teamId", null),
                };
                if (terms === "") {
                  return [unpaged, [noTeamOpt]];
                }
                const [p, ts] = await api.teams.page(pg, terms);
                return [
                  p,
                  [
                    ...ts.map((t) => ({
                      key: t.id,
                      title: t.name,
                    })),
                    noTeamOpt,
                  ],
                ];
              }}
            />
            <Dropdown
              buttonClass="is-small"
              title="Player"
              onChange={(option) =>
                setAnother("playerId", option.key as string)
              }
              value={another.playerId}
              kind="search"
              textSearchRequireTerms
              options={(page, terms) =>
                api.players.page(page, terms).then((ps) => [
                  ps[0],
                  ps[1].map((p) => ({
                    key: p.id,
                    title: p.discordTag,
                  })),
                ])
              }
            />
          </div>
        </td>
        <td>
          <Dropdown
            buttonClass="is-small"
            title="Status"
            value={another.status}
            onChange={(opt) => {
              if (opt.key !== undefined) setAnother("status", opt.key);
            }}
            options={[
              { key: "signup", title: "Signed-up" },
              { key: "checkin", title: "Checked-in" },
            ]}
          />
        </td>
        <td>
          <button
            disabled={another.playerId === undefined}
            class="button is-small"
            onClick={async () => {
              const { playerId, teamId, seedValue, status } = another;
              if (typeof playerId !== "string") return;
              setState("extra", (es) => [
                ...es,
                {
                  status,
                  seedValue,
                  playerId,
                  teamId: teamId ?? undefined,
                },
              ]);
              setAnother({ ...initialState });
            }}
          >
            <Icon type="add" />
            <span>Add</span>
          </button>
        </td>
      </tr>
    );
  };

  return (
    <table class="table is-fullwidth">
      <colgroup>
        <col span="1" style="width:120px" />
        <col span="1" />
        <col span="1" style="width:250px" />
        <col span="1" style="width:250px" />
      </colgroup>
      <thead>
        <tr>
          <th>
            Seed <Icon type="arrow-down" />
          </th>
          <th>Participant</th>
          <th>Status</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        <props.getSignups.For>
          {(signup) => (
            <tr>
              <td>
                <input
                  disabled={
                    !isEditable() || state.changes[signup.id] === "drop"
                  }
                  class="input is-small"
                  type="number"
                  placeholder="Seed"
                  tabIndex="1"
                  value={state.seeds[signup.id] ?? ""}
                  onInput={debounce((ev) => {
                    const value = Number.parseInt(ev.target.value);
                    if (state.seeds[signup.id] === value) return;
                    setState("seeds", signup.id, value);
                  }, 50)}
                />
              </td>
              <td class="participant">
                <SignupView
                  signup={signup as CupSignup}
                  player={getPlayers()?.[signup.playerId] ?? undefined}
                  team={
                    typeof signup.teamId === "string"
                      ? getTeams()?.[signup.teamId] ?? undefined
                      : undefined
                  }
                  showCheckin={false}
                />
              </td>
              <td class="participant">
                <small>
                  {signup.checkedIn ? "Checked in" : "Signed up"}
                  {state.changes[signup.id] && <Icon type="arrow-forward" />}
                  <Case
                    key={state.changes[signup.id] ?? "none"}
                    options={{
                      checkin: () => "checked-in",
                      checkout: () => "signed up",
                      drop: () => "dropped",
                      none: () => undefined,
                    }}
                  />
                </small>
              </td>
              <td>
                <div class="buttons has-addons">
                  <button
                    disabled={!isEditable()}
                    class="button is-small"
                    classList={{
                      "is-dark": state.changes[signup.id] === "drop",
                      "is-selected": state.changes[signup.id] === "drop",
                    }}
                    onClick={() => {
                      setState("changes", signup.id, (cur) =>
                        cur === "drop" ? undefined : "drop"
                      );
                    }}
                  >
                    Drop
                  </button>

                  <button
                    disabled={!isEditable()}
                    class="button is-small"
                    classList={{
                      "is-dark": state.changes[signup.id] === "checkin",
                      "is-selected": state.changes[signup.id] === "checkin",
                    }}
                    onClick={() => {
                      setState("changes", signup.id, (cur) =>
                        cur === "checkin" ? undefined : "checkin"
                      );
                    }}
                  >
                    Check-in
                  </button>

                  <button
                    disabled={!isEditable()}
                    class="button is-small"
                    classList={{
                      "is-dark": state.changes[signup.id] === "checkout",
                      "is-selected": state.changes[signup.id] === "checkout",
                    }}
                    onClick={() => {
                      setState("changes", signup.id, (cur) =>
                        cur === "checkout" ? undefined : "checkout"
                      );
                    }}
                  >
                    Check-out
                  </button>

                  <button
                    disabled={!isEditable()}
                    class="button is-small"
                    onClick={() => {
                      setState("changes", signup.id, undefined);
                    }}
                  >
                    <Icon type="backspace" />
                  </button>
                </div>
              </td>
            </tr>
          )}
        </props.getSignups.For>
        <For each={state.extra}>
          {(extra, index) => (
            <tr>
              <td>
                <input
                  class="input is-small"
                  disabled
                  placeholder="Seed"
                  value={extra.seedValue ?? ""}
                />
              </td>
              <td>
                <PlayerLink playerId={extra.playerId} teamId={extra.teamId} />
              </td>
              <td>
                <small>
                  <Icon type="arrow-forward" />
                  <Case
                    key={extra.status}
                    options={{
                      checkin: () => "Checked in",
                      drop: () => "Dropped",
                      signup: () => "Signed up",
                    }}
                  />
                </small>
              </td>
              <td>
                <button
                  class="button is-small is-warning"
                  onClick={() => {
                    setState("extra", (es) => {
                      const n = [...es];
                      n.splice(index());
                      return n;
                    });
                  }}
                >
                  Drop
                </button>
              </td>
            </tr>
          )}
        </For>
        <SignupAdderRow />
      </tbody>
      <tfoot>
        <tr>
          <th>
            Seed <Icon type="arrow-down" />
          </th>
          <th>Participant</th>
          <th>Status</th>
          <th>
            <button
              class="button is-small is-link"
              onClick={onSaveChanges}
              disabled={!isEditable()}
            >
              Save changes
            </button>
          </th>
        </tr>
      </tfoot>
    </table>
  );
}
