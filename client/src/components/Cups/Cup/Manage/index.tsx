import { JSX } from "solid-js";
import { Cup, CupSignup } from "../../../../api";
import useApi from "../../../../context/Api";
import { Refreshable } from "../../../async/Refreshable";
import Resource from "../../../async/Resource";
import SignupsControls from "./SignupsControls";
import StagesControls from "./StagesControls";

export default function Manage(props: {
  cup: Cup;
  getSignups: Refreshable<CupSignup[]>;
}): JSX.Element {
  const api = useApi();
  return (
    <>
      <h2 class="heading">Signups</h2>
      <SignupsControls cup={props.cup} getSignups={props.getSignups} />

      <h2 class="heading">Stages</h2>
      <Resource data={() => api.cups.stages.byParentId(props.cup.id)}>
        {(stages) => <StagesControls cup={props.cup} stages={stages} />}
      </Resource>
    </>
  );
}
