import { cloneDeep, debounce } from "lodash";
import { createSignal, createState, untrack, unwrap } from "solid-js";
import { StructError } from "superstruct";
import Form from "../../../ui/Form";
import { useCupForm, useCupStageForm } from "./form";
import { createCupForm } from "./types";
import { parseCupCreateForm } from "./validate";
import ShowStructError from "../../../../superstruct/StructError";
import useApi from "../../../../context/Api";
import { navigate } from "../../../../router";
import { failure } from "../../../Toast";
import { CupCreateForm } from "../../../../api";

export default function () {
  const api = useApi();
  const [state, setState] = createState(createCupForm());
  const [validation, setValidation] = createSignal<
    StructError | CupCreateForm | undefined
  >(undefined);
  const cupForm = useCupForm(useCupStageForm(), true);

  const validate = debounce(
    () => setValidation(parseCupCreateForm(cloneDeep(state))),
    250,
    { leading: false, trailing: true }
  );

  untrack(() => {
    setValidation(parseCupCreateForm(unwrap(state)));
  });

  function onCreateCup() {
    const valid = validation();
    if (valid !== undefined && !(valid instanceof StructError)) {
      api.cups.create(valid).then(
        (cupId) => {
          navigate({ to: "cups.cup", params: { id: cupId } });
        },
        (err) => {
          failure(String(err));
        }
      );
    }
  }

  return (
    <>
      <Form
        state={[
          () => state,
          (...args: any[]) => {
            const r = (setState as any)(...args);
            setValidation(undefined);
            validate();
            return r;
          },
        ]}
        form={cupForm}
      />

      <hr />

      <button
        disabled={
          validation() === undefined || validation() instanceof StructError
        }
        class="button is-primary"
        onClick={onCreateCup}
      >
        Create
      </button>

      <ShowStructError value={validation()} />
    </>
  );
}
