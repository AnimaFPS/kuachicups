import { format } from "date-fns";
import {
  Cup,
  CupStage,
  CupStageFormat,
  CupStageScoring,
  GroupPointAward,
} from "../../../../api";

export interface CupForm {
  name: string;
  gameModeId: { active: undefined | string };
  ownerTeamId: { active: undefined | string };
  description: string;
  isPublished: boolean;
  isSignupsClosed: boolean;
  stages: CupStageForm[];
}

export interface CupStageForm {
  id?: string;
  index: number;
  title: string;
  // gameMatchControlId: { active: undefined | string },
  startImmediately: boolean;
  starttime: [undefined | Date, { date: string; time: string }];
  checkinduration: number | string;
  maxParticipants: number | string;
  format: { active: undefined | CupStageFormat };
  groupSize: string | number;
  groupPointAward: { active: undefined | GroupPointAward };
  //  scoringGroup: StageScoringForm;
  scorings: CupStageScoringForm[];
}

export interface CupStageScoringForm {
  bestof: number | string;
  elimRound: number | string;
  isLb: undefined | boolean;
  groups: boolean;
  isPointScore: boolean;
  pointScoreGreatestIsWinner: boolean;
  isTimeScore: boolean;
  isTimeScoreRace: boolean;
}

export function createCupForm(h?: {
  cup: Cup;
  stages: CupStage[];
  scorings: { [stageId: string]: CupStageScoring[] };
}): CupForm {
  return {
    gameModeId: { active: h?.cup.gameModeId },
    name: h?.cup.title ?? "",
    description: h?.cup.descriptionMd ?? "",
    ownerTeamId: { active: h?.cup.ownerTeamId ?? undefined },
    isSignupsClosed: h?.cup.isSignupsClosed ?? false,
    isPublished: h?.cup.isPublished ?? false,
    stages:
      h?.stages.map(
        (s, index): CupStageForm => ({
          id: s.id,
          index,
          title: s.title,
          format: { active: s.format },
          groupSize: s.groupSize ?? "",
          groupPointAward: { active: s.groupPointAward },
          starttime: [
            s.starttime ?? undefined,
            {
              date:
                s.starttime instanceof Date
                  ? format(s.starttime, "yyyy-MM-dd")
                  : "",
              time:
                s.starttime instanceof Date ? format(s.starttime, "HH:mm") : "",
            },
          ],
          checkinduration:
            s.starttime instanceof Date && s.checkintime instanceof Date
              ? (s.starttime.getTime() - s.checkintime.getTime()) / 60000
              : "",
          maxParticipants: s.maxParticipants ?? "",
          startImmediately: s.startImmediately,
          scorings: (h?.scorings?.[s.id] ?? []).map(
            (sc): CupStageScoringForm => ({
              bestof: sc.bestof,
              elimRound: sc.elimRound ?? "",
              isLb: sc.isLb ?? undefined,
              groups: sc.groups,
              isTimeScore: sc.isTimeScore ?? false,
              isTimeScoreRace: sc.isTimeScoreRace ?? false,
              isPointScore: sc.isPointScore ?? false,
              pointScoreGreatestIsWinner:
                sc.pointScoreGreatestIsWinner ?? false,
            })
          ),
        })
      ) ?? [],
  };
}

export function createCupStageForm(index: number): CupStageForm {
  return {
    index,
    title: `Stage ${index + 1}`,
    starttime: [undefined, { time: "", date: "" }],
    startImmediately: index > 0,
    // gameMatchControlId: { active: last?.gameModeId?.active??undefined },
    maxParticipants: "",
    groupSize: "",
    checkinduration: "",
    groupPointAward: { active: GroupPointAward.Winloss },
    format: { active: undefined },
    scorings: [],
  };
}

export function createCupStageScoringForm(_index: number): CupStageScoringForm {
  return {
    bestof: 3,
    groups: false,
    elimRound: 0,
    isLb: false,
    isTimeScore: false,
    isTimeScoreRace: false,
    isPointScore: true,
    pointScoreGreatestIsWinner: true,
  };
}
