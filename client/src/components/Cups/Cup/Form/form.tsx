import { CupStageFormat, GroupPointAward } from "../../../../api";
import useApi from "../../../../context/Api";
import { useTeamsOfAuth } from "../../../Teams/MyTeams";
import * as Dropdown from "../../../ui/Dropdown";
import {
  checkbox,
  datetime,
  duration,
  MarkdownField,
  number,
  pipe,
  select,
  showif,
  SomeFieldConstr,
  subform,
  text,
  visual,
} from "../../../ui/Form";
import {
  createCupStageForm,
  createCupStageScoringForm,
  CupForm,
  CupStageForm,
  CupStageScoringForm,
} from "./types";

export function useCupForm(
  stageForm: SomeFieldConstr<CupStageForm>[],
  allowAddRemove: boolean
): SomeFieldConstr<CupForm>[] {
  const api = useApi();

  const getMyTeams = (() => {
    const teams = useTeamsOfAuth();
    return async () => Dropdown.simpleItems(await teams());
  })();

  return [
    select(["ownerTeamId"], {
      label: "Owner",
      title: "Select owning team",
      description: "Must be admins",
      render: (opt) => (typeof opt === "string" ? opt : opt.title),
      options: getMyTeams,
    }),

    text(["name"], {
      label: "Name",
    }),

    select(["gameModeId"], {
      label: "Game",
      title: "Select a game",
      render: (opt) => () =>
        typeof opt === "string" ? (
          opt
        ) : opt.isActive ? (
          <>
            {opt.extra.game.name} / {opt.extra.mode.name}
          </>
        ) : (
          opt.title
        ),
      options: api.games.modes.dropdownOptions,
    }),

    visual((state, setState) => (
      <MarkdownField
        label="Description"
        get={() => state.description}
        set={(d: string) => setState("description", d)}
      />
    )),

    checkbox(["isPublished"], {
      label: "Publish immediately",
    }),

    checkbox(["isSignupsClosed"], {
      label: "Closed signups",
      description: "Use this to create invite-only cups.",
    }),

    subform(["stages"], {
      create: createCupStageForm,
      label: "stage",
      form: stageForm,
      allowAddRemove,
    }),
  ];
}

export function useCupStageForm(): SomeFieldConstr<CupStageForm>[] {
  return [
    text(["title"], { label: "Title" }),

    showif((s) => s.index > 0, [
      checkbox(["startImmediately"], {
        label: "Start immediately",
        description:
          "Used to start a non-start stage immediately after the preceeding one",
      }),
    ]),

    showif((s) => s.index === 0 || !s.startImmediately, [
      datetime(["starttime"], {
        label: "Start time",
      }),
      duration(["checkinduration"], {
        label: "Check-in duration (minutes)",
        precision: "m",
      }),
    ]),

    number(["maxParticipants"], {
      label: "Max participants",
      description:
        'OPTIONAL. If this is a later stage, this value is used as a cut-off, e.g. for "top 8"',
    }),

    select(["format"], {
      label: "Format",
      title: "Format",
      render: (opt) => () => (typeof opt === "string" ? opt : opt.title),
      options: [
        { title: "Groups", key: CupStageFormat.Groups },
        {
          title: "Single elimination",
          key: CupStageFormat.SingleElim,
        },
        {
          title: "Double elimination",
          key: CupStageFormat.DoubleElim,
        },
        { title: "Ladder", key: CupStageFormat.Ladder },
      ],
    }),

    showif(
      (stage, setStage) => {
        const isGroups = stage.format.active === CupStageFormat.Groups;
        const populated0 =
          stage.scorings[0] !== undefined && stage.scorings[0].groups === true;
        if (isGroups && !populated0) {
          setStage("scorings", 0, {
            elimRound: undefined,
            isLb: undefined,
            bestof: 3,
            groups: true,
            isTimeScore: false,
            isTimeScoreRace: false,
            isPointScore: true,
            pointScoreGreatestIsWinner: true,
          });
        }
        return isGroups && populated0;
      },
      [
        number(["groupSize"], {
          label: "Group size",
          description:
            "If left blank, a group size will be computed automatically",
          min: 2,
        }),

        select(["groupPointAward"], {
          label: "Group points type",
          title: "Group points type",
          render: (opt) => () => (typeof opt === "string" ? opt : opt.title),
          options: [
            { title: "Win/loss", key: GroupPointAward.Winloss },
            { title: "Match scores", key: GroupPointAward.ScoreOnly },
          ],
        }),

        number(["scorings", 0, "bestof"], {
          label: "Best of",
          min: 1,
        }),
      ]
    ),

    showif(
      (stage) =>
        stage.format.active === CupStageFormat.SingleElim ||
        stage.format.active === CupStageFormat.DoubleElim,
      [
        pipe((getStage) => [
          subform(["scorings"], {
            create: createCupStageScoringForm,
            label: "scoring configuration",
            allowAddRemove: true,
            description: (
              <div class="content">
                <p>
                  <small>
                    At least one scoring configuration must be added.
                  </small>
                </p>
                <p>
                  <small>
                    Note that for elimination rounds, there must be at least one
                    scoring configuration starting at round 0. For double
                    elimination, there must additionally be at least one scoring
                    configuration starting at round 0 and with the lower bracket
                    checked.
                  </small>
                </p>
              </div>
            ),
            form: [
              visual((_s: CupStageScoringForm) => (
                <h2 class="heading">Round score configuration</h2>
              )),
              number(["elimRound"], {
                label: "Round",
                description: (
                  <p>
                    The configuration here applies to all rounds including and
                    later than the round number given here, starting at zero,
                    unless overridden by another configuration starting at a
                    later round.
                  </p>
                ),
                min: 0,
                step: 1,
              }),
              showif(
                () => getStage().format.active === CupStageFormat.DoubleElim,
                [
                  checkbox(["isLb"], {
                    label: "Lower bracket",
                  }),
                ]
              ),
              number(["bestof"], {
                label: "Best of",
                min: 1,
              }),
            ],
          }),
        ]),
      ]
    ),
  ];
}
