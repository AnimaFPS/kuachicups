import {
  object,
  string,
  refine,
  boolean,
  size,
  array,
  date,
  enums,
  optional,
  intersection,
  StructError,
  defaulted,
  trimmed,
  create,
  masked,
} from "superstruct";
import {
  CupCreateForm,
  CupStageFormat,
  CupStageUpdateForm,
  CupUpdateForm,
  GroupPointAward,
} from "../../../../api";
import { sub } from "date-fns";
import { CupForm, CupStageForm } from "./types";
import { Uuid } from "../../../../superstruct/uuid";
import { Nat, Odd, Pos } from "../../../../superstruct/numbers";
import {
  datetime,
  optionalS,
  selected,
} from "../../../../superstruct/form_util";

function prepareStageForm(s: CupStageForm) {
  return {
    ...s,
    index: undefined,
    checkinduration: undefined,
    checkintime: s.startImmediately
      ? undefined
      : sub(s.starttime[0] as Date, {
          minutes: s.checkinduration as number,
        }),
  };
}

/*
 * Validating/parsing a [[CupForm]] (the UI state) into a [[CupCreateForm]]
 * (what the backend wants)
 */

export function parseCupCreateForm(form: CupForm): StructError | CupCreateForm {
  try {
    return create(
      {
        ...form,
        stages: form.stages.map(prepareStageForm),
      },
      cupCreateForm
    );
  } catch (e) {
    return e;
  }
}

const cupStageScoringForm = masked(
  object({
    bestof: intersection([Pos, Odd]),
    groups: boolean(),
    elimRound: optionalS(Nat),
    isLb: optional(boolean()),
    isPointScore: optional(boolean()),
    isTimeScore: optional(boolean()),
    isTimeScoreRace: optional(boolean()),
    pointScoreGreatestIsWinner: optional(boolean()),
  })
);

const cupStageCreateForm = refine(
  masked(
    object({
      id: optional(Uuid), // actually only used for updates, yolo
      // superstruct seems to break coercians with assign/intersection so that
      // is why
      format: selected(
        enums([
          CupStageFormat.Groups,
          CupStageFormat.SingleElim,
          CupStageFormat.DoubleElim,
        ])
      ),
      groupPointAward: selected(
        defaulted(
          enums([GroupPointAward.Winloss, GroupPointAward.ScoreOnly]),
          GroupPointAward.Winloss
        )
      ),
      groupSize: optionalS(Pos),
      maxParticipants: optionalS(Pos),
      scorings: size(array(cupStageScoringForm), 1, 10),
      startImmediately: boolean(),
      starttime: datetime(optional(date())),
      checkintime: datetime(optional(date())),
      title: size(trimmed(string()), 1, 80),
    })
  ),
  "CupStageCreateForm.scorings.size",
  (s) => {
    switch (s.format) {
      case CupStageFormat.DoubleElim:
        return s.scorings.length > 1;
    }
    return true;
  }
);

const cupCreateForm = masked(
  object({
    description: size(trimmed(string()), 1, 2000),
    gameModeId: selected(Uuid),
    isPublished: boolean(),
    isSignupsClosed: boolean(),
    name: size(trimmed(string()), 1, 80),
    ownerTeamId: selected(Uuid),
    stages: size(array(cupStageCreateForm), 1, 100),
  })
);

/**
 * Validating cup updates
 */

export function parseCupUpdateForm(form: CupForm): StructError | CupUpdateForm {
  try {
    return create(form, cupUpdateForm);
  } catch (e) {
    return e;
  }
}

const cupUpdateForm = masked(
  object({
    description: size(trimmed(string()), 1, 2000),
    gameModeId: selected(Uuid),
    isPublished: boolean(),
    isSignupsClosed: boolean(),
    name: size(trimmed(string()), 1, 80),
    ownerTeamId: selected(Uuid),
  })
);

export function parseCupStageUpdateForm(
  form: CupForm
): StructError | (CupStageUpdateForm & { id: string })[] {
  try {
    return (create(
      {
        ...form,
        stages: form.stages.map(prepareStageForm),
      },
      cupStageUpdateForm
    ).stages as (CupStageUpdateForm & {
      id?: undefined | string;
    })[]) as (CupStageUpdateForm & { id: string })[];
  } catch (e) {
    return e;
  }
}

const cupStageUpdateForm = masked(
  object({
    stages: size(array(cupStageCreateForm), 1, 100),
  })
);
