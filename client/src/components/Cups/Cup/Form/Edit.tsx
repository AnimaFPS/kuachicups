import { cloneDeep, debounce, isEqual } from "lodash";
import { createComponent, createMemo, createState, untrack } from "solid-js";
import { StructError } from "superstruct";
import {
  Cup,
  CupStage,
  CupStageScoring,
  CupStageUpdateForm,
  CupUpdateForm,
} from "../../../../api";
import useApi from "../../../../context/Api";
import { navigate } from "../../../../router";
import ShowStructError from "../../../../superstruct/StructError";
import { useAsync } from "../../../async/Awaitable";
import Form from "../../../ui/Form";
import { useCupStore } from "../Store";
import { useCupForm, useCupStageForm } from "./form";
import { createCupForm } from "./types";
import { parseCupStageUpdateForm, parseCupUpdateForm } from "./validate";

export default function HydrateEditForm(props: { cup: Cup }) {
  const api = useApi();
  const data = useAsync(async () => {
    const stages = await api.cups.stages.byParentId(props.cup.id, true);
    const pp = [];
    const scorings: { [stageId: string]: CupStageScoring[] } = {};
    for (const stage of stages) {
      pp.push(
        api
          .cupsCupStageScorings({ cupId: props.cup.id, stageId: stage.id })
          .then((these) => {
            scorings[stage.id] = these;
          })
      );
    }
    await Promise.all(pp);
    return { cup: props.cup, stages, scorings };
  });
  return <data.Await>{(p) => createComponent(EditForm, p)}</data.Await>;
}

function EditForm(props: {
  cup: Cup;
  stages: CupStage[];
  scorings: { [stageId: string]: CupStageScoring[] };
}) {
  const api = useApi();
  const store = useCupStore();
  const initialState = createCupForm(props);
  const [state, setState] = createState(cloneDeep(initialState));

  const [validation, setValidation] = createState({
    cup: undefined as undefined | CupUpdateForm | StructError,
    stages: undefined as
      | undefined
      | (CupStageUpdateForm & { id: string })[]
      | StructError,
    changed: false as boolean,
  });

  const validationOk = createMemo(
    () =>
      validation.cup !== undefined &&
      !(validation.cup instanceof StructError) &&
      validation.stages !== undefined &&
      !(validation.stages instanceof StructError)
  );

  const cupForm = useCupForm(useCupStageForm(), false);

  const validate = debounce(
    () => {
      const clone = cloneDeep(state);
      const result = {
        cup: parseCupUpdateForm(clone),
        stages: parseCupStageUpdateForm(clone),
        changed: !isEqual(clone, initialState),
      };
      setValidation(result);
      return result;
    },
    250,
    { leading: false, trailing: true }
  );

  untrack(() => {
    const clone = cloneDeep(state);
    setValidation({
      cup: parseCupUpdateForm(clone),
      stages: parseCupStageUpdateForm(clone),
    });
  });

  async function onUpdateCup() {
    const { cup, stages } = validation;
    if (cup === undefined || cup instanceof StructError) return;
    if (stages === undefined || stages instanceof StructError) return;
    await api.cupsCupUpdate({
      id: props.cup.id,
      cupUpdateForm: cup,
    });
    await Promise.all(
      stages.map((stage) =>
        api.cupsCupStageUpdate({
          stageId: stage.id,
          cupStageUpdateForm: stage,
        })
      )
    );
    store.cup.update();
    store.stage.update();
    store.stages.update();
    navigate({ to: "cups.cup", params: { id: props.cup.id } });
  }

  /*
  function onDeleteCup() {
    api.cupsCupDelete({ id: props.cup.id }).then(() => {
      delete api.cups.cache[props.cup.id];
      delete api.cups.stages.parentCache[props.cup.id];
      navigate({ to: "cups", params: { page: "0", active: "true" } });
    });
  }
  function onResetCup() {
    api.cupsCupReset({ id: props.cup.id }).then(() => {
      delete api.cups.cache[props.cup.id];
      delete api.cups.stages.parentCache[props.cup.id];
      navigate({ to: "cups.cup", params: { id: props.cup.id } });
    });
  }
  */

  return (
    <>
      <Form
        state={[
          () => state,
          (...args: any[]) => {
            const r = (setState as any)(...args);
            setValidation({ cup: undefined, stages: undefined });
            validate();
            return r;
          },
        ]}
        form={cupForm}
      />

      <hr />

      <button
        disabled={!validationOk() || !validation.changed}
        class="button is-primary"
        onClick={onUpdateCup}
      >
        Update
      </button>

      <ShowStructError value={validation.cup} />
      <br />
      <ShowStructError value={validation.stages} />
    </>
  );
}
