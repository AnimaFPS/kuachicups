import log from "loglevel";
import { JSX } from "solid-js";
import { Cup } from "../../../api";
import { login } from "../../../api/oauth2";
import useApi from "../../../context/Api";
import useAuth, { LoginState } from "../../../context/Auth";
import { Link, useRoute } from "../../../router";
import { createRefreshable, Refreshable } from "../../async/Refreshable";
import { CaseOf } from "../../Case";
import { Countdown } from "../../Time";
import * as Dropdown from "../../ui/Dropdown";
import Icon from "../../ui/Icon";

export interface Props {
  cup: Cup;
}

type CannotSignupReason = "closed" | "missed" | "finished";

export type State =
  | { tag: "canSignup"; mustCheckinOnSignup: boolean; requiresTeam: boolean }
  | { tag: "signedUp"; checkin: "checkedIn"; begun: boolean }
  | { tag: "signedUp"; checkin: "cannotCheckinTooEarly"; checkintime: Date }
  | { tag: "signedUp"; checkin: "canCheckin" | "cannotCheckinTooLate" }
  | { tag: "cannotSignup"; reason: CannotSignupReason }
  | { tag: "noStages" }
  | { tag: "loading" };

function CanSignup(props: {
  requiresTeam: boolean;
  mustCheckinOnSignup: boolean;
  onSignup: (teamId?: string) => void;
  onSignupTeam: (teamId: string) => void;
}): JSX.Element {
  const api = useApi();
  const getAuth = useAuth();
  const getRoute = useRoute();
  return (
    <div class="level is-mobile is-marginless">
      <span>Signup available&nbsp;</span>
      <CaseOf data={getAuth()} key="state">
        {{
          [LoginState.LoggedIn]: (auth) => (
            <Dropdown.default
              isRight
              class="is-link"
              title="Join"
              renderTrigger={() => "Join"}
              kind="fetch"
              options={async () => {
                const requiresTeam = props.requiresTeam;
                const roles = await api.players.teamRoles.byParentId(
                  auth.login.id
                );
                const teams = await api.teams.byIds(roles.map((r) => r.teamId));
                const options: Dropdown.Options<string> = [];
                if (!requiresTeam)
                  options.push({
                    title: () => (
                      <>
                        Signup as <b>{auth.login.discordUsername}</b>
                      </>
                    ),
                    isActive: false,
                    onClick: () => props.onSignup(),
                  });
                for (const team of teams) {
                  options.push({
                    title: () => (
                      <>
                        Signup under <b>{team.name}</b>
                      </>
                    ),
                    isActive: false,
                    onClick: requiresTeam
                      ? () => props.onSignupTeam(team.id)
                      : () => props.onSignup(team.id),
                  });
                }
                return [Dropdown.unpaged, options];
              }}
            />
          ),
          [LoginState.NotLoggedIn]: () => (
            <button
              class="button is-light is-link"
              onClick={() => login(getRoute())}
            >
              Login to signup
            </button>
          ),
        }}
      </CaseOf>
    </div>
  );
}

const PanelButton = (props: {
  label: JSX.Element;
  action: JSX.Element;
  onClick: () => void;
}) => (
  <div class="panel-block">
    <div class="level is-w100 is-flex">
      <div class="level-left">
        <div class="level-item">{props.label}</div>
      </div>
      <div class="level-right my-0">
        <div class="level-item">
          <button class="button is-danger" onClick={props.onClick}>
            {props.action}
          </button>
        </div>
      </div>
    </div>
  </div>
);

function SignedUp(props: {
  cup: Cup;
  state: State & { tag: "signedUp" };
  onCheckout: () => void;
  onCheckin: () => void;
  onLeaveCup: () => void;
}): JSX.Element {
  function CheckedInBegun() {
    return (
      <>
        <div class="panel-block">Good luck!</div>
        {typeof props.cup.currentStage === "number" && (
          <div class="panel-block">
            <Link
              to="cups.cup.stage"
              params={{
                id: props.cup.id,
                stageNo: props.cup.currentStage + "",
              }}
            >
              <Icon type="arrow-forward" /> Current stage & match control
            </Link>
          </div>
        )}
      </>
    );
  }

  return (
    <nav class="panel is-marginless">
      <CaseOf data={props.state} key="checkin">
        {{
          checkedIn: (s) => () =>
            s.begun === true ? (
              <CheckedInBegun />
            ) : (
              <PanelButton
                label="Checked in!"
                action="Check-out"
                onClick={props.onCheckout}
              />
            ),

          canCheckin: () => (
            <PanelButton
              label="Checkin to compete!"
              action="Checkin"
              onClick={props.onCheckin}
            />
          ),

          cannotCheckinTooEarly: (s) => (
            <>
              <div class="panel-block">
                Checkin available&nbsp;
                <Countdown tick="s" target={s.checkintime} />
              </div>
              <PanelButton
                label="Signed up"
                action="Leave"
                onClick={props.onLeaveCup}
              />
            </>
          ),

          cannotCheckinTooLate: () => (
            <div class="panel-block">Missed checkin</div>
          ),
        }}
      </CaseOf>
    </nav>
  );
}

function CannotSignup(props: { reason: CannotSignupReason }): JSX.Element {
  const message: Record<CannotSignupReason, string> = {
    closed: "Cannot signup",
    missed: "Missed signup period",
    finished: "Cup is finished",
  };
  return <div>{message[props.reason]}</div>;
}

export default function SignupButton(props: Props): JSX.Element {
  const api = useApi();
  const getState = createState(props);

  function handler<T, Args extends any[]>(
    up: (...args: Args) => Promise<T>
  ): (...args: Args) => Promise<void> {
    return (...args) =>
      up(...args).then(() => {
        getState.update();
      });
  }

  const onCheckin = handler(() => api.cupsCupCheckin({ cupId: props.cup.id }));
  const onCheckout = handler(() =>
    api.cupsCupCheckout({ cupId: props.cup.id })
  );
  const onLeaveCup = handler(() =>
    api.cupsCupSignupLeave({ cupId: props.cup.id })
  );

  const onSignup = handler(async (teamId?: string) => {
    const state = getState();
    if (state === undefined) return;
    if (state.tag === "signedUp") {
      log.error("tried to solo signup but already signed up");
      return;
    }
    if (state.tag === "cannotSignup") {
      log.error("tried to solo signup but cannot sign up");
      return;
    }
    if (state.tag === "canSignup") {
      await (state.requiresTeam
        ? api.cupsCupSignupTeam({ cupId: props.cup.id, teamId })
        : api.cupsCupSignupSolo({ cupId: props.cup.id, teamId }));
      if (state.mustCheckinOnSignup) await onCheckin();
    }
  });

  return (
    <CaseOf
      data={getState() ?? ({ tag: "loading" } as State)}
      key="tag"
      options={{
        canSignup: (s) => (
          <CanSignup
            mustCheckinOnSignup={s.mustCheckinOnSignup}
            requiresTeam={s.requiresTeam}
            onSignupTeam={onSignup}
            onSignup={onSignup}
          />
        ),
        cannotSignup: (s) => <CannotSignup reason={s.reason} />,
        signedUp: (s) => (
          <SignedUp
            cup={props.cup}
            state={s}
            onCheckin={onCheckin}
            onCheckout={onCheckout}
            onLeaveCup={onLeaveCup}
          />
        ),
      }}
    />
  );
}

function createState(props: { cup: Cup }): Refreshable<State> {
  const api = useApi();
  const getAuth = useAuth();
  async function getMyCupSignup(refresh?: boolean) {
    const auth = await getAuth().then;
    const signups = await api.cups.signups.byParentId(props.cup.id, refresh);
    for (const signup of signups) {
      if (signup.playerId === auth.login!.id) return signup;
    }
    return undefined;
  }

  async function getRequiresTeamSignup(refresh?: boolean) {
    const mode = await api.games.modes.byId(props.cup.gameModeId, refresh);
    return mode.entrantType === "team";
  }

  async function getFirstStage(refresh?: boolean) {
    return api.cups.stages.byId(props.cup.id, 0, refresh);
  }

  return createRefreshable(
    async (refresh?: boolean): Promise<State> => {
      if (props.cup.isFinished) {
        return { tag: "cannotSignup", reason: "finished" };
      }
      const now = new Date();
      const mySignup = await getMyCupSignup(refresh);
      const first = await getFirstStage(refresh);
      if (first === undefined) return { tag: "noStages" };
      if (first.starttime == null || first.checkintime == null) {
        return { tag: "cannotSignup", reason: "closed" };
      }
      const begun = now >= first.starttime;
      const checkinBegun = now >= (first.checkintime as Date);
      // not signed up
      if (mySignup === undefined) {
        // can sign up
        if (!begun) {
          return {
            tag: "canSignup",
            mustCheckinOnSignup: checkinBegun,
            requiresTeam: await getRequiresTeamSignup(refresh),
          };
        }
        // too late
        return { tag: "cannotSignup", reason: "missed" };
      }
      // if we have a signup and signups are closed, then we were one of the
      // invited players, and we can still manage our signup
      if (props.cup.isSignupsClosed && mySignup === undefined) {
        return { tag: "cannotSignup", reason: "closed" };
      }
      // allow checking out? nah that defeats the purpose
      // have a "withdraw" button maybe
      if (mySignup.checkedIn) {
        return { tag: "signedUp", checkin: "checkedIn", begun };
      }
      if (!begun) {
        if (checkinBegun) {
          return { tag: "signedUp", checkin: "canCheckin" };
        }
        return {
          tag: "signedUp",
          checkin: "cannotCheckinTooEarly",
          checkintime: first.checkintime,
        };
      }
      return { tag: "signedUp", checkin: "cannotCheckinTooLate" };
    }
  );
}
