import { format } from "date-fns";
import log from "loglevel";
import { JSX, Match, Show, Switch } from "solid-js";
import { CupMatch2p, CupStage, CupStageFormat } from "../../../../api";
import useApi from "../../../../context/Api";
import { Link } from "../../../../router";
import { createRefreshable, RefreshableI } from "../../../async/Refreshable";
import { Countdown } from "../../../Time";
import Standings from "../Standings";
import { useCupStore } from "../Store";
import ElimMatches from "./ElimMatches";
import GroupMatches from "./GroupMatches";
import MatchControl from "./MatchControl";

export const stageFormats: Record<CupStageFormat, JSX.Element> = {
  [CupStageFormat.Groups]: "Groups",
  [CupStageFormat.DoubleElim]: "Double Elim",
  [CupStageFormat.SingleElim]: "Single Elim",
  [CupStageFormat.Ladder]: "Ladder",
};

export default function Stage(p: { stage: CupStage }) {
  log.info("Stage", p.stage);
  const api = useApi();

  const matches = createRefreshable(
    (ref) => api.cups.stages.matches.byParentId(p.stage.id, ref),
    []
  );

  matches.startInterval(20);

  function onClickMatch() {
    // TODO make this add an extra match to the match control thing
  }

  return (
    <>
      <h2 class="title is-4">
        <Link
          to="cups.cup.stage"
          params={{ id: p.stage.cupId, stageNo: p.stage.stageNo + "" }}
        >
          {p.stage.title} - {stageFormats[p.stage.format]}
        </Link>
      </h2>
      {p.stage.isStarted ? (
        <>
          <StageMatches
            matches={matches}
            stage={p.stage}
            onClickMatch={onClickMatch}
          />
          <div class="pending-match-area">
            <h2 class="heading mt-5">Pending matches</h2>
            <CupStagePanel matches={matches} />
          </div>
          <h2 class="heading mt-5">Standings</h2>
          <Standings />
        </>
      ) : (
        <StageTime stage={p.stage} />
      )}
    </>
  );
}

function CupStagePanel(p: { matches: RefreshableI<CupMatch2p[]> }) {
  const get = useCupStore();
  return (
    <get.stage.Await>
      {(stage) => <MatchControl matches={p.matches} stage={stage} />}
    </get.stage.Await>
  );
}

function StageMatches(p: {
  stage: CupStage;
  matches: RefreshableI<CupMatch2p[]>;
  onClickMatch: (id: number) => void;
}) {
  return (
    <Show when={p.matches()?.length! > 0}>
      <Switch>
        <Match when={p.stage.format === CupStageFormat.Groups}>
          <GroupMatches matches={p.matches} stage={p.stage} />
        </Match>
        <Match
          when={
            p.stage.format === CupStageFormat.DoubleElim ||
            p.stage.format === CupStageFormat.SingleElim
          }
        >
          <ElimMatches
            format={p.stage.format as any}
            matches={p.matches}
            stage={p.stage}
            onClickMatch={p.onClickMatch}
          />
        </Match>
      </Switch>
    </Show>
  );
}

export function StageTime(props: { stage: CupStage }) {
  return () => {
    const { startImmediately, starttime, checkintime } = props.stage;
    if (startImmediately) {
      return (
        <dl>
          <dt>Starting time</dt>
          <dd>Immediately following previous stage</dd>
        </dl>
      );
    }
    if (starttime == null || checkintime == null) return undefined;
    const start = new Date(starttime);
    const checkin = new Date(checkintime);
    return (
      <dl>
        <dt>Starting time</dt>
        <dd>
          {format(start, "PPPPpppp")} (<Countdown tick="s" target={start} />)
        </dd>
        <dt>Check-in time</dt>
        <dd>
          {format(checkin, "PPPPpppp")} (<Countdown tick="s" target={checkin} />
          )
        </dd>
      </dl>
    );
  };
}

export function StageList(props: { cupId: string }) {
  const get = useCupStore();
  return (
    <get.stages.For>
      {(stage: CupStage) => (
        <div class="box">
          <h2 class="subtitle">
            <Link
              to="cups.cup.stage"
              params={{ id: props.cupId, stageNo: stage.stageNo + "" }}
            >
              {stage.title} - {stageFormats[stage.format]}
            </Link>
          </h2>
          <StageTime stage={stage} />
        </div>
      )}
    </get.stages.For>
  );
}
