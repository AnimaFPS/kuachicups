import { createMemo, For, Show } from "solid-js";
import { CupMatch2p, CupStage } from "../../../../api";
import { RefreshableI } from "../../../async/Refreshable";
import { SignupLoader } from "../CupSignup";
import { MatchName } from "./MatchName";
import ScoreCell from "./ScoreCell";

export interface Props {
  matches: RefreshableI<CupMatch2p[]>;
  stage: CupStage;
}

export default function GroupStageMatches(props: Props) {
  const p = createMemo(() => {
    const groupsByNo: { [k: number]: CupMatch2p[] } = {};
    let numGroups = 1;
    for (const m of props.matches()) {
      const groupNo = m.groupNo;
      if (typeof groupNo === "number") {
        const groupMatches = groupsByNo[groupNo] ?? (groupsByNo[groupNo] = []);
        numGroups = Math.max(groupNo, numGroups);
        groupMatches.push(m);
      }
    }
    const groups: CupMatch2p[][] = Object.assign([], groupsByNo);
    for (const g of groups) {
      g.sort((a, b) => a.lid - b.lid);
    }
    return groups;
  });

  return (
    <div class="cup-groups">
      <For each={p()}>
        {(group, groupNo) => (
          <div class="box has-background-light is-paddingless cup-group">
            <table class="table is-bordered cup-groups-table">
              <thead>
                <tr>
                  <th class="cup-groups-title" colSpan={3}>
                    Group {groupNo() + 1}
                  </th>
                </tr>
              </thead>
              <tbody>
                <For each={group}>
                  {(m: CupMatch2p) => (
                    <tr class="cup-group-match">
                      <td>
                        <MatchName match={m} />
                      </td>
                      <td
                        classList={{
                          "cup-group-match-win": m.winnerId === m.highId,
                          "cup-group-match-loss": m.loserId === m.highId,
                        }}
                      >
                        <div class="cup-group-signup">
                          <SignupLoader
                            cupId={props.stage.cupId}
                            signup={m.highId ?? undefined}
                          />
                          <Show when={m.isScored}>
                            <span>
                              <ScoreCell
                                self="high"
                                report={m.lowReport ?? []}
                              />
                            </span>
                          </Show>
                        </div>
                      </td>
                      <td
                        classList={{
                          "cup-group-match-win": m.winnerId === m.lowId,
                          "cup-group-match-loss": m.loserId === m.lowId,
                        }}
                      >
                        <div class="cup-group-signup">
                          <SignupLoader
                            cupId={props.stage.cupId}
                            signup={m.lowId ?? undefined}
                          />
                          <Show when={m.isScored}>
                            <span>
                              <ScoreCell
                                self="low"
                                report={m.lowReport ?? []}
                              />
                            </span>
                          </Show>
                        </div>
                      </td>
                    </tr>
                  )}
                </For>
              </tbody>
            </table>
          </div>
        )}
      </For>
    </div>
  );
}
