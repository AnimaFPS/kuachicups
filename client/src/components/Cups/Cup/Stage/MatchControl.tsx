import { isLeft, isRight } from "fp-ts/lib/Either";
import * as t from "io-ts";
import { failure } from "io-ts/lib/PathReporter";
import { cloneDeep } from "lodash";
import log from "loglevel";
import {
  createEffect,
  createMemo,
  createState,
  For,
  produce,
  Show,
  untrack,
} from "solid-js";
import {
  CupSignup,
  CupStage,
  CupStageScoring,
  CupMatch2p,
  ScoreReport,
} from "../../../../api";
import useApi from "../../../../context/Api";
import useAuth from "../../../../context/Auth";
import { useAsync } from "../../../async/Awaitable";
import { createRefreshable, RefreshableI } from "../../../async/Refreshable";
import { EnumFromTo } from "../../../Case";
import { failure as reportError } from "../../../Toast";
import Keyed from "../../../ui/Keyed";
import { SignupLoader } from "../CupSignup";
import { MatchName } from "./MatchName";

export interface Props {
  stage: CupStage;
  matches: RefreshableI<CupMatch2p[]>;
}

export default function (props: Props) {
  const api = useApi();
  const getAuth = useAuth();

  const getPendingMatches = createRefreshable(async () => {
    const auth = getAuth();
    const self = auth.login?.id;
    const admin = auth.admin;
    if (typeof self !== "string") {
      return [];
    }
    const signups = await api.cupsCupSignupsSelf({ cupId: props.stage.cupId });
    const matches = admin
      ? await api.cupsCupStagePendingMatches({ stageId: props.stage.id })
      : await api.cupsCupStagePendingMatchesFor({
          stageId: props.stage.id,
          signupIds: signups.map((s) => s.id),
        });
    const signupsById: Record<string, CupSignup> = {};
    for (const s of signups) {
      signupsById[s.id] = s;
    }
    const r: Omit<MatchProps, "cupId" | "update">[] = [];
    for (const match of matches) {
      if (typeof match.lowId !== "string" || typeof match.highId !== "string")
        continue;
      const scoring = await api.cups.scorings.byId(match.scoringId);
      const signup =
        signupsById[match.lowId ?? ""] ??
        signupsById[match.highId ?? ""] ??
        undefined;
      r.push({ match, scoring, signup, admin });
    }
    return r;
  }, []);

  function update() {
    props.matches.update();
    getPendingMatches.update();
  }

  const lastMod = createMemo(
    () => {
      let last: Date | undefined = undefined;
      for (const { updatedAt } of props.matches()) {
        if (last === undefined || updatedAt > last) last = updatedAt;
      }
      return last;
    },
    undefined,
    (a, b) => a?.getTime() === b?.getTime()
  );

  createEffect(() => {
    getPendingMatches.update();
    //  lastMod();
    // getPendingMatches.update();
  });

  return (
    <Show when={getPendingMatches().length > 0}>
      <div class="match-control-column">
        <For each={getPendingMatches()}>
          {(p) => (
            <div class="card match-control-box">
              <div class="card-header">
                <div class="card-header-title level has-text-weight-normal">
                  <div>
                    <SignupLoader
                      cupId={props.stage.cupId}
                      signup={p.match.highId ?? undefined}
                    />
                    &nbsp; / &nbsp;
                    <SignupLoader
                      cupId={props.stage.cupId}
                      signup={p.match.lowId ?? undefined}
                    />
                  </div>
                  <div>
                    <MatchName match={p.match} />
                  </div>
                </div>
              </div>
              <Keyed key={p.match.id}>
                <MatchControl
                  cupId={props.stage.cupId}
                  signup={p.signup}
                  admin={p.admin}
                  match={p.match}
                  scoring={p.scoring}
                  update={update}
                />
              </Keyed>
            </div>
          )}
        </For>
      </div>
    </Show>
  );
}

interface MatchProps {
  cupId: string;
  match: CupMatch2p;
  scoring: CupStageScoring;
  signup?: CupSignup;
  admin: boolean;
  update: () => void;
}

export function MatchControl(props: MatchProps) {
  if (props.scoring.isPointScore) {
    return PointScore(props);
  }
  return "Unsupported scoring type";
}

const cupMatchReportForm2pTy = t.type({
  matchId: t.number,
  lowReport: t.array(t.Int),
  highReport: t.array(t.Int),
});

const defaultScoreReport: any[] = [];

function PointScore(props: MatchProps) {
  const self =
    props.signup?.id === props.match.lowId
      ? "low"
      : props.signup?.id === props.match.highId
      ? "high"
      : undefined;

  const other =
    self === undefined ? undefined : self === "high" ? "low" : "high";

  interface State {
    form: {
      high: number | string;
      low: number | string;
    }[];
    admin: {
      high: number | string;
      low: number | string;
    }[];
    external: {
      low: undefined | ScoreReport[];
      high: undefined | ScoreReport[];
      "": undefined; // just a hack
    };
  }

  const initialState = (): State =>
    cloneDeep({
      external: {
        low: props.match.lowReport ?? undefined,
        high: props.match.highReport ?? undefined,
        "": undefined,
      },
      admin:
        props.admin && self !== undefined
          ? props.match?.[self === "high" ? "highReport" : "lowReport"] ??
            defaultScoreReport
          : [],
      form:
        self === undefined
          ? defaultScoreReport
          : self === "high"
          ? props.match.highReport ?? defaultScoreReport
          : props.match.lowReport ?? defaultScoreReport,
    });

  const [state, setState] = createState<State>(initialState());

  let id0 = untrack(() => props.match.id);
  const thisMatch = createMemo(() => props.match.id, id0);
  createEffect(() => {
    const next = thisMatch();
    if (id0 === next) return;
    setState(initialState());
  });

  const api = useApi();

  async function onSubmit(override: boolean) {
    function onReportError(err: any) {
      reportError(
        <>
          <h1 class="heading">Reporting error.</h1>
          <pre>
            <code>{JSON.stringify(err, null, 2)}</code>
          </pre>
        </>
      );
    }
    const form = {
      matchId: props.match.id,
      lowReport: undefined as undefined | any[],
      highReport: undefined as undefined | any[],
    };
    if (props.admin) {
      form.lowReport = state.admin.map((lh) => lh.low);
      form.highReport = state.admin.map((lh) => lh.high);
    } else {
      form.lowReport = state.form.map((lh) => lh.low);
      form.highReport = state.form.map((lh) => lh.high);
    }
    const validated = cupMatchReportForm2pTy.decode(form);
    if (
      isRight(validated) &&
      validated.right.lowReport.length === validated.right.highReport.length
    ) {
      try {
        if (override) {
          await api.cupsMatchOverrideReport2p({
            cupMatchReportForm2p: validated.right,
            matchId: props.match.id,
          });
        } else {
          await api.cupsMatchReport2p({
            cupMatchReportForm2p: validated.right,
            matchId: props.match.id,
          });
        }
        props.update();
      } catch (e) {
        onReportError(e);
      }
    } else if (isLeft(validated)) {
      reportError(
        <>
          <h1 class="heading">Validation error.</h1>
          {failure(validated.left).map((s) => (
            <>
              {s}
              <br />
            </>
          ))}
        </>
      );
    }
  }

  function valid(ix: number): boolean {
    const report = state?.[props.admin ? "admin" : "form"]?.[ix];
    return (
      report === undefined ||
      (report.high === "" && report.low === "") ||
      (Number.isSafeInteger(report.high) && Number.isSafeInteger(report.low))
    );
  }

  const disableLastMatch = createMemo(
    () => {
      const { bestof } = props.scoring;
      const form = state?.[props.admin ? "admin" : "form"];
      let highs = 0;
      let lows = 0;
      for (const { high, low } of form) {
        if (typeof high === "number" && typeof low === "number") {
          highs += high > low ? 1 : 0;
          lows += high > low ? 0 : 1;
        }
      }
      const disable = highs !== lows && highs + lows === bestof - 1;
      // log.info({ highs, lows, bestof, disable });
      return disable;
    },
    false,
    (a, b) => a === b
  );

  createEffect(() => {
    const { bestof } = props.scoring;
    if (disableLastMatch()) {
      setState(
        "form",
        produce((f: any[]) => {
          if (f.length === bestof) {
            f.splice(bestof - 1, 1);
          }
        })
      );
    }
  });

  const highName = useAsync(async () => {
    const sid = props.match.highId;
    if (typeof sid !== "string") return undefined;
    const pid = (await api.cups.signups.byId(props.cupId, sid)).playerId;
    return (await api.players.byId(pid)).discordUsername;
  });

  const lowName = useAsync(async () => {
    const sid = props.match.lowId;
    if (typeof sid !== "string") return undefined;
    const pid = (await api.cups.signups.byId(props.cupId, sid)).playerId;
    return (await api.players.byId(pid)).discordUsername;
  });

  function MatchControlRow(p: { ix: number; disabled?: boolean }) {
    return (
      <tr>
        <td class="py-2 has-text-right is-family-monospace">{p.ix + 1}</td>
        {(["high", "low"] as const).map((self: "high" | "low") => (
          <td class="py-2">
            <div class="field has-addons">
              {props.admin ? (
                <>
                  <div class="control">
                    <input
                      disabled={p.disabled}
                      placeholder={
                        p.disabled
                          ? "Already have a winner"
                          : self === "high"
                          ? highName()
                          : lowName()
                      }
                      type="number"
                      class="input is-small"
                      classList={{ ["is-danger"]: !valid(p.ix) }}
                      value={state.form?.[p.ix]?.[self] ?? ""}
                      onChange={(ev) => {
                        const value = ev.target.value.trim();
                        setState("admin", p.ix, {
                          [self]: value === "" ? value : Number.parseInt(value),
                        });
                      }}
                    />
                  </div>
                  <Show when={state.external?.high?.[p.ix]?.[self]}>
                    {(score) => (
                      <div class="control">
                        <div class="button is-small is-static">{score}</div>
                      </div>
                    )}
                  </Show>
                  <Show when={state.external?.low?.[p.ix]?.[self]}>
                    {(score) => (
                      <div class="control">
                        <div class="button is-small is-static">{score}</div>
                      </div>
                    )}
                  </Show>
                </>
              ) : (
                <>
                  <div class="control">
                    <input
                      disabled={p.disabled}
                      placeholder={
                        p.disabled
                          ? "Already have a winner"
                          : self === "high"
                          ? highName()
                          : lowName()
                      }
                      type="number"
                      class="input is-small"
                      classList={{ ["is-danger"]: !valid(p.ix) }}
                      value={state.form?.[p.ix]?.[self] ?? ""}
                      onChange={(ev) => {
                        const value = ev.target.value.trim();
                        setState("form", p.ix, {
                          [self]: value === "" ? value : Number.parseInt(value),
                        });
                      }}
                    />
                  </div>
                  <Show when={state.external?.[other ?? ""]?.[p.ix]?.[self]}>
                    {(score) => (
                      <div class="control">
                        <div class="button is-small is-static">{score}</div>
                      </div>
                    )}
                  </Show>
                </>
              )}
            </div>
          </td>
        ))}
      </tr>
    );
  }

  return (
    <>
      <div class="card-content px-0 pb-0 pt-2">
        <table class="table is-narrow is-fullwidth mb-2">
          <tbody>
            <EnumFromTo from={0} to={props.scoring.bestof}>
              {(ix) => (
                <MatchControlRow
                  ix={ix}
                  disabled={
                    ix === props.scoring.bestof - 1 ? disableLastMatch() : false
                  }
                />
              )}
            </EnumFromTo>
          </tbody>
        </table>
      </div>
      <div class="card-footer">
        <Show when={props.admin}>
          <a class="card-footer-item py-2" onClick={() => onSubmit(true)}>
            Admin override
          </a>
        </Show>
        <Show when={props.signup !== undefined}>
          <a class="card-footer-item py-2" onClick={() => onSubmit(false)}>
            Submit
          </a>
        </Show>
      </div>
    </>
  );
}

function formatTheirScore(score: number | undefined) {
  if (score === undefined) {
    return "";
  }
  return `(${score})`;
}

/*
function TimeScore(props: MatchProps) {
  return "time";
}
*/
