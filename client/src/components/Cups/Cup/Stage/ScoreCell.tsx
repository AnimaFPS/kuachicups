import { For } from "solid-js";

const WL = (props: { win: boolean; score: number }) => (
  <>
    <span class={props.win ? "match-icon-win" : "match-icon-loss"}>
      {props.score}
    </span>{" "}
  </>
);

export default function(props: {
  self: "high" | "low";
  report: { high: number; low: number }[];
}) {
  return (
    <For each={props.report}>
      {r => (
        <WL
          win={props.self === "high" ? r.high > r.low : r.low > r.high}
          score={props.self === "high" ? r.high : r.low}
        />
      )}
    </For>
  );
}
