import memoized from "micro-memoize";
import { CupMatch2p, ElimMatchType } from "../../../../api";

function makeAlphabetise(a: number) {
  return (i: number) => {
    const r: number[] = [];
    do {
      const rem = i-- % 26;
      r.push(a + rem);
      i = (i / 26) | 0;
    } while (i > 0);
    return String.fromCharCode(...r);
  };
}

const asAlpha = memoized(makeAlphabetise("a".charCodeAt(0)));
const asALPHA = memoized(makeAlphabetise("A".charCodeAt(0)));

export function matchName(m: CupMatch2p) {
  if (typeof m !== "object" || m === undefined) return "";
  return m.elimType === ElimMatchType.LB || m.elimType == null
    ? asAlpha(m.lid)
    : asALPHA(m.lid);
}

export function MatchName(p: { match: CupMatch2p }) {
  return <span class="match-id">{matchName(p.match)}</span>;
}
