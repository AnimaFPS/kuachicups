import {
  HierarchyNode,
  HierarchyPointNode,
  stratify,
  tree,
} from "d3-hierarchy";
import { curveLinear, curveStep, line } from "d3-shape";
import { cloneDeep, sortBy } from "lodash";
import { For, createEffect, createMemo, untrack } from "solid-js";
import { Dynamic } from "solid-js/web";
import { CupMatch2p, CupStage, CupStageFormat } from "../../../../api";
import useApi from "../../../../context/Api";
import { useContentWidth } from "../../../../context/Size";
import { router } from "../../../../router";
import Await from "../../../async/Await";
import { useAsync } from "../../../async/Awaitable";
import { EnumFromTo } from "../../../Case";
import { DiscordAvatarImg, useDiscordAvatar } from "../../../discord/Avatar";
import { CupSignupProps, SignupLoader } from "../CupSignup";
import { matchName } from "./MatchName";
import ScoreCell from "./ScoreCell";

export interface Props {
  format: CupStageFormat.SingleElim | CupStageFormat.DoubleElim;
  matches: () => CupMatch2p[];
  stage: CupStage;
  onClickMatch: (m: number) => void;
}

type N = CupMatch2p & { x?: number; y?: number };

type HN = HierarchyNode<N>;

const matchVGap = 10;
const matchHeight = 64;
const matchHalfHeight = matchHeight * 0.5;
const matchWidth = 190;
const matchHGap = 28;

declare module "solid-js" {
  namespace JSX {
    export interface IntrinsicElements {
      feDropShadow: {
        dx?: string | number;
        dy?: string | number;
        stdDeviation?: string | number;
        "flood-color"?: string;
      };
    }
  }
}

export default function ElimMatches(props: Props) {
  const contentWidth = useContentWidth();

  // assign names to every match
  // gives LB matches lowercase names a-z and UB ones A-Z
  let sorted: N[] = sortBy(cloneDeep(untrack(props.matches)), (a) => [
    a.elimRound,
    a.elimIndex,
  ]);

  const stratifier = stratify<CupMatch2p>()
    .parentId((m) => (m.elimWinnerMatchId ?? "") + "")
    .id((m) => m.id + "");

  const elbow = line().curve(curveStep);
  const lin = line().curve(curveLinear);

  const stra = stratifier(sorted);
  const width = stra.height * (matchWidth + matchHGap); // width to the last origin point of a match
  const height = (matchHeight + matchVGap) * stra.leaves().length; // height to the last match
  const root = tree<N>()
    .size([height, width])
    .separation(() => 1)(stra);

  const nodeById: { [id: number]: HN } = {};
  const xs: number[] = [];
  const mysteriousYOffset = (matchHeight + matchVGap) * -0.5;
  root.each(((m: HierarchyPointNode<N>) => {
    const x = width - m.y;
    if (xs[root.height - m.depth] === undefined) xs[root.height - m.depth] = x;
    m.data.x = x;
    m.data.y = m.x + mysteriousYOffset; // + height * 0.5;
    nodeById[m.data.id] = m;
  }) as any);

  const viewBox = {
    minX: -matchHGap, // Math.round(-pad),
    minY: 0, // Math.round(-pad),
    width: width + (matchWidth + matchHGap) + matchHGap,
    height: height + matchHalfHeight, // Math.round(height + pad)
  };

  const api = useApi();
  const descendants = root.descendants();

  function ElimMatchLoader(p: { node: HN }) {
    const match = useAsync(() =>
      api.cups.stages.matches.byId(props.stage.id, p.node.data.id)
    );
    // const updated = createMemo(() => match()?.updatedAt.getTime());
    return (
      <match.Await>
        {(m: CupMatch2p) => (
          <ElimMatch match={m} stage={props.stage} nodeById={nodeById} />
        )}
      </match.Await>
    );
  }

  return (
    <div class="elim-bracket" style={{ width: contentWidth() + "px" }}>
      <svg
        class="elim-bracket-svg"
        style={{ width: `${viewBox.width}px` }}
        viewBox={`${viewBox.minX} ${viewBox.minY} ${viewBox.width} ${viewBox.height}`}
        preserveAspectRatio="xMidYMid meet"
      >
        <defs>
          <clipPath id="avatar-clip">
            <circle
              r={avatarWidth * 0.5}
              cx={avatarWidth * 0.5}
              cy={avatarWidth * 0.5}
            />
          </clipPath>

          <linearGradient id="sudden-dropoff">
            <stop offset="0%" stop-color="#FFF" />
            <stop offset="95%" stop-color="#FFF" />
            <stop offset="100%" stop-color="#000" />
          </linearGradient>

          <EnumFromTo from={0} to={5}>
            {(i) => [
              <mask id={"signup-mask-" + i}>
                <rect
                  fill="url(#sudden-dropoff)"
                  width={matchWidth * (1 - matchScoresWidthPropByScores[i]) - 6}
                  height={matchHeight}
                />
              </mask>,
              <clipPath id={"signup-clip-" + i}>
                <rect
                  width={matchWidth * (1 - matchScoresWidthPropByScores[i]) - 6}
                  height={matchHeight}
                />
              </clipPath>,
            ]}
          </EnumFromTo>

          <filter id="shadow">
            <feDropShadow
              dx="0"
              dy="4"
              stdDeviation="6"
              flood-color="rgba(0,0,0,0.04)"
            />
          </filter>
          <filter id="shadow-big">
            <feDropShadow
              dx="0"
              dy="4"
              stdDeviation="3"
              flood-color="rgba(0,0,0,0.09)"
            />
          </filter>
        </defs>
        <g class="elim-round-labels">
          {xs.map((x, round) => (
            <g transform={`translate(${x}, 0)`}>
              <text x={matchWidth * 0.5}>round {round}</text>
            </g>
          ))}
        </g>
        <g transform={`translate(0, ${matchVGap * 2})`}>
          <g class="elim-drop-paths">
            {descendants.map((hn) => {
              const {
                data: { elimLoserMatchId, x, y },
              } = hn;
              if (typeof elimLoserMatchId !== "number") return "";
              const drop = nodeById[elimLoserMatchId]!.data;
              const path = lin([
                [x! + matchWidth, y! + matchHeight],
                [drop.x! + matchWidth, drop.y! + matchHeight],
              ])!;
              return <path d={path} />;
            })}
          </g>
          <g class="elim-match-paths">
            {descendants.map((hn) => {
              const { parent: parentNode, data: child } = hn;
              if (parentNode === null) return "";
              const parent = parentNode.data;
              const path = elbow([
                [parent.x!, parent.y! + matchHalfHeight],
                [child.x! + matchWidth, child.y! + matchHalfHeight],
              ])!;
              return <path d={path} />;
            })}
          </g>
          <g class="elim-matches">
            {descendants.map((hn) => (
              <g
                class="elim-match-g"
                transform={`translate(${hn.data.x!}, ${hn.data.y!})`}
              >
                <rect
                  class="elim-match"
                  width={matchWidth}
                  height={matchHeight}
                />
                <a onClick={() => props.onClickMatch(hn.data.id)}>
                  <text
                    class="elim-match-id"
                    dx={matchWidth + 2}
                    dy={matchHalfHeight + 12}
                  >
                    {matchName(hn.data)}
                  </text>
                </a>
                <ElimMatchLoader node={hn} />
                <rect
                  class="elim-match-outline"
                  width={matchWidth}
                  height={matchHeight}
                />
              </g>
            ))}
          </g>
        </g>
      </svg>
    </div>
  );
}

const matchScoresWidthPropByScores: Record<number, number> = {
  0: 0,
  1: 0.12,
  2: 0.24,
  3: 0.3,
  4: 0.33,
  5: 0.3333,
};

const mh12 = matchHeight * 0.5;
const mh14 = matchHeight * 0.25;
const mh34 = matchHeight * 0.75;
const avatarWidth = 20;
const avatarWidth_2 = avatarWidth * 0.5;

function RoundedImage(props: { href: string; dx: number; dy: number }) {
  const href = createMemo(() => encodeURI(props.href));
  return (
    <g
      innerHTML={`
        <image
          x="0"
          y="0"
          transform="translate(${props.dx}, ${props.dy})"
          width="${avatarWidth}"
          height="${avatarWidth}"
          href="${href()}"
          clip-path="url(#avatar-clip)"
        />
      `}
    />
  );
}

function SignupView(
  props: CupSignupProps & { lenClamped: number; dy: number }
) {
  const avatar = useDiscordAvatar(() => ({
    discordAvatar: props.player?.discordAvatar!,
    discordId: props.player?.discordId!,
    size: 24,
  }));
  return (
    <g>
      <a
        class="elim-signup"
        href={router.buildPath("players.profile", {
          id: props.player?.id ?? "",
        })}
        target="_blank"
      >
        <Await data={avatar} fallback={undefined}>
          {(avatar) => (
            <RoundedImage href={avatar} dx={6} dy={props.dy - avatarWidth_2} />
          )}
        </Await>
        <text
          dx={10 + (avatar() !== undefined ? avatarWidth : 0)}
          dy={props.dy}
          mask={`url(#signup-mask-${props.lenClamped})`}
          clip-path={`url(#signup-clip-${props.lenClamped})`}
          dominant-baseline="middle"
          text-anchor="start"
        >
          {props.player?.discordUsername}
        </text>
      </a>
    </g>
  );
}

function ElimMatch(props: {
  match: CupMatch2p;
  stage: CupStage;
  nodeById: { [id: number]: HN };
}) {
  function MatchRow(p: { self: "high" | "low" }) {
    const id = props.match[(p.self + "Id") as "highId" | "lowId"];
    const fromId =
      props.match[p.self === "high" ? "elimHighMatchId" : "elimLowMatchId"];

    const report = () => props.match.lowReport;
    const len = createMemo(() => report()?.length ?? 0);
    const lenClamped = createMemo(() => Math.min(5, len()));
    const matchScoresWidth = createMemo(
      () => matchWidth * (matchScoresWidthPropByScores?.[len()] ?? 0.36)
    );
    const matchSignupsWidth = createMemo(() => matchWidth - matchScoresWidth());
    const offs = createMemo(() => matchScoresWidth() / len());
    const dy = createMemo(() => (p.self === "high" ? mh14 : mh34));

    return (
      <>
        {typeof id === "string" ? (
          <SignupLoader
            cupId={props.stage.cupId}
            signup={id}
            render={(props) => (
              <SignupView lenClamped={lenClamped()} dy={dy()} {...props} />
            )}
          />
        ) : (
          <text
            class={typeof fromId === "number" ? "elim-match-id" : ""}
            dx={10}
            dy={dy()}
          >
            {typeof fromId === "number"
              ? matchName(props.nodeById[fromId]?.data!)
              : "Bye"}
          </text>
        )}
        <For each={report()?.map((h) => h[p.self]) ?? []}>
          {(m, i) => (
            <text
              class="elim-score"
              dx={matchSignupsWidth() + i() * offs()}
              dy={dy()}
            >
              {m}
            </text>
          )}
        </For>
      </>
    );
  }

  return (
    <g class="elim-match-content">
      {props.match.isScored && (
        <rect
          class="winner-bg"
          y={props.match.highId === props.match.winnerId ? 0 : mh12}
          height={mh12}
          width={matchWidth}
        />
      )}
      <MatchRow self="high" />
      <MatchRow self="low" />
    </g>
  );
}
