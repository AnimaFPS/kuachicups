import { JSX, lazy, Show, Suspense } from "solid-js";
import { Cup, Role } from "../../../api";
import useApi from "../../../context/Api";
import useAuth, { LoginState } from "../../../context/Auth";
import { Link, Router } from "../../../router";
import { CounterProvider } from "../../async/Counter";
import { createRefreshable } from "../../async/Refreshable";
import { CaseOf } from "../../Case";
import RefreshButton from "../../ui/RefreshButton";
import SignupButton from "./SignupButton";
import { default as Stage, StageList } from "./Stage";
import Standings from "./Standings";
import { CupStore, useCupStore } from "./Store";

const ManagePage = lazy(() => import("./Manage"));
const EditForm = lazy(() => import("./Form/Edit"));

export default function (props: { id: string }): JSX.Element {
  return (
    <CounterProvider name="cup">
      <CupStore id={props.id}>
        <CupView />
      </CupStore>
    </CounterProvider>
  );
}

function CupView() {
  const get = useCupStore();
  return (
    <get.cup.Await>
      {(cup: Cup) => (
        <div class="columns">
          <div class="column">
            <Router assume="cups.cup">
              {{
                render: (p) => (
                  <>
                    <div class="level is-mobile">
                      <div>
                        <h1 class="title">
                          <Link to="cups.cup" params={{ id: cup.id }}>
                            {cup.title}
                          </Link>
                          <get.ownerTeam.Await>
                            {(team) => (
                              <>
                                <span class="has-text-weight-light">
                                  &nbsp; hosted by &nbsp;
                                </span>
                                <Link
                                  to="teams.profile"
                                  params={{ id: team.id }}
                                >
                                  {team.name}
                                </Link>
                              </>
                            )}
                          </get.ownerTeam.Await>
                        </h1>
                      </div>
                      <RefreshButton
                        update={get.update}
                        isLoading={get.loading}
                      />
                    </div>
                    <hr />
                    {p.children}
                  </>
                ),
                fallback: () => (
                  <div class="columns">
                    <div class="column">
                      <h2 class="heading">Game / mode</h2>
                      <h2 class="subtitle is-size-5">
                        <get.gameMode.Await>
                          {(gm) => (
                            <>
                              {gm.game.name} / {gm.mode.name}
                            </>
                          )}
                        </get.gameMode.Await>
                      </h2>
                      {cup.descriptionHtml !== "" && (
                        <>
                          <h2 class="heading">Description</h2>
                          <div
                            class="content"
                            innerHTML={cup.descriptionHtml}
                          />
                        </>
                      )}
                      <hr />
                      <h2 class="heading">
                        {cup.isFinished ? "Standings" : "Current Standings"}
                      </h2>
                      <Standings />
                      <h2 class="heading">Stages</h2>
                      <StageList cupId={cup.id} />
                    </div>
                    <hr />
                  </div>
                ),
                manage: {
                  render: () => (
                    <ManagePage cup={cup} getSignups={get.signups} />
                  ),
                },
                edit: {
                  render: () => <EditForm cup={cup} />,
                },
                stage: {
                  render: () => (
                    <Suspense fallback="">
                      <Stage stage={get.stage.state.value!} />
                    </Suspense>
                  ),
                },
              }}
            </Router>
          </div>
        </div>
      )}
    </get.cup.Await>
  );
}

export function CupPanel(props: { cupId: string }) {
  const api = useApi();
  const getAuth = useAuth();

  enum ST {
    NotLoggedIn,
    Loading,
    Ok,
  }

  const getState = createRefreshable(
    async (refresh?: boolean) => {
      const cup = await api.cups.byId(props.cupId, refresh);
      const auth = await getAuth().then;
      if (auth.state !== LoginState.LoggedIn) {
        return { tag: ST.NotLoggedIn as const };
      }
      const role = await api.players.teamRoles.roleWithin(
        auth.login.id,
        cup.ownerTeamId,
        refresh
      );
      return { tag: ST.Ok as const, role, cup };
    },
    { tag: ST.Loading as const }
  );

  return (
    <CaseOf data={getState()} key="tag">
      {{
        [ST.Ok]: (p) => (
          <Controls cup={p.cup} isOwner={p.role === Role.Admin} />
        ),
      }}
    </CaseOf>
  );
}

/**
 * Stuff to put on the right panel
 */
function Controls(props: { isOwner: boolean | undefined; cup: Cup }) {
  return (
    <Router assume="cups.cup">
      {{
        stage: { render: () => undefined },
        fallback: () => (
          <>
            <SignupButton cup={props.cup} />
            <Show when={props.isOwner}>
              <hr />
              <h2 class="heading">Administration</h2>
              <ul class="menu-list">
                <li>
                  <Link nav to="cups.cup.manage" params={{ id: props.cup.id }}>
                    Manage
                  </Link>
                </li>
                <li>
                  <Link nav to="cups.cup.edit" params={{ id: props.cup.id }}>
                    Edit
                  </Link>
                </li>
              </ul>
            </Show>
          </>
        ),
      }}
    </Router>
  );
}
