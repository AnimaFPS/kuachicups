import { format } from "date-fns";
import { Suspense } from "solid-js";
import { Cup, PagedForUuid } from "../../api";
import useApi from "../../context/Api";
import { Link } from "../../router";
import { useAsync } from "../async/Awaitable";
import Pages from "../Paginate";
import { unpaged } from "../ui/Dropdown";

export default (props: { type?: "active" | "inactive" }) => {
  const api = useApi();

  function inactiveCups(): Promise<[PagedForUuid, Cup[]]> {
    // TODO
    return Promise.resolve([unpaged, []]);
  }

  function getCups(page: number) {
    if (props.type === "active") return api.cups.page(page);
    return inactiveCups();
  }

  return (
    <Pages
      route="teams"
      source={getCups}
      renderContent={(c) => c}
      renderItem={(i) => {
        const gm = useAsync(async (ref) => {
          const mode = await api.games.modes.byId(i.gameModeId, ref);
          const game = await api.games.byId(mode.gameId, ref);
          return { game, mode };
        });
        const host = useAsync((ref) => api.teams.byId(i.ownerTeamId, ref));
        const currentStage = useAsync((ref) =>
          api.cups.stages.byId(i.id, i.currentStage ?? 0, ref)
        );
        return (
          <div class="box">
            <div class="hero">
              <div class="cup-preview">
                <h1 class="cup-title title">
                  <Link to="cups.cup" params={{ id: i.id }}>
                    {i.title}
                  </Link>
                  &nbsp; hosted by &nbsp;
                  <host.Await>
                    {(t) => (
                      <Link to="teams.profile" params={{ id: t.id }}>
                        {t.name}
                      </Link>
                    )}
                  </host.Await>
                </h1>
                <div class="cup-info">
                  <div>
                    <h2 class="heading">Game</h2>
                    <span>{gm()?.game?.name}</span>
                  </div>
                  <div>
                    <h2 class="heading">Check-in time</h2>
                    {currentStage()?.checkintime != null
                      ? format(currentStage()!.checkintime!, "PPpp")
                      : undefined}
                  </div>
                  <div>
                    <h2 class="heading">Start time</h2>
                    {currentStage()?.starttime != null
                      ? format(currentStage()!.starttime!, "PPpp")
                      : undefined}
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      }}
      renderItems={(p) => <div class="pages-items">{p.children}</div>}
    />
  );
};
