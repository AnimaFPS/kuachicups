import { lazy, Show } from "solid-js";
import useAuth from "../../context/Auth";
import { Link, Router, useRoute } from "../../router";
import { create } from "../../state/slice/cups";
import Icon from "../ui/Icon";
import Layout from "../ui/Layout";
import Cup, { CupPanel } from "./Cup";
import List from "./List";

const Create = lazy(() => import("./Cup/Form/Create"));
const Edit = lazy(() => import("./Cup/Form/Edit"));

export default () => {
  const route = useRoute();
  return (
    <Router assume="cups">
      {{
        new: { render: Create },
        cup: { render: () => <Cup id={route().params.id} /> },
        fallback: () => {
          const p = route().params;
          const active = p.active === "true" || p.active === true;
          return <List type={active ? "active" : "inactive"} />;
        },
        render: (p) => <Layout panel={CupsPanel}>{p.children}</Layout>,
      }}
    </Router>
  );
};

function CupsPanel() {
  const route = useRoute();
  const auth = useAuth();
  const cc = create();
  cc.getCup(route().params.id);
  return (
    <Router assume="cups">
      {{
        cup: {
          fallback: () => <CupPanel cupId={route().params.id} />,
        },
        fallback: () => (
          <aside class="menu">
            <p class="menu-label">Cups</p>
            <ul class="menu-list">
              <li>
                <Link nav to="cups" params={{ active: "true", page: "0" }}>
                  Active cups
                </Link>
              </li>
              <li>
                <Link nav to="cups" params={{ active: "false", page: "0" }}>
                  Inactive cups
                </Link>
              </li>
            </ul>
            <Show when={auth().admin}>
              <p class="menu-label">Administration</p>
              <ul class="menu-list">
                <li>
                  <Link nav to="cups.new">
                    <Icon type="create" />
                    Create a cup
                  </Link>
                </li>
              </ul>
            </Show>
          </aside>
        ),
      }}
    </Router>
  );
}
