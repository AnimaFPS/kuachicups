import { cloneDeep } from "lodash";
import log from "loglevel";
import { createEffect, lazy } from "solid-js";
import { navigate, Router, useRoute } from "../router";
import Cups from "./Cups";
import Player from "./Player";
import Anns from "./Posts/Ann";
import Media from "./Posts/Media";
import Teams from "./Teams";

const PP = lazy(() => import("./PrivacyPolicy"));

const UnimplementedRoute = () => {
  const route = useRoute();
  return (
    <>
      <p>Unimplemented route</p>
      <pre>
        <code>{JSON.stringify(route(), null, 2)}</code>
      </pre>
    </>
  );
};

function AnnRedir() {
  createEffect(() => {
    navigate({ to: "announcements" });
  });
  return undefined;
}

export default () => {
  const route = useRoute();
  createEffect(() => {
    log.info("route changed", cloneDeep(route()));
  });

  return (
    <Router
      children={{
        fallback: () => <UnimplementedRoute />,
        index: { render: AnnRedir },
        announcements: { fallback: Anns },
        media: { fallback: Media },
        logged_in: { render: () => "Logged in!" },
        cups: { render: Cups },
        teams: { render: Teams },
        privacy_policy: { render: PP },
        players: {
          profile: {
            render: (p) => (
              <Player self={p.params.id === "self"} id={p.params.id} />
            ),
          },
        },
      }}
    />
  );
};
