import log from "loglevel";
import { PagedForUuid, PostAnn } from "../../../api";
import useApi from "../../../context/Api";
import useAuth from "../../../context/Auth";
import { Link, Router, useRoute } from "../../../router";
import Resource from "../../async/Resource";
import { LazyProvider, Lazily } from "../../Lazy";
import { SimplePaged } from "../../Paginate";
import Layout from "../../ui/Layout";
import NewAnn, { EditAnn } from "./New";
import AnnounceView from "./View";

export default function Announcements() {
  const api = useApi();
  const here = useRoute();
  const self = useAuth();
  return (
    <Layout panel={self().admin ? <AnnPanel /> : undefined}>
      <Router assume="announcements">
        {{
          new: { render: () => <NewAnn /> },
          announcement: {
            edit: { render: () => <EditAnn id={here().params.id} /> },
            fallback: () => <Ann id={here().params.id} />,
          },
          unpublished: {
            render: () => (
              <AnnList pageSource={api.announcements.unpublished.page} />
            ),
          },
          fallback: () => <AnnList pageSource={api.announcements.page} />,
        }}
      </Router>
    </Layout>
  );
}

function AnnList(props: {
  pageSource(
    page?: number
  ): Promise<[PagedForUuid, (() => Promise<PostAnn>)[]]>;
}) {
  log.info("AnnList");
  const here = useRoute();
  return (
    <div class="anns">
      <SimplePaged
        source={props.pageSource}
        getPage={() => here().params.page}
        renderItem={(item) => (
          <div class="ann-wrapper">
            <Lazily>
              <Resource data={item}>
                {(item) => <AnnounceView item={item} />}
              </Resource>
            </Lazily>
            <hr class="lazy-margin" />
          </div>
        )}
        renderItems={(p) => (
          <LazyProvider length={() => p.items.length} monotonic>
            {p.children}
          </LazyProvider>
        )}
        renderContent={(content) => content}
        renderPageButtons={(props) => (
          <nav class="pagination" aria-label="pagination">
            <Link
              class="pagination-previous"
              to="announcements"
              params={{ page: props.prev + "" }}
              disabled={props.prev === undefined}
            >
              Previous
            </Link>
            <Link
              class="pagination-next"
              to="announcements"
              params={{ page: props.next + "" }}
              disabled={props.next === undefined}
            >
              Next
            </Link>
          </nav>
        )}
      />
    </div>
  );
}

function Ann(props: { id: string }) {
  const api = useApi();
  return (
    <Resource
      data={() => api.announcements.byId(props.id)}
      children={(item) => <AnnounceView item={item} />}
    />
  );
}

function AnnPanel() {
  const route = useRoute();
  return (
    <aside class="menu">
      <ul class="menu-list">
        <li>
          <Link
            classList={{ "is-active": route().name === "announcements" }}
            to="announcements"
          >
            Announcements
          </Link>
        </li>
        <li>
          <Link nav to="announcements.unpublished">
            Unpublished announcements
          </Link>
        </li>
      </ul>
      <p class="menu-label">Administration</p>
      <ul class="menu-list">
        <li>
          <Link nav to="announcements.new">
            Create a post
          </Link>
        </li>
      </ul>
    </aside>
  );
}
