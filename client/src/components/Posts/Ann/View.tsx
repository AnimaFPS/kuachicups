import { Show } from "solid-js";
import { PostAnn } from "../../../api";
import useApi from "../../../context/Api";
import useAuth from "../../../context/Auth";
import { Link, navigate, router } from "../../../router";
import PlayerLink from "../../Player/Link";
import { DateTime } from "../../Time";
import Icon from "../../ui/Icon";

export default function AnnounceView(p: { item: PostAnn }) {
  const api = useApi();
  const auth = useAuth();
  function onPublish() {
    if (p.item.isPublished) return;
    api
      .postsPublishAnn({
        postId: p.item.id,
        uri:
          location.origin +
          router.buildPath("announcements.announcement", {
            id: p.item.id,
          }),
      })
      .then(() => {
        navigate({ to: "announcements" });
      });
  }
  return (
    <article class="ann box">
      <section class="ann-hero hero is-black is-bold mb-4">
        <div class="hero-body">
          <div class="level is-mobile">
            <div class="is-marginless">
              <Link
                class="ann-title title"
                to="announcements.announcement"
                params={{ id: p.item.id }}
                innerHTML={p.item.titleHtml ?? ""}
              />
            </div>
            <div class="level-right">
              <Link
                class="button is-text is-small"
                to="announcements.announcement"
                params={{ id: p.item.id }}
              >
                <Icon type="link" />
              </Link>
            </div>
          </div>
          <h2 class="subtitle ann-subtitle">
            <PlayerLink playerId={p.item.authorId} />
            <small>
              &nbsp;published <DateTime time={p.item.createdAt} />
            </small>
          </h2>

          <Show when={auth().admin}>
            <div class="buttons is-pulled-right">
              <Link
                class="button is-primary is-outlined is-small"
                to="announcements.announcement.edit"
                params={{ id: p.item.id }}
              >
                Edit
              </Link>
              <Show when={!p.item.isPublished}>
                <button
                  class="button is-link is-outlined is-small"
                  onClick={onPublish}
                >
                  Publish
                </button>
              </Show>
            </div>
          </Show>
        </div>
      </section>

      <div class="content block" innerHTML={p.item.contentHtml ?? ""} />
    </article>
  );
}
