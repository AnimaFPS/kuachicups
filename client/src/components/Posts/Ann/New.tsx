import { Show, createState, untrack } from "solid-js";
import { PostAnn } from "../../../api";
import { router, navigate } from "../../../router";
import useApi from "../../../context/Api";
import useAuth, { RequireLogin } from "../../../context/Auth";
import Resource from "../../async/Resource";
import { failure } from "../../Toast";
import ConfirmButton from "../../ui/ConfirmButton";
import Form, { checkbox, MarkdownField, text, visual } from "../../ui/Form";

export default function NewAnn() {
  return (
    <RequireLogin>
      <Inner />
    </RequireLogin>
  );
}

export function EditAnn(props: { id: string }) {
  const api = useApi();
  return (
    <Resource data={() => api.announcements.byId(props.id)}>
      {(ann) => <Inner ann={ann} />}
    </Resource>
  );
}

interface S {
  titleMd: string;
  contentMd: string;
  publishImmediately: boolean | undefined;
}

function Inner(props: { ann?: PostAnn }) {
  const api = useApi();
  const auth = useAuth();
  const [state, setState] = createState<S>(
    untrack(() => ({
      titleMd: props.ann?.titleMd ?? "",
      contentMd: props.ann?.contentMd ?? "",
      publishImmediately: props.ann?.isPublished ? undefined : true,
    }))
  );

  function annUri(id: string) {
    return (
      location.origin +
      router.buildPath("announcements.announcement", {
        id,
      })
    );
  }

  function navigateToAnn(id: string) {
    router.navigate("announcements.announcement", {
      id,
    });
  }

  async function onCreate() {
    const authorId = auth().login!.id;
    const { titleMd, contentMd, publishImmediately } = state;
    const m = await api.postsCreateAnn({
      createPostAnn: {
        authorId,
        titleMd,
        contentMd,
      },
    });
    if (publishImmediately) {
      await api.postsPublishAnn({
        uri: annUri(m.id),
        postId: m.id,
      });
      api.announcements.cache[m.id] = undefined;
    } else {
      api.announcements.cache[m.id] = Promise.resolve(m);
    }
    navigateToAnn(m.id);
  }

  async function onUpdate() {
    const authorId = auth().login!.id;
    const { titleMd, contentMd, publishImmediately } = state;
    const ann = props.ann;
    if (ann === undefined) return;
    const isPublished = ann.isPublished === true;
    const m = await api.postsUpdateAnn({
      updatePostAnn: {
        contentMd,
        authorId,
        titleMd,
        id: ann.id,
      },
    });
    if (!isPublished && publishImmediately) {
      await api.postsPublishAnn({
        uri: annUri(m.id),
        postId: m.id,
      });
      api.announcements.cache[m.id] = undefined;
    } else {
      api.announcements.cache[m.id] = Promise.resolve(m);
    }
    navigateToAnn(m.id);
  }

  function onSubmit() {
    if (props.ann === undefined) {
      onCreate();
    } else {
      onUpdate();
    }
  }

  function onDelete() {
    api.postsDeleteAnn({ postId: props!.ann!.id }).then(
      () => {
        navigate({ to: "announcements" });
      },
      () => {
        failure("Failed to delete post");
      }
    );
  }

  return (
    <>
      <h2 class="title is-4">
        {props.ann === undefined ? "Create announcement" : "Edit announcement"}
      </h2>
      <hr />
      <Form<S>
        state={[() => state, setState]}
        form={[
          text(["titleMd"], {
            label: "Title",
          }),
          visual(() => (
            <MarkdownField
              label="Content"
              get={() => state.contentMd ?? ""}
              set={(t: string) => setState("contentMd", t)}
              charLimit={2048}
            />
          )),
          checkbox(["publishImmediately"], {
            label: "Publish immediately",
            innerProps: { disabled: state.publishImmediately === undefined },
          }),
        ]}
      />
      <div class="buttons">
        <Show when={props.ann !== undefined}>
          <ConfirmButton class="is-danger" onConfirm={onDelete}>
            Delete
          </ConfirmButton>
        </Show>
        <button class="button is-primary" onClick={onSubmit}>
          {props.ann === undefined ? "Create" : "Update"}
        </button>
      </div>
    </>
  ); // "New announcement";
}
