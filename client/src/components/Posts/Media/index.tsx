import { format } from "date-fns";
import { createSignal, Show } from "solid-js";
import { PostMedia } from "../../../api";
import useApi from "../../../context/Api";
import { Link, useRoute } from "../../../router";
import { DiscordAvatarImg } from "../../discord/Avatar";
import { Lazily, LazyProvider } from "../../Lazy";
import { SimplePaged } from "../../Paginate";
import Icon from "../../ui/Icon";
import Layout from "../../ui/Layout";
import Resource from "../../async/Resource";

function MediaView(p: { item: PostMedia }) {
  return (
    <>
      <div class="media-left media-avatar-area">
        <MediaAvatar
          userId={p.item.discordUserId}
          avatarId={p.item.discordAvatar}
        />
      </div>
      <div class="media-content is-of-vis">
        <p>
          <strong>{p.item.discordName}</strong>{" "}
          <small>{format(new Date(p.item.discordTimestamp), "PPpp")}</small>
        </p>
        <div
          class="media-content-content"
          innerHTML={p.item.discordContentHtml ?? ""}
        />
        <Show when={typeof p.item.embedKind === "string"}>
          <div class="is-flex">
            <EmbedView embed={p.item} />
          </div>
        </Show>
      </div>
    </>
  );
}

function MediaAvatar(p: {
  userId: string;
  avatarId: string | undefined | null;
}) {
  return () =>
    typeof p.avatarId === "string" ? (
      <DiscordAvatarImg
        class="discord-avatar"
        discordId={p.userId}
        discordAvatar={p.avatarId}
      />
    ) : (
      <Icon type="person" class="discord-avatar is-64x64" />
    );
}

function EmbedView(p: { embed: PostMedia }) {
  return p.embed.embedKind === "video" &&
    typeof p.embed.embedThumbnailUrl === "string" &&
    typeof p.embed.embedVideoUrl === "string" ? (
    <EmbedVideoView
      thumbnailUrl={p.embed.embedThumbnailUrl}
      videoUrl={p.embed.embedVideoUrl}
      {...p.embed}
    />
  ) : (
    <pre>
      <code>{JSON.stringify(p.embed, null, 2)}</code>
    </pre>
  );
}

let vid = 0;

function EmbedVideo(props: {
  provider: string | undefined;
  video: string;
  href: string | undefined;
}) {
  const divId = `player-${vid++}`;

  switch (props.provider) {
    case "Twitch":
      return (
        <iframe
          class="media-object"
          src={props.video.replace("&parent=meta.tag", "&parent=kuachi.gg")}
        />
      );

    default:
      return <iframe class="media-object" src={props.video} />;
  }
}

function EmbedVideoView(
  p: PostMedia & {
    thumbnailUrl: string;
    videoUrl: string;
  }
) {
  const [showEmbed, setShowEmbed] = createSignal(false);
  return (
    <div class="box">
      {showEmbed() ? (
        <EmbedVideo
          provider={p.embedProviderName ?? undefined}
          video={p.videoUrl}
          href={p.embedVideoUrl ?? undefined}
        />
      ) : (
        <>
          <Show when={typeof p.embedAuthorName === "string"}>
            <h3 class="title is-6 mb-2">
              {typeof p.embedAuthorUrl === "string" ? (
                <a href={p.embedAuthorUrl}>{p.embedAuthorName}</a>
              ) : (
                p.embedAuthorName
              )}
            </h3>
          </Show>

          <Show when={typeof p.embedTitle === "string"}>
            <h3 class="title is-5 mb-2">
              {typeof p.embedVideoUrl === "string" ? (
                <a href={p.embedVideoUrl}>{p.embedTitle}</a>
              ) : (
                p.embedTitle
              )}
            </h3>
          </Show>

          <a
            class="media-thumbnail image"
            href={p.embedUrl ?? undefined}
            onClick={(ev) => {
              ev.preventDefault();
              setShowEmbed(true);
            }}
          >
            <div class="media-thumbnail-playicon">
              <Icon class="big-icon" innerClass="big-icon" type="play" />
            </div>
            <img src={p.thumbnailUrl} />
          </a>
        </>
      )}
    </div>
  );
}

function MediaLoading() {
  return (
    <>
      <button class="button is-text is-loading" />
      <div class="lazy-loader" />
    </>
  );
}

export default function Medias() {
  const api = useApi();
  const here = useRoute();
  return (
    <Layout>
      <SimplePaged
        source={api.media.page}
        getPage={() => here().params.page}
        renderItem={(item) => (
          <article class="media">
            <Lazily>
              <Resource data={item} fallback={MediaLoading}>
                {(item) => <MediaView item={item} />}
              </Resource>
            </Lazily>
            <div class="lazy-margin" />
          </article>
        )}
        renderItems={(p) => (
          <LazyProvider length={() => p.items.length} monotonic>
            <div class="medias">{p.children}</div>
          </LazyProvider>
        )}
        renderContent={(content) => content}
        renderPageButtons={(props) => (
          <nav class="pagination" aria-label="pagination">
            <Link
              class="pagination-previous"
              to="media"
              params={{ page: props.prev + "" }}
              disabled={props.prev === undefined}
            >
              Previous
            </Link>
            <Link
              class="pagination-next"
              to="media"
              params={{ page: props.next + "" }}
              disabled={props.next === undefined}
            >
              Next
            </Link>
          </nav>
        )}
      />
    </Layout>
  );
}
