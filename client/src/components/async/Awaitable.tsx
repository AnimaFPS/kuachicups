import {
  createComputed,
  createMemo,
  createResource,
  createResourceState,
  createRoot,
  JSX,
  State,
  untrack,
} from "solid-js";
import { Loading } from "../Loading";
import RefreshButton from "../ui/RefreshButton";
import { genericAwait } from "./Await";
import { Counter, createCounter } from "./Counter";
import { genericFor } from "./For";
import { proxyListen, ProxyListen } from "./proxyListen";
import { Listen, NonUndefined } from "./types";

export type Awaitable<T, U = undefined> = ProxyListen<
  AwaitableCls<NonUndefined<T>, U>
>;

export type AwaitableI<R> = Awaitable<R, NonUndefined<R>>;

export class AwaitableCls<T, U = undefined> implements Listen<T, U> {
  constructor(asyncFn: (ref?: boolean) => Promise<T>);
  constructor(
    asyncFn: (ref?: boolean) => Promise<T>,
    def: U,
    counter?: Counter
  );
  constructor(
    asyncFn: (ref?: boolean) => Promise<T>,
    def?: U,
    counter = createCounter()
  ) {
    const [state, loadState] = createResourceState<{
      value: T | undefined;
    }>({ value: undefined });
    const loading = createMemo(() => state.loading.value);
    const defined = createMemo(
      () => state.value !== undefined,
      false,
      (a: boolean, b: boolean) => a === b
    );
    const initialised = new Promise<void>((resolve) => {
      createRoot((dispose) => {
        createComputed(() => {
          if (defined()) {
            resolve();
            dispose();
          }
        });
      });
    });

    createComputed(() => {
      const refresh = counter.leadingEdge();
      const result = asyncFn(refresh); // listen to anything in asyncFn
      loadState({ value: () => result });
    });

    this.update = counter.signal.increment;
    this.state = state;
    this.loading = loading;
    this.def = def as U;
    this.defined = defined;
    this.then = () =>
      initialised.then(
        () =>
          new Promise((resolve, reject) => {
            if (this.state.value !== undefined) {
              // note `defined()` which `wait` resolves on requires state.value
              // !== undefined so this shoud be ok
              resolve(this.state.value as NonUndefined<State<T>>);
            } else {
              reject("Awaitable.then() failed, value is undefined");
            }
          })
      );
    this.startInterval = (sec) => counter.signal.startInterval(sec);
    this.stopInterval = () => counter.signal.stopInterval();
  }
  def: U;
  then: () => Promise<NonUndefined<State<T>>>;
  state: State<{ value: T | undefined }>;
  defined: () => boolean;
  loading: () => boolean;
  update: () => void;
  startInterval: (sec: number) => void;
  stopInterval: () => void;
  listen = () =>
    (this.state.value as NonUndefined<State<T>> | undefined) ?? this.def;
  For = genericFor<T, U>(this);
  Await = genericAwait<T, U>(this);

  RefreshButton = () => (
    <RefreshButton isLoading={this.loading} update={this.update} />
  );

  Load = (p: { children: JSX.Element }) => () =>
    this.loading() ? p.children : <Loading />;
}

export function useAsync<T>(
  asyncFn: (ref?: boolean) => Promise<T>
): Awaitable<T>;

export function useAsync<T, U>(
  asyncFn: (ref?: boolean) => Promise<T>,
  def: U
): Awaitable<T, U>;

export function useAsync<T, U = undefined>(
  asyncFn: (ref?: boolean) => Promise<T>,
  def?: U
): Awaitable<T, U> {
  return proxyListen(new AwaitableCls(asyncFn, def)) as any;
}

export function usePromise<T>(p: Promise<T>): () => T | undefined {
  const [getter, setter] = createResource<T | undefined>();
  setter(() => p);
  return getter;
}
