import { State } from "solid-js";

export type NonUndefined<T> = T extends undefined ? never : T;

export interface Listen<T, U = undefined> {
  listen(): NonUndefined<State<T>> | U;
  defined(): boolean;
  state: State<{ value: T | undefined }>;
}
