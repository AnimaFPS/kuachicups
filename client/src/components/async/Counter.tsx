import { debounce } from "lodash";
import {
  createContext,
  createMemo,
  createSignal,
  JSX,
  onCleanup,
  untrack,
  useContext,
} from "solid-js";

export interface Counter {
  leadingEdge(): boolean;
  signal: CounterSignal;
}

export interface CounterSignal {
  name?: string;
  counter(): number;
  increment(): void;
  startInterval(seconds: number, retry?: number): void;
  stopInterval(): void;
  intervalDuration?: number;
  interval?: NodeJS.Timeout;
}

export function CounterProvider(props: {
  name?: string;
  children: JSX.Element;
}) {
  const signal = createCounterSignal(props.name);
  return (
    <CounterContext.Provider value={{ signal }}>
      {props.children}
    </CounterContext.Provider>
  );
}

export function useCounter(name?: string): Counter {
  const signal =
    useContext(CounterContext).signal ?? createCounterSignal(name ?? "_local");
  return createCounter(signal);
}

export function createCounterSignal(name?: string): CounterSignal {
  const [counter, setCounter] = createSignal(0);
  return {
    name,
    increment: debounce(
      () => {
        setCounter(untrack(counter) + 1);
      },
      50,
      { leading: true, trailing: false }
    ),
    counter,
    startInterval(sec: number, retry: number = 0) {
      untrack(() => {
        if (this.interval !== undefined) {
          if (
            typeof this.intervalDuration === "number" &&
            sec < this.intervalDuration
          ) {
            this.stopInterval();
          } else {
            // skip, there's an existing interval that's shorter than the one we
            // want here anyway
            if (retry === 0) {
              onCleanup(() => this.startInterval(sec, retry + 1));
            }
            return;
          }
        }
        this.intervalDuration = sec;
        this.interval = setInterval(this.increment, 1000 * sec);
        onCleanup(this.stopInterval);
      });
    },
    stopInterval() {
      untrack(() => {
        const interval = this.interval;
        if (interval !== undefined) {
          clearInterval(interval);
          this.interval = undefined;
          this.intervalDuration = undefined;
        }
      });
    },
  };
}

export function createCounter(
  signal: CounterSignal = createCounterSignal()
): Counter {
  let lastCount = -1;
  return {
    signal,
    leadingEdge: () => {
      const now = signal.counter();
      if (now > lastCount) {
        lastCount = now;
        return true;
      }
      return false;
    },
  };
}

const CounterContext = createContext({
  signal: undefined as CounterSignal | undefined,
});
