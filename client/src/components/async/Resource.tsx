import { createResource, JSX, createComputed } from "solid-js";
import Await from "./Await";

export default function Resource<T>(props: {
  data: () => Promise<T>;
  children: (val: T) => JSX.Element;
  fallback?: JSX.Element;
}) {
  const [res, setRes] = createResource<T>();
  createComputed(() => setRes(props.data));
  return (
    <Await data={res} children={props.children} fallback={props.fallback} />
  );
}
