import { Awaitable, AwaitableCls } from "./Awaitable";
import { useCounter } from "./Counter";
import { proxyListen } from "./proxyListen";
import { NonUndefined } from "./types";

export type Refreshable<T, U = undefined> = Awaitable<T, U>;
export type RefreshableI<R> = Refreshable<R, NonUndefined<R>>;

export function createRefreshable<T>(
  asyncFn: (ref?: boolean) => Promise<T>
): Awaitable<T>;

export function createRefreshable<T, U>(
  asyncFn: (refresh?: boolean) => Promise<T>,
  def: U
): Awaitable<T, U>;

export function createRefreshable<T, U = undefined>(
  asyncFn: (refresh?: boolean) => Promise<T>,
  def?: U
): Awaitable<T, U> {
  return proxyListen(
    new AwaitableCls<T, U>(asyncFn, def as U, useCounter())
  ) as any;
}
