interface Listener<T extends object = any> {
  listen(): T;
  state: { value: undefined | T };
}

export type ProxyListen<O extends Listener> = Omit<O, "listen"> &
  ListenTo<ReturnType<O["listen"]>>;

type ListenTo<R> = { (): R } & {
  [K in keyof R]: R[K];
};

export function proxyListen<O extends Listener>(obj: O): ProxyListen<O> {
  return new Proxy(obj.listen, {
    get: function (_outer, prop: any) {
      return (obj as any)[prop];
    },
  }) as any;
}
