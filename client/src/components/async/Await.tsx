import { createMemo, JSX, untrack } from "solid-js";
import { Loading } from "../Loading";
import { Listen, NonUndefined } from "./types";

export default function Await<T>(props: {
  data: () => T | undefined;
  children: (val: T) => JSX.Element;
  fallback?: JSX.Element;
}) {
  const fallback = props.fallback !== undefined ? props.fallback : <Loading />;
  return createMemo(() => {
    const data = props.data();
    return untrack(() => {
      if (data !== undefined) return props.children(data);
      return fallback;
    });
  });
}

export function genericAwait<T, U>(
  self: Listen<T, U>
): (props: {
  children: (val: NonUndefined<T>) => JSX.Element;
  fallback?: JSX.Element;
}) => JSX.Element {
  return (props) => {
    return () =>
      self.defined() ? props.children(self.state.value as any) : props.fallback;
  };
}
