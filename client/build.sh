#!/usr/bin/env bash
set -euo pipefail
cd "$(dirname "$0")"
if [ -d build-prod ]; then
  rm -r build-prod
fi
yarn
yarn build
