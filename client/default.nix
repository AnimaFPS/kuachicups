{ pkgs ? import ../nixpkgs.nix }:
pkgs.buildEnv {
  extraPrefix = "/client/static";
  name = "kuachicups-client";
  paths = [ ./build-prod ];
}
