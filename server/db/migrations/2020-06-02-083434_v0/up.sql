CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- PLAYERS
-----------

CREATE TABLE player (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    discord_id text NOT NULL UNIQUE,
    discord_username text NOT NULL,
    discord_discriminator text NOT NULL,
    discord_tag text NOT NULL GENERATED ALWAYS AS ('@' || discord_username || '#' || discord_discriminator) STORED,
    discord_avatar text,
    discord_email text,
    created_at timestamptz NOT NULL DEFAULT now(),
    updated_at timestamptz NOT NULL DEFAULT now(),
    UNIQUE (discord_username, discord_discriminator)
);

CREATE INDEX player_fts_idx ON player USING GIN (to_tsvector('english', discord_tag));

-- TEAMS
-----------

CREATE TABLE team (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    created_at timestamptz NOT NULL DEFAULT now(),
    updated_at timestamptz NOT NULL DEFAULT now(),
    name text NOT NULL,
    blurb_md text,
    blurb_html text,
    discord_server_id text,
    discord_server_avatar text
);

CREATE INDEX team_fts_idx ON team USING GIN (to_tsvector('english', name || ' ' || blurb_md));

CREATE TYPE social AS ENUM (
    'twitch',
    'twitter',
    'facebook',
    'youtube',
    'website'
);

CREATE TABLE player_social(
    player_id uuid NOT NULL REFERENCES player(id),
    service social NOT NULL,
    uri text NOT NULL,
    UNIQUE (player_id, service)
);

CREATE TABLE team_social(
    team_id uuid NOT NULL REFERENCES team(id),
    service social NOT NULL,
    uri text NOT NULL,
    UNIQUE (team_id, service)
);

CREATE TYPE team_player_role AS ENUM ('player', 'captain', 'admin');

CREATE TABLE team_member (
    player_id uuid NOT NULL REFERENCES player(id),
    player_role team_player_role NOT NULL DEFAULT 'player',
    team_id uuid NOT NULL REFERENCES team(id),
    created_at timestamptz NOT NULL DEFAULT now(),
    updated_at timestamptz NOT NULL DEFAULT now(),
    UNIQUE (player_id, team_id)
);

CREATE TABLE team_member_invite (
    invite_by_player_id uuid NOT NULL REFERENCES player(id),
    player_id uuid NOT NULL REFERENCES player(id),
    team_id uuid NOT NULL REFERENCES team(id),
    created_at timestamptz NOT NULL DEFAULT now(),
    UNIQUE (player_id, team_id)
);

CREATE TABLE admin_team (
    team_id uuid NOT NULL PRIMARY KEY REFERENCES team(id)
);

CREATE VIEW site_admin_player AS
    SELECT player.*
    FROM player
    JOIN team_member ON player.id = team_member.player_id
    JOIN admin_team ON admin_team.team_id = team_member.team_id;

CREATE VIEW site_admin_team AS
    SELECT team.*
    FROM team
    JOIN admin_team ON admin_team.team_id = team.id;

-- 1. The admin team
-- 2. Adds new players mike haymo kuachi to it
WITH admin_teams AS (
    INSERT INTO team(name, discord_server_id, discord_server_avatar, blurb_md, blurb_html)
    VALUES ('Admins', '649789878925525013', '7042478c7965fecbe287981f830850e7', 'the kuachi gang', 'the kuachi gang')
    RETURNING id
), "_0" AS (
    INSERT INTO admin_team(team_id) SELECT id FROM admin_teams
), "_1" AS (
    INSERT INTO team_social(team_id, service, uri) SELECT id, 'website', 'https://kuachicups.com' FROM admin_teams
), admin_users AS (
    INSERT INTO player(discord_id, discord_username, discord_discriminator, discord_avatar)
    VALUES
        ('257811893773795328', 'mike',    '9892', '264e8fa5906783e115e0dd6ca086f89d'),
        ('251673000506687488', 'omyah',   '3423', '30aca420352fd9a1e6703f5fba3d7f9e'),
        ('345550372191862785', 'kuachi',  '0205', '322aa2b5661e3552d80fd0dc07b8c457'),
        ('243893246789419008', 'vertigo', '8986', 'a_ff7b4763fbc059f7c862b31f2b47211e'),
        ('191022900088668160', 'SHrulez', '7194', '3dec3811eeccac17cbd069f002d01e1d')
    RETURNING id
)
    INSERT INTO
        team_member (player_id, player_role, team_id)
    SELECT
        admin_users.id AS player_id, 'admin', admin_teams.id AS team_id
    FROM
        admin_teams, admin_users;

INSERT INTO player(discord_id, discord_username, discord_discriminator, discord_avatar)
VALUES
  ('286439631791063040', 'muffin', '8309', 'f8361a5fb1cacde2e018afeaf3507e9c'),
  ('108919814092779520', '2g, elim[f]', '0420', '4f673039db386897631805473210f3f0'),
  ('737952717531774997', 'friday', '0669', 'f8d44ef3fdc9fc762c4c6d83893ea698'),
  ('112479669105815552', 'tjk', '8253', '3bc933efa76f917a0f621555e3b4053d')
  ;

-- GAMES
-----------

CREATE TYPE entrant_type AS ENUM (
    'player',
    'team'
);

CREATE TABLE game (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    name text NOT NULL UNIQUE,
    slug text NOT NULL UNIQUE,
    description_md text NOT NULL DEFAULT '',
    description_html text NOT NULL DEFAULT '',
    rules_md text NOT NULL DEFAULT '',
    rules_html text NOT NULL DEFAULT '',
    avatar text NOT NULL DEFAULT '',
    created_at timestamptz NOT NULL DEFAULT now(),
    updated_at timestamptz NOT NULL DEFAULT now()
);

CREATE TABLE game_mode (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    game_id uuid NOT NULL REFERENCES game(id),
    name text NOT NULL,
    slug text NOT NULL,
    description_md text NOT NULL DEFAULT '',
    description_html text NOT NULL DEFAULT '',
    rules_md text NOT NULL DEFAULT '',
    rules_html text NOT NULL DEFAULT '',
    avatar text NOT NULL DEFAULT '',

    -- null = either player or team can enter
    entrant_type entrant_type NULL,

    -- postgres has a type int4range that would be useful here but sqlx does not
    -- support this very well

    entrants_per_match_min integer NOT NULL DEFAULT 2,
    entrants_per_match_max integer NULL, -- null = infinite

    -- used entrant_type=team. when entrant_type=null this may be used if a
    -- cup_stage is configured to accept teams, in which case effectively
    -- entrant_type=team
    --
    -- it seems elegant to think of a player as a team of 1, but please do not
    -- do this as, without requiring everybody to make a team for themselves
    -- alone, or creating ones per player, the signups definitely are different
    team_size_min integer NULL,

    created_at timestamptz NOT NULL DEFAULT now(),
    updated_at timestamptz NOT NULL DEFAULT now(),

    UNIQUE(game_id, name),
    UNIQUE(game_id, slug)
);

-- Diabotical game modes
WITH dbt AS (
    INSERT INTO game (name, slug) VALUES ('Diabotical', 'dbt') RETURNING id
)
    INSERT INTO
        game_mode (
            game_id,
            name,
            slug,
            entrant_type,
            entrants_per_match_min,
            entrants_per_match_max,
            team_size_min
        )
    SELECT
        id,
        name,
        slug,
        ety::entrant_type,
        lower(int4range(epm)),
        upper(int4range(epm)),
        ts
    FROM
        dbt, (
        values
            ('Duel'         , 'duel'        , 'player' , '[2,2]', null),
            ('Arena'        , 'arena'       , null     , '[2,)' , 2),
            ('Shaft Arena'  , 'shaft_arena' , null     , '[2,)' , 2),
            ('Rocket Arena' , 'rocket_arena', null     , '[2,)' , 2),
            ('Brawl'        , 'brawl'       , null     , '[2,)' , 2),
            ('TDM'          , 'tdm'         , 'team'   , '[2,]' , 2),
            ('Wipeout'      , 'wo'          , 'team'   , '[2,2]', 3),
            ('CTF'          , 'ctf'         , 'team'   , '[2,2]', 3),
            ('MacGuffin'    , 'mg'          , 'team'   , '[2,2]', 3),
            ('Instagib'     , 'ig'          , null     , '[2,)' , 2),
            ('Time Trial'   , 'race'        , 'player' , '[2,)' , null)
        ) as modes(name, slug, ety, epm, ts);

-- Quake Champions game modes
WITH qc AS (
    INSERT INTO game (name, slug) VALUES ('Quake Champions', 'qc') RETURNING id
)
    INSERT INTO
        game_mode (
            game_id,
            name,
            slug,
            entrant_type,
            entrants_per_match_min,
            entrants_per_match_max,
            team_size_min
        )
    SELECT
        id,
        name,
        slug,
        ety::entrant_type,
        lower(int4range(epm)),
        upper(int4range(epm)),
        ts
    FROM
        qc, (
        values
            ('Duel'       , 'duel'  , 'player' , '[2,2]', null),
            ('TDM'        , 'tdm'   , 'team'   , '[2,]' , 2)
        ) as modes(name, slug, ety, epm, ts);

-- Quake Live game modes
WITH ql AS (
    INSERT INTO game (name, slug) VALUES ('Quake Live', 'ql') RETURNING id
)
    INSERT INTO
        game_mode (
            game_id,
            name,
            slug,
            entrant_type,
            entrants_per_match_min,
            entrants_per_match_max,
            team_size_min
        )
    SELECT
        id,
        name,
        slug,
        ety::entrant_type,
        lower(int4range(epm)),
        upper(int4range(epm)),
        ts
    FROM
        ql, (
        values
            ('Duel'       , 'duel'  , 'player' , '[2,2]', null),
            ('CA'         , 'ca'    , null     , '[2,2]', null),
            ('CTF'        , 'ctf'   , 'team'   , '[2,2]', 3),
            ('TDM'        , 'tdm'   , 'team'   , '[2,2]', 2)
        ) as modes(name, slug, ety, epm, ts);

-- CUPS
-----------------

CREATE TABLE cup (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  game_mode_id uuid NOT NULL REFERENCES game_mode(id),
  owner_team_id uuid NOT NULL REFERENCES team(id),
  title text NOT NULL,
  slug text NOT NULL UNIQUE,
  description_md text NOT NULL DEFAULT '',
  description_html text NOT NULL DEFAULT '',
  current_stage integer DEFAULT 0,
  is_signups_closed boolean NOT NULL DEFAULT false,
  is_finished boolean NOT NULL GENERATED ALWAYS AS (current_stage IS NULL) STORED,
  is_published boolean NOT NULL DEFAULT false,
  created_at timestamptz NOT NULL DEFAULT now(),
  updated_at timestamptz NOT NULL DEFAULT now()
);

CREATE TYPE cup_stage_format AS ENUM (
  'double_elim',
  'single_elim',
  'groups',
  'ladder'
);

CREATE TYPE group_point_award_type AS ENUM (
  'match_score_only', -- e.g. number of flags captured
  'match_winloss'
);

CREATE TABLE cup_stage (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  stage_no integer NOT NULL DEFAULT 0,
  -- match control schema, see src/schema/model/match_control.rs
  -- match_control jsonb NOT NULL,
  cup_id uuid NOT NULL REFERENCES cup(id),
  title text NOT NULL,
  slug text NOT NULL,
  start_immediately boolean NOT NULL,
  checkintime timestamptz NULL,
  starttime timestamptz NULL,
  is_started boolean NOT NULL DEFAULT false,
  CONSTRAINT stage_zero_has_start_time CHECK (
    CASE
      WHEN stage_no = 0 THEN checkintime IS NOT NULL AND starttime IS NOT NULL AND NOT start_immediately
      WHEN start_immediately THEN checkintime IS NULL AND starttime IS NULL
      ELSE checkintime IS NOT NULL AND starttime IS NOT NULL
    END),
  CONSTRAINT stage_no_nonneg CHECK (stage_no >= 0),
  format cup_stage_format NOT NULL,
  max_participants integer NULL,
  group_size integer NULL,
  group_point_award group_point_award_type NOT NULL DEFAULT 'match_winloss',
  created_at timestamptz NOT NULL DEFAULT now(),
  updated_at timestamptz NOT NULL DEFAULT now(),
  UNIQUE(cup_id, slug),
  UNIQUE(cup_id, stage_no)
);

CREATE TABLE cup_signup (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  player_id uuid NOT NULL REFERENCES player(id),
  team_id uuid REFERENCES team(id),
  cup_id uuid NOT NULL REFERENCES cup(id),
  checked_in boolean NOT NULL GENERATED ALWAYS AS (checkedin_time IS NOT NULL) STORED,
  signup_time timestamptz NOT NULL DEFAULT now(),
  checkedin_time timestamptz NULL,
  seed_value integer NULL,
  CONSTRAINT have_signup CHECK (player_id IS NOT NULL OR team_id IS NOT NULL),
  -- invariants:
  --
  -- a player_id must be specified for signups to cup_stages with game that is
  -- has 'player' contestent_type.
  --
  -- a team_id must be specified for signups to cup_stages with game that is has
  -- 'team' contestent_type.
  --
  -- when both a team and player are specified, that indicates that the signup
  -- is a player playing under a specific team; it's valid for any game mode
  -- that has 'player' signups
  UNIQUE(player_id, cup_id)
);

-- Scoring configuration The `elim_round` and `is_lb` columns are queried when
-- matches are created to link them to a specific cup_stage_scoring row.
--
-- These rows specify things like:
-- 1. The pickban system (TBD)
-- 2. The
CREATE TABLE cup_stage_scoring (
  id serial PRIMARY KEY,
  cup_stage_id uuid NULL REFERENCES cup_stage(id),
  bestof integer NOT NULL DEFAULT 1,
  CONSTRAINT bestof_pos_odd CHECK ((bestof > 0) AND ((bestof % 2) <> 0)),
  elim_round integer,
  is_lb boolean,
  UNIQUE (cup_stage_id, elim_round, is_lb),
  groups boolean NOT NULL DEFAULT false,
  is_point_score boolean,
  point_score_greatest_is_winner boolean,
  is_time_score boolean,
  is_time_score_race boolean,

  CONSTRAINT have_elim_round CHECK (
    (elim_round IS NOT NULL AND is_lb IS NOT NULL) OR
    (elim_round IS NULL AND is_lb IS NULL)
  ),

  CONSTRAINT point_score_fully_specified CHECK (
    NOT (is_point_score IS TRUE) OR
    ((is_point_score IS TRUE) AND (point_score_greatest_is_winner IS TRUE))
  ),

  CONSTRAINT time_score_fully_specified CHECK (
    NOT (is_time_score IS TRUE) OR
    ((is_time_score IS TRUE) AND (is_time_score_race IS NOT NULL))
  ),

  CONSTRAINT have_score_type CHECK (
    (is_point_score AND NOT is_time_score) OR
    (is_time_score AND NOT is_point_score)),

  created_at timestamptz NOT NULL DEFAULT now(),
  updated_at timestamptz NOT NULL DEFAULT now()
);

CREATE TYPE elim_match_type AS ENUM ('WB', 'LB', 'GF1', 'GF2');

-- A 2 player match tied to a particular cup stage
CREATE TABLE cup_match_2p (
  id serial PRIMARY KEY,
  lid integer NOT NULL, -- the "local" match id, use this for match names
  cup_stage_id uuid NOT NULL REFERENCES cup_stage(id),
  UNIQUE (cup_stage_id, lid),

  -- Whichever configuration isn't null is the valid/used one. yolo
  -- # Group configuration
  group_no integer,

  -- # Alternatively, elim configuration
  elim_round integer,
  elim_index integer,
  elim_type smallint, -- 0=WB, 1=LB, 2=GF1, 3=GF2

  UNIQUE (cup_stage_id, elim_round, elim_index),
  -- I would use an enum but they are a pain in the ass with sqlx at the moment

  elim_winner_match_id integer REFERENCES cup_match_2p(id),
  elim_winner_to_high boolean, -- whether to put this match into the high or low seed

  elim_loser_match_id integer REFERENCES cup_match_2p(id),
  elim_loser_to_high boolean,

  elim_low_match_id integer REFERENCES cup_match_2p(id),
  elim_high_match_id integer REFERENCES cup_match_2p(id),

  -- null for high_id or low_id means:
  -- - for group matches, a bye
  -- - for elim matches, it means the preceeding matches are not complete OR this match is a round 0 bye
  low_id uuid REFERENCES cup_signup(id),

  high_id uuid REFERENCES cup_signup(id),

  low_report_low integer[],
  low_report_high integer[],
  CONSTRAINT have_low_report CHECK (cardinality(low_report_low) = cardinality(low_report_high)),
  high_report_low integer[],
  high_report_high integer[],
  CONSTRAINT have_high_report CHECK (cardinality(high_report_low) = cardinality(high_report_high)),

  is_scored boolean NOT NULL GENERATED ALWAYS AS (
    winner_id IS NOT NULL
    -- the loser_id can be null sometimes if this match was a bye
  ) STORED,

  winner_id uuid,
  loser_id uuid,

  scoring_id integer NOT NULL REFERENCES cup_stage_scoring(id),
  created_at timestamptz NOT NULL DEFAULT now(),
  updated_at timestamptz NOT NULL DEFAULT now()
);

-- POSTS
-- - Announcements
-- - #media
-------------

CREATE TABLE image (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  image_filename text NOT NULL,
  image_title text NULL,
  image_description_md text NULL,
  image_description_html text NULL,
  image_binary bytea NOT NULL
);

CREATE TABLE post_ann (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  author_id uuid NOT NULL REFERENCES player(id),
  title_md text,
  title_html text,
  content_md text,
  content_html text,
  hero_image_id uuid REFERENCES image(id),
  uri text,
  published_time timestamptz,
  is_published boolean NOT NULL GENERATED ALWAYS AS (published_time IS NOT NULL) STORED,
  created_at timestamptz NOT NULL DEFAULT now(),
  updated_at timestamptz NOT NULL DEFAULT now()
);

CREATE TABLE post_ann_to_discord (
  source_post_id uuid NOT NULL PRIMARY KEY REFERENCES post_ann(id),
  discord_msg_id numeric NOT NULL UNIQUE,
  created_at timestamptz NOT NULL DEFAULT now()
);

CREATE TABLE post_media (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  approver_id uuid NOT NULL REFERENCES player(id),
  discord_msg_id text NOT NULL UNIQUE,
  discord_user_id text NOT NULL,
  discord_name text NOT NULL,
  discord_discriminator text NOT NULL,
  discord_avatar text,
  discord_timestamp text NOT NULL,
  discord_content_md text,
  discord_content_html text,
  embed_url text,
  embed_description text,
  embed_title text,
  embed_author_name text,
  embed_author_url text,
  embed_author_icon_url text,
  embed_thumbnail_url text,
  embed_thumbnail_width integer,
  embed_thumbnail_height integer,
  embed_image_width integer,
  embed_image_height integer,
  embed_image_url text,
  embed_kind text,
  embed_video_url text,
  embed_video_width integer,
  embed_video_height integer,
  embed_provider_name text,
  embed_provider_name_url text,
  created_at timestamptz NOT NULL DEFAULT now(),
  updated_at timestamptz NOT NULL DEFAULT now()
);
