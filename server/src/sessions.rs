use futures::{ready, Future, FutureExt};
use rand::{distributions::Alphanumeric, rngs::ThreadRng, thread_rng, Rng};
use std::collections::HashMap;
use std::fmt;
use std::fmt::Display;
use std::pin::Pin;
use std::rc::Rc;
use std::sync::Arc;
use std::task::{Context, Poll};
use std::time::{Duration, Instant};
use tokio::{
    sync::{Mutex, RwLock, RwLockWriteGuard},
    task::{spawn, JoinHandle},
    task_local,
    time::{delay_queue, interval, DelayQueue},
};

use oauth2::PkceCodeVerifier;

const SESSION_ID_LEN: usize = 24;
const SESSION_MAX_CREATE_ATTEMPTS: u8 = 20;
const SESSION_TTL: Duration = Duration::from_secs(1_209_600); // 2 weeks
const SESSION_LOOP_INTERVAL: Duration = Duration::from_secs(30);

#[derive(Eq, PartialEq, Hash, Clone, Debug)]
pub struct SessionId(pub String);

impl Display for SessionId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "SessionId({})", self.0)
    }
}

#[derive(Clone, Debug)]
pub struct SessionData {
    pub pkce_code_verifier: Arc<PkceCodeVerifier>,
}

#[derive(Clone, Debug)]
pub struct Session {
    pub id: SessionId,
    pub expiry: Instant,
    pub data: SessionData,
}

pub type SessionsMap = HashMap<SessionId, (Arc<RwLock<Session>>, delay_queue::Key)>;

#[derive(Debug)]
pub struct Sessions {
    sessions: SessionsMap,
    expirations: DelayQueue<SessionId>,
}

#[derive(Clone)]
pub struct SessionManager {
    inner: Arc<Mutex<Sessions>>,
}

impl SessionManager {
    pub fn new() -> SessionManager {
        SessionManager {
            inner: Arc::new(Mutex::new(Sessions {
                sessions: HashMap::default(),
                expirations: DelayQueue::new(),
            })),
        }
    }

    pub async fn create_session(
        &self,
        data: SessionData,
        session_ttl: Option<Duration>,
    ) -> (SessionId, Arc<RwLock<Session>>) {
        let mut inner = self.inner.lock().await;
        let rng = thread_rng();
        for _attempt in 0..SESSION_MAX_CREATE_ATTEMPTS {
            let id = &SessionId(
                rng.sample_iter(&Alphanumeric)
                    .take(SESSION_ID_LEN)
                    .collect(),
            );
            if inner.sessions.get(id).is_none() {
                let ttl = session_ttl.unwrap_or(SESSION_TTL);
                let expiry_time = std::time::Instant::now() + ttl;
                let expire_key = inner
                    .expirations
                    .insert_at(id.clone(), tokio::time::Instant::from_std(expiry_time));
                let sess = Arc::new(RwLock::new(Session {
                    id: id.clone(),
                    expiry: expiry_time,
                    data,
                }));
                inner
                    .sessions
                    .insert(id.clone(), (sess.clone(), expire_key));
                return (id.clone(), sess);
            }
        }
        panic!(
            "failed to create a session after {} attempts",
            SESSION_MAX_CREATE_ATTEMPTS
        );
    }

    pub async fn to_json(&self) -> {

    }

    pub async fn print(&self) {
        let inner = self.inner.lock().await;
        println!("{:#?}", inner.sessions);
    }

    pub async fn get(&self, id: &SessionId) -> Option<Arc<RwLock<Session>>> {
        let inner = self.inner.lock().await;
        let entry = inner.sessions.get(id)?;
        let sess = entry.0.clone();
        if std::time::Instant::now() > sess.read().await.expiry {
            /*
            // XXX commented out because the purge task _should_ be doing this
            // for us.
            //
            // DelayQueue::remove can panic if the key does not exist. But
            // since we already have the lock on inner there should be no risk
            // of it being removed while "in flight"
            let dk = &entry.1.clone();
            inner.expirations.remove(dk);
            */
            None
        } else {
            Some(sess)
        }
    }

    /// WARN purge task should be doing this for you
    pub async fn expire(&self, id: &SessionId) -> Option<Arc<RwLock<Session>>> {
        let mut inner = self.inner.lock().await;
        let entry = inner.sessions.get(id)?;
        let dk = &entry.1.clone();
        let sess = entry.0.clone();
        inner.expirations.remove(dk);
        inner.sessions.remove(id);
        Some(sess)
    }

    pub async fn len(&self) -> usize {
        let inner = self.inner.lock().await;
        inner.expirations.len()
    }

    pub async fn is_alive(&self, id: &SessionId) -> bool {
        self.get(id).await.is_some()
    }

    pub fn purge_expired(&self) -> SessionManagerFuture {
        SessionManagerFuture(self.inner.clone())
    }

    pub async fn create_purge_task(&self) -> JoinHandle<()> {
        let purger = self.purge_expired();

        spawn(async move {
            task_local! {
                pub static FUT: SessionManagerFuture;
            }

            FUT.scope(purger, async move {
                let mut interval = interval(SESSION_LOOP_INTERVAL);
                let purger = FUT.with(Clone::clone);
                loop {
                    interval.tick().await;
                    match purger.clone().await {
                        Err(SessionManagerPollError::MutexLockError(_)) => {}
                        Err(x) => {
                            panic!("sessions died {:?}", x);
                        }
                        _ => {}
                    };
                }
            })
            .await;
        })
    }
}

#[derive(Clone)]
pub struct SessionManagerFuture(Arc<Mutex<Sessions>>);

#[derive(Debug)]
pub enum SessionManagerPollError {
    MutexLockError(tokio::sync::TryLockError),
    TimeError(tokio::time::Error),
}

impl From<tokio::sync::TryLockError> for SessionManagerPollError {
    fn from(x: tokio::sync::TryLockError) -> SessionManagerPollError {
        SessionManagerPollError::MutexLockError(x)
    }
}

impl From<tokio::time::Error> for SessionManagerPollError {
    fn from(x: tokio::time::Error) -> SessionManagerPollError {
        SessionManagerPollError::TimeError(x)
    }
}

impl Future for SessionManagerFuture {
    type Output = Result<u32, SessionManagerPollError>;

    fn poll(self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut expiries = 0;
        let mut inner = self.0.try_lock()?;
        while let Some(res) = ready!(inner.expirations.poll_expired(ctx)) {
            let entry = res?;
            let val = inner.sessions.remove(entry.get_ref()).unwrap();
            expiries += 1;
        }
        Poll::Ready(Ok(expiries))
    }
}
