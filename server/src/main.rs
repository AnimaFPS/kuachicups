#![feature(proc_macro_hygiene, decl_macro)]
#![feature(concat_idents)]
#![feature(box_syntax, box_patterns)]
#![feature(refcell_take)]
#![feature(bool_to_option)]
#![feature(associated_type_defaults)]
#![allow(dead_code, unused_imports)]
pub mod app;
pub mod auto_froms;
pub mod bot;
pub mod config;
pub mod db;
pub mod default_builder;
pub mod discord_markdown;
pub mod handlers;
pub mod render_markdown;

async fn server(config: config::Config) -> Result<(), app::Error> {
    let secrets = config::Secrets::generate();
    let app = app::App::create(config, secrets).await?;
    app.start().launch().await?;
    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), app::Error> {
    pretty_env_logger::init();
    let config = config::Config::create()?;
    let bot_config = config.clone();
    tokio::spawn(async move {
        bot::main(bot_config).await.expect("bot failed");
    });
    server(config).await?;
    Ok(())
}
