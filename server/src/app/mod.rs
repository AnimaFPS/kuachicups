use csrf::ChaCha20Poly1305CsrfProtection;
use oauth2;
use rocket::{http, response, routes, Request};
use rocket_contrib::serve::{Options as StaticOptions, StaticFiles};
use rocket_okapi::routes_with_openapi;
use sqlx::postgres::PgPool;
use std::path::PathBuf;
use std::sync::Arc;

pub mod discord_client;
pub use self::discord_client::DiscordClientBuilder;
use crate::auto_froms;
use crate::config::{Config, Error as ConfigError, Secrets};
use crate::db;
use crate::handlers;

const OAUTH2_AUTH_URL: &'static str = "https://discord.com/api/oauth2/authorize";
const OAUTH2_TOKEN_URL: &'static str = "https://discord.com/api/oauth2/token";

#[derive(Debug)]
pub enum Error {
    DbError(db::Error),
    MovineError(movine::errors::Error),
    ConfigError(ConfigError),
    RocketError(rocket::error::Error),
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:#?}", self)
    }
}

#[derive(Debug)]
pub enum AppReject {
    CsrfError(Option<csrf::CsrfError>),
    Base64DecodeError(data_encoding::DecodeError),
}

auto_froms!(AppReject, {
    data_encoding::DecodeError = AppReject::Base64DecodeError;
    csrf::CsrfError = |x| AppReject::CsrfError(Some(x));
});

impl std::error::Error for Error {}

impl<'r, 'o: 'r> response::Responder<'r, 'o> for AppReject {
    fn respond_to(self, _: &'r Request<'_>) -> response::Result<'o> {
        if cfg!(debug_assertions) {
            Err(http::Status::new(
                418,
                match self {
                    AppReject::CsrfError(_) => "csrf error",
                    AppReject::Base64DecodeError(_) => "base64 error",
                },
            ))
        } else {
            Err(http::Status::Forbidden)
        }
    }
}

auto_froms!(Error, {
    ConfigError = Error::ConfigError;
    movine::errors::Error = Error::MovineError;
    db::Error = Error::DbError;
    sqlx::Error = |s| Error::DbError(db::Error::SqlxError(s));
    rocket::error::Error = Error::RocketError;
});

pub struct App {
    secrets: Secrets,
    pub config: Config,
    pub pg: PgPool,
}

#[derive(Debug, Clone)]
pub struct SettingCache {
    pub index_file: PathBuf,
}

impl App {
    pub async fn create(config: Config, secrets: Secrets) -> Result<Arc<App>, Error> {
        let pg = PgPool::connect(&config.db_uri).await?;
        Ok(Arc::new(App {
            secrets,
            config,
            pg,
        }))
    }

    pub fn start(&self) -> rocket::Rocket {
        use oauth2::{basic::BasicClient, AuthUrl, ClientId, ClientSecret, RedirectUrl, TokenUrl};

        let protect = ChaCha20Poly1305CsrfProtection::from_key(self.secrets.csrf_secret);
        let settings = SettingCache {
            index_file: self.config.static_dir.to_owned().join("index.html"),
        };

        let oauth2 = BasicClient::new(
            ClientId::new(self.config.oauth2_client_id.to_owned()),
            Some(ClientSecret::new(
                self.config.oauth2_client_secret.to_owned(),
            )),
            AuthUrl::new(OAUTH2_AUTH_URL.to_string()).unwrap(),
            Some(TokenUrl::new(OAUTH2_TOKEN_URL.to_string()).unwrap()),
        )
        .set_redirect_url(
            RedirectUrl::new(self.config.discord_redirect_url.to_owned())
                .expect("Cannot create oauth2 redirect URL"),
        );

        let discord = DiscordClientBuilder::new(self.config.discord_bot_token.clone());

        let r = rocket::ignite()
            .manage(self.config.clone())
            .manage(settings)
            .manage(oauth2)
            .manage(self.pg.clone())
            .manage(protect)
            .manage(discord)
            .mount(
                "/",
                routes![
                    handlers::index,
                    handlers::client_index_anywhere,
                    handlers::logged_in,
                ],
            )
            .mount("/api", {
                use handlers::api::*;
                routes_with_openapi![
                    handlers::api::blank,
                    handlers::api::test_csrf,
                    player::current_session,
                    player::current_session_is_admin,
                    player::current_session_team_invites,
                    player::get_oauth2_challenge,
                    player::logout,
                    player::players_page,
                    player::profile,
                    player::teams,
                    posts::anns,
                    posts::anns_page,
                    posts::anns_unpublished_page,
                    posts::create_ann,
                    posts::update_ann,
                    posts::publish_ann,
                    posts::delete_ann,
                    posts::medias_page,
                    posts::medias,
                    cups::cups,
                    cups::cups_page,
                    cups::cup_stages,
                    cups::cup_stage_scorings,
                    cups::cup_signups,
                    cups::cup_signups_self,
                    cups::cup_signup_team,
                    cups::cup_signup_solo,
                    cups::cup_signup_leave,
                    cups::cup_checkin,
                    cups::cup_checkout,
                    cups::cup_create,
                    cups::cup_update,
                    cups::cup_stage_update,
                    cups::cup_stage_restart,
                    cups::cup_delete,
                    cups::cup_reset,
                    cups::cup_stage_delete,
                    cups::cup_set_seeds,
                    cups::cup_add_signup,
                    cups::cup_stage_initialise,
                    cups::cup_stage_matches_2p,
                    cups::cup_stage_pending_matches_for,
                    cups::cup_stage_pending_matches,
                    cups::cup_stage_scoring,
                    cups::cup_stage_standings,
                    cups::cup_current_stage,
                    cups::match_report_2p,
                    cups::match_override_report_2p,
                    cups::match_result_2p,
                    teams::teams,
                    teams::teams_page,
                    teams::players,
                    teams::create,
                    teams::update,
                    teams::set_member,
                    teams::override_member,
                    teams::invites,
                    teams::make_invite,
                    teams::accept_invite,
                    teams::reject_invite,
                    teams::leave,
                    game::games,
                    game::games_page,
                    game::game_modes,
                    game::game_mode_by_id,
                ]
            });

        if self.config.static_enabled {
            r.mount(
                "/static",
                StaticFiles::new(self.config.static_dir.clone(), StaticOptions::None).rank(20),
            )
        } else {
            r
        }
    }
}
