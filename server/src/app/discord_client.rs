use twilight_http::{client::Client, ratelimiting::Ratelimiter, Error};

pub type DiscordClient = Client;
pub type DiscordError = Error;

#[derive(Debug, Clone)]
pub struct DiscordClientBuilder {
    ratelimiter: Ratelimiter,
    reqwest: reqwest::Client,
    bot_token: String,
}

impl DiscordClientBuilder {
    pub fn new(bot_token: String) -> Self {
        DiscordClientBuilder {
            ratelimiter: Ratelimiter::new(),
            reqwest: reqwest::Client::new(),
            bot_token,
        }
    }

    pub fn client_bearer(&self, oauth2_bearer_token: &str) -> Result<DiscordClient, DiscordError> {
        Client::builder()
            .token(format!("Bearer {}", oauth2_bearer_token))
            .reqwest_client(self.reqwest.clone())
            .ratelimiter(self.ratelimiter.clone())
            .build()
    }

    pub fn client(&self) -> Result<DiscordClient, DiscordError> {
        Client::builder()
            .token(&self.bot_token)
            .reqwest_client(self.reqwest.clone())
            .ratelimiter(self.ratelimiter.clone())
            .build()
    }
}
