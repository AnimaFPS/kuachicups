use chrono::{DateTime, TimeZone, Utc};
use schemars::{gen::SchemaGenerator, schema::Schema, JsonSchema};
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use uuid::Uuid;

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(remote = "Uuid")]
pub struct UuidDef(
    #[serde(serialize_with = "u128_to_uuid")]
    #[serde(deserialize_with = "uuid_from_str")]
    #[serde(getter = "uuid_to_u128")]
    u128,
);

impl JsonSchema for UuidDef {
    fn schema_name() -> String {
        Uuid::schema_name()
    }
    fn json_schema(gen: &mut SchemaGenerator) -> Schema {
        Uuid::json_schema(gen)
    }
}

impl From<UuidDef> for Uuid {
    fn from(uuid: UuidDef) -> Uuid {
        Uuid::from_u128(uuid.0)
    }
}

fn uuid_to_u128(uuid: &Uuid) -> u128 {
    uuid.as_u128()
}

fn u128_to_uuid<S: Serializer>(
    uuid: &u128,
    serializer: S,
) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error> {
    Uuid::serialize(&Uuid::from_u128(*uuid), serializer)
}

fn uuid_from_str<'de, D>(deserializer: D) -> Result<u128, D::Error>
where
    D: Deserializer<'de>,
{
    Uuid::deserialize(deserializer).map(|u| u.as_u128())
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(remote = "Option::<Uuid>")]
pub struct OptUuidDef(
    #[serde(serialize_with = "opt_u128_to_uuid")]
    #[serde(deserialize_with = "uuid_from_opt_str")]
    #[serde(getter = "opt_uuid_to_u128")]
    Option<u128>,
);

impl JsonSchema for OptUuidDef {
    fn schema_name() -> String {
        Option::<Uuid>::schema_name()
    }
    fn json_schema(gen: &mut SchemaGenerator) -> Schema {
        Option::<Uuid>::json_schema(gen)
    }
}

impl From<OptUuidDef> for Option<Uuid> {
    fn from(opt: OptUuidDef) -> Option<Uuid> {
        opt.0.map(|u| UuidDef(u).into())
    }
}

fn opt_uuid_to_u128(opt: &Option<Uuid>) -> Option<u128> {
    opt.map(|uuid| uuid_to_u128(&uuid))
}

fn opt_u128_to_uuid<S: Serializer>(
    uuid: &Option<u128>,
    serializer: S,
) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error> {
    Option::<Uuid>::serialize(&uuid.map(Uuid::from_u128), serializer)
}

fn uuid_from_opt_str<'de, D>(deserializer: D) -> Result<Option<u128>, D::Error>
where
    D: Deserializer<'de>,
{
    Option::<Uuid>::deserialize(deserializer).map(|u| u.map(|u| u.as_u128()))
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(remote = "Vec::<Uuid>")]
pub struct VecUuidDef(
    #[serde(getter = "vec_uuid_to_u128")]
    #[serde(serialize_with = "vec_u128_to_uuid")]
    Vec<u128>,
);

impl JsonSchema for VecUuidDef {
    fn schema_name() -> String {
        Vec::<Uuid>::schema_name()
    }

    fn json_schema(gen: &mut SchemaGenerator) -> Schema {
        Vec::<Uuid>::json_schema(gen)
    }
}

impl From<VecUuidDef> for Vec<Uuid> {
    fn from(opt: VecUuidDef) -> Vec<Uuid> {
        opt.0.iter().map(|u| Uuid::from_u128(*u)).collect()
    }
}

fn vec_u128_to_uuid<S: Serializer>(
    uuid: &Vec<u128>,
    serializer: S,
) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error> {
    Vec::<Uuid>::serialize(
        &uuid.iter().map(|u| Uuid::from_u128(*u)).collect(),
        serializer,
    )
}

fn vec_uuid_to_u128(opt: &Vec<Uuid>) -> Vec<u128> {
    opt.iter().map(|uuid| uuid_to_u128(&uuid)).collect()
}

fn uuid_from_vec_str<'de, D>(deserializer: D) -> Result<Vec<u128>, D::Error>
where
    D: Deserializer<'de>,
{
    Vec::<Uuid>::deserialize(deserializer).map(|u| u.iter().map(|u| u.as_u128()).collect())
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(remote = "DateTime::<Utc>")]
pub struct DateTimeUtcDef(
    #[serde(serialize_with = "timestampms_to_dt")]
    #[serde(deserialize_with = "timestampms_from_dt")]
    #[serde(getter = "DateTime::timestamp_millis")]
    pub i64,
);

impl JsonSchema for DateTimeUtcDef {
    fn schema_name() -> String {
        String::schema_name()
    }

    fn json_schema(gen: &mut SchemaGenerator) -> Schema {
        String::json_schema(gen)
    }
}

fn timestampms_to_dt<S: Serializer>(
    dt: &i64,
    serializer: S,
) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error> {
    DateTime::<Utc>::serialize(&Utc.timestamp_millis(*dt), serializer)
}

fn timestampms_from_dt<'de, D>(deserializer: D) -> Result<i64, D::Error>
where
    D: Deserializer<'de>,
{
    DateTime::<Utc>::deserialize(deserializer).map(|utc| utc.timestamp_millis())
}

impl From<DateTimeUtcDef> for DateTime<Utc> {
    fn from(dt: DateTimeUtcDef) -> DateTime<Utc> {
        Utc.timestamp_millis(dt.0)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(remote = "Option::<DateTime::<Utc>>")]
pub struct OptDateTimeUtcDef(
    #[serde(serialize_with = "opt_timestampms_to_dt")]
    #[serde(deserialize_with = "opt_timestampms_from_dt")]
    #[serde(getter = "opt_timestamp_millis")]
    pub Option<i64>,
);

impl JsonSchema for OptDateTimeUtcDef {
    fn schema_name() -> String {
        Option::<String>::schema_name()
    }

    fn json_schema(gen: &mut SchemaGenerator) -> Schema {
        Option::<String>::json_schema(gen)
    }
}

fn opt_timestampms_to_dt<S: Serializer>(
    optms: &Option<i64>,
    serializer: S,
) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error> {
    Option::<DateTime<Utc>>::serialize(
        &(match optms {
            Some(ms) => Some(Utc.timestamp_millis(*ms)),
            None => None,
        }),
        serializer,
    )
}

fn opt_timestampms_from_dt<'de, D>(deserializer: D) -> Result<Option<i64>, D::Error>
where
    D: Deserializer<'de>,
{
    Option::<DateTime<Utc>>::deserialize(deserializer)
        .map(|optutc: Option<DateTime<Utc>>| optutc.map(|utc| utc.timestamp_millis()))
}

impl From<OptDateTimeUtcDef> for Option<DateTime<Utc>> {
    fn from(dt: OptDateTimeUtcDef) -> Option<DateTime<Utc>> {
        dt.0.map(|t| Utc.timestamp_millis(t))
    }
}

fn opt_timestamp_millis(opt: &Option<DateTime<Utc>>) -> Option<i64> {
    match opt {
        Some(dt) => Some(DateTime::timestamp_millis(&dt)),
        None => None,
    }
}
