use crate::db::{paged1, ById, Error, Page, PageItem, Paged, Paginated, Result};
use crate::{by_id_impl, paginated_impl};
use chrono::{DateTime, Utc};
use derive_builder::Builder;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::query_as;
use uuid::Uuid;

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct PostAnn {
    pub id: Uuid,
    pub author_id: Uuid,
    pub title_md: Option<String>,
    pub title_html: Option<String>,
    pub content_md: Option<String>,
    pub content_html: Option<String>,
    pub hero_image_id: Option<Uuid>,
    pub uri: Option<String>,
    pub published_time: Option<DateTime<Utc>>,
    pub is_published: bool,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct PostAnnUnpublished(pub PostAnn);

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct PostMedia {
    pub id: Uuid,
    pub approver_id: Uuid,
    pub discord_msg_id: String,
    pub discord_user_id: String,
    pub discord_name: String,
    pub discord_discriminator: String,
    pub discord_avatar: Option<String>,
    pub discord_timestamp: String,
    pub discord_content_md: Option<String>,
    pub discord_content_html: Option<String>,
    pub embed_url: Option<String>,
    pub embed_description: Option<String>,
    pub embed_title: Option<String>,
    pub embed_author_name: Option<String>,
    pub embed_author_url: Option<String>,
    pub embed_author_icon_url: Option<String>,
    pub embed_thumbnail_url: Option<String>,
    pub embed_thumbnail_width: Option<i32>,
    pub embed_thumbnail_height: Option<i32>,
    pub embed_image_width: Option<i32>,
    pub embed_image_height: Option<i32>,
    pub embed_image_url: Option<String>,
    pub embed_kind: Option<String>,
    pub embed_video_url: Option<String>,
    pub embed_video_width: Option<i32>,
    pub embed_video_height: Option<i32>,
    pub embed_provider_name: Option<String>,
    pub embed_provider_name_url: Option<String>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

by_id_impl!(PostAnn, "post_ann", Uuid);
by_id_impl!(PostMedia, "post_media", Uuid);
paginated_impl!(PostMedia, "post_media", 16);

#[async_trait::async_trait]
impl ById for PostAnnUnpublished {
    type Id = Uuid;
    fn id(&self) -> Self::Id {
        self.0.id()
    }

    async fn by_id<'e, E: sqlx::Executor<'e, Database = sqlx::Postgres>>(
        db: E,
        id: Self::Id,
    ) -> Result<Self> {
        Ok(PostAnnUnpublished(PostAnn::by_id(db, id).await?))
    }

    async fn by_ids<'e, E: sqlx::Executor<'e, Database = sqlx::Postgres>>(
        db: E,
        ids: &[Self::Id],
    ) -> Result<Vec<Self>> {
        Ok(PostAnn::by_ids(db, ids)
            .await?
            .iter()
            .map(|post| PostAnnUnpublished(post.clone()))
            .collect())
    }
}

#[async_trait::async_trait]
impl Paginated for PostAnnUnpublished {
    fn page_len() -> usize {
        16
    }

    async fn all<'e, E: sqlx::Executor<'e, Database = sqlx::Postgres>>(db: E) -> Result<Vec<Self>> {
        Ok(
            query_as!(PostAnn, "SELECT * FROM post_ann WHERE NOT is_published",)
                .fetch_all(db)
                .await?
                .iter()
                .map(|post| PostAnnUnpublished(post.clone()))
                .collect(),
        )
    }

    async fn page<'e, E: sqlx::Executor<'e, Database = sqlx::Postgres>>(
        db: E,
        page: Page,
    ) -> Result<Paged<<Self as ById>::Id>> {
        let rows: Vec<PageItem<<Self as ById>::Id>> = query_as!(
            PageItem,
            r#"
                SELECT id, updated_at
                FROM post_ann
                WHERE NOT is_published
                ORDER BY created_at DESC OFFSET $1 LIMIT $2
            "#,
            (Self::page_len() as i64) * page.0,
            (Self::page_len() as i64) + 1,
        )
        .fetch_all(db)
        .await?;
        Ok(paged1::<<Self as ById>::Id>(rows, page, Self::page_len()))
    }
}

/// Retrieves _published_ announcements
#[async_trait::async_trait]
impl Paginated for PostAnn {
    fn page_len() -> usize {
        16
    }

    async fn all<'e, E: sqlx::Executor<'e, Database = sqlx::Postgres>>(db: E) -> Result<Vec<Self>> {
        Ok(
            query_as!(PostAnn, "SELECT * FROM post_ann WHERE is_published")
                .fetch_all(db)
                .await?,
        )
    }

    async fn page<'e, E: sqlx::Executor<'e, Database = sqlx::Postgres>>(
        db: E,
        page: Page,
    ) -> Result<Paged<<Self as ById>::Id>> {
        let rows: Vec<PageItem<<Self as ById>::Id>> = query_as!(
            PageItem,
            r#"
                SELECT id, updated_at
                FROM post_ann
                WHERE is_published
                ORDER BY published_time DESC OFFSET $1 LIMIT $2
            "#,
            (Self::page_len() as i64) * page.0,
            (Self::page_len() as i64) + 1,
        )
        .fetch_all(db)
        .await?;
        Ok(paged1::<<Self as ById>::Id>(rows, page, Self::page_len()))
    }
}
