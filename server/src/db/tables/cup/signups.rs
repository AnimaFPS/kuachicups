use super::model::{Cup, CupSignup};
use crate::db::{Executor, Result, DB};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, Done};
use uuid::Uuid;

/// Queries
///
///

impl Cup {
    /// Get all the signups ordered by their seed value
    pub async fn get_all_signups(db: impl Executor<'_>, cup_id: Uuid) -> Result<Vec<CupSignup>> {
        Ok(query_as!(
            CupSignup,
            "
                SELECT * FROM cup_signup
                WHERE cup_id = $1
                ORDER BY seed_value ASC NULLS LAST
            ",
            cup_id,
        )
        .fetch_all(db)
        .await?)
    }

    /// Get all checked-in signups, ordered by their seed value
    pub async fn get_checkedin_signups(
        db: impl Executor<'_>,
        cup_id: Uuid,
    ) -> Result<Vec<CupSignup>> {
        Ok(query_as!(
            CupSignup,
            "
                SELECT * FROM cup_signup
                WHERE cup_id = $1 AND checked_in
                ORDER BY seed_value ASC NULLS LAST
            ",
            cup_id,
        )
        .fetch_all(db)
        .await?)
    }

    pub async fn make_signup_solo(db: &DB, cup_id: Uuid, player_id: Uuid) -> Result<bool> {
        let mut transaction = db.begin().await?;
        Cup::require_signup_available(&mut transaction, cup_id).await?;
        Cup::require_solo_entrant_type(&mut transaction, cup_id).await?;
        let result = query!(
            "INSERT INTO cup_signup(cup_id, player_id) VALUES($1, $2)",
            cup_id,
            player_id,
        )
        .execute(&mut transaction)
        .await?
        .rows_affected()
            > 0;
        transaction.commit().await?;
        Ok(result)
    }

    pub async fn make_signup_solo_under_team(
        db: &DB,
        cup_id: Uuid,
        player_id: Uuid,
        team_id: Uuid,
    ) -> Result<bool> {
        let mut transaction = db.begin().await?;
        Cup::require_signup_available(&mut transaction, cup_id).await?;
        Cup::require_solo_entrant_type(&mut transaction, cup_id).await?;
        let result = query!(
            "INSERT INTO cup_signup(cup_id, player_id, team_id) VALUES($1, $2, $3)",
            cup_id,
            player_id,
            team_id,
        )
        .execute(&mut transaction)
        .await?
        .rows_affected()
            > 0;
        transaction.commit().await?;
        Ok(result)
    }

    pub async fn make_signup_team(
        db: &DB,
        cup_id: Uuid,
        player_id: Uuid,
        team_id: Uuid,
    ) -> Result<bool> {
        let mut transaction = db.begin().await?;
        Cup::require_signup_available(&mut transaction, cup_id).await?;
        Cup::require_team_entrant_type(&mut transaction, cup_id).await?;
        let result = query!(
            "INSERT INTO cup_signup(cup_id, player_id, team_id) VALUES($1, $2, $3)",
            cup_id,
            player_id,
            team_id,
        )
        .execute(&mut transaction)
        .await?
        .rows_affected()
            > 0;
        transaction.commit().await?;
        Ok(result)
    }

    pub async fn player_leave(db: &DB, cup_id: Uuid, player_id: Uuid) -> Result<bool> {
        let mut transaction = db.begin().await?;
        let result = query!(
            "DELETE FROM cup_signup WHERE cup_id = $1 AND player_id = $2",
            cup_id,
            player_id,
        )
        .execute(&mut transaction)
        .await?
        .rows_affected()
            > 0;
        transaction.commit().await?;
        Ok(result)
    }

    pub async fn player_checkin(db: &DB, cup_id: Uuid, player_id: Uuid) -> Result<bool> {
        let mut transaction = db.begin().await?;
        Cup::require_checkin_available(&mut transaction, cup_id).await?;
        let result = query!(
            "
            UPDATE cup_signup
            SET checkedin_time = now()
            WHERE cup_id = $1 AND player_id = $2 ",
            cup_id,
            player_id,
        )
        .execute(&mut transaction)
        .await?
        .rows_affected()
            > 0;
        transaction.commit().await?;
        Ok(result)
    }

    pub async fn player_checkout(db: &DB, cup_id: Uuid, player_id: Uuid) -> Result<bool> {
        let mut transaction = db.begin().await?;
        Cup::require_checkin_available(&mut transaction, cup_id).await?;
        let result = query!(
            "
            UPDATE cup_signup
            SET checkedin_time = NULL
            WHERE cup_id = $1 AND player_id = $2 ",
            cup_id,
            player_id,
        )
        .execute(&mut transaction)
        .await?
        .rows_affected()
            > 0;
        transaction.commit().await?;
        Ok(result)
    }

    pub async fn player_signups_self(
        db: impl Executor<'_>,
        cup_id: Uuid,
        player_id: Uuid,
    ) -> Result<Vec<CupSignup>> {
        Ok(query_as!(
            CupSignup,
            "SELECT * FROM cup_signup WHERE cup_id = $1 AND player_id = $2",
            cup_id,
            player_id,
        )
        .fetch_all(db)
        .await?)
    }

    pub async fn set_seeds(db: &DB, seeds: Vec<SetSeed>) -> Result<bool> {
        for seed in seeds {
            if Some(true) == seed.drop {
                query!("DELETE FROM cup_signup WHERE id = $1", seed.signup_id)
                    .execute(db)
                    .await?;
            } else {
                query!(
                    r#"
                        UPDATE cup_signup
                        SET
                            seed_value = $1,
                            checkedin_time = (CASE
                                WHEN ($3 IS true) AND (checkedin_time IS NULL) THEN now()
                                WHEN ($3 IS false) AND (checkedin_time IS NOT NULL) THEN null
                                ELSE checkedin_time
                            END)
                        WHERE id = $2
                    "#,
                    seed.seed_value,
                    seed.signup_id,
                    seed.checkin
                )
                .execute(db)
                .await?;
            }
        }
        Ok(true)
    }

    pub async fn add_signup(db: &DB, cup_id: Uuid, add: AddSignup) -> Result<bool> {
        query!(
            "
            INSERT INTO cup_signup(cup_id, player_id, team_id, seed_value, checkedin_time)
            VALUES($1, $2, $3, $4, CASE
                WHEN ($5 IS true) THEN now()
                WHEN ($5 IS false) THEN null
                ELSE null
            END) ",
            cup_id,
            add.player_id,
            add.team_id,
            add.seed_value,
            add.checkin
        )
        .execute(db)
        .await?;
        Ok(true)
    }
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub struct SetSeed {
    signup_id: Uuid,
    checkin: Option<bool>,
    drop: Option<bool>,
    seed_value: Option<i32>,
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub struct AddSignup {
    player_id: Uuid,
    team_id: Option<Uuid>,
    seed_value: Option<i32>,
    checkin: Option<bool>,
}
