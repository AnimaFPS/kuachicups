use super::super::model::{
    Cup, CupMatch2p, CupMatch2pRow, CupSignup, CupStage, CupStageFormat, ElimMatchType,
    GroupPointAward,
};
use crate::db::{ById, Error, Executor, JsonRow, JsonToDbResult, Result, DB};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, Acquire, Postgres, Transaction};
use std::cmp::max;
use std::collections::HashMap;
use uuid::Uuid;

#[derive(Clone, Serialize, Deserialize, JsonSchema)]
pub struct Ranking {
    pub signup: CupSignup,
    pub score: i32,
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub struct Standings {
    pub rankings: Vec<Ranking>,
    pub is_complete: bool,
}

impl Cup {
    pub async fn current_stage(cup_id: Uuid, db: impl Executor<'_> + Copy) -> Result<Option<Uuid>> {
        let r = query!(
            "
            SELECT s.id AS cup_stage_id
            FROM cup c JOIN cup_stage s ON (c.id = s.cup_id AND c.current_stage = s.stage_no)
            WHERE c.id = $1",
            cup_id,
        )
        .fetch_optional(db)
        .await?;
        match r {
            Some(r) => Ok(Some(r.cup_stage_id)),
            None => Ok(None),
        }
    }
}

impl CupStage {
    pub async fn current(cup_id: Uuid, db: impl Executor<'_> + Copy) -> Result<Option<CupStage>> {
        let stage_id = Cup::current_stage(cup_id, db).await?;
        match stage_id {
            Some(stage_id) => Ok(Some(CupStage::by_id(db, stage_id).await?)),
            None => Ok(None),
        }
    }

    pub async fn next(&self, db: impl Executor<'_> + Copy) -> Result<Option<CupStage>> {
        // TODO this could be much simpler if postgres enums are supported by sqlx
        let r = query!(
            "SELECT id FROM cup_stage WHERE cup_id = $1 AND stage_no = $2",
            self.cup_id,
            self.stage_no + 1,
        )
        .fetch_optional(db)
        .await?;
        match r {
            Some(r) => Ok(Some(CupStage::by_id(db, r.id).await?)),
            None => Ok(None),
        }
    }

    pub async fn is_complete(db: impl Executor<'_>, stage_id: Uuid) -> Result<bool> {
        let all_is_scored = query!(
            "
            SELECT bool_and(m.is_scored) AS val
            FROM cup_match_2p m
            WHERE m.cup_stage_id = $1 ",
            stage_id
        )
        .fetch_one(db)
        .await?;
        Ok(all_is_scored.val == Some(true))
    }

    pub async fn standings(&self, db: impl Executor<'_> + Copy) -> Result<Standings> {
        let matches = query_as!(
            CupMatch2pRow,
            "SELECT * FROM cup_match_2p WHERE cup_stage_id = $1",
            self.id
        )
        .fetch_all(db)
        .await?
        .iter()
        .map(|m| CupMatch2p::from_row(m))
        .collect::<Vec<_>>();

        let is_complete = matches.iter().all(|m| m.is_scored);

        let signups_by_id: HashMap<Uuid, CupSignup> = {
            let signups = Cup::get_checkedin_signups(db, self.cup_id).await?;
            let mut r = HashMap::new();
            for cs in signups {
                r.insert(cs.id, cs);
            }
            r
        };

        log::info!("matches = {:#?}", matches);

        let mut rankings: Vec<(&CupSignup, i32)> = match self.format {
            CupStageFormat::Groups => {
                struct SignupInfo {
                    losses: i32,
                    wins: i32,
                    group_no: i32,
                    seed: i32,
                }
                let mut infos: HashMap<Uuid, SignupInfo> = HashMap::new();
                for m in matches {
                    let group_no = m
                        .group_no
                        .expect("no group_no for match in groups format stage");
                    if let Some(winner_id) = m.winner_id {
                        let signup = signups_by_id
                            .get(&winner_id)
                            .expect("no signup for winner in groups");
                        let winner_info = infos.entry(winner_id).or_insert(SignupInfo {
                            losses: 0,
                            wins: 0,
                            group_no,
                            seed: signup.seed_value.unwrap_or(signups_by_id.len() as i32),
                        });
                        winner_info.wins += 1;
                    }
                    // there might not be a loser in case of byes
                    if let Some(loser_id) = m.loser_id {
                        let signup = signups_by_id
                            .get(&loser_id)
                            .expect("no signup for loser in groups");
                        let loser_info = infos.entry(loser_id).or_insert(SignupInfo {
                            losses: 0,
                            wins: 0,
                            group_no,
                            seed: signup.seed_value.unwrap_or(signups_by_id.len() as i32),
                        });
                        loser_info.losses += 1;
                    }
                }
                let mut infos = infos.iter().collect::<Vec<(&Uuid, &SignupInfo)>>();
                infos.sort_by_key(|(_uuid, info)| {
                    (std::cmp::Reverse(info.wins), info.group_no, info.seed)
                });
                infos
                    .iter()
                    .map(|(uuid, info)| (signups_by_id.get(uuid).expect("no signup"), info.wins))
                    .collect()
            }

            CupStageFormat::SingleElim => {
                #[derive(Clone)]
                struct ElimStatus {
                    last_round_won: Option<(i32, i32)>,
                    last_round_lost: Option<(i32, i32)>,
                }
                let default_elim_status = ElimStatus {
                    last_round_won: None,
                    last_round_lost: None,
                };
                let mut signup_to_status: HashMap<Uuid, ElimStatus> = HashMap::new();
                for m in matches {
                    let round_no = m
                        .elim_round
                        .expect("no elim_round for match in single elim format stage");
                    let round_index = m
                        .elim_index
                        .expect("no elim_index for match in single elim format stage");
                    let round_key = (round_no, -round_index);
                    if let Some(winner_id) = m.winner_id {
                        let entry = signup_to_status
                            .entry(winner_id)
                            .or_insert(default_elim_status.clone());
                        entry.last_round_won = Some(
                            entry
                                .last_round_won
                                .map_or(round_key, |r| max(r, round_key)),
                        );
                    }
                    if let Some(loser_id) = m.loser_id {
                        let entry = signup_to_status
                            .entry(loser_id)
                            .or_insert(default_elim_status.clone());
                        entry.last_round_lost = Some(
                            entry
                                .last_round_lost
                                .map_or(round_key, |r| max(r, round_key)),
                        );
                    }
                }
                signup_to_status
                    .iter()
                    .map(|(uuid, status)| {
                        (
                            signups_by_id.get(uuid).expect("no signup"),
                            status.last_round_won.unwrap_or((0, 0)).0,
                        )
                    })
                    .collect()
            }
            CupStageFormat::DoubleElim => {
                #[derive(Clone)]
                struct ElimStatusDE {
                    last_ub_round_won: Option<(i32, i32)>,
                    last_ub_round_lost: Option<(i32, i32)>,
                    last_lb_round_won: Option<(i32, i32)>,
                    last_lb_round_lost: Option<(i32, i32)>,
                }
                let default_elim_status = ElimStatusDE {
                    last_ub_round_won: None,
                    last_ub_round_lost: None,
                    last_lb_round_won: None,
                    last_lb_round_lost: None,
                };
                let mut signup_to_status: HashMap<Uuid, ElimStatusDE> = HashMap::new();
                for m in matches {
                    let round_no = m.elim_round.expect("no elim_round in elim match");
                    let round_index = m
                        .elim_index
                        .expect("no elim_index for match in double elim format stage");
                    let round_key = (round_no, round_index);
                    let is_lb = m.elim_type == Some(ElimMatchType::LB);
                    if let Some(winner_id) = m.winner_id {
                        let entry = signup_to_status
                            .entry(winner_id)
                            .or_insert(default_elim_status.clone());
                        if is_lb {
                            entry.last_lb_round_won = Some(
                                entry
                                    .last_lb_round_won
                                    .map_or(round_key, |r| max(r, round_key)),
                            );
                        } else {
                            entry.last_ub_round_won = Some(
                                entry
                                    .last_ub_round_won
                                    .map_or(round_key, |r| max(r, round_key)),
                            );
                        }
                    }
                    if let Some(loser_id) = m.loser_id {
                        let entry = signup_to_status
                            .entry(loser_id)
                            .or_insert(default_elim_status.clone());
                        if is_lb {
                            entry.last_lb_round_lost = Some(
                                entry
                                    .last_lb_round_lost
                                    .map_or(round_key, |r| max(r, round_key)),
                            );
                        } else {
                            entry.last_ub_round_lost = Some(
                                entry
                                    .last_ub_round_lost
                                    .map_or(round_key, |r| max(r, round_key)),
                            );
                        }
                    }
                }

                signup_to_status
                    .iter()
                    .map(|(uuid, status)| {
                        (
                            uuid,
                            max(
                                status.last_ub_round_won,
                                // TODO ensure this is accurate TODO this hinges
                                // on the lower bracket trees being balanced,
                                // but right now they are not!!
                                status.last_lb_round_won.map(|r| (r.0 / 2, r.1)),
                            ),
                        )
                    })
                    .map(|(uuid, round)| {
                        (
                            signups_by_id.get(uuid).expect("no signup"),
                            round.unwrap_or((-1, 0)).0 + 1,
                        )
                    })
                    .collect()
            }
            _ => vec![],
        };
        rankings.sort_by_key(|(signup, score)| {
            let signup = signups_by_id.get(&signup.id).expect("no such signup");
            (std::cmp::Reverse(*score), signup.seed_value, signup.id)
        });
        log::info!("rankings = {:#?}", rankings);
        Ok(Standings {
            is_complete,
            rankings: rankings
                .iter()
                .map(|(cs, score)| Ranking {
                    signup: (**cs).clone(),
                    score: *score,
                })
                .collect(),
        })
    }

    /*
        let signup = signups_by_id
            .get(uuid)
            .expect(&format!("no such signup {}", uuid));
    */

    pub async fn advance(cup_stage_id: Uuid, db: &DB) -> Result<()> {
        let is_complete = CupStage::is_complete(db, cup_stage_id).await?;
        if is_complete {
            let stage = CupStage::by_id(db, cup_stage_id).await?;
            let next = stage.next(db).await?;
            if let Some(next) = next {
                let standings = stage.standings(db).await?;
                if standings.is_complete != is_complete {
                    log::error!("mismatched is_complete state!");
                    return Err(Error::SomeError("mismatched is_complete state"));
                }
                if next.can_start() {
                    CupStage::start_stage(db, next.cup_id, next.id).await?;
                }
            }
        }
        Ok(())
    }
}
