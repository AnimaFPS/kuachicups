use super::super::{CupStage, CupStageScoring};
use crate::db::{Error, Executor, Result};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, Postgres, Transaction};
use uuid::Uuid;

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct CupStageScoringForm {
    pub elim_round: Option<i32>,
    pub is_lb: Option<bool>,
    pub groups: bool,
    pub bestof: i32,
    pub is_point_score: Option<bool>,
    pub point_score_greatest_is_winner: Option<bool>,
    pub is_time_score: Option<bool>,
    pub is_time_score_race: Option<bool>,
}

impl CupStage {
    pub async fn set_scoring_config(
        cup_stage_id: Uuid,
        scorings: &Vec<CupStageScoringForm>,
        mut db: Transaction<'_, Postgres>,
    ) -> Result<()> {
        // first unlink any existing scorings. this means any matches already
        // made on the stage are not going to have their scoring overridden
        query!(
            "UPDATE cup_stage_scoring SET cup_stage_id = null WHERE cup_stage_id = $1",
            cup_stage_id
        )
        .execute(&mut db)
        .await?;
        for scoring in scorings {
            query!(
                "
                INSERT INTO cup_stage_scoring(
                    cup_stage_id,
                    elim_round,
                    is_lb,
                    groups,
                    bestof,
                    is_point_score,
                    point_score_greatest_is_winner,
                    is_time_score,
                    is_time_score_race)
                VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
                ",
                cup_stage_id,
                scoring.elim_round,
                scoring.is_lb,
                scoring.groups,
                scoring.bestof,
                scoring.is_point_score,
                scoring.point_score_greatest_is_winner,
                scoring.is_time_score,
                scoring.is_time_score_race,
            )
            .execute(&mut db)
            .await?;
        }
        db.commit().await?;
        Ok(())
    }

    pub async fn scoring_elim(
        &self,
        elim_round: i32,
        is_lb: bool,
        db: impl Executor<'_> + Copy,
    ) -> Result<CupStageScoring> {
        if query!(
            "
            SELECT format = 'single_elim' OR format = 'double_elim' AS is_elim
            FROM cup_stage
            WHERE id = $1",
            self.id,
        )
        .fetch_one(db)
        .await?
        .is_elim
            != Some(true)
        {
            return Err(Error::SomeError("not a elim bracket"));
        }
        let scoring = query_as!(
            CupStageScoring,
            "
            SELECT *
            FROM cup_stage_scoring
            WHERE cup_stage_id = $1 AND elim_round <= $2 AND is_lb = $3
            ORDER BY elim_round DESC
            LIMIT 1 ",
            self.id,
            elim_round,
            is_lb,
        )
        .fetch_one(db)
        .await?;
        log::info!("got scoring_elim = {:#?}", scoring);
        Ok(scoring)
    }

    pub async fn scoring_group(&self, db: impl Executor<'_> + Copy) -> Result<CupStageScoring> {
        if query!(
            "
            SELECT format = 'groups' AS is_groups
            FROM cup_stage
            WHERE id = $1",
            self.id,
        )
        .fetch_one(db)
        .await?
        .is_groups
            != Some(true)
        {
            return Err(Error::SomeError("not a group bracket"));
        }
        let scoring = query_as!(
            CupStageScoring,
            "
            SELECT *
            FROM cup_stage_scoring
            WHERE cup_stage_id = $1 AND groups = true
            LIMIT 1",
            self.id,
        )
        .fetch_one(db)
        .await?;
        Ok(scoring)
    }
}
