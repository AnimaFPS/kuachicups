// Node algorithms inspired by 'Double-Elimination Tournaments: Counting and
// Calculating' https://www.jstor.org/stable/2685040
use super::super::model::ElimMatchType as MatchType;
use itertools::Itertools;
use recur_fn::{recur_fn, RecurFn};
use serde::{Deserialize, Serialize};
use std::cell::RefCell;
use std::cmp::max;
use std::iter::{empty, once, repeat, Iterator};
use std::rc::Rc;
use std::sync::{Arc, RwLock};

#[derive(Debug, Clone, Copy)]
pub enum LinkFun {
    Reverse,
    Swap,
}

impl LinkFun {
    pub fn swap<T>(mut top: Vec<T>) -> (Vec<T>, Vec<T>) {
        let k = top.len();
        assert!(k.is_power_of_two());
        let bot = top.split_off(k / 2);
        (bot, top)
    }

    pub fn reverse<T>(mut top: Vec<T>) -> (Vec<T>, Vec<T>) {
        let k = top.len();
        assert!(k.is_power_of_two());
        let mut bot = top.split_off(k / 2);
        top.reverse();
        bot.reverse();
        (top, bot)
    }

    pub fn interpret<T>(input: Vec<T>, commands: &[LinkFun]) -> Vec<T> {
        let mut stack: Vec<(usize, Vec<T>)> = Vec::new();
        let mut output: Vec<T> = Vec::with_capacity(input.len());
        // log::info!("input = {:?}, commands = {:?}", input, commands);
        // assert!(commands.len() <= ((input.len() as f32).log2().floor() as usize));
        stack.push((0, input));
        while let Some((i, mut input)) = stack.pop() {
            if i < commands.len() && input.len() > 1 {
                let cmd = commands[i];
                match cmd {
                    LinkFun::Reverse => {
                        let (left, right) = LinkFun::reverse(input);
                        stack.push((i + 1, right));
                        stack.push((i + 1, left));
                    }
                    LinkFun::Swap => {
                        let (left, right) = LinkFun::swap(input);
                        stack.push((i + 1, right));
                        stack.push((i + 1, left));
                    }
                }
            } else {
                output.append(&mut input);
            }
        }
        output
    }

    /// The ith string in the language of strings lexicographically sorted
    /// P={s,r}^(log(n))
    pub fn lexi_pattern(num_rounds: u32, i: u32, l: u32) -> Vec<LinkFun> {
        let len = num_rounds - l;
        vec![LinkFun::Reverse, LinkFun::Swap]
            .iter()
            .cloned()
            .permutations(len as usize)
            .nth(i as usize)
            .expect("no ith item")
    }

    pub fn optimal_pattern(round: u32, num_rounds: u32) -> Vec<LinkFun> {
        let l = ((num_rounds as f32) - (num_rounds as f32).log2()).floor() as u32;
        if round < l {
            LinkFun::lexi_pattern(num_rounds, round, l)
        } else {
            let len = num_rounds - round;
            repeat(LinkFun::Swap).take(len as usize).collect()
        }
    }

    pub fn optimal_linking<T>(round: u32, num_rounds: u32, input: Vec<T>) -> Vec<T> {
        let pat = LinkFun::optimal_pattern(round, num_rounds);
        LinkFun::interpret(input, &pat)
    }
}

#[derive(Clone, Debug)]
pub enum Contestent {
    Seed {
        /** NOTE this is *not* the seed value -- the seed values are used to
         * sort players, and their rank is their index in the final sorted Vec
         * of players */
        rank: u32,
    },
    /** The loser of this specific match */
    Loser(Arc<RwLock<Node>>),
}

/** A tournament structure for either single or double elimination */
#[derive(Clone, Debug)]
pub enum Node {
    Match {
        id: u32,
        // round: u32,
        // depth: u32,
        // index: u32,
        match_type: MatchType,
        /** High seed */
        high: Arc<RwLock<Node>>,
        /** Low seed */
        low: Arc<RwLock<Node>>,
        drop: Option<Arc<RwLock<Node>>>,
    },
    In(Arc<RwLock<Contestent>>),
    Bye,
}

impl Node {
    pub fn new(self) -> Arc<RwLock<Self>> {
        Arc::new(RwLock::new(self))
    }

    pub fn bracket(self) -> Bracket {
        Bracket::new(self)
    }

    pub fn name(&self) -> String {
        self.name_recur(true)
    }

    pub fn name_recur(&self, cont: bool) -> String {
        if !cont {
            match self {
                In(contestent) => format!(
                    "In({})",
                    match &*contestent.read().unwrap() {
                        Seed { rank } => format!("Seed({})", rank),
                        Loser(lb) => format!("Loser(_)"),
                    }
                ),
                Bye => "Bye".to_string(),
                Match {
                    id,
                    match_type,
                    drop,
                    ..
                } => format!(
                    "Match({}, {:?}{})",
                    id,
                    match_type,
                    if let Some(d) = drop {
                        format!(" drop to __")
                    } else {
                        "".to_string()
                    }
                ),
            }
        } else {
            match self {
                In(contestent) => format!(
                    "In({})",
                    match &*contestent.read().unwrap() {
                        Seed { rank } => format!("Seed({})", rank),
                        Loser(lb) => format!("Loser({})", lb.read().unwrap().name_recur(false)),
                    }
                ),
                Bye => "Bye".to_string(),
                Match {
                    id,
                    match_type,
                    drop,
                    ..
                } => format!(
                    "Match({}, {:?}{})",
                    id,
                    match_type,
                    if let Some(d) = drop {
                        format!(" drop to {}", d.read().unwrap().name_recur(false))
                    } else {
                        "".to_string()
                    }
                ),
            }
        }
    }

    pub fn match_id(&self) -> Option<u32> {
        match self {
            Match { id, .. } => Some(*id),
            _ => None,
        }
    }

    pub fn high(&self) -> Option<&Arc<RwLock<Node>>> {
        match self {
            Match { high, .. } => Some(high),
            _ => None,
        }
    }

    pub fn low(&self) -> Option<&Arc<RwLock<Node>>> {
        match self {
            Match { low, .. } => Some(low),
            _ => None,
        }
    }

    pub fn children(&self) -> Option<(&Arc<RwLock<Node>>, &Arc<RwLock<Node>>)> {
        match self {
            Match { high, low, .. } => Some((high, low)),
            _ => None,
        }
    }

    pub fn height(&self) -> usize {
        match self {
            Match { high, low, .. } => {
                let height_high = high.read().unwrap().height();
                let height_low = low.read().unwrap().height();
                1 + max(height_high, height_low)
            }
            _ => 0,
        }
    }

    pub fn is_empty(&self) -> bool {
        match self {
            Bye => true,
            In(input) => match &*input.read().unwrap() {
                Seed { .. } => false,
                Loser(loser) => loser.read().unwrap().is_empty(),
            },
            Match { high, low, .. } => {
                high.read().unwrap().is_empty() && low.read().unwrap().is_empty()
            }
        }
    }
}

impl Contestent {
    pub fn new(self) -> Arc<RwLock<Self>> {
        Arc::new(RwLock::new(self))
    }
}

#[derive(Clone, Debug)]
pub struct Bracket {
    pub root: Arc<RwLock<Node>>,
    next_id: Rc<RefCell<u32>>,
}

use Contestent::*;
use Node::*;

impl Bracket {
    pub fn new(node: Node) -> Bracket {
        Bracket {
            root: Arc::new(RwLock::new(node)),
            next_id: Rc::new(RefCell::new(0)),
        }
    }

    pub fn contestents(&self) -> Vec<Arc<RwLock<Contestent>>> {
        let mut r: Vec<Arc<RwLock<Contestent>>> = Vec::new();
        let mut queue: Vec<Arc<RwLock<Node>>> = Vec::new();
        queue.push(self.root.clone());
        while let Some(here) = queue.pop() {
            match &*here.read().unwrap() {
                Match { high, low, .. } => {
                    queue.push(high.clone());
                    queue.push(low.clone());
                }
                In(c) => {
                    r.push(c.clone());
                }
                Bye => {}
            }
        }
        r
    }

    pub fn name(&self) -> String {
        match &*self.root.read().unwrap() {
            In(_) => format!("{:?}", self),
            Bye => "Bye".to_string(),
            Match { id, match_type, .. } => format!("Match({}, {:?})", id, match_type),
        }
    }

    pub fn pprint(&self) {
        println!("");
        fn go(node: Arc<RwLock<Node>>, prefix: String, last: bool) {
            let prefix_current = if last { "`- " } else { "|- " };
            let prefix_child = if last { "   " } else { "|  " };
            let prefix = prefix + prefix_child;
            let node = node.read().unwrap();
            println!("{}{}{}", prefix, prefix_current, node.name());
            if let Some(high) = node.high() {
                go(high.clone(), prefix.to_string(), false);
            }
            if let Some(low) = node.low() {
                go(low.clone(), prefix.to_string(), false);
            }
        }
        go(self.root.clone(), "".to_string(), true)
    }

    pub fn single_elim(signups: u32) -> Bracket {
        let mut wb = Self::winner_bracket(signups, true);
        wb.simplify();
        wb
    }

    pub fn winner_bracket(signups: u32, has_gf: bool) -> Bracket {
        if signups == 0 {
            return Bye.bracket();
        } else if signups == 1 {
            let seed = Seed { rank: 0 }.new();
            return Match {
                id: 0,
                match_type: MatchType::WB,
                high: In(seed).new(),
                low: Bye.new(),
                drop: None,
            }
            .bracket();
        } else if signups == 2 {
            let high = Seed { rank: 0 }.new();
            let low = Seed { rank: 1 }.new();
            return Match {
                id: 0,
                match_type: MatchType::WB,
                high: In(high).new(),
                low: In(low).new(),
                drop: None,
            }
            .bracket();
        }
        let mut seeding = slaughter_seeding(signups);
        seeding.reverse();
        let seeding = Rc::new(RefCell::new(seeding));
        let next_id = Rc::new(RefCell::new(0));
        let get_next_id = || {
            let mut next_id = next_id.borrow_mut();
            let id = *next_id;
            *next_id += 1;
            id
        };
        let matches = seeding.borrow().len() as u32;
        let rounds = ((2 * matches) as f32).log2().floor() as u32;
        let go = recur_fn(|go, depth: u32| -> Arc<RwLock<Node>> {
            let round = rounds - depth - 1;
            match round {
                0 => {
                    let mut seeding = seeding.borrow_mut();
                    let seeds = seeding.pop();
                    match seeds {
                        Some((high, low)) => Match {
                            id: get_next_id(),
                            match_type: MatchType::WB,
                            high: match high {
                                Some(high) => In(Seed { rank: high }.new()),
                                None => Bye,
                            }
                            .new(),
                            low: match low {
                                Some(low) => In(Seed { rank: low }.new()),
                                None => Bye,
                            }
                            .new(),
                            drop: None,
                        },
                        None => Bye,
                    }
                    .new()
                }
                _ => Match {
                    id: get_next_id(),
                    match_type: if round >= rounds - 1 && has_gf {
                        MatchType::GF1
                    } else {
                        MatchType::WB
                    },
                    high: go(depth + 1),
                    low: go(depth + 1),
                    drop: None,
                }
                .new(),
            }
        });
        Bracket {
            root: go.call(0),
            next_id,
        }
    }

    pub fn levels(&self) -> Vec<Vec<Arc<RwLock<Node>>>> {
        let mut result: Vec<Rc<RefCell<Vec<Arc<RwLock<Node>>>>>> = Vec::new();
        let mut queue: Vec<(usize, Arc<RwLock<Node>>)> = Vec::new();
        queue.push((0, self.root.clone()));
        while let Some((height, node)) = queue.pop() {
            if let Match { high, low, .. } = &*node.clone().read().unwrap() {
                queue.push((height + 1, low.clone()));
                queue.push((height + 1, high.clone()));
                let level = result.get_mut(height).cloned();
                let level = if level.is_none() && result.len() == height {
                    let r = Rc::new(RefCell::new(Vec::new()));
                    result.push(r.clone());
                    r
                } else if let Some(level) = level {
                    level
                } else {
                    panic!("cannot flatten bracket")
                };
                level.borrow_mut().push(node);
            }
        }
        result.reverse();
        result.iter().map(|round| round.take()).collect()
    }

    pub fn id(&self) -> u32 {
        let mut id = self.next_id.borrow_mut();
        let id0 = *id;
        *id += 1;
        id0
    }

    pub fn add_loser_bracket(self) -> Bracket {
        let wb_matches: Vec<Vec<Arc<RwLock<Node>>>> = self.levels();
        let wb_rounds = wb_matches.len();

        let wb_drops: Vec<(usize, Vec<Arc<RwLock<Node>>>)> = wb_matches
            .iter()
            .enumerate()
            .map(|(round, matches)| {
                (
                    round,
                    LinkFun::optimal_linking(round as u32, wb_rounds as u32, matches.clone()),
                )
            })
            .collect::<Vec<_>>();

        let rounds = wb_rounds * 2;
        let lb_rounds = rounds - 2; // the LB rounds = the rounds minus the two
                                    // special grand finals

        let wb_drops = Arc::new(RwLock::new(wb_drops));
        let make_loser_bracket = recur_fn(|go, depth: usize| -> Arc<RwLock<Node>> {
            let round = lb_rounds - depth;
            if round == 0 {
                // this is the initial loser bracket round where there is nobody
                // here and we have to seed the lb
                let mut wb_drops = wb_drops.write().unwrap();
                let wb_round_0 = &mut wb_drops.get_mut(0).expect("no round 0").1;
                if let Some(d) = wb_round_0.pop() {
                    let loser = In(Loser(d.clone()).new()).new();
                    {
                        let mut d = d.write().unwrap();
                        match *d {
                            Match { ref mut drop, .. } => {
                                *drop = Some(loser.clone());
                            }
                            _ => {}
                        }
                    };
                    loser
                } else {
                    Bye.new()
                }
            } else if round % 2 == 0 {
                // this is a round where we have to add losers to the existing
                // lb the high seed is the one who dropped from the WB. the low
                // seed is already in the LB, and we have to generate the rest
                // of the LB that they come from as well
                let wb_round = round / 2;
                let wb_match = wb_drops
                    .write()
                    .unwrap()
                    .get_mut(wb_round)
                    .expect(&format!("no wb_round {}", round))
                    .1
                    .pop();
                if let Some(wb_match) = wb_match {
                    let loser = In(Loser(wb_match.clone()).new()).new();
                    match *wb_match.write().unwrap() {
                        Match { ref mut drop, .. } => {
                            *drop = Some(loser.clone());
                        }
                        _ => {}
                    };
                    Match {
                        id: self.id(),
                        match_type: MatchType::LB,
                        high: loser.clone(),
                        low: go(depth + 1),
                        drop: None,
                    }
                    .new()
                } else {
                    log::warn!("a wb_match at round {} didn't exist", round);
                    go(depth + 1)
                }
            } else {
                // this is a round where losers face off with eachother
                Match {
                    id: self.id(),
                    match_type: MatchType::LB,
                    high: go(depth + 1),
                    low: go(depth + 1),
                    drop: None,
                }
                .new()
            }
        });

        let gf1 = Match {
            id: self.id(),
            match_type: MatchType::GF1,
            high: self.root.clone(),
            low: make_loser_bracket.call(0),
            drop: None,
        }
        .new();

        let gf1_loser = In(Loser(gf1.clone()).new()).new();

        let gf2 = Match {
            id: self.id(),
            match_type: MatchType::GF2,
            high: gf1.clone(),
            low: gf1_loser.clone(),
            drop: None,
        }
        .new();

        match *gf1.write().unwrap() {
            Match { ref mut drop, .. } => {
                *drop = Some(gf1_loser);
            }
            _ => {}
        }

        Bracket {
            next_id: self.next_id,
            root: gf2,
        }
    }

    pub fn double_elim(signups: u32) -> Bracket {
        let winner_bracket = Bracket::winner_bracket(signups, false);
        let mut de = winner_bracket.add_loser_bracket();
        de.simplify();
        de.pprint();
        de
    }

    pub fn simplify_tick(&mut self) {
        #[derive(Debug)]
        struct SimplifyNode {
            node: Arc<RwLock<Node>>,
            byes: Rc<RefCell<(bool, bool)>>,
            parent: Option<usize>,
            is_high: Option<bool>,
        }

        let mut queue = vec![(None, None, self.root.clone())];
        let mut back = vec![];
        while let Some((is_high, parent, node)) = queue.pop() {
            let i = back.len();
            back.push(SimplifyNode {
                parent,
                is_high,
                node: node.clone(),
                byes: Rc::new(RefCell::new((false, false))),
            });
            let node = node.write().unwrap();
            match &*node {
                Bye => {}
                Match { high, low, .. } => {
                    queue.push((Some(true), Some(i), high.clone()));
                    queue.push((Some(false), Some(i), low.clone()));
                }
                _ => {}
            }
        }

        let mut back = back.iter().collect::<Vec<_>>();
        let parents = back.clone();
        back.reverse();

        for SimplifyNode {
            node,
            byes,
            parent,
            is_high,
        } in back
        {
            let parent: Option<&SimplifyNode> = match parent {
                Some(parent) => parents.get(*parent).map(|p| *p),
                None => None,
            };
            let (high_bye, low_bye) = *byes.borrow();
            if high_bye && low_bye {
                let mut node = node.write().unwrap();
                // both children are byes
                match *node {
                    Match {
                        drop: Some(ref mut drop),
                        ..
                    } => {
                        *drop.write().unwrap() = Bye;
                    }
                    _ => {}
                }
                *node = Bye;
            } else if high_bye || low_bye {
                // one of the child matches are byes
                let (repl, drop) = match &*node.read().unwrap() {
                    Match { high, drop, .. } if low_bye => (Some(high.clone()), drop.clone()),
                    Match { low, drop, .. } if high_bye => (Some(low.clone()), drop.clone()),
                    _ => (None, None),
                };
                match repl {
                    Some(repl) => {
                        match drop {
                            Some(drop) => {
                                *drop.write().unwrap() = Bye;
                            }
                            None => {}
                        }
                        *node.write().unwrap() = repl.read().unwrap().clone();
                    }
                    None => {}
                }
            }

            match &*node.read().unwrap() {
                Bye => match parent {
                    Some(SimplifyNode { byes, .. }) => {
                        let mut byes = byes.borrow_mut();
                        if *is_high == Some(true) {
                            byes.0 = true;
                        } else if *is_high == Some(false) {
                            byes.1 = true;
                        }
                    }
                    _ => {}
                },
                _ => {}
            }
        }
    }

    pub fn height(&self) -> usize {
        self.root.read().unwrap().height()
    }

    pub fn simplify(&mut self) {
        self.simplify_tick()
    }
}

pub fn slaughter_seeding(signups: u32) -> Vec<(Option<u32>, Option<u32>)> {
    if signups == 0 {
        return Vec::new();
    }
    let max_depth = (signups as f32).log2().ceil() as u32;
    let mut winners = vec![Some(0)];
    for depth in 1..=max_depth {
        let next_matches = 2_usize.pow(depth);
        let mut next_winners = std::iter::repeat(None)
            .take(next_matches)
            .collect::<Vec<Option<u32>>>();
        assert_eq!(winners.len(), 2_usize.pow(depth - 1));
        let mut avail = (winners.len() as u32..winners.len() as u32 * 2).collect_vec();
        let sorted_winners = {
            let mut iw = winners.iter().cloned().enumerate().collect_vec();
            iw.sort_by_key(|(_index, seed)| seed.clone());
            iw
        };
        for (index, seed) in sorted_winners {
            let loser = avail.pop();
            next_winners[index * 2] = Some(seed.expect("no winner!"));
            next_winners[index * 2 + 1] = loser;
        }
        winners = next_winners;
    }
    let mut r = Vec::new();
    let check_signup = |i: u32| -> Option<u32> {
        if i < signups {
            Some(i)
        } else {
            None
        }
    };
    for i in 0..(winners.len() / 2) {
        let high = winners[i * 2].expect("no high seed index");
        let low = winners[i * 2 + 1].expect("no low seed index");
        r.push((check_signup(high), check_signup(low)));
    }
    r
}

#[cfg(test)]
mod test {
    use super::slaughter_seeding;
    #[test]
    fn slaughter_seeding_2() {
        assert_eq!(slaughter_seeding(2), vec![(Some(0), Some(1)),]);
    }

    #[test]
    fn slaughter_seeding_3() {
        assert_eq!(
            slaughter_seeding(3),
            vec![(Some(0), None), (Some(1), Some(2)),]
        );
    }

    #[test]
    fn slaughter_seeding_4() {
        assert_eq!(
            slaughter_seeding(4),
            vec![(Some(0), Some(3)), (Some(1), Some(2)),]
        );
    }

    #[test]
    fn slaughter_seeding_5() {
        assert_eq!(
            slaughter_seeding(5),
            vec![
                (Some(0), None),
                (Some(3), Some(4)),
                (Some(1), None),
                (Some(2), None),
            ]
        );
    }

    #[test]
    fn slaughter_seeding_6() {
        assert_eq!(
            slaughter_seeding(6),
            vec![
                (Some(0), None),
                (Some(3), Some(4)),
                (Some(1), None),
                (Some(2), Some(5)),
            ]
        );
    }

    #[test]
    fn slaughter_seeding_7() {
        assert_eq!(
            slaughter_seeding(7),
            vec![
                (Some(0), None),
                (Some(3), Some(4)),
                (Some(1), Some(6)),
                (Some(2), Some(5)),
            ]
        );
    }

    #[test]
    fn slaughter_seeding_8() {
        assert_eq!(
            slaughter_seeding(8),
            vec![
                (Some(0), Some(7)),
                (Some(3), Some(4)),
                (Some(1), Some(6)),
                (Some(2), Some(5)),
            ]
        );
    }
}
