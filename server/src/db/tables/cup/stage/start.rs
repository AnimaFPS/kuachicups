use super::super::model::{
    Cup, CupMatch2p, CupMatch2pRow, CupSignup, CupStage, CupStageFormat, GroupPointAward,
};
use super::bracket::Contestent;
use crate::db::{ById, Error, Executor, JsonRow, JsonToDbResult, Result, DB};
use comprehension::iter;
use sqlx::{query, query_as, Acquire, Postgres, Transaction};
use std::cmp::max;
use std::collections::HashMap;
use uuid::Uuid;

type Position = bool;
const High: bool = true;
const Low: bool = false;

#[derive(Copy, Clone)]
pub struct Parents {
    high: i32,
    low: i32,
}

impl CupStage {
    pub async fn get_all_signups(&self, db: impl Executor<'_>) -> Result<Vec<CupSignup>> {
        let signups = Cup::get_all_signups(db, self.cup_id).await?;
        Ok(signups)
    }

    pub async fn get_seeding(&self, db: impl Executor<'_> + Copy) -> Result<Vec<CupSignup>> {
        let signups = if self.stage_no > 0 {
            let last_stage = query!(
                "
                    SELECT id
                    FROM cup_stage
                    WHERE stage_no = $1 AND cup_id = $2
                ",
                self.stage_no - 1,
                self.cup_id,
            ).fetch_one(db).await?;
            let last_stage = CupStage::by_id(
                db,
                last_stage.id
            ).await?;
            last_stage.standings(db)
                .await?
                .rankings
                .iter()
                .map(|r| r.signup.clone())
                .collect()
        } else {
            Cup::get_checkedin_signups(db, self.cup_id).await?
        };
        Ok(signups)
    }

    pub fn can_start(&self) -> bool {
        use chrono::offset::Utc;
        let now = Utc::now();
        self.start_immediately || Some(now) >= self.starttime
    }

    pub async fn start_stage(db: &DB, cup_id: Uuid, stage_id: Uuid) -> Result<bool> {
        log::info!("cup_id = {:?}, stage_id = {:?}", cup_id, stage_id);
        let can_init = query!(
            "
                SELECT (cup_id = $1 AND NOT is_started) AS ok
                FROM cup_stage WHERE id = $2
            ",
            cup_id,
            stage_id,
        )
        .fetch_one(db)
        .await?
        .ok;
        if can_init != Some(true) {
            Err(Error::SomeError("cup_id incorrect"))?;
        }
        let stage: CupStage = CupStage::by_id(db, stage_id).await?;
        if !stage.can_start() {
            Err(Error::SomeError("cannot start this stage yet"))?;
        }
        let signups = stage.get_seeding(db).await?;
        let signups = match stage.max_participants {
            Some(max_participants) => {
                log::info!("max_participants = {}", max_participants);
                signups.iter().take(max_participants as usize).cloned().collect()
            }
           None => signups,
        };
        log::info!("beginning {:?}", stage.format);
        match stage.format {
            CupStageFormat::Groups => stage.populate_groups_2p(signups, db).await?,
            CupStageFormat::SingleElim => stage.populate_elim_2p(signups, db).await?,
            CupStageFormat::DoubleElim => stage.populate_elim_2p(signups, db).await?,
            _ => {}
        };
        query!(
            "UPDATE cup_stage SET is_started = true WHERE id = $1",
            stage_id
        )
        .execute(db)
        .await?;
        CupStage::run_byes(db, stage_id).await?;
        query!(
            "UPDATE cup SET current_stage = $1, updated_at = now() WHERE id = $2",
            stage.stage_no,
            cup_id
        )
        .execute(db)
        .await?;
        Ok(true)
    }

    pub async fn run_byes(db: &DB, stage_id: Uuid) -> Result<()> {
        let match_ids_by_round = query!(
            "
            SELECT array_agg(id) AS ids
            FROM cup_match_2p
            WHERE cup_stage_id = $1
            GROUP BY elim_round
            ORDER BY elim_round ",
            stage_id
        )
        .fetch_all(db)
        .await?;
        let mut transaction = db.begin().await?;
        for match_ids in match_ids_by_round {
            for match_id in match_ids.ids.iter().flatten() {
                CupMatch2p::compute_winner(transaction.begin().await?, *match_id).await?;
            }
        }
        transaction.commit().await?;
        Ok(())
    }

    pub async fn populate_elim_2p(&self, signups: Vec<CupSignup>, db: &DB) -> Result<()> {
        use super::super::model::ElimMatchType as MatchType;
        use super::bracket::{Bracket, Contestent, Node};
        use std::borrow::Borrow;
        use std::cell::RefCell;
        use std::collections::BTreeMap;
        use std::iter;
        use std::sync::{Arc, RwLock};

        log::info!("populating elim with {:#?}", signups);

        /*
         * In general the brackets don't have a very rigid structure due to
         * simplifying and byes, so we can't rely on easily being able to count
         * the indices of each match in a round across different branches of the
         * tree. Instead we just assign an index per round that gets incremented
         * whenever we make a match insert
         */
        let mut lengths: Vec<i32> = vec![];
        let mut get_next_index = |depth: i32| -> i32 {
            if let Some(ix) = lengths.get_mut(depth as usize) {
                let here = *ix;
                *ix += 1;
                println!("lengths = {:?}", lengths);
                here
            } else {
                for _ in lengths.len()..=(depth as usize) {
                    lengths.push(0);
                }
                // increment our specific index
                let ix = lengths.get_mut(depth as usize).expect("wat");
                let here = *ix;
                *ix += 1;
                here
            }
        };

        #[derive(Copy, Clone, Debug)]
        enum Cmd {
            InsMatch {
                lid: u32,
                round: i32,
                index: i32,
                position: Option<Position>,
                match_type: MatchType,
                parent_lid: Option<u32>,
            },
            AddBye {
                lid: u32,
                position: Position,
            },
            AddSeed {
                lid: u32,
                position: Position,
                rank: u32,
            },
            DropLoser {
                origin_lid: u32,
                dest_lid: u32,
                dest_position: Position,
            },
        }

        /*
         * Why all this song and dance creating a little interpreter? well,
         * mainly because I can't figure out how to make bracket.rs stuff Send
         * i.e. how to interleave sqlx 'await's in the bracket creation.
         */

        let cmds = {
            // these are just here for ordering the commands
            let mut cmds0 = Vec::new();
            let mut cmds1 = Vec::new();
            let mut cmds2 = Vec::new();

            let bracket = match self.format {
                CupStageFormat::SingleElim => Ok(Bracket::single_elim(signups.len() as u32)),
                CupStageFormat::DoubleElim => Ok(Bracket::double_elim(signups.len() as u32)),
                _ => Err(Error::SomeError("invalid format for populate_elim_2p")),
            }?;

            bracket.pprint();

            let rounds = bracket.height();

            #[derive(Clone)]
            struct N {
                index: i32,
                depth: i32,
                node: Arc<RwLock<Node>>,
                parent_lid: Option<u32>,
                is_high: Option<bool>,
            }

            let mut queue = vec![N {
                index: 0,
                depth: 0,
                node: bracket.root,
                parent_lid: None,
                is_high: None,
            }];

            while let Some(n) = queue.pop() {
                let N {
                    index,
                    depth,
                    node,
                    parent_lid,
                    is_high,
                } = n;
                let round = (rounds as i32) - depth - 1;
                let position = is_high;
                match &*node.read().unwrap() {
                    Node::Match {
                        id,
                        high,
                        low,
                        match_type,
                        ..
                    } => {
                        let lid = *id;
                        let index_2 = get_next_index(depth);
                        let index_1 = get_next_index(depth);
                        cmds0.push(Cmd::InsMatch {
                            lid,
                            round,
                            index,
                            match_type: *match_type,
                            parent_lid,
                            position,
                        });
                        queue.push(N {
                            index: index_1,
                            depth: depth + 1,
                            node: low.clone(),
                            parent_lid: Some(lid),
                            is_high: Some(false),
                        });
                        queue.push(N {
                            index: index_2,
                            depth: depth + 1,
                            node: high.clone(),
                            parent_lid: Some(lid),
                            is_high: Some(true),
                        });
                    }

                    Node::Bye => cmds1.push(Cmd::AddBye {
                        lid: parent_lid.expect("no parent"),
                        position: position.expect("no position"),
                    }),

                    Node::In(contestent) => match &*contestent.read().unwrap() {
                        Contestent::Loser(loser) => cmds1.push(Cmd::DropLoser {
                            origin_lid: loser
                                .read()
                                .unwrap()
                                .match_id()
                                .expect("no match dropped from"),
                            dest_lid: parent_lid.expect("no parent"),
                            dest_position: position.expect("no position"),
                        }),
                        // do at the very end after byes etc so that the code to
                        // advance players after byes works
                        Contestent::Seed { rank } => cmds2.push(Cmd::AddSeed {
                            lid: parent_lid.expect("no parent"),
                            position: position.expect("no position"),
                            rank: *rank,
                        }),
                    },
                };
            }

            [cmds0, cmds1, cmds2].concat()
        };

        log::info!("populating elim by {:#?}", cmds);

        let mut transaction: Transaction<Postgres> = db.begin().await?;

        for cmd in cmds {
            match cmd {
                Cmd::InsMatch {
                    lid,
                    round,
                    index,
                    match_type,
                    position,
                    parent_lid,
                } => {
                    let scoring_id = self
                        .scoring_elim(round, match_type == MatchType::LB, db)
                        .await?
                        .id;
                    let new_match = query!(
                        "
                            INSERT INTO cup_match_2p(
                                cup_stage_id,
                                lid,
                                elim_round,
                                elim_index,
                                elim_type,
                                scoring_id,
                                elim_winner_match_id,
                                elim_winner_to_high
                            )
                            SELECT $1, $2, $3, $4, $5, $6, (SELECT m.id FROM cup_match_2p m WHERE m.cup_stage_id = $1 AND m.lid = $7), $8
                            RETURNING id
                        ",
                        self.id,
                        lid as i32,
                        round,
                        index,
                        match_type as i16,
                        scoring_id,
                        parent_lid.map(|plid| plid as i32),
                        position.map(|pos| pos == High),
                    )
                    .fetch_one(&mut transaction)
                    .await?;

                    if let Some(parent_lid) = parent_lid {
                        query!(
                            "
                                UPDATE cup_match_2p
                                SET
                                    elim_high_match_id = coalesce($1, elim_high_match_id),
                                    elim_low_match_id = coalesce($2, elim_low_match_id)
                                WHERE
                                    cup_stage_id = $3 AND lid = $4
                            ",
                            (position == Some(High)).then_some(new_match.id),
                            (position == Some(Low)).then_some(new_match.id),
                            self.id,
                            parent_lid as i32,
                        )
                        .execute(&mut transaction)
                        .await?;
                    }
                }

                Cmd::AddSeed {
                    lid,
                    position,
                    rank,
                } => {
                    let signup = signups
                        .get(rank as usize)
                        .expect(&format!("signup {} didn't exist", rank));
                    query!(
                        "
                            UPDATE cup_match_2p
                            SET high_id = coalesce($1, high_id), low_id = coalesce($2, low_id)
                            WHERE cup_stage_id = $3 AND lid = $4
                        ",
                        (position == High).then_some(signup.id),
                        (position == Low).then_some(signup.id),
                        self.id,
                        lid as i32,
                    )
                    .execute(&mut transaction)
                    .await?;
                }

                Cmd::AddBye { lid, position } => {
                    // update the match that has this bye
                    let dest = query!(
                        "
                            UPDATE cup_match_2p
                            SET winner_id = (CASE WHEN $1 THEN high_id ELSE low_id END)
                            WHERE cup_stage_id = $2 AND lid = $3
                            RETURNING winner_id, elim_winner_match_id, elim_winner_to_high
                        ",
                        position == High,
                        self.id,
                        lid as i32
                    )
                    .fetch_one(&mut transaction)
                    .await?;

                    // advance the winner if there was one
                    query!(
                        "
                            UPDATE cup_match_2p
                            SET high_id = coalesce($1, high_id), low_id = coalesce($2, low_id)
                            WHERE cup_stage_id = $3 AND lid = $4
                        ",
                        (position == High).then_some(dest.winner_id).flatten(),
                        (position == Low).then_some(dest.winner_id).flatten(),
                        self.id,
                        lid as i32
                    )
                    .execute(&mut transaction)
                    .await?;
                }

                Cmd::DropLoser {
                    origin_lid,
                    dest_lid,
                    dest_position,
                } => {
                    // this is where the drop goes to
                    let dest = query!(
                        "
                            SELECT id FROM cup_match_2p WHERE cup_stage_id = $1 AND lid = $2
                        ",
                        self.id,
                        dest_lid as i32
                    )
                    .fetch_one(&mut transaction)
                    .await?;

                    // set the loser destination for our origin
                    let origin = query!(
                        "
                            UPDATE cup_match_2p
                            SET elim_loser_match_id = $1, elim_loser_to_high = $2
                            WHERE cup_stage_id = $3 AND lid = $4
                            RETURNING id
                        ",
                        dest.id,
                        dest_position == High,
                        self.id,
                        origin_lid as i32,
                    )
                    .fetch_one(&mut transaction)
                    .await?;

                    // now set either elim_low_match_id or elim_high_match_id on
                    // the destination to the origin match
                    query!(
                        "
                            UPDATE cup_match_2p
                            SET elim_low_match_id = coalesce($1, elim_low_match_id),
                                elim_high_match_id = coalesce($2, elim_high_match_id)
                            WHERE id = $3
                        ",
                        (dest_position == Low).then_some(origin.id),
                        (dest_position == High).then_some(origin.id),
                        dest.id,
                    )
                    .execute(&mut transaction)
                    .await?;
                }
            }
        }

        transaction.commit().await?;

        Ok(())
    }

    pub async fn populate_groups_2p(&self, signups: Vec<CupSignup>, db: &DB) -> Result<()> {
        let scoring_id = self.scoring_group(db).await?.id;
        let mut transaction = db.begin().await?;
        assert_eq!(self.format, CupStageFormat::Groups);

        let group_size = self
            .group_size
            .unwrap_or(guess_decent_group_size(signups.len()));

        let group_draws: Vec<Vec<&CupSignup>> = {
            let num_groups = ((signups.len() as f64) / (group_size as f64)).ceil() as i32;
            let grouped: Vec<(&CupSignup, i32)> =
                signups.iter().zip(group_indices(num_groups)).collect();
            assert_eq!(grouped.len(), signups.len());
            let mut draws: Vec<Vec<&CupSignup>> = (0..num_groups).map(|_| vec![]).collect();
            for (signup, group_ix) in grouped.iter() {
                let group = draws
                    .get_mut(*group_ix as usize)
                    .expect("group didn't exist");
                group.push(signup);
            }
            draws
        };

        let group_matchups: Vec<Vec<(&CupSignup, &CupSignup)>> = iter![
            iter![
                (*s1, *s2);
                (i, s1) <- draw.iter().enumerate(),
                s2 <- draw.iter().skip(i),
                s1.id != s2.id
            ].collect();
            ref draw <- group_draws
        ]
        .collect();

        let mut next_lid = 0;

        for (group_no, group) in group_matchups.iter().enumerate() {
            for (s1, s2) in group.iter() {
                let (low, high) = if s1.seed_value >= s2.seed_value {
                    (s1, s2)
                } else {
                    (s2, s1)
                };
                let lid = next_lid;
                next_lid += 1;
                query!(
                    "
                        INSERT INTO cup_match_2p(
                            lid,
                            cup_stage_id,
                            group_no,
                            low_id,
                            high_id,
                            scoring_id)
                        VALUES($1, $2, $3, $4, $5, $6)
                    ",
                    lid,
                    self.id,
                    group_no as i32,
                    low.id,
                    high.id,
                    scoring_id,
                )
                .execute(&mut transaction)
                .await?;
            }
        }
        transaction.commit().await?;
        Ok(())
    }
}

pub fn guess_decent_group_size(num_signups: usize) -> i32 {
    match num_signups {
        len @ (0..=5) => len as i32,
        6 => 3,
        7 => 4,
        8 => 4,
        9 => 3,
        10 => 5,
        11 => 4,
        12 => 4,
        13 => 7,
        14 => 7,
        15 => 5,
        16 => 4,
        len => (len as f64).sqrt().ceil() as i32,
    }
}

// num_groups=4 => 0,1,2,3,3,2,1,0,0,1,2,3,3,2,1,0,...
pub fn group_indices(num_groups: i32) -> impl Iterator<Item = i32> {
    let ascent = 0..num_groups;
    let descent = num_groups..0;
    ascent.chain(descent).cycle()
}
