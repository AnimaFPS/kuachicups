use super::model::{Cup, CupStage};
use crate::db::{
    tables::player::{Player, Role},
    Error, Executor, Result,
};
use sqlx::query;
use uuid::Uuid;

impl CupStage {
    pub async fn require_ownership(
        db: impl Executor<'_> + Clone,
        cup_stage_id: Uuid,
        player_id: Uuid,
    ) -> Result<()> {
        let team_id = query!(
            "
            SELECT cup.owner_team_id
            FROM cup JOIN cup_stage ON cup_stage.cup_id = cup.id
            WHERE cup_stage.id = $1
            GROUP BY cup.id",
            cup_stage_id,
        )
        .fetch_one(db.clone())
        .await?
        .owner_team_id;
        if Player::has_role_within(db, player_id, team_id, Role::Captain).await {
            Ok(())
        } else {
            Err(Error::AuthError)
        }
    }
}

impl Cup {
    pub async fn require_ownership(
        db: impl Executor<'_> + Clone,
        cup_id: Uuid,
        player_id: Uuid,
    ) -> Result<()> {
        let team_id = query!(
            "SELECT cup.owner_team_id FROM cup WHERE cup.id = $1",
            cup_id,
        )
        .fetch_one(db.clone())
        .await?
        .owner_team_id;
        if Player::has_role_within(db, player_id, team_id, Role::Captain).await {
            Ok(())
        } else {
            Err(Error::AuthError)
        }
    }

    pub async fn require_solo_entrant_type(db: impl Executor<'_>, cup_id: Uuid) -> Result<()> {
        let is_solo_entrant_type = query!(
            "SELECT (entrant_type = 'player') OR (entrant_type IS NULL) AS is_solo
            FROM cup JOIN game_mode ON cup.game_mode_id = game_mode.id
            WHERE cup.id = $1 ",
            cup_id
        )
        .fetch_one(db)
        .await?;
        if is_solo_entrant_type.is_solo.unwrap_or(false) {
            Ok(())
        } else {
            Err(Error::SomeError("require solo entrant type"))
        }
    }

    pub async fn require_team_entrant_type(db: impl Executor<'_>, cup_id: Uuid) -> Result<()> {
        let is_team_entrant_type = query!(
            "
            SELECT (entrant_type = 'team') OR (entrant_type IS NULL) AS is_team
            FROM cup JOIN game_mode ON cup.game_mode_id = game_mode.id
            WHERE cup.id = $1 ",
            cup_id
        )
        .fetch_one(db)
        .await?;
        if is_team_entrant_type.is_team.unwrap_or(false) {
            Ok(())
        } else {
            Err(Error::SomeError("require solo entrant type"))
        }
    }

    pub async fn require_checkin_available(db: impl Executor<'_>, cup_id: Uuid) -> Result<()> {
        let r = query!("
            SELECT NOT cup.is_finished AND (now() >= checkintime) AND (now() <= starttime) AS can_checkin
            FROM cup JOIN cup_stage ON cup.current_stage = cup_stage.stage_no AND cup.id = cup_stage.cup_id
            WHERE cup.id = $1", cup_id).fetch_one(db).await?;
        if r.can_checkin.unwrap_or(false) {
            Ok(())
        } else {
            Err(Error::SomeError("require checkin available"))
        }
    }

    pub async fn require_signup_available(db: impl Executor<'_>, cup_id: Uuid) -> Result<()> {
        let r = query!("
            SELECT NOT cup.is_finished AND NOT cup.is_signups_closed AND (now() <= starttime) AS can_signup
            FROM cup JOIN cup_stage ON cup.current_stage = cup_stage.stage_no AND cup.id = cup_stage.cup_id
            WHERE cup.id = $1 ", cup_id).fetch_one(db).await?;
        if r.can_signup.unwrap_or(false) {
            Ok(())
        } else {
            Err(Error::SomeError("require signup available"))
        }
    }
}
