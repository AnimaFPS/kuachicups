use super::model::{Cup, CupStage};
use crate::db::{Result, DB};
use crate::render_markdown::markdown_to_html;
use slug::slugify;
use sqlx::{query, query_as, Done, Executor, Postgres, Transaction};
use uuid::Uuid;

impl Cup {
    pub async fn create(
        db: impl Executor<'_, Database = Postgres>,
        owner_team_id: Uuid,
        game_mode_id: Uuid,
        title: String,
        description_md: String,
        is_signups_closed: bool,
        is_published: bool,
    ) -> Result<Cup> {
        Ok(query_as!(
            Cup,
            r#"
            INSERT INTO cup(
                title,
                slug,
                description_md,
                description_html,
                is_signups_closed,
                is_published,
                owner_team_id,
                game_mode_id)
             VALUES($1, $2, $3, $4, $5, $6, $7, $8)
             RETURNING *
        "#,
            title.clone(),
            slugify(title),
            description_md,
            markdown_to_html(&description_md),
            is_signups_closed,
            is_published,
            owner_team_id,
            game_mode_id
        )
        .fetch_one(db)
        .await?)
    }

    pub async fn delete(mut db: Transaction<'_, Postgres>, id: Uuid) -> Result<bool> {
        query!(
            "
                UPDATE cup_stage_scoring
                SET cup_stage_id = null
                WHERE cup_stage_id = ANY (SELECT s.id FROM cup_stage s WHERE s.cup_id = $1)
            ",
            id,
        )
        .execute(&mut db)
        .await?;

        // why is that one nullable and this one not? good question.
        query!(
            "
                UPDATE cup_match_2p
                SET cup_stage_id = uuid_nil()
                WHERE cup_stage_id = ANY (SELECT s.id FROM cup_stage s WHERE s.cup_id = $1)
            ",
            id,
        )
        .execute(&mut db)
        .await?;

        query!("DELETE FROM cup_stage WHERE cup_id = $1", id)
            .execute(&mut db)
            .await?;

        query!("DELETE FROM cup_signup WHERE cup_id = $1", id)
            .execute(&mut db)
            .await?;

        query!("DELETE FROM cup WHERE id = $1", id)
            .execute(&mut db)
            .await?;

        db.commit().await?;

        Ok(true)
    }

    pub async fn update(
        db: impl Executor<'_, Database = Postgres>,
        id: Uuid,
        title: Option<String>,
        description_md: Option<String>,
        is_signups_closed: Option<bool>,
        is_published: Option<bool>,
    ) -> Result<Option<Cup>> {
        if title.is_none() && description_md.is_none() && is_published.is_none() {
            return Ok(None);
        }
        Ok(Some(
            query_as!(
                Cup,
                r#"
                UPDATE cup SET
                    title = coalesce($1, title),
                    slug = coalesce($2, slug),
                    description_md = coalesce($3, description_md),
                    description_html = coalesce($4, description_html),
                    is_signups_closed = coalesce($5, is_signups_closed),
                    is_published = coalesce($6, is_published),
                    updated_at = now()
                WHERE id = $7
                RETURNING *
                "#,
                title.clone(),
                title.map(slugify),
                description_md,
                description_md.as_deref().map(markdown_to_html),
                is_signups_closed,
                is_published,
                id
            )
            .fetch_one(db)
            .await?,
        ))
    }

    pub async fn reset(db: &DB, id: Uuid) -> Result<()> {
        let mut t = db.begin().await?;
        query!(
            "
            DELETE FROM cup_match_2p
            WHERE cup_stage_id = ANY (SELECT s.id FROM cup_stage s WHERE s.cup_id = $1)
            ",
            id
        )
        .execute(&mut t)
        .await?;
        query!(
            "UPDATE cup_stage SET is_started = false WHERE cup_id = $1",
            id
        )
        .execute(&mut t)
        .await?;
        query!("UPDATE cup SET current_stage = 0 WHERE id = $1", id)
            .execute(&mut t)
            .await?;
        t.commit().await?;
        Ok(())
    }
}
