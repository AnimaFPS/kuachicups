use super::model::*;
use crate::db;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, Acquire, Done, Postgres, Transaction};
use uuid::Uuid;

pub enum MatchReporter {
    Low,
    High,
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub struct PlayerPendingMatch {
    signup: CupSignup,
    match_: CupMatch2p,
}

impl CupMatch2p {
    pub async fn signups_pending_matches(
        db: &db::DB,
        stage_id: Uuid,
        signup_ids: Vec<Uuid>,
    ) -> db::Result<Vec<CupMatch2p>> {
        let rows = query_as!(
            CupMatch2pRow,
            "
                SELECT m.*
                FROM cup_match_2p m
                WHERE m.cup_stage_id = $1
                 AND (m.low_id = ANY($2) OR m.high_id = ANY($2))
                 AND NOT m.is_scored
                ORDER BY lid ASC
            ",
            stage_id,
            &signup_ids,
        )
        .fetch_all(db)
        .await?;
        Ok(rows.iter().map(CupMatch2p::from_row).collect())
    }

    pub async fn pending_matches(db: &db::DB, stage_id: Uuid) -> db::Result<Vec<CupMatch2p>> {
        let rows = query_as!(
            CupMatch2pRow,
            "
                SELECT m.*
                FROM cup_match_2p m
                WHERE m.cup_stage_id = $1 AND NOT m.is_scored
                ORDER BY lid ASC
            ",
            stage_id,
        )
        .fetch_all(db)
        .await?;
        Ok(rows.iter().map(CupMatch2p::from_row).collect())
    }

    pub async fn require_participant_of(
        db: &db::DB,
        match_id: i32,
        player_id: Uuid,
    ) -> db::Result<MatchReporter> {
        let is_low = query!(
            "
                SELECT m.low_id = s.id AS is_low
                FROM cup_match_2p m JOIN cup_signup s ON (m.low_id = s.id OR m.high_id = s.id)
                WHERE m.id = $1 AND s.player_id = $2
            ",
            match_id,
            player_id
        )
        .fetch_one(db)
        .await?
        .is_low;
        if let Some(is_low) = is_low {
            Ok(if is_low {
                MatchReporter::Low
            } else {
                MatchReporter::High
            })
        } else {
            Err(db::Error::SomeError("must be participant of that match"))
        }
    }

    pub async fn compute_winner(
        mut db: Transaction<'_, Postgres>,
        match_id: i32,
    ) -> db::Result<()> {
        let m = CupMatch2p::by_id(&mut db, match_id).await?;

        let bye_winner = if m.group_no == None {
            match (
                (m.elim_low_match_id, m.low_id),
                (m.elim_high_match_id, m.high_id),
            ) {
                ((None, None), (_, Some(w))) => Some(w),
                ((_, Some(w)), (None, None)) => Some(w),
                _ => None,
            }
        } else {
            match (m.low_id, m.high_id) {
                (Some(low), None) => Some(low),
                (None, Some(high)) => Some(high),
                _ => None,
            }
        };

        log::info!("bye_winner = {:#?}", bye_winner);

        let _result: Option<(Uuid, Option<Uuid>)> = if let Some(bye_winner) = bye_winner {
            query!(
                "UPDATE cup_match_2p SET winner_id = $1, updated_at = now() WHERE id = $2",
                bye_winner,
                match_id
            )
            .execute(&mut db)
            .await?;
            Some((bye_winner, None))
        } else if let (Some(low), Some(high)) = (m.low_report, m.high_report) {
            if low == high {
                let high_wins = low.iter().filter(|m| m.high > m.low).count();
                let low_wins = low.iter().filter(|m| m.low > m.high).count();
                if high_wins == low_wins {
                    return Err(db::Error::SomeError("tied results!"));
                }
                let high_id = m.high_id.expect("must have high_id");
                let low_id = m.high_id.expect("must have low_id");
                let (winner_id, loser_id) = if high_wins > low_wins {
                    query!(
                        "UPDATE cup_match_2p SET winner_id = high_id, loser_id = low_id, updated_at = now() WHERE id = $1",
                        match_id
                    )
                    .execute(&mut db)
                    .await?;
                    (high_id, low_id)
                } else {
                    query!(
                        "UPDATE cup_match_2p SET winner_id = low_id, loser_id = high_id, updated_at = now() WHERE id = $1",
                        match_id
                    )
                    .execute(&mut db)
                    .await?;
                    (low_id, high_id)
                };
                Some((winner_id, Some(loser_id)))
            } else {
                None
            }
        } else {
            return Ok(());
        };

        db.commit().await?;
        Ok(())
    }

    pub async fn make_report(
        db: &db::DB,
        match_id: i32,
        reporter: MatchReporter,
        low_report: &[i32],
        high_report: &[i32],
    ) -> db::Result<()> {
        if low_report.len() != high_report.len() {
            return Err(db::Error::SomeError("mismatch report lengths"));
        }

        let mut transaction = db.begin().await?;

        let am_low = match reporter {
            MatchReporter::Low => true,
            MatchReporter::High => false,
        };

        let bestof = query!(
            "
            SELECT s.bestof
            FROM cup_stage_scoring s
            JOIN cup_match_2p m ON m.scoring_id = s.id
            WHERE m.id = $1 ",
            match_id
        )
        .fetch_one(db)
        .await?
        .bestof;

        let min_games = ((bestof as f32) / 2.0).ceil() as usize;
        let max_games = bestof as usize;
        let games = min_games..=max_games;

        let high_wins = low_report
            .iter()
            .zip(high_report.iter())
            .map(|(l, h)| {
                if l > h {
                    MatchReporter::Low
                } else {
                    MatchReporter::High
                }
            })
            .filter(|m| match m {
                MatchReporter::High => true,
                _ => false,
            })
            .count();

        let low_wins = low_report.len() - high_wins;

        log::info!("games = {:?}", games);
        log::info!("low_wins = {:?}", low_wins);
        log::info!("high_wins = {:?}", high_wins);
        log::info!("low_report = {:?}", low_report);
        log::info!("high_report = {:?}", high_report);

        if !((games.contains(&low_wins) || games.contains(&high_wins))
            && low_report.len() <= max_games)
        {
            log::error!("invalid scores for bestof {}", bestof);
            return Err(db::Error::SomeError("invalid scores"));
        }

        let rows = if am_low {
            query!(
                "
                UPDATE cup_match_2p
                SET low_report_low = $1, low_report_high = $2, updated_at = now()
                WHERE id = $3
            ",
                low_report,
                high_report,
                match_id
            )
            .execute(&mut transaction)
            .await?
        } else {
            query!(
                "
                UPDATE cup_match_2p
                SET high_report_low = $1, high_report_high = $2, updated_at = now()
                WHERE id = $3
            ",
                low_report,
                high_report,
                match_id
            )
            .execute(&mut transaction)
            .await?
        };

        CupMatch2p::compute_winner(transaction.begin().await?, match_id).await?;

        match rows.rows_affected() {
            0 => Err(db::Error::SomeError("match did not update, maybe wrong id")),
            1 => {
                transaction.commit().await?;
                let m = CupMatch2p::from_row(
                    query_as!(
                        CupMatch2pRow,
                        "SELECT * FROM cup_match_2p WHERE id = $1",
                        match_id,
                    )
                    .fetch_one(db)
                    .await?,
                );
                if m.is_scored {
                    m.advance_around(db).await?;
                }
                Ok(())
            }
            _ => Err(db::Error::SomeError(
                "match update affected more than one row",
            )),
        }
    }

    pub async fn advance_around(&self, db: &db::DB) -> db::Result<()> {
        let mut transaction = db.begin().await?;

        if let (Some(elim_next_match_id), Some(elim_next_match_is_high), Some(winner_id)) = (
            self.elim_winner_match_id,
            self.elim_winner_to_high,
            self.winner_id,
        ) {
            if elim_next_match_is_high {
                query!(
                    "UPDATE cup_match_2p SET high_id = $1, updated_at = now() WHERE id = $2",
                    winner_id,
                    elim_next_match_id
                )
                .execute(&mut transaction)
                .await?;
            } else {
                query!(
                    "UPDATE cup_match_2p SET low_id = $1, updated_at = now() WHERE id = $2",
                    winner_id,
                    elim_next_match_id
                )
                .execute(&mut transaction)
                .await?;
            }
        }

        if let (Some(elim_loser_match_id), Some(loser_id)) =
            (self.elim_loser_match_id, self.loser_id)
        {
            let loser_match = CupMatch2p::by_id(&mut transaction, elim_loser_match_id).await?;
            let mut set_loser = true;

            if self.elim_type == Some(ElimMatchType::GF1) {
                // only self.high_id can drop to the "lb" (the 2nd gf) in this case
                // if self.high_id wins then set gf2 to reflect high_id has won
                if Some(loser_id) != self.high_id {
                    set_loser = false;
                    if let Some(winner_id) = self.winner_id {
                        query!(
                            "
                                UPDATE cup_match_2p
                                SET winner_id = $1, updated_at = now()
                                WHERE id = $2
                            ",
                            winner_id,
                            loser_match.id
                        )
                        .execute(&mut transaction)
                        .await?;
                    } else {
                        return Err(db::Error::SomeError("no winner_id to give gf2 to"));
                    }
                }
            }

            if set_loser {
                if loser_match.elim_low_match_id == Some(self.id) {
                    query!(
                        "UPDATE cup_match_2p SET low_id = $1, updated_at = now() WHERE id = $2",
                        loser_id,
                        elim_loser_match_id,
                    )
                    .execute(&mut transaction)
                    .await?;
                } else if loser_match.elim_high_match_id == Some(self.id) {
                    query!(
                        "UPDATE cup_match_2p SET high_id = $1, updated_at = now() WHERE id = $2",
                        loser_id,
                        elim_loser_match_id,
                    )
                    .execute(&mut transaction)
                    .await?;
                } else {
                    return Err(db::Error::SomeError(
                        "no matching high/low of match to drop to",
                    ));
                }
            }
        }

        transaction.commit().await?;
        CupStage::advance(self.cup_stage_id, db).await?;
        Ok(())
    }

    pub async fn by_id(db: impl db::Executor<'_>, id: i32) -> db::Result<CupMatch2p> {
        let row = query_as!(
            CupMatch2pRow,
            "SELECT * FROM cup_match_2p cm WHERE cm.id = $1 ",
            id,
        )
        .fetch_one(db)
        .await?;
        Ok(CupMatch2p::from_row(row))
    }

    pub async fn by_cup_stage_id(db: &db::DB, stage_id: Uuid) -> db::Result<Vec<CupMatch2p>> {
        let rows = query_as!(
            CupMatch2pRow,
            "
            SELECT * FROM cup_match_2p
            WHERE cup_stage_id = $1
            ORDER BY elim_type, elim_round, elim_index",
            stage_id,
        )
        .fetch_all(db)
        .await?;
        Ok(rows.iter().map(CupMatch2p::from_row).collect())
    }

    // boilerplate
    pub fn from_row(row: impl AsRef<CupMatch2pRow>) -> Self {
        let row = row.as_ref();
        CupMatch2p {
            id: row.id,
            lid: row.lid,
            scoring_id: row.scoring_id,
            cup_stage_id: row.cup_stage_id,
            group_no: row.group_no,
            elim_round: row.elim_round,
            elim_index: row.elim_index,
            elim_type: row.elim_type.and_then(num_traits::FromPrimitive::from_i16),
            elim_winner_match_id: row.elim_winner_match_id,
            elim_winner_to_high: row.elim_winner_to_high,
            elim_loser_match_id: row.elim_loser_match_id,
            elim_loser_to_high: row.elim_loser_to_high,
            elim_low_match_id: row.elim_low_match_id,
            elim_high_match_id: row.elim_high_match_id,
            low_id: row.low_id,
            high_id: row.high_id,
            low_report: {
                match &row.low_report_low {
                    Some(ll) => match &row.low_report_high {
                        Some(lh) if ll.len() == lh.len() => Some(
                            ll.iter()
                                .zip(lh.iter())
                                .map(|(low, high)| ScoreReport {
                                    low: *low,
                                    high: *high,
                                })
                                .collect::<Vec<ScoreReport>>(),
                        ),
                        _ => None,
                    },
                    None => None,
                }
            },
            high_report: {
                match &row.high_report_low {
                    Some(ll) => match &row.high_report_high {
                        Some(lh) if ll.len() == lh.len() => Some(
                            ll.iter()
                                .zip(lh.iter())
                                .map(|(low, high)| ScoreReport {
                                    low: *low,
                                    high: *high,
                                })
                                .collect::<Vec<ScoreReport>>(),
                        ),
                        _ => None,
                    },
                    None => None,
                }
            },
            winner_id: row.winner_id,
            loser_id: row.loser_id,
            is_scored: row.is_scored,
            created_at: row.created_at,
            updated_at: row.updated_at,
        }
    }
}

impl CupMatch2pRow {
    pub fn into(self) -> CupMatch2p {
        CupMatch2p::from_row(&self)
    }
}

impl AsRef<CupMatch2pRow> for CupMatch2pRow {
    fn as_ref(&self) -> &CupMatch2pRow {
        self
    }
}
