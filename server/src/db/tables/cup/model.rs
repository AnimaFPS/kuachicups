use crate::db::{
    tables::{player::Player, team::Role},
    Authorize, Error, Executor, HasParent, JsonRow, JsonToDbResult, Result,
};
use crate::{by_id_impl, by_id_json_impl, paginated_impl};
use async_trait::async_trait;
use chrono::{DateTime, Utc};
use derive_builder::Builder;
use num_derive::{FromPrimitive, ToPrimitive};
use num_traits::{FromPrimitive, ToPrimitive};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, Postgres};
use uuid::Uuid;

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct Cup {
    pub id: Uuid,
    pub owner_team_id: Uuid,
    pub game_mode_id: Uuid,
    pub slug: String,
    pub title: String,
    pub description_md: String,
    pub description_html: String,
    pub current_stage: Option<i32>,
    pub is_signups_closed: bool,
    pub is_finished: bool,
    pub is_published: bool,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct CupStage {
    pub id: Uuid,
    pub stage_no: i32,
    pub cup_id: Uuid,
    pub title: String,
    pub slug: String,
    pub start_immediately: bool,
    pub is_started: bool,
    pub checkintime: Option<DateTime<Utc>>,
    pub starttime: Option<DateTime<Utc>>,
    pub format: CupStageFormat,
    pub max_participants: Option<i32>,
    pub group_size: Option<i32>,
    pub group_point_award: GroupPointAward,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, JsonSchema, sqlx::Type)]
#[serde(rename_all = "snake_case")]
pub enum GroupPointAward {
    MatchScoreOnly,
    MatchWinloss,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, JsonSchema, sqlx::Type, PartialEq, Eq)]
#[serde(rename_all = "snake_case")]
pub enum CupStageFormat {
    DoubleElim,
    SingleElim,
    Groups,
    Ladder,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct CupStageScoring {
    pub id: i32,
    pub cup_stage_id: Option<Uuid>,
    pub bestof: i32,
    pub elim_round: Option<i32>,
    pub is_lb: Option<bool>,
    pub groups: bool,
    pub is_point_score: Option<bool>,
    pub point_score_greatest_is_winner: Option<bool>,
    pub is_time_score: Option<bool>,
    pub is_time_score_race: Option<bool>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct CupSignup {
    pub id: Uuid,
    pub player_id: Uuid,
    pub team_id: Option<Uuid>,
    pub cup_id: Uuid,
    pub checked_in: bool,
    pub signup_time: DateTime<Utc>,
    pub checkedin_time: Option<DateTime<Utc>>,
    pub seed_value: Option<i32>,
}

#[derive(
    JsonSchema,
    Serialize,
    Deserialize,
    Clone,
    Copy,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Debug,
    FromPrimitive,
    ToPrimitive,
)]
pub enum ElimMatchType {
    WB = 0,
    LB = 1,
    GF1 = 2,
    /// The WB winner lost to the LB winner, only applicable in double
    /// elimination
    GF2 = 3,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct CupMatch2p {
    pub id: i32,
    pub lid: i32,
    pub cup_stage_id: Uuid,
    pub group_no: Option<i32>,
    pub elim_round: Option<i32>,
    pub elim_index: Option<i32>,
    pub elim_type: Option<ElimMatchType>,
    pub elim_winner_match_id: Option<i32>,
    pub elim_winner_to_high: Option<bool>,
    pub elim_loser_match_id: Option<i32>,
    pub elim_loser_to_high: Option<bool>,
    pub elim_low_match_id: Option<i32>,
    pub elim_high_match_id: Option<i32>,
    pub scoring_id: i32,
    pub low_id: Option<Uuid>,
    pub high_id: Option<Uuid>,
    pub low_report: Option<Vec<ScoreReport>>,
    pub high_report: Option<Vec<ScoreReport>>,
    pub is_scored: bool,
    pub winner_id: Option<Uuid>,
    pub loser_id: Option<Uuid>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct CupMatch2pRow {
    pub id: i32,
    pub lid: i32,
    pub cup_stage_id: Uuid,
    pub group_no: Option<i32>,
    pub elim_round: Option<i32>,
    pub elim_index: Option<i32>,
    pub elim_type: Option<i16>,
    pub elim_winner_match_id: Option<i32>,
    pub elim_winner_to_high: Option<bool>,
    pub elim_loser_match_id: Option<i32>,
    pub elim_loser_to_high: Option<bool>,
    pub elim_low_match_id: Option<i32>,
    pub elim_high_match_id: Option<i32>,
    pub scoring_id: i32,
    pub low_id: Option<Uuid>,
    pub high_id: Option<Uuid>,
    pub low_report_low: Option<Vec<i32>>,
    pub low_report_high: Option<Vec<i32>>,
    pub high_report_low: Option<Vec<i32>>,
    pub high_report_high: Option<Vec<i32>>,
    pub is_scored: bool,
    pub winner_id: Option<Uuid>,
    pub loser_id: Option<Uuid>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder, PartialEq, Eq)]
pub struct ScoreReport {
    pub high: i32,
    pub low: i32,
}

// Basic queries
by_id_impl!(Cup, "cup", Uuid);
by_id_impl!(CupStageScoring, "cup_stage_scoring", i32);
by_id_json_impl!(CupStage, "cup_stage", Uuid);
by_id_impl!(CupSignup, "cup_signup", Uuid);
paginated_impl!(Cup, "cup");
paginated_impl!(CupStageScoring, "cup_stage_scoring");

#[async_trait]
impl HasParent<Cup> for CupStage {
    fn parent_id(&self) -> Uuid {
        self.cup_id
    }

    async fn by_parent_id<'e, E: Executor<'e, Database = Postgres>>(
        db: E,
        id: Uuid,
    ) -> Result<Vec<Self>> {
        query_as!(
            JsonRow,
            "
            SELECT to_jsonb((cup_stage.*)) as obj
            FROM cup_stage
            WHERE cup_id = $1
            ORDER BY stage_no ASC ",
            id
        )
        .fetch_all(db)
        .await?
        .from_json()
    }
}

#[async_trait]
impl HasParent<CupStage> for CupStageScoring {
    type ParentId = Option<Uuid>;

    fn parent_id(&self) -> Option<Uuid> {
        self.cup_stage_id
    }

    async fn by_parent_id<'e, E: Executor<'e, Database = Postgres>>(
        db: E,
        id: Uuid,
    ) -> Result<Vec<Self>> {
        Ok(query_as!(
            CupStageScoring,
            "SELECT * FROM cup_stage_scoring WHERE cup_stage_id = $1",
            id
        )
        .fetch_all(db)
        .await?)
    }
}
