use super::model::*;
use crate::db::{Executor, JsonRow, JsonToDbResult, Result, TeamMemberInvite};
use async_trait::async_trait;
use sqlx::query_as;
use uuid::Uuid;

impl Player {
    pub async fn teams(db: impl Executor<'_>, player_id: Uuid) -> Result<Vec<PlayerTeam>> {
        query_as!(
            JsonRow,
            "SELECT jsonb_build_object('role', player_role, 'team_id', team_id) AS obj
            FROM team_member
            WHERE team_member.player_id = $1 ",
            player_id
        )
        .fetch_all(db)
        .await?
        .from_json()
    }

    pub async fn invites(db: impl Executor<'_>, player_id: Uuid) -> Result<Vec<TeamMemberInvite>> {
        query_as!(
            JsonRow,
            "SELECT to_jsonb((i.*)) AS obj
            FROM team_member_invite i
            WHERE player_id = $1",
            player_id
        )
        .fetch_all(db)
        .await?
        .from_json()
    }
}
