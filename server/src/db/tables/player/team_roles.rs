use super::model::*;
use crate::db::{Executor, JsonRow, JsonToDbResult, Result};
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use sqlx::query_as;
use uuid::Uuid;

impl Role {
    pub fn includes(self, test: Role) -> bool {
        self >= test
    }
}

impl Player {
    pub async fn role_within(
        db: impl Executor<'_>,
        player_id: Uuid,
        team_id: Uuid,
    ) -> Result<Role> {
        query_as!(
            JsonRow,
            "
            SELECT to_jsonb(player_role) AS obj
            FROM team_member
            WHERE team_member.player_id = $1 AND team_member.team_id = $2 ",
            player_id,
            team_id,
        )
        .fetch_one(db)
        .await?
        .from_json()
    }

    pub async fn has_role_within(
        db: impl Executor<'_>,
        player_id: Uuid,
        team_id: Uuid,
        test: Role,
    ) -> bool {
        let role = Player::role_within(db, player_id, team_id).await;
        match role {
            Ok(r) => r.includes(test),
            _ => false,
        }
    }
}
