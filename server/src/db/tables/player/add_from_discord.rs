use super::model::Player;
use crate::db::{Error, Executor, Result};
use log::{error, info};
use sqlx::query_as;
use twilight_model::user::CurrentUser;

impl Player {
    pub async fn add_from_discord(
        db: impl Executor<'_> + Clone,
        user: CurrentUser,
    ) -> Result<Player> {
        let discord_id = format!("{}", user.id);

        let player = query_as!(
            Player,
            "SELECT * FROM player WHERE discord_id = $1",
            discord_id,
        )
        .fetch_one(db.clone())
        .await;

        match player {
            // Return an existing player, but maybe update the discord info if
            // needed
            Ok(player) => {
                if player.discord_avatar == user.avatar
                    && player.discord_username == user.name
                    && player.discord_discriminator == user.discriminator
                    && player.discord_email == user.email
                {
                    Ok(player)
                } else {
                    // update the discord credentials for this player
                    info!(
                        "updating discord credentials for {:#?} to match {:#?}",
                        player, user
                    );
                    Ok(query_as!(
                        Player,
                        r#"
                            UPDATE player
                            SET
                                discord_avatar = $1,
                                discord_username = $2,
                                discord_discriminator = $3,
                                discord_email = $4,
                                updated_at = now()
                            WHERE discord_id = $5
                            RETURNING player.*
                        "#,
                        user.avatar,
                        user.name,
                        user.discriminator,
                        user.email,
                        discord_id,
                    )
                    .fetch_one(db)
                    .await?)
                }
            }

            // Create a new player
            Err(_) => {
                if !user.verified {
                    error!("discord user is not verified {}", user.id);
                    return Err(Error::AuthError);
                }
                if user.bot {
                    error!("discord user is a bot {}", user.id);
                    return Err(Error::AuthError);
                }
                info!("creating player from discord user {:#?}", user);
                let player = query_as!(
                    Player,
                    r#"
                    INSERT INTO player (
                        discord_id,
                        discord_username,
                        discord_discriminator,
                        discord_avatar,
                        discord_email)
                    VALUES($1, $2, $3, $4, $5)
                    RETURNING player.*
                "#,
                    discord_id,
                    user.name,
                    user.discriminator,
                    user.avatar,
                    user.email
                )
                .fetch_one(db)
                .await?;
                Ok(player)
            }
        }
    }
}
