use super::model::*;
use crate::db::{
    paged1, ById, JsonRow, JsonToDbResult, Page, PageItem, Paged, Paginated, PaginatedSearch,
    Result,
};
use async_trait::async_trait;
use sqlx::{Executor, Postgres};

#[async_trait]
impl PaginatedSearch for Player {
    async fn page_search<'e, E: Executor<'e, Database = Postgres>>(
        db: E,
        page: Page,
        search: &str,
    ) -> Result<Paged<<Self as ById>::Id>> {
        let rows: Vec<PageItem<<Self as ById>::Id>> = sqlx::query_as!(
            PageItem,
            "
                SELECT id, updated_at
                FROM player
                WHERE to_tsvector(discord_tag) @@ to_tsquery($1)
                ORDER BY created_at DESC OFFSET $2 LIMIT $3
            ",
            search,
            (Self::page_len() as i64) * page.0,
            (Self::page_len() as i64) + 1,
        )
        .fetch_all(db)
        .await?;
        Ok(paged1::<<Self as ById>::Id>(rows, page, Self::page_len()))
    }
}
