use super::model::*;
use crate::db::{Error, JsonRow, JsonToDbResult, Result, Role, DB};
use crate::render_markdown::markdown_to_html;
use chrono::{DateTime, Utc};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, Done};
use uuid::Uuid;

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct TeamCreateForm {
    pub owner_id: Uuid,
    pub name: String,
    pub blurb_md: String,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct TeamUpdateForm {
    pub id: Uuid,
    pub name: Option<String>,
    pub blurb_md: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct TeamMember {
    pub player_id: Uuid,
    pub player_role: Role,
    pub team_id: Uuid,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct SetTeamMember {
    pub player_id: Uuid,
    pub player_role: Option<Role>,
    pub team_id: Uuid,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct InviteTeamMember {
    pub invite_by_player_id: Uuid,
    pub player_id: Uuid,
    pub team_id: Uuid,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct AcceptInviteTeamMember {
    pub player_id: Uuid,
    pub team_id: Uuid,
}

impl Team {
    pub async fn create(form: TeamCreateForm, db: &DB) -> Result<Team> {
        let mut t = db.begin().await?;
        let team = query_as!(
            Team,
            "
            INSERT INTO team(name, blurb_md, blurb_html)
            VALUES($1, $2, $3)
            RETURNING team.* ",
            form.name,
            form.blurb_md,
            markdown_to_html(&form.blurb_md),
        )
        .fetch_one(&mut t)
        .await?;
        query!(
            "
            INSERT INTO team_member(player_id, player_role, team_id)
            VALUES($1, 'admin', $2)",
            form.owner_id,
            team.id
        )
        .execute(&mut t)
        .await?;
        t.commit().await?;
        Ok(team)
    }

    pub async fn update(form: TeamUpdateForm, db: &DB) -> Result<Team> {
        let team = query_as!(
            Team,
            "
            UPDATE team
            SET name = coalesce($1, name),
                blurb_md = coalesce($2, blurb_md),
                blurb_html = coalesce($3, blurb_html),
                updated_at = now()
            WHERE id = $4
            RETURNING team.* ",
            form.name,
            form.blurb_md.clone(),
            form.blurb_md.map(|md| markdown_to_html(&md)),
            form.id
        )
        .fetch_one(db)
        .await?;
        Ok(team)
    }

    pub async fn members(team_id: Uuid, db: &DB) -> Result<Vec<TeamMember>> {
        query_as!(
            JsonRow,
            "SELECT to_jsonb((m.*)) AS obj FROM team_member m WHERE team_id = $1",
            team_id
        )
        .fetch_all(db)
        .await?
        .from_json()
    }

    pub async fn invites(team_id: Uuid, db: &DB) -> Result<Vec<TeamMemberInvite>> {
        query_as!(
            JsonRow,
            "SELECT to_jsonb((i.*)) AS obj FROM team_member_invite i WHERE team_id = $1",
            team_id
        )
        .fetch_all(db)
        .await?
        .from_json()
    }

    pub async fn make_invite(invite: InviteTeamMember, db: &DB) -> Result<u64> {
        let already_in = query!(
            "SELECT true AS already_in FROM team_member WHERE player_id = $1 AND team_id = $2",
            invite.player_id,
            invite.team_id,
        )
        .fetch_optional(db)
        .await?;
        if let Some(_) = already_in {
            return Err(Error::SomeError("player already in that team"));
        }
        Ok(query!(
            "
            INSERT INTO team_member_invite(invite_by_player_id, player_id, team_id)
            VALUES($1, $2, $3)",
            invite.invite_by_player_id,
            invite.player_id,
            invite.team_id
        )
        .execute(db)
        .await?
        .rows_affected())
    }

    pub async fn accept_invite(invite: AcceptInviteTeamMember, db: &DB) -> Result<u64> {
        let mut t = db.begin().await?;
        let invite = query_as!(
            TeamMemberInvite,
            "SELECT i.* FROM team_member_invite i
            WHERE player_id = $1 AND team_id = $2",
            invite.player_id,
            invite.team_id
        )
        .fetch_one(&mut t)
        .await?;
        let ins = query!(
            "
            INSERT INTO team_member(player_id, team_id)
            VALUES($1, $2)
            ",
            invite.player_id,
            invite.team_id,
        )
        .execute(&mut t)
        .await?
        .rows_affected();
        // TODO instead of deleting, these should maybe just set a flag like
        // meaning accepted/rejected
        let dels = query!(
            "DELETE FROM team_member_invite WHERE player_id = $1 AND team_id = $2",
            invite.player_id,
            invite.team_id
        )
        .execute(&mut t)
        .await?
        .rows_affected();
        t.commit().await?;
        Ok(ins + dels)
    }

    pub async fn reject_invite(invite: AcceptInviteTeamMember, db: &DB) -> Result<u64> {
        Ok(query!(
            "DELETE FROM team_member_invite WHERE player_id = $1 AND  team_id = $2",
            invite.player_id,
            invite.team_id,
        )
        .execute(db)
        .await?
        .rows_affected())
    }

    pub async fn set_member(
        set_member: SetTeamMember,
        can_add_member: bool,
        db: &DB,
    ) -> Result<u64> {
        use std::collections::HashMap;
        let existing_member: Option<TeamMember> = query_as!(
            JsonRow,
            "
            SELECT to_json((m.*)) AS obj
            FROM team_member m
            WHERE team_id = $1 AND player_id = $2",
            set_member.team_id,
            set_member.player_id
        )
        .fetch_optional(db)
        .await?
        .from_json()?;
        let mut transaction = db.begin().await?;
        let r = if let Some(m) = existing_member {
            if Some(m.player_role) == set_member.player_role {
                Ok(0)
            } else if let Some(player_role) = set_member.player_role {
                let r = query!(
                    "UPDATE team_member
                    SET player_role = (($1::jsonb)->>0)::team_player_role
                    WHERE player_id = $2 AND team_id = $3",
                    serde_json::to_value(&player_role).expect("cannot encode role"),
                    set_member.player_id,
                    set_member.team_id
                )
                .execute(&mut transaction)
                .await?;
                Ok(r.rows_affected())
            } else {
                let r = query!(
                    "DELETE FROM team_member WHERE player_id = $1 AND team_id = $2",
                    set_member.player_id,
                    set_member.team_id
                )
                .execute(&mut transaction)
                .await?;
                Ok(r.rows_affected())
            }
        } else if can_add_member {
            if let Some(player_role) = set_member.player_role {
                let r = query!(
                    "INSERT INTO team_member(player_id, player_role, team_id)
                    VALUES($1, (($2::jsonb)->>0)::team_player_role, $3)",
                    set_member.player_id,
                    serde_json::to_value(&player_role).expect("cannot encode role"),
                    set_member.team_id
                )
                .execute(&mut transaction)
                .await?;
                Ok(r.rows_affected())
            } else {
                Err(Error::SomeError("no such member to delete"))
            }
        } else {
            Err(Error::SomeError("no such member to add"))
        }?;
        transaction.commit().await?;
        Ok(r)
    }
}
