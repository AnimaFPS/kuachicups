pub mod from_json;
pub mod orphan;
pub mod permission;
pub mod query;
pub mod result;
pub mod tables;
pub use from_json::*;
pub use orphan::*;
pub use permission::*;
pub use query::*;
pub use result::*;
pub use tables::*;

pub trait Executor<'a>: sqlx::Executor<'a, Database = sqlx::Postgres> + Sized {}
impl<'a, T> Executor<'a> for T where T: sqlx::Executor<'a, Database = sqlx::Postgres> {}

pub type DB = sqlx::PgPool;
