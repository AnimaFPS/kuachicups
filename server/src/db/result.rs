use crate::auto_froms;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    JsonError(serde_json::error::Error),
    SqlxError(sqlx::Error),
    SomeError(&'static str),
    TwilightHttpError(twilight_http::Error),
    AuthError,
    NoneError,
}

auto_froms!(Error, {
    sqlx::Error = Error::SqlxError;
    serde_json::error::Error = Error::JsonError;
    twilight_embed_builder::EmbedBuildError = |_| Error::SomeError("EmbedBuildError");
    twilight_embed_builder::EmbedAuthorNameError = |_| Error::SomeError("EmbedAuthorNameError");
    twilight_embed_builder::EmbedColorError = |_| Error::SomeError("EmbedColorError");
    twilight_embed_builder::EmbedDescriptionError = |_| Error::SomeError("EmbedDescriptionError");
    twilight_embed_builder::EmbedTitleError = |_| Error::SomeError("EmbedTitleError");
    twilight_embed_builder::EmbedFieldError = |_| Error::SomeError("EmbedFieldError");
    twilight_embed_builder::EmbedFooterTextError = |_| Error::SomeError("EmbedFooterTextError");
    twilight_embed_builder::ImageSourceAttachmentError =
        |_| Error::SomeError("ImageSourceAttachmentError");
    twilight_embed_builder::ImageSourceUrlError = |_| Error::SomeError("ImageSourceUrlError");
    twilight_http::Error = Error::TwilightHttpError;
    twilight_http::request::channel::message::create_message::CreateMessageError =
        |_| Error::SomeError("CreateMessageError");
    twilight_http::request::channel::message::update_message::UpdateMessageError =
        |_| Error::SomeError("UpdateMessageError");
});

// let Error be used as a result for rocket handlers
use rocket::{
    request::Request,
    response::{status::BadRequest, Responder},
};

impl<'r, 'o: 'r> Responder<'r, 'o> for Error {
    fn respond_to(self, request: &'r Request) -> rocket::response::Result<'o> {
        BadRequest(Some(format!("{:#?}", self))).respond_to(request)
    }
}

// let Error be used for type information for openapi spec
use okapi::openapi3::Responses;
use rocket_okapi::{
    gen::OpenApiGenerator, openapi, response::OpenApiResponder, Result as OkapiResult,
};

impl OpenApiResponder for Error {
    fn responses(gen: &mut OpenApiGenerator) -> OkapiResult<Responses> {
        <BadRequest<String> as OpenApiResponder>::responses(gen)
    }
}

impl std::error::Error for Error {}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "db::Error")
    }
}
