use crate::db::query::by_id::*;
use crate::db::Result;
use async_trait::async_trait;
use chrono::{DateTime, Utc};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{Executor, Postgres};
use uuid::Uuid;

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct Page(pub i64);

pub const PAGE_LEN: usize = 64;

#[derive(Debug, Clone, Serialize, JsonSchema)]
pub struct PageItem<Id> {
    pub id: Id,
    pub updated_at: DateTime<Utc>,
}

#[derive(Debug, Clone, Serialize, JsonSchema)]
pub struct Paged<Id> {
    pub page: Page,
    pub has_more: bool,
    pub total_count: i32,
    pub items: Vec<PageItem<Id>>,
}

#[async_trait]
pub trait Paginated: ById {
    fn page_len() -> usize {
        PAGE_LEN as usize
    }
    async fn all<'e, E: Executor<'e, Database = Postgres>>(db: E) -> Result<Vec<Self>>;
    async fn page<'e, E: Executor<'e, Database = Postgres>>(
        db: E,
        page: Page,
    ) -> Result<Paged<<Self as ById>::Id>>;
}

#[async_trait]
pub trait PaginatedSearch: Paginated {
    async fn page_search<'e, E: Executor<'e, Database = Postgres>>(
        db: E,
        page: Page,
        search: &str,
    ) -> Result<Paged<<Self as ById>::Id>>;
}

#[macro_export]
macro_rules! paginated_impl {
    ($type:ident, $table:literal, $page_len:expr, $order_by:expr) => {
        #[allow(dead_code)]
        #[async_trait::async_trait]
        impl crate::db::query::paginate::Paginated for $type {
            fn page_len() -> usize {
                $page_len
            }

            async fn all<'e, E: sqlx::Executor<'e, Database = sqlx::Postgres>>(
                db: E,
            ) -> crate::db::Result<Vec<Self>> {
                Ok(
                    sqlx::query_as!($type, "SELECT * FROM " + $table + " ORDER BY " + $order_by)
                        .fetch_all(db)
                        .await?,
                )
            }

            async fn page<'e, E: sqlx::Executor<'e, Database = sqlx::Postgres>>(
                db: E,
                page: crate::db::query::paginate::Page,
            ) -> crate::db::Result<
                crate::db::query::paginate::Paged<<Self as crate::db::by_id::ById>::Id>,
            > {
                use crate::db::query::by_id::ById;
                use crate::db::query::paginate::{paged1, Page, PageItem};
                let rows: Vec<PageItem<<Self as ById>::Id>> = sqlx::query_as!(
                    PageItem,
                    "SELECT id, updated_at FROM "
                        + $table
                        + " ORDER BY "
                        + $order_by
                        + " "
                        + " OFFSET $1 LIMIT $2",
                    (Self::page_len() as i64) * page.0,
                    (Self::page_len() as i64) + 1,
                )
                .fetch_all(db)
                .await?;
                Ok(paged1::<<Self as ById>::Id>(rows, page, Self::page_len()))
            }
        }
    };

    ($type:ident, $table:literal, $page_len:expr) => {
        paginated_impl!($type, $table, $page_len, "created_at DESC");
    };

    ($type:ident, $table:literal) => {
        paginated_impl!(
            $type,
            $table,
            crate::db::query::paginate::PAGE_LEN,
            "created_at DESC"
        );
    };
}

pub fn paged1<Id: Clone + Serialize + JsonSchema>(
    mut items: Vec<PageItem<Id>>,
    page: Page,
    page_len: usize,
) -> Paged<Id> {
    use std::cmp::Ordering;
    let next_start: Option<Id> = match items.len().cmp(&page_len) {
        Ordering::Less => None,
        Ordering::Equal => None,
        Ordering::Greater => items.pop().map(|pi| pi.id),
    };
    let total_count = items.len() as i32;
    let has_more = next_start.is_some();
    Paged {
        page,
        has_more,
        total_count,
        items,
    }
}
