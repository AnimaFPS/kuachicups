use crate::db::Result;
use async_trait::async_trait;
use sqlx::{Executor, Postgres};
use uuid::Uuid;

#[async_trait]
pub trait HasParent<Parent>: Sized {
    type ParentId = Uuid;

    fn parent_id(&self) -> Self::ParentId;

    async fn by_parent_id<'e, E: Executor<'e, Database = Postgres>>(
        db: E,
        id: Uuid,
    ) -> Result<Vec<Self>>;
}
