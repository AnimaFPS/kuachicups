use crate::db::{ById, Error, Executor, Result};
use async_trait::async_trait;
use log::error;
use uuid::Uuid;

#[derive(Clone, Copy, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub enum Permit {
    Read,
    ReadWrite,
}

#[derive(Copy, Clone, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub struct PermitFor<T> {
    _marker: std::marker::PhantomData<T>,
    permit: Permit,
}

// never actually used; instead everything has its own ugly permission methods
// TODO unify permissions model

#[async_trait]
pub trait Authorize: Sized {
    async fn can_read<'e, E: sqlx::Executor<'e, Database = sqlx::Postgres> + Clone>(
        _db: E,
        _player_id: Uuid,
        _obj_id: Uuid,
    ) -> Result<bool> {
        Ok(true)
    }

    async fn can_readwrite<'e, E: sqlx::Executor<'e, Database = sqlx::Postgres> + Clone>(
        _db: E,
        _player_id: Uuid,
        _obj_id: Uuid,
    ) -> Result<bool> {
        Ok(false)
    }

    async fn authorize<'e, E: sqlx::Executor<'e, Database = sqlx::Postgres> + Clone>(
        db: E,
        player_id: Uuid,
        obj_id: Uuid,
        perm: Permit,
    ) -> Result<()> {
        match perm {
            Permit::Read => {
                if Self::can_read(db, player_id, obj_id).await? {
                    return Ok(());
                }
            }
            Permit::ReadWrite => {
                if Self::can_readwrite(db, player_id, obj_id).await? {
                    return Ok(());
                }
            }
        }
        error!("deny {:?} to player {:?}", perm, player_id);
        Err(Error::AuthError)
    }
}
