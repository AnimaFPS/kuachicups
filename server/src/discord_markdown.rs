use comrak::{
    arena_tree::Node,
    format_commonmark,
    nodes::{Ast, AstNode, NodeValue},
    parse_document, Arena, ComrakExtensionOptions, ComrakOptions, ComrakParseOptions,
    ComrakRenderOptions,
};
use std::cell::RefCell;
use std::default::Default;
use NodeValue::*;

/*
 * Markdown supported by discord:
 *
 * 1. **bold**
 * 2. `inline code`
 * 3. ~~strike through~~
 * 4. *italics*
 * 5. ```[lang] code blocks```
 * 6. combinations of the above
 * 7. > block quotes (only up to one level deep)
 * 8. _underline_
 *
 * Supported in embeds:
 * 9. (links)[https://links]
 *
 */

pub fn textual_md(source: &str) -> String {
    let arena = Arena::new();
    let options = ComrakOptions {
        extension: ComrakExtensionOptions {
            strikethrough: true,
            tagfilter: false,
            table: true,
            autolink: false,
            tasklist: false,
            superscript: false,
            header_ids: None,
            footnotes: false,
            description_lists: false,
        },
        parse: ComrakParseOptions {
            smart: true,
            default_info_string: None,
        },
        render: ComrakRenderOptions::default(),
    };
    let doc = parse_document(&arena, source, &options);
    let mut v = Vec::new();
    let mut r = String::new();
    textify(doc, &mut v);
    for i in v {
        r.push_str(&String::from_utf8(i).expect("textual_md invalid utf8"));
    }
    r
}

pub fn discord_md(source: &str) -> String {
    let arena = Arena::new();
    let options = ComrakOptions {
        extension: ComrakExtensionOptions {
            strikethrough: true,
            tagfilter: false,
            table: true,
            autolink: false,
            tasklist: false,
            superscript: false,
            header_ids: None,
            footnotes: false,
            description_lists: false,
        },
        parse: ComrakParseOptions {
            smart: true,
            default_info_string: None,
        },
        render: ComrakRenderOptions::default(),
    };
    let doc = parse_document(&arena, source, &options);
    let mut out = Vec::new();
    discordify(&arena, &doc);
    format_commonmark(&doc, &options, &mut out).expect("discord_md io error");
    String::from_utf8(out)
        .expect("discord_md invalid utf8")
        .replace("\\_\\_", "__")
}

fn go<'a, F>(node: &'a AstNode<'a>, f: &F)
where
    F: Fn(&'a AstNode<'a>) -> bool,
{
    if f(node) {
        for c in node.children() {
            go(c, f);
        }
    }
}

fn inlinify<'a>(arena: &'a Arena<AstNode<'a>>, node: &'a AstNode<'a>, next: &'a AstNode<'a>) {
    if !node.data.borrow().value.block() {
        next.append(node);
        return;
    }
    for child in node.children() {
        let val = &child.data.borrow().value;
        if !val.block() {
            next.append(child);
        } else if let Item(item) = val {
            let next_item = arena.alloc(AstNode::new(RefCell::new(Ast::new(Item(item.clone())))));
            inlinify(arena, child, next_item);
            next.append(next_item);
        } else if let List(list) = val {
            let next_list = arena.alloc(AstNode::new(RefCell::new(Ast::new(List(list.clone())))));
            for list_item in child.descendants() {
                if let Item(item) = list_item.data.borrow().value {
                    let next_item =
                        arena.alloc(AstNode::new(RefCell::new(Ast::new(Item(item.clone())))));
                    inlinify(arena, list_item, next_item);
                    next_list.append(next_item);
                }
            }
            next.append(next_list);
        } else {
            inlinify(arena, child, next);
        }
    }
}

fn textify<'a, 'o>(node: &'a AstNode<'a>, out: &'o mut Vec<Vec<u8>>) {
    for child in node.descendants() {
        if let Text(text) = &child.data.borrow().value {
            out.push(text.clone());
        }
    }
}

fn discordify<'a>(arena: &'a Arena<AstNode<'a>>, root: &'a AstNode<'a>) {
    go(root, &|node| match &node.data.borrow().value {
        Heading(_) => {
            let next = arena.alloc(AstNode::new(RefCell::new(Ast::new(Strong))));
            let mut texts: Vec<Vec<u8>> = Vec::new();
            texts.push("__".as_bytes().to_vec());
            textify(node, &mut texts);
            texts.push("__".as_bytes().to_vec());
            for text in texts {
                next.append(arena.alloc(AstNode::new(RefCell::new(Ast::new(Text(text))))));
            }
            node.insert_before(next);
            node.insert_before(arena.alloc(AstNode::new(RefCell::new(Ast::new(SoftBreak)))));
            node.detach();
            false
        }

        // drop these entirely
        DescriptionList
        | DescriptionItem(_)
        | DescriptionTerm
        | DescriptionDetails
        | HtmlBlock(_)
        | FootnoteDefinition(_)
        | TaskItem(_)
        | Table(_)
        | TableRow(_)
        | TableCell
        | HtmlInline(_)
        | FootnoteReference(_) => {
            let next = arena.alloc(AstNode::new(RefCell::new(Ast::new(Paragraph))));
            let mut texts: Vec<Vec<u8>> = Vec::new();
            textify(node, &mut texts);
            for text in texts {
                next.append(arena.alloc(AstNode::new(RefCell::new(Ast::new(Text(text))))));
            }
            node.insert_before(next);
            node.detach();
            false
        }

        // inline inside these block types
        BlockQuote | List(_) | Item(_) | Paragraph => {
            let next = arena.alloc(AstNode::new(RefCell::new(Ast::new(
                node.data.borrow().value.clone(),
            ))));
            inlinify(arena, node, next);
            node.insert_before(next);
            match &node.data.borrow().value {
                List(_) => {
                    node.insert_before(
                        arena.alloc(AstNode::new(RefCell::new(Ast::new(LineBreak)))),
                    );
                }
                _ => {}
            }
            node.detach();
            false
        }

        _ => true,
    });
}
