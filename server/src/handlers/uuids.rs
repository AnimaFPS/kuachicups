use crate::auto_froms;
use okapi::openapi3::{Parameter, ParameterValue};
use rocket::http::RawStr;
use rocket::request::{FromFormValue, FromParam};
use rocket_contrib::uuid::Uuid as RocketUuid;
use rocket_okapi::gen::OpenApiGenerator;
use rocket_okapi::request::{OpenApiFromFormValue, OpenApiFromParam};
use rocket_okapi::Result as OResult;
use schemars::{gen::SchemaGenerator, schema::Schema, JsonSchema};
use uuid::Uuid;

pub struct Uuids(pub Vec<Uuid>);
auto_froms!(Uuids, { Vec<Uuid> = Uuids; });

impl JsonSchema for Uuids {
    fn schema_name() -> String {
        Vec::<Uuid>::schema_name()
    }
    fn json_schema(gen: &mut SchemaGenerator) -> Schema {
        Vec::<Uuid>::json_schema(gen)
    }
}

impl<'r> FromParam<'r> for Uuids {
    type Error = &'r RawStr;
    fn from_param(param: &'r RawStr) -> Result<Self, Self::Error> {
        let uuids: Result<Vec<Uuid>, Self::Error> = String::from_param(param)?
            .split(',')
            .map(|u| Uuid::parse_str(u).or(Err(param)))
            .collect();
        Ok(Uuids(uuids?))
    }
}

impl<'r> OpenApiFromParam<'r> for Uuids {
    fn path_parameter(gen: &mut OpenApiGenerator, name: String) -> OResult<Parameter> {
        let schema = gen.json_schema::<Vec<Uuid>>();
        Ok(Parameter {
            name,
            location: "path".to_owned(),
            description: None,
            required: true,
            deprecated: false,
            allow_empty_value: false,
            value: ParameterValue::Schema {
                style: None,
                explode: None,
                allow_reserved: false,
                schema,
                example: None,
                examples: None,
            },
            extensions: Default::default(),
        })
    }
}

pub struct Uuid1(pub Uuid);
auto_froms!(Uuid1, {
    Uuid = Uuid1;
});

impl JsonSchema for Uuid1 {
    fn schema_name() -> String {
        Uuid::schema_name()
    }
    fn json_schema(gen: &mut SchemaGenerator) -> Schema {
        Uuid::json_schema(gen)
    }
}

impl<'r> FromParam<'r> for Uuid1 {
    type Error = uuid::Error;
    fn from_param(param: &'r RawStr) -> Result<Self, Self::Error> {
        RocketUuid::from_param(param).map(|u| Uuid1(u.into_inner()))
    }
}

impl<'v> FromFormValue<'v> for Uuid1 {
    type Error = &'v RawStr;
    fn from_form_value(form_value: &'v RawStr) -> Result<Self, Self::Error> {
        RocketUuid::from_form_value(form_value).map(|u| Uuid1(u.into_inner()))
    }
}

impl<'r> OpenApiFromFormValue<'r> for Uuid1 {
    fn query_parameter(
        gen: &mut OpenApiGenerator,
        name: String,
        required: bool,
    ) -> OResult<Parameter> {
        let schema = gen.json_schema::<Uuid>();
        Ok(Parameter {
            name,
            location: "query".to_owned(),
            description: None,
            required,
            deprecated: false,
            allow_empty_value: false,
            value: ParameterValue::Schema {
                style: None,
                explode: None,
                allow_reserved: false,
                schema,
                example: None,
                examples: None,
            },
            extensions: Default::default(),
        })
    }
}

impl<'r> OpenApiFromParam<'r> for Uuid1 {
    fn path_parameter(gen: &mut OpenApiGenerator, name: String) -> OResult<Parameter> {
        let schema = gen.json_schema::<Uuid>();
        Ok(Parameter {
            name,
            location: "path".to_owned(),
            description: None,
            required: true,
            deprecated: false,
            allow_empty_value: false,
            value: ParameterValue::Schema {
                style: None,
                explode: None,
                allow_reserved: false,
                schema,
                example: None,
                examples: None,
            },
            extensions: Default::default(),
        })
    }
}
