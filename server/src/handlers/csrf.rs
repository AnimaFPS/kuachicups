use async_trait::async_trait;
use csrf::{ChaCha20Poly1305CsrfProtection, CsrfProtection, CSRF_COOKIE_NAME, CSRF_HEADER};
use data_encoding::BASE64;
use rocket::{
    fairing,
    fairing::Fairing,
    http::{Cookie, CookieJar, Method, SameSite, Status},
    outcome::Outcome::*,
    request::{FromRequest, Outcome, Request, State},
};

const CSRF_TTL_SECS: i64 = 60 * 60 * 24 * 7;

#[derive(Debug)]
pub struct ProvideCsrf;

#[async_trait]
impl<'a, 'r> FromRequest<'a, 'r> for ProvideCsrf {
    type Error = ();
    async fn from_request(request: &'a Request<'r>) -> Outcome<Self, Self::Error> {
        let csrf = State::<ChaCha20Poly1305CsrfProtection>::from_request(request).await;
        match csrf {
            Success(csrf) => {
                provide_csrf(&csrf, request.cookies());
                Success(ProvideCsrf)
            }
            Failure(err) => Failure(err),
            Forward(err) => Forward(err),
        }
    }
}

pub fn provide_csrf(csrf: &ChaCha20Poly1305CsrfProtection, cookies: &CookieJar) {
    let (token, cookie) = csrf.generate_token_pair(None, CSRF_TTL_SECS).unwrap();
    let token = token.b64_string();
    let cookie = cookie.b64_string();
    cookies.add(
        Cookie::build(CSRF_COOKIE_NAME, cookie)
            .path("/")
            .same_site(SameSite::Lax)
            .finish(),
    );
    cookies.add(
        Cookie::build(CSRF_HEADER, token)
            .path("/")
            .same_site(SameSite::Strict)
            .finish(),
    );
}

#[derive(Debug)]
pub struct ProtectCsrf;

#[async_trait]
impl<'a, 'r> FromRequest<'a, 'r> for ProtectCsrf {
    type Error = ();

    async fn from_request(request: &'a Request<'r>) -> Outcome<Self, Self::Error> {
        use csrf::{CSRF_COOKIE_NAME, CSRF_HEADER};
        use rocket::outcome::Outcome::*;
        let csrf = State::<ChaCha20Poly1305CsrfProtection>::from_request(request).await;
        match csrf {
            Success(csrf) => {
                let cookies = request.cookies();
                let headers = request.headers();
                let token = headers.get(CSRF_HEADER).last();
                let cookie = &cookies.get(CSRF_COOKIE_NAME).map(|c| c.value().to_string());
                match (cookie, token) {
                    (Some(cookie), Some(token)) => {
                        if validate_csrf(csrf.inner(), token, cookie) {
                            log::info!("csrf ok!\ncookie={:?}\ntoken={:?}", cookie, token);
                            Success(ProtectCsrf)
                        } else {
                            log::error!(
                                "csrf exists but is invalid\ncookie={:?}\ntoken={:?}",
                                cookie,
                                token
                            );
                            Failure((Status::Forbidden, ()))
                        }
                    }
                    _ => {
                        log::error!(
                            "missing csrf parameter\ncookie={:?}\ntoken={:?}",
                            cookie,
                            token
                        );
                        Failure((Status::Forbidden, ()))
                    }
                }
            }
            Failure(err) => {
                log::error!("csrf error {:#?}", err);
                Failure(err)
            }
            Forward(err) => {
                log::error!("csrf forward error {:#?}", err);
                Failure((Status::Forbidden, err))
            }
        }
    }
}

pub fn validate_csrf(csrf: &ChaCha20Poly1305CsrfProtection, token: &str, cookie: &str) -> bool {
    let token = BASE64
        .decode(token.as_bytes())
        .ok()
        .and_then(|t| csrf.parse_token(&t).ok());
    let cookie = BASE64
        .decode(cookie.as_bytes())
        .ok()
        .and_then(|c| csrf.parse_cookie(&c).ok());
    match (token, cookie) {
        (Some(token), Some(cookie)) => csrf.verify_token_pair(&token, &cookie),
        _ => false,
    }
}
