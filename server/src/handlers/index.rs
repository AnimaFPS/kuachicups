use super::csrf::{ProtectCsrf, ProvideCsrf};
use crate::app::{DiscordClientBuilder, SettingCache};
use crate::db::{Player, DB};
use oauth2::PkceCodeVerifier;
use rocket::{
    get,
    http::uri::Segments,
    http::{Cookie, CookieJar, SameSite, Status},
    request::FromSegments,
    response::NamedFile,
    State,
};
use serde::{Deserialize, Serialize};
use std::convert::TryInto;
use std::time::Duration as StdDuration;
use time::Duration;

use super::api::player::{
    UserSession, OAUTH2_DEFAULT_EXPIRY, OAUTH2_PKCE_CODE_VERIFIER_COOKIE_NAME,
    USER_SESSION_COOKIE_NAME,
};

#[derive(Serialize, Deserialize)]
struct OAuth2State {
    dest: String,
    csrf: String,
}

pub struct ClientIndexPath;
impl FromSegments<'_> for ClientIndexPath {
    type Error = ();
    #[inline(always)]
    fn from_segments(segments: Segments<'_>) -> Result<Self, Self::Error> {
        let pb = segments.into_path_buf(false).map_err(|_| ())?;
        if pb.starts_with("api") {
            return Err(());
        }
        if let Some(_) = pb.extension() {
            return Err(());
        }
        Ok(ClientIndexPath)
    }
}

#[get("/", rank = 10)]
pub async fn index(settings: State<'_, SettingCache>, _csrf: ProvideCsrf) -> Option<NamedFile> {
    NamedFile::open(settings.index_file.clone()).await.ok()
}

#[get("/<_path..>", rank = 30)]
pub async fn client_index_anywhere(
    _path: ClientIndexPath,
    settings: State<'_, SettingCache>,
    csrf: ProvideCsrf,
) -> Option<NamedFile> {
    index(settings, csrf).await
}

#[get("/logged_in?<code>&<state>")]
pub async fn logged_in(
    code: String,
    state: String,
    cookies: &CookieJar<'_>,
    db: State<'_, DB>,
    client: State<'_, oauth2::basic::BasicClient>,
    protect: State<'_, csrf::ChaCha20Poly1305CsrfProtection>,
    dcb: State<'_, DiscordClientBuilder>,
    settings: State<'_, SettingCache>,
) -> Result<NamedFile, Status> {
    log::info!("got auth code {:#?}", code);

    use super::csrf::{provide_csrf, validate_csrf};
    use oauth2::{
        basic::BasicTokenResponse, reqwest::async_http_client, AsyncCodeTokenRequest,
        AuthorizationCode, TokenResponse,
    };

    // 1. Verify CSRF
    let csrf_cookie = &cookies
        .get(csrf::CSRF_COOKIE_NAME)
        .map_or(Err(Status::Forbidden), |c| Ok(c.value().to_string()))?;

    log::info!("got csrf cookie {:#?}", csrf_cookie);

    if !validate_csrf(&protect, &state, csrf_cookie) {
        return Err(Status::Forbidden);
    }

    // 2. Get the PKCE verifier which we stored as an encrypted cookie on the client
    let pkce_verifier = PkceCodeVerifier::new(
        cookies
            .get_private(OAUTH2_PKCE_CODE_VERIFIER_COOKIE_NAME)
            .ok_or(Status::Forbidden)?
            .value()
            .to_string(),
    );

    log::info!("got pkce verifier");

    // 3. Perform OAuth2 token exchange
    let token: BasicTokenResponse = client
        .exchange_code(AuthorizationCode::new(code))
        .set_pkce_verifier(pkce_verifier)
        .request_async(async_http_client)
        .await
        .map_err(|err| {
            log::error!("Cannot create BasicTokenResponse due to\n{:#?}", err);
            Status::Forbidden
        })?;

    log::info!("got token");

    // 4. Remove the PKCE verifier cookie
    cookies.remove_private(
        Cookie::build(OAUTH2_PKCE_CODE_VERIFIER_COOKIE_NAME, "")
            .path("/logged_in")
            .same_site(SameSite::Lax)
            .finish(),
    );

    log::info!("removed pkce");

    // 10. Provide new CSRF cookie/header pair for the client app to use
    provide_csrf(&protect, cookies);

    log::info!("provided csrf");

    let max_age: Duration = {
        let std: StdDuration = token.expires_in().unwrap_or(OAUTH2_DEFAULT_EXPIRY);
        // When upgrading to time 0.2 use
        std.try_into().ok().ok_or(Status::Forbidden)?
    };

    log::info!("made max_age {:#?}", max_age);

    // Now we actually create a user!
    //
    // 11. Get the user info from discord
    let discord = dcb
        .client_bearer(token.access_token().secret())
        .expect("cannot create discord client");

    let user = discord
        .current_user()
        .await
        .expect("cannot query current user");

    let player = Player::add_from_discord(db.inner(), user)
        .await
        .ok()
        .ok_or(Status::Forbidden)?;

    log::info!("got player {:#?}", player);

    let session = UserSession {
        response: token,
        user: player.id,
    };

    // 12. Set the session!
    cookies.add_private(
        Cookie::build(
            USER_SESSION_COOKIE_NAME,
            serde_json::to_string(&session)
                .ok()
                .ok_or(Status::Forbidden)?,
        )
        .path("/")
        .http_only(true)
        .same_site(SameSite::Strict)
        .max_age(max_age)
        .finish(),
    );

    // 13. Now just serve the client app
    index(settings, ProvideCsrf).await.ok_or(Status::NotFound)
}
