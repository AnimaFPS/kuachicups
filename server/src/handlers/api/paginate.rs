pub use crate::db::{paged1, Page, PageItem, Paged, Paginated, PaginatedSearch, PAGE_LEN};
use crate::db::{ById, DB};
use crate::handlers::uuids::Uuid1;
use async_trait::async_trait;
use rocket_contrib::json::Json;
use sqlx::{Executor, Postgres};
use uuid::Uuid;

#[async_trait]
pub trait PaginateApi: Paginated {
    async fn get_by_ids(db: &DB, ids: &[Self::Id]) -> Option<Json<Vec<Self>>>;
    async fn get_by_id(db: &DB, id: Self::Id) -> Option<Json<Self>>;
    async fn get_page(db: &DB, page: Option<i64>) -> Option<Json<Paged<Self::Id>>>;
    async fn get_all(db: &DB) -> Option<Json<Vec<Self>>>;
}

#[async_trait]
impl<T: Paginated> PaginateApi for T
where
    <Self as ById>::Id: Send + Sync,
{
    async fn get_by_ids(db: &DB, ids: &[<Self as ById>::Id]) -> Option<Json<Vec<Self>>> {
        match T::by_ids(db, ids).await {
            Ok(r) => Some(Json(r)),
            Err(_) => None,
        }
    }

    async fn get_by_id(db: &DB, id: <Self as ById>::Id) -> Option<Json<Self>> {
        match T::by_id(db, id).await {
            Ok(r) => Some(Json(r)),
            Err(_) => None,
        }
    }

    async fn get_page(db: &DB, page: Option<i64>) -> Option<Json<Paged<<Self as ById>::Id>>> {
        T::page(db, Page(page.unwrap_or(0))).await.ok().map(Json)
    }

    async fn get_all(db: &DB) -> Option<Json<Vec<Self>>> {
        T::all(db).await.ok().map(Json)
    }
}

#[async_trait]
pub trait PaginateSearchApi: Paginated {
    async fn get_page_search(
        db: &DB,
        page: Option<i64>,
        search: &str,
    ) -> Option<Json<Paged<Self::Id>>>;
}

#[async_trait]
impl<T: PaginatedSearch> PaginateSearchApi for T
where
    <Self as ById>::Id: Send + Sync,
{
    async fn get_page_search(
        db: &DB,
        page: Option<i64>,
        search: &str,
    ) -> Option<Json<Paged<<Self as ById>::Id>>> {
        T::page_search(db, Page(page.unwrap_or(0)), search)
            .await
            .ok()
            .map(Json)
    }
}
