use super::paginate::{Paged, PaginateApi, PaginateSearchApi};
use crate::db;
use crate::db::{
    ById, IsAdmin, Paginated, PaginatedSearch, Player, PlayerTeam, Result, Role, TeamMemberInvite,
    DB,
};
use crate::handlers::{
    csrf::ProtectCsrf,
    uuids::{Uuid1, Uuids},
};
use oauth2::{PkceCodeChallenge, TokenResponse};
use rocket::{
    get,
    http::Status,
    http::{Cookie, CookieJar, SameSite},
    request::{FromRequest, Outcome, Request},
    State,
};
use rocket_contrib::json::Json;
use rocket_okapi::openapi;
use serde::{Deserialize, Serialize};
use sqlx::{Executor, Postgres};
use std::convert::TryInto;
use std::time::Duration as StdDuration;
use time::Duration;
use uuid::Uuid;

pub const OAUTH2_PKCE_CODE_VERIFIER_COOKIE_NAME: &'static str = "oauth2pkce";
pub const USER_SESSION_COOKIE_NAME: &'static str = "user";
pub const OAUTH2_DEFAULT_EXPIRY: StdDuration = StdDuration::from_secs(604800);

#[derive(Serialize, Deserialize, Debug)]
pub struct UserSession {
    pub response: oauth2::basic::BasicTokenResponse,
    pub user: Uuid,
}

pub struct AdminSession(pub UserSession);

impl UserSession {
    pub fn cookie(session: Option<&Self>) -> Cookie {
        let value = session
            .map(|s| serde_json::to_string(s).expect("cannot create cookie json"))
            .unwrap_or("".to_string());
        let max_age: StdDuration = session
            .and_then(|s| s.response.expires_in())
            .unwrap_or(StdDuration::from_secs(0));
        Cookie::build(USER_SESSION_COOKIE_NAME, value)
            .path("/")
            .http_only(true)
            .same_site(SameSite::Strict)
            .max_age(
                max_age
                    .try_into()
                    .expect("std->time duration conversion failed"),
            )
            .finish()
    }

    pub async fn require_team_role(
        &self,
        db: impl Executor<'_, Database = Postgres>,
        team: Uuid,
        role: Role,
    ) -> Result<()> {
        if Player::has_role_within(db, self.user, team, role).await {
            Ok(())
        } else {
            Err(db::Error::AuthError)
        }
    }
}

#[rocket::async_trait]
impl<'a, 'r> FromRequest<'a, 'r> for UserSession {
    type Error = ();
    async fn from_request(request: &'a Request<'r>) -> Outcome<Self, Self::Error> {
        match <&'a CookieJar>::from_request(request).await {
            Outcome::Success(cookies) => match cookies.get_private(USER_SESSION_COOKIE_NAME) {
                Some(session_json) => match serde_json::de::from_str(session_json.value()) {
                    Ok(session) => {
                        return Outcome::Success(session);
                    }
                    _ => {}
                },
                _ => {}
            },
            _ => {}
        }
        Outcome::Failure((Status::Forbidden, ()))
    }
}

#[rocket::async_trait]
impl<'a, 'r> FromRequest<'a, 'r> for AdminSession {
    type Error = ();
    async fn from_request(request: &'a Request<'r>) -> Outcome<Self, Self::Error> {
        let session = UserSession::from_request(request).await;
        let db = State::<'_, DB>::from_request(request).await;
        match (session, db) {
            (Outcome::Success(session), Outcome::Success(db)) => {
                let r = Player::is_admin(db.inner(), session.user).await;
                if r.ok() == Some(IsAdmin { is_admin: true }) {
                    return Outcome::Success(AdminSession(session));
                }
            }
            _ => {}
        }
        Outcome::Failure((Status::Forbidden, ()))
    }
}

/// OAuth2 challenge using private cookie to store the code verifier
#[openapi]
#[get("/oauth2_challenge")]
pub async fn get_oauth2_challenge(_csrf: ProtectCsrf, cookies: &CookieJar<'_>) -> String {
    let (challenge, verifier) = PkceCodeChallenge::new_random_sha256();
    cookies.add_private(
        Cookie::build(
            OAUTH2_PKCE_CODE_VERIFIER_COOKIE_NAME,
            verifier.secret().to_owned(),
        )
        .path("/logged_in")
        .same_site(SameSite::Lax)
        .http_only(true)
        .finish(),
    );
    challenge.as_str().to_string()
}

#[openapi]
#[get("/logout")]
pub fn logout(_csrf: ProtectCsrf, cookies: &CookieJar<'_>) {
    cookies.remove_private(UserSession::cookie(None));
}

#[openapi]
#[get("/session/user")]
pub async fn current_session(
    _csrf: ProtectCsrf,
    session: UserSession,
    db: State<'_, DB>,
) -> Option<Json<Player>> {
    Player::by_id(db.inner(), session.user).await.ok().map(Json)
}

#[openapi]
#[get("/session/is_admin")]
pub async fn current_session_is_admin(
    _csrf: ProtectCsrf,
    session: UserSession,
    db: State<'_, DB>,
) -> Option<Json<IsAdmin>> {
    Player::is_admin(db.inner(), session.user)
        .await
        .ok()
        .map(Json)
}

#[openapi]
#[get("/session/team_invites")]
pub async fn current_session_team_invites(
    _csrf: ProtectCsrf,
    session: UserSession,
    db: State<'_, DB>,
) -> Result<Json<Vec<TeamMemberInvite>>> {
    Ok(Json(Player::invites(db.inner(), session.user).await?))
}

#[openapi]
#[get("/profile/<ids>")]
pub async fn profile(ids: Uuids, db: State<'_, DB>) -> Option<Json<Vec<Player>>> {
    Player::get_by_ids(db.inner(), &ids.0).await
}

#[openapi]
#[get("/profile/<id>/teams")]
pub async fn teams(id: Uuid1, db: State<'_, DB>) -> Option<Json<Vec<PlayerTeam>>> {
    Some(Json(Player::teams(db.inner(), id.0).await.ok()?))
}

#[openapi]
#[get("/players/page?<page>&<terms>")]
pub async fn players_page(
    page: Option<i64>,
    terms: Option<String>,
    db: State<'_, DB>,
) -> Option<Json<Paged<Uuid>>> {
    if let Some(terms) = terms {
        Player::get_page_search(db.inner(), page, &terms).await
    } else {
        Player::get_page(db.inner(), page).await
    }
}
