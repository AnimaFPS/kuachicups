use super::{
    paginate::PaginateApi,
    player::{AdminSession, UserSession},
};
use crate::handlers::{
    csrf::ProtectCsrf,
    uuids::{Uuid1, Uuids},
};
use crate::{
    db,
    db::tables::*,
    db::{ById, HasParent, PageItem, Paged, DB},
};
use chrono::{DateTime, Utc};
use rocket::{get, post, State};
use rocket_contrib::json::Json;
use rocket_okapi::openapi;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, Acquire};
use std::marker::PhantomData;
use uuid::Uuid;

/*
 * Basic queries
 * =============
 * */

/// Retrieve cups by ids
#[openapi]
#[get("/cups/<ids>")]
pub async fn cups(ids: Uuids, db: State<'_, DB>) -> Option<Json<Vec<Cup>>> {
    Cup::get_by_ids(db.inner(), &ids.0).await
}

/// Retrieve a page of cups
#[openapi]
#[get("/cups/page?<page>")]
pub async fn cups_page(page: Option<i64>, db: State<'_, DB>) -> Option<Json<Paged<Uuid>>> {
    Cup::get_page(db.inner(), page).await
}

/// Retrieve the stages of a cup
#[openapi]
#[get("/cup/<id>/stages")]
pub async fn cup_stages(id: Uuid1, db: State<'_, DB>) -> Option<Json<Vec<CupStage>>> {
    CupStage::by_parent_id(db.inner(), id.0)
        .await
        .ok()
        .map(Json)
}

/// Retrieve the scorings of a stage
#[openapi]
#[get("/cup/<_cup_id>/stage/<stage_id>/scorings")]
pub async fn cup_stage_scorings(
    _cup_id: Uuid1,
    stage_id: Uuid1,
    db: State<'_, DB>,
) -> Option<Json<Vec<CupStageScoring>>> {
    CupStageScoring::by_parent_id(db.inner(), stage_id.0)
        .await
        .ok()
        .map(Json)
}

/*
 * Signup operations
 * =================
 */

/// Retrieve the signups of a cup, ordered by seed value
#[openapi]
#[get("/cup/<id>/signups")]
pub async fn cup_signups(id: Uuid1, db: State<'_, DB>) -> Result<Json<Vec<CupSignup>>, db::Error> {
    Ok(Json(Cup::get_all_signups(db.inner(), id.0).await?))
}

/// Retrieve signups that match the current session
#[openapi]
#[get("/cup/<cup_id>/signups_self")]
pub async fn cup_signups_self(
    cup_id: Uuid1,
    session: UserSession,
    db: State<'_, DB>,
) -> Result<Json<Vec<CupSignup>>, db::Error> {
    let signups = Cup::player_signups_self(db.inner(), cup_id.0, session.user).await?;
    Ok(Json(signups))
}

/// Checkin solo as the current user
#[openapi]
#[post("/cup/<cup_id>/solo_signup?<team_id>")]
pub async fn cup_signup_solo(
    cup_id: Uuid1,
    team_id: Option<Uuid1>,
    session: UserSession,
    _csrf: ProtectCsrf,
    db: State<'_, DB>,
) -> Result<Json<bool>, db::Error> {
    let ok = match team_id {
        Some(tid) => {
            Cup::make_signup_solo_under_team(db.inner(), cup_id.0, session.user, tid.0).await?
        }
        None => Cup::make_signup_solo(db.inner(), cup_id.0, session.user).await?,
    };
    Ok(Json(ok))
}

/// Checkin solo as the current user with a specified team
#[openapi]
#[post("/cup/<cup_id>/team_signup?<team_id>")]
pub async fn cup_signup_team(
    cup_id: Uuid1,
    team_id: Option<Uuid1>,
    session: UserSession,
    _csrf: ProtectCsrf,
    db: State<'_, DB>,
) -> Result<Json<bool>, db::Error> {
    match team_id {
        Some(tid) => Ok(Json(
            Cup::make_signup_team(db.inner(), cup_id.0, session.user, tid.0).await?,
        )),
        None => Err(db::Error::SomeError("must have team")),
    }
}

/// Leave whatever signup has the current user session player id
#[openapi]
#[post("/cup/<cup_id>/leave")]
pub async fn cup_signup_leave(
    cup_id: Uuid1,
    session: UserSession,
    _csrf: ProtectCsrf,
    db: State<'_, DB>,
) -> Result<Json<bool>, db::Error> {
    let ok = Cup::player_leave(db.inner(), cup_id.0, session.user).await?;
    Ok(Json(ok))
}

/// Checkin whatever signup we have under this session
#[openapi]
#[post("/cup/<cup_id>/checkin")]
pub async fn cup_checkin(
    cup_id: Uuid1,
    session: UserSession,
    _csrf: ProtectCsrf,
    db: State<'_, DB>,
) -> Result<Json<bool>, db::Error> {
    let ok = Cup::player_checkin(db.inner(), cup_id.0, session.user).await?;
    Ok(Json(ok))
}

/// Checkout whatever signup we have under this session
#[openapi]
#[post("/cup/<cup_id>/checkout")]
pub async fn cup_checkout(
    cup_id: Uuid1,
    session: UserSession,
    _csrf: ProtectCsrf,
    db: State<'_, DB>,
) -> Result<Json<bool>, db::Error> {
    let ok = Cup::player_checkout(db.inner(), cup_id.0, session.user).await?;
    Ok(Json(ok))
}

/*
 * Cup creation and updating
 * =========================
 */

#[derive(Serialize, Deserialize, JsonSchema)]
pub struct CupCreateForm {
    pub name: String,
    pub game_mode_id: Uuid,
    pub owner_team_id: Uuid,
    pub is_signups_closed: bool,
    pub is_published: bool,
    pub description: String,
    pub stages: Vec<CupStageCreateForm>,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct CupStageCreateForm {
    pub title: String,
    pub start_immediately: bool,
    pub checkintime: Option<DateTime<Utc>>,
    pub starttime: Option<DateTime<Utc>>,
    pub format: CupStageFormat,
    pub max_participants: Option<i32>,
    pub group_size: Option<i32>,
    pub group_point_award: GroupPointAward,
    pub scorings: Vec<CupStageScoringForm>,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct CupCreate {
    pub cup: Cup,
    pub stages: Vec<CupStage>,
}

/// Create a cup. Must be an admin
#[openapi]
#[post("/cup/create", format = "json", data = "<cup>")]
pub async fn cup_create(
    cup: Json<CupCreateForm>,
    db: State<'_, DB>,
    _sess: AdminSession,
    _csrf: ProtectCsrf,
) -> Result<Json<CupCreate>, db::Error> {
    let Json(cup) = cup;
    let db: &DB = db.inner();
    let mut transaction = db.begin().await?;
    let stages = cup.stages;
    let cup = Cup::create(
        &mut transaction,
        cup.owner_team_id,
        cup.game_mode_id,
        cup.name,
        cup.description,
        cup.is_signups_closed,
        cup.is_published,
    )
    .await?;
    let stages = {
        let result: Vec<CupStage> = vec![];
        for (i, stage) in stages.iter().enumerate() {
            let scorings = &stage.scorings;
            let stage = CupStage::create(
                &mut transaction,
                &stage.title,
                cup.id,
                stage.start_immediately,
                stage.checkintime,
                stage.starttime,
                i as i32,
                stage.format,
                stage.max_participants,
                stage.group_size,
                stage.group_point_award,
            )
            .await?;
            CupStage::set_scoring_config(stage.id, scorings, transaction.begin().await?).await?;
        }
        result
    };
    transaction.commit().await?;
    Ok(Json(CupCreate { cup, stages }))
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub struct CupUpdateForm {
    pub game_mode_id: Option<Uuid>,
    pub name: Option<String>,
    pub description: Option<String>,
    pub is_signups_closed: Option<bool>,
    pub is_published: Option<bool>,
}

/// Update a cup. Must be a captain of the team that owns the cup.
#[openapi]
#[post("/cup/<id>/update", format = "json", data = "<cup>")]
pub async fn cup_update(
    id: Uuid1,
    cup: Json<CupUpdateForm>,
    db: State<'_, DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> Result<Json<Option<Cup>>, db::Error> {
    let db: &DB = db.inner();
    Cup::require_ownership(db, id.0, sess.user).await?;
    let mut transaction = db.begin().await?;
    let Json(cup) = cup;
    let cup = Cup::update(
        &mut transaction,
        id.0,
        cup.name,
        cup.description,
        cup.is_signups_closed,
        cup.is_published,
    )
    .await?;
    transaction.commit().await?;
    Ok(Json(cup))
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct CupStageUpdateForm {
    /// None means this is a new stage
    pub stage_id: Option<Uuid>,
    pub title: Option<String>,
    pub checkintime: Option<DateTime<Utc>>,
    pub starttime: Option<DateTime<Utc>>,
    pub format: Option<CupStageFormat>,
    pub max_participants: Option<Option<i32>>,
    pub group_size: Option<Option<i32>>,
    pub group_point_award: Option<GroupPointAward>,
}

/// Update a cup stage. Must be a captain of the team that owns the cup.
#[openapi]
#[post("/cup_stage/<stage_id>/update", format = "json", data = "<stage>")]
pub async fn cup_stage_update(
    stage_id: Uuid1,
    stage: Json<CupStageUpdateForm>,
    db: State<'_, DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> Result<Json<CupStage>, db::Error> {
    let db = db.inner();
    CupStage::require_ownership(db, stage_id.0, sess.user).await?;
    let mut transaction = db.begin().await?;
    let Json(stage) = stage;
    let stage = CupStage::update(
        &mut transaction,
        stage_id.0,
        stage.title,
        None, // cup_id cannot change
        stage.checkintime,
        stage.starttime,
        None, // no changing of stage_no as requires reordering the other stages
        stage.format,
        stage.max_participants,
        stage.group_size,
        stage.group_point_award,
    )
    .await?;
    transaction.commit().await?;
    Ok(Json(stage))
}

/// Update a cup stage. Must be a captain of the team that owns the cup.
#[openapi]
#[post("/cup/<cup_id>/stage/<stage_id>/restart")]
pub async fn cup_stage_restart(
    cup_id: Uuid1,
    stage_id: Uuid1,
    db: State<'_, DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> Result<(), db::Error> {
    let db = db.inner();
    CupStage::require_ownership(db, stage_id.0, sess.user).await?;
    let cur = query!(
        "SELECT current_stage, (SELECT s.id FROM cup_stage s WHERE s.cup_id = cup.id AND s.stage_no = cup.current_stage) FROM cup WHERE id = $1", cup_id.0)
        .fetch_one(db)
        .await?;
    if cur.id == Some(stage_id.0) {
        query!(
            "DELETE FROM cup_match_2p WHERE cup_stage_id = $1",
            stage_id.0
        )
        .execute(db)
        .await?;
        query!(
            "UPDATE cup_stage SET is_started = false WHERE id = $1",
            stage_id.0
        )
        .execute(db)
        .await?;
        CupStage::start_stage(db, cup_id.0, stage_id.0).await?;
    } else {
        log::warn!("cannot restart that stage, it is not the current stage");
    }
    Ok(())
}

/// Update a cup. Must be a captain of the team that owns the cup.
#[openapi]
#[post("/cup/<id>/reset")]
pub async fn cup_reset(
    id: Uuid1,
    db: State<'_, DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> Result<(), db::Error> {
    let db: &DB = db.inner();
    Cup::require_ownership(db, id.0, sess.user).await?;
    Cup::reset(db, id.0).await?;
    Ok(())
}

#[openapi]
#[post("/cup/<id>/delete")]
pub async fn cup_delete(
    id: Uuid1,
    db: State<'_, DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> Result<Json<bool>, db::Error> {
    let db = db.inner();
    Cup::require_ownership(db, id.0, sess.user).await?;
    Ok(Json(Cup::delete(db.begin().await?, id.0).await?))
}

#[openapi]
#[post("/cup/<id>/set_seeds", format = "json", data = "<seeds>")]
pub async fn cup_set_seeds(
    id: Uuid1,
    seeds: Json<Vec<SetSeed>>,
    db: State<'_, DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> Result<Json<bool>, db::Error> {
    let db = db.inner();
    Cup::require_ownership(db, id.0, sess.user).await?;
    Ok(Json(Cup::set_seeds(db, seeds.0).await?))
}

#[openapi]
#[post("/cup/<id>/add_signup", format = "json", data = "<signup>")]
pub async fn cup_add_signup(
    id: Uuid1,
    signup: Json<AddSignup>,
    db: State<'_, DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> Result<Json<bool>, db::Error> {
    let db = db.inner();
    Cup::require_ownership(db, id.0, sess.user).await?;
    Ok(Json(Cup::add_signup(db, id.0, signup.0).await?))
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub struct CurrentStage {
    pub stage_id: Option<Uuid>,
}

#[openapi]
#[get("/cup/<cup_id>/current_stage")]
pub async fn cup_current_stage(cup_id: Uuid1, db: State<'_, DB>) -> db::Result<Json<CurrentStage>> {
    let db = db.inner();
    let stage_id = Cup::current_stage(cup_id.0, db).await?;
    Ok(Json(CurrentStage { stage_id }))
}

#[openapi]
#[post("/cup_stage/<stage_id>/delete")]
pub async fn cup_stage_delete(
    stage_id: Uuid1,
    db: State<'_, DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> Result<Json<bool>, db::Error> {
    let db = db.inner();
    Cup::require_ownership(db, stage_id.0, sess.user).await?;
    Ok(Json(CupStage::delete(db.begin().await?, stage_id.0).await?))
}

#[openapi]
#[post("/cup/<cup_id>/stage/<stage_id>/initialise")]
pub async fn cup_stage_initialise(
    cup_id: Uuid1,
    stage_id: Uuid1,
    sess: UserSession,
    _csrf: ProtectCsrf,
    db: State<'_, DB>,
) -> Result<Json<bool>, db::Error> {
    let db = db.inner();
    Cup::require_ownership(db, cup_id.0, sess.user).await?;
    Ok(Json(CupStage::start_stage(db, cup_id.0, stage_id.0).await?))
}

#[openapi]
#[get("/cup_stage/<stage_id>/matches_2p")]
pub async fn cup_stage_matches_2p(
    stage_id: Uuid1,
    db: State<'_, DB>,
) -> db::Result<Json<Vec<CupMatch2p>>> {
    Ok(Json(
        CupMatch2p::by_cup_stage_id(db.inner(), stage_id.0).await?,
    ))
}

#[openapi]
#[get("/cup_stage/<stage_id>/pending_matches/<signup_ids>")]
pub async fn cup_stage_pending_matches_for(
    stage_id: Uuid1,
    signup_ids: Uuids,
    db: State<'_, DB>,
) -> db::Result<Json<Vec<CupMatch2p>>> {
    let signup_ids = signup_ids.0;
    let matches = if signup_ids.len() > 0 {
        CupMatch2p::signups_pending_matches(db.inner(), stage_id.0, signup_ids).await?
    } else {
        CupMatch2p::pending_matches(db.inner(), stage_id.0).await?
    };
    Ok(Json(matches))
}

#[openapi]
#[get("/cup_stage/<stage_id>/pending_matches")]
pub async fn cup_stage_pending_matches(
    stage_id: Uuid1,
    db: State<'_, DB>,
) -> db::Result<Json<Vec<CupMatch2p>>> {
    let matches = CupMatch2p::pending_matches(db.inner(), stage_id.0).await?;
    Ok(Json(matches))
}

#[openapi]
#[get("/cup_stage_scoring/<scoring_id>")]
pub async fn cup_stage_scoring(
    scoring_id: i32,
    db: State<'_, DB>,
) -> db::Result<Json<CupStageScoring>> {
    let db = db.inner();
    Ok(Json(CupStageScoring::by_id(db, scoring_id).await?))
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct CupMatchReportForm2p {
    match_id: i32,
    low_report: Vec<i32>,
    high_report: Vec<i32>,
}

#[openapi]
#[post("/cup_match/<match_id>/report", format = "json", data = "<report>")]
pub async fn match_report_2p(
    match_id: i32,
    report: Json<CupMatchReportForm2p>,
    sess: UserSession,
    _csrf: ProtectCsrf,
    db: State<'_, DB>,
) -> db::Result<Json<CupMatch2p>> {
    let db = db.inner();
    let reporter = CupMatch2p::require_participant_of(db, match_id, sess.user).await?;
    CupMatch2p::make_report(
        db,
        match_id,
        reporter,
        &report.low_report,
        &report.high_report,
    )
    .await?;
    Ok(Json(CupMatch2p::by_id(db, match_id).await?))
}

#[openapi]
#[post(
    "/cup_match/<match_id>/override_report",
    format = "json",
    data = "<report>"
)]
pub async fn match_override_report_2p(
    match_id: i32,
    report: Json<CupMatchReportForm2p>,
    sess: UserSession,
    _csrf: ProtectCsrf,
    db: State<'_, DB>,
) -> db::Result<Json<CupMatch2p>> {
    let db = db.inner();
    let cup_id = query!(
        "SELECT s.cup_id FROM cup_match_2p m JOIN cup_stage s ON s.id = m.cup_stage_id LIMIT 1"
    )
    .fetch_one(db)
    .await?
    .cup_id;
    Cup::require_ownership(db, cup_id, sess.user).await?;
    CupMatch2p::make_report(
        db,
        match_id,
        MatchReporter::Low,
        &report.low_report,
        &report.high_report,
    )
    .await?;
    CupMatch2p::make_report(
        db,
        match_id,
        MatchReporter::High,
        &report.low_report,
        &report.high_report,
    )
    .await?;
    Ok(Json(CupMatch2p::by_id(db, match_id).await?))
}

#[openapi]
#[get("/cup_match/<match_id>/result")]
pub async fn match_result_2p(match_id: i32, db: State<'_, DB>) -> db::Result<Json<CupMatch2p>> {
    Ok(Json(CupMatch2p::by_id(db.inner(), match_id).await?))
}

#[openapi]
#[get("/cup_stage/<stage_id>/standings")]
pub async fn cup_stage_standings(
    stage_id: Uuid1,
    db: State<'_, DB>,
) -> db::Result<Json<Standings>> {
    let db = db.inner();
    let stage = CupStage::by_id(db, stage_id.0).await?;
    let standings = stage.standings(db).await?;
    Ok(Json(standings))
}
