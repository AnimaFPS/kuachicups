use super::paginate::{Paged, PaginateApi};
use crate::db::{ById, Game, GameMode, HasParent, Result, DB};
use crate::handlers::uuids::{Uuid1, Uuids};
use rocket::{get, State};
use rocket_contrib::json::Json;
use rocket_okapi::openapi;
use std::marker::PhantomData;
use uuid::Uuid;

#[openapi]
#[get("/games?<page>")]
pub async fn games_page(page: Option<i64>, db: State<'_, DB>) -> Option<Json<Paged<Uuid>>> {
    Game::get_page(db.inner(), page).await
}

#[openapi]
#[get("/games?all")]
pub async fn games_all(db: State<'_, DB>) -> Option<Json<Vec<Game>>> {
    Game::get_all(db.inner()).await
}

#[openapi]
#[get("/games/<ids>", rank = 2)]
pub async fn games(ids: Uuids, db: State<'_, DB>) -> Option<Json<Vec<Game>>> {
    Game::get_by_ids(db.inner(), &ids.0).await
}

#[openapi]
#[get("/games/<id>/modes", rank = 1)]
pub async fn game_modes(id: Uuid1, db: State<'_, DB>) -> Result<Json<Vec<GameMode>>> {
    GameMode::by_parent_id(db.inner(), id.0).await.map(Json)
}

#[openapi]
#[get("/games/modes/<id>", rank = 0)]
pub async fn game_mode_by_id(id: Uuid1, db: State<'_, DB>) -> Result<Json<GameMode>> {
    GameMode::by_id(db.inner(), id.0).await.map(Json)
}

/*
#[openapi]
#[get("/games/<id>/mc_pb_cfg_v1")]
pub async fn game_mode_match_control_cfg_v1(
    id: Uuid1,
    db: State<'_, DB>,
) -> Result<Json<Vec<GameModeMatchControlConfig>>> {
    GameModeMatchControlConfig::by_parent_id(db.inner(), &id.0)
        .await
        .map(Json)
}
*/
