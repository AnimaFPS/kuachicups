use chrono::{DateTime, Utc};
use rocket::{get, post, State};
use rocket_contrib::json::Json;
use rocket_okapi::openapi;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use slug::slugify;
use sqlx::{query, query_as};
use std::marker::PhantomData;
use uuid::Uuid;

use super::super::csrf::ProtectCsrf;
use super::super::uuids::{Uuid1, Uuids};
use super::paginate::{paged1, Page, PageItem, Paged, PaginateApi, PAGE_LEN};
use super::player::AdminSession;
use crate::app::{DiscordClientBuilder, SettingCache};
use crate::db::{
    ById, CreatePostAnn, Error, Paginated, PostAnn, PostAnnUnpublished, PostMedia, Result,
    UpdatePostAnn, DB,
};

#[openapi]
#[get("/anns/<ids>")]
pub async fn anns(ids: Uuids, db: State<'_, DB>) -> Result<Json<Vec<PostAnn>>> {
    Ok(Json(PostAnn::by_ids(db.inner(), &ids.0).await?))
}

#[openapi]
#[get("/anns/page?<page>")]
pub async fn anns_page(page: Option<i64>, db: State<'_, DB>) -> Option<Json<Paged<Uuid>>> {
    PostAnn::get_page(db.inner(), page).await
}

#[openapi]
#[get("/anns/unpublished/page?<page>")]
pub async fn anns_unpublished_page(
    page: Option<i64>,
    db: State<'_, DB>,
    _sess: AdminSession,
) -> Option<Json<Paged<Uuid>>> {
    PostAnnUnpublished::get_page(db.inner(), page).await
}

#[openapi]
#[post("/posts/new", format = "json", data = "<post>")]
pub async fn create_ann(
    post: Json<CreatePostAnn>,
    db: State<'_, DB>,
    sess: AdminSession,
    _csrf: ProtectCsrf,
) -> Result<Json<PostAnn>> {
    assert_eq!(post.0.author_id, sess.0.user);
    Ok(Json(PostAnn::create(post.0, db.inner()).await?))
}

#[openapi]
#[post("/posts/update", format = "json", data = "<post>")]
pub async fn update_ann(
    post: Json<UpdatePostAnn>,
    db: State<'_, DB>,
    dcb: State<'_, DiscordClientBuilder>,
    _sess: AdminSession,
    _csrf: ProtectCsrf,
) -> Result<Json<PostAnn>> {
    // allows updates from other users
    let discord = dcb.inner().client()?;
    Ok(Json(PostAnn::update(post.0, discord, db.inner()).await?))
}

#[openapi]
#[post("/posts/delete/<post_id>")]
pub async fn delete_ann(
    post_id: Uuid1,
    db: State<'_, DB>,
    dcb: State<'_, DiscordClientBuilder>,
    _sess: AdminSession,
    _csrf: ProtectCsrf,
) -> Result<()> {
    // allows deletes from other users
    let discord = dcb.inner().client()?;
    Ok(PostAnn::delete(post_id.0, discord, db.inner()).await?)
}

#[openapi]
#[post("/posts/<post_id>/publish_ann/<uri>")]
pub async fn publish_ann(
    post_id: Uuid1,
    uri: String,
    db: State<'_, DB>,
    dcb: State<'_, DiscordClientBuilder>,
    _sess: AdminSession,
    _csrf: ProtectCsrf,
) -> Result<()> {
    let db = db.inner();
    let post = PostAnn::by_id(db, post_id.0).await?;
    let discord = dcb.inner().client()?;
    Ok(post.publish(uri, discord, db).await?)
}

////////////
//////////// MEDIA
////////////

#[openapi]
#[get("/media/page?<page>")]
pub async fn medias_page(page: Option<i64>, db: State<'_, DB>) -> Option<Json<Paged<Uuid>>> {
    PostMedia::get_page(db.inner(), page).await
}

#[openapi]
#[get("/media/<ids>")]
pub async fn medias(ids: Uuids, db: State<'_, DB>) -> Result<Json<Vec<PostMedia>>> {
    Ok(Json(PostMedia::by_ids(db.inner(), &ids.0).await?))
}
