use super::{
    paginate::{Paged, PaginateApi, PaginateSearchApi},
    player::{AdminSession, UserSession},
};
use crate::{
    db,
    db::{
        AcceptInviteTeamMember, InviteTeamMember, Player, Role, SetTeamMember, Team,
        TeamCreateForm, TeamMember, TeamMemberInvite, TeamPlayer, TeamUpdateForm, DB,
    },
    handlers::{
        csrf::ProtectCsrf,
        uuids::{Uuid1, Uuids},
    },
};
use rocket::{get, post, State};
use rocket_contrib::json::Json;
use rocket_okapi::openapi;
use std::marker::PhantomData;
use uuid::Uuid;

#[openapi]
#[get("/teams/<ids>")]
pub async fn teams(ids: Uuids, db: State<'_, DB>) -> Option<Json<Vec<Team>>> {
    Team::get_by_ids(db.inner(), &ids.0).await
}

#[openapi]
#[get("/teams/page?<page>&<terms>")]
pub async fn teams_page(
    page: Option<i64>,
    terms: Option<String>,
    db: State<'_, DB>,
) -> Option<Json<Paged<Uuid>>> {
    if let Some(terms) = terms {
        Team::get_page_search(db.inner(), page, &terms).await
    } else {
        Team::get_page(db.inner(), page).await
    }
}

#[openapi]
#[get("/team/<id>/players")]
pub async fn players(id: Uuid1, db: State<'_, DB>) -> Option<Json<Vec<TeamPlayer>>> {
    Some(Json(Team::players(db.inner(), &id.0).await.ok()?))
}

#[openapi]
#[get("/team/<id>/invites")]
pub async fn invites(
    id: Uuid1,
    db: State<'_, DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> db::Result<Json<Vec<TeamMemberInvite>>> {
    let db = db.inner();
    sess.require_team_role(db, id.0, Role::Admin).await?;
    Ok(Json(Team::invites(id.0, db).await?))
}

#[openapi]
#[post("/team/create", data = "<team>", format = "json")]
pub async fn create(
    team: Json<TeamCreateForm>,
    db: State<'_, DB>,
    _sess: UserSession,
    _csrf: ProtectCsrf,
) -> db::Result<Json<Team>> {
    Ok(Json(Team::create(team.0, db.inner()).await?))
}

#[openapi]
#[post("/team/update", data = "<team>", format = "json")]
pub async fn update(
    team: Json<TeamUpdateForm>,
    db: State<'_, DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> db::Result<Json<Team>> {
    let db = db.inner();
    sess.require_team_role(db, team.0.id, Role::Admin).await?;
    Ok(Json(Team::update(team.0, db).await?))
}

#[openapi]
#[post("/team/set_member", data = "<member>", format = "json")]
pub async fn set_member(
    member: Json<SetTeamMember>,
    db: State<'_, DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> db::Result<Json<u64>> {
    let db = db.inner();
    sess.require_team_role(db, member.0.team_id, Role::Admin)
        .await?;
    Ok(Json(Team::set_member(member.0, false, db).await?))
}

#[openapi]
#[post("/team/member_leave/<team_id>")]
pub async fn leave(
    team_id: Uuid1,
    db: State<'_, DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> db::Result<Json<u64>> {
    let db = db.inner();
    sess.require_team_role(db, team_id.0, Role::Player).await?;
    Ok(Json(
        Team::set_member(
            SetTeamMember {
                team_id: team_id.0,
                player_id: sess.user,
                player_role: None,
            },
            false,
            db,
        )
        .await?,
    ))
}

#[openapi]
#[post("/team/override_member", data = "<member>", format = "json")]
pub async fn override_member(
    member: Json<SetTeamMember>,
    db: State<'_, DB>,
    _sess: AdminSession,
    _csrf: ProtectCsrf,
) -> db::Result<Json<u64>> {
    let db = db.inner();
    Ok(Json(Team::set_member(member.0, true, db).await?))
}

#[openapi]
#[post("/team/make_invite", data = "<invite>", format = "json")]
pub async fn make_invite(
    invite: Json<InviteTeamMember>,
    db: State<'_, DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> db::Result<Json<u64>> {
    if sess.user != invite.0.invite_by_player_id {
        return Err(db::Error::SomeError("must own invite"));
    }
    let db = db.inner();
    sess.require_team_role(db, invite.0.team_id, Role::Admin)
        .await?;
    Ok(Json(Team::make_invite(invite.0, db).await?))
}

#[openapi]
#[post("/team/reject_invite", data = "<invite>", format = "json")]
pub async fn reject_invite(
    invite: Json<AcceptInviteTeamMember>,
    db: State<'_, DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> db::Result<Json<u64>> {
    if sess.user != invite.0.player_id {
        return Err(db::Error::SomeError("must own invite"));
    }
    let db = db.inner();
    Ok(Json(Team::reject_invite(invite.0, db).await?))
}

#[openapi]
#[post("/team/accept_invite", data = "<invite>", format = "json")]
pub async fn accept_invite(
    invite: Json<AcceptInviteTeamMember>,
    db: State<'_, DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> db::Result<Json<u64>> {
    if sess.user != invite.0.player_id {
        return Err(db::Error::SomeError("must own invite"));
    }
    let db = db.inner();
    Ok(Json(Team::accept_invite(invite.0, db).await?))
}
