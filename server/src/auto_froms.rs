#[macro_export]
macro_rules! auto_froms {
    ($enum:ty, {
        $( $variant:ty = $constr:expr ;)*
    }) => {
        $(
            impl std::convert::From< $variant > for $enum {
                #[inline(always)]
                fn from(x: $variant) -> $enum {
                    $constr(x)
                }
            }
        )*
    };
}
