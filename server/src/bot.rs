use crate::config::Config;
use crate::db;
use crate::db::{Player, PostMedia, DB, MEDIA_CHANNEL};
use futures::stream::StreamExt;
use sqlx::postgres::PgPool;
use std::error::Error;
use std::time::Duration;
use twilight_gateway::{Event, Intents, Shard};
use twilight_http::Client;
use twilight_model::{
    channel::{Reaction, ReactionType},
    gateway::payload::{ReactionAdd, ReactionRemove},
    guild::Member,
    id::{ChannelId, EmojiId, GuildId, MessageId, RoleId, UserId},
};
use twilight_standby::Standby;

const GUILD: GuildId = GuildId(649789878925525013);
const ADD_EMOJI: EmojiId = EmojiId(767932236497551442);
const ADD_EMOJI_ANIMATED: bool = false;
const ADD_EMOJI_NAME: &'static str = "gg";
const ADMIN_ROLE: RoleId = RoleId(649792108831703041);

async fn add_media_msg(
    user_id: String,
    channel_id: ChannelId,
    message_id: MessageId,
    db: DB,
    client: Client,
) -> Result<(), db::Error> {
    log::info!("adding media {}", message_id);
    let player_id = Player::by_discord_id(&db, &user_id).await?.id;
    let is_admin = Player::is_admin(&db, player_id).await?.is_admin;
    if is_admin {
        if let Some(message) = client.message(channel_id, message_id).await? {
            let post = PostMedia::create(player_id, message, &db).await?;
            log::info!("added media {:#?}", post);
        }
    }
    Ok(())
}

async fn remove_media_msg(
    user_id: String,
    channel_id: ChannelId,
    message_id: MessageId,
    db: DB,
    client: Client,
) -> Result<(), db::Error> {
    log::info!("removing media {}", message_id);
    let player_id = Player::by_discord_id(&db, &user_id).await?.id;
    let is_admin = Player::is_admin(&db, player_id).await?.is_admin;
    if is_admin {
        if let Some(message) = client.message(channel_id, message_id).await? {
            PostMedia::delete(message, &db).await?;
            log::info!("removed media {}", message_id);
        }
    }
    Ok(())
}

pub async fn main(config: Config) -> Result<(), Box<dyn Error>> {
    let db = PgPool::connect(&config.db_uri).await?;
    let intents = Intents::GUILD_MESSAGES
        | Intents::GUILD_MESSAGE_REACTIONS
        | Intents::GUILD_EMOJIS
        | Intents::GUILD_MEMBERS;
    let client = Client::new(config.discord_bot_token.clone());
    let mut shard = Shard::new(config.discord_bot_token, intents);
    let mut events = shard.events();
    shard.start().await?;

    while let Some(event) = events.next().await {
        match &event {
            Event::ReactionAdd(box ReactionAdd(Reaction {
                channel_id,
                message_id,
                emoji:
                    ReactionType::Custom {
                        animated: emoji_animated,
                        id: emoji_id,
                        name: Some(emoji_name),
                    },
                member: Some(Member { roles, user, .. }),
                ..
            })) if *channel_id == MEDIA_CHANNEL
                && *emoji_animated == ADD_EMOJI_ANIMATED
                && *emoji_id == ADD_EMOJI
                && &*emoji_name == ADD_EMOJI_NAME
                && roles.iter().any(|r| *r == ADMIN_ROLE) =>
            {
                tokio::spawn(add_media_msg(
                    user.id.to_string(),
                    *channel_id,
                    *message_id,
                    db.clone(),
                    client.clone(),
                ));
            }

            Event::ReactionRemove(box ReactionRemove(Reaction {
                channel_id,
                message_id,
                user_id,
                emoji:
                    ReactionType::Custom {
                        animated: emoji_animated,
                        id: emoji_id,
                        name: Some(emoji_name),
                    },
                ..
            })) if *channel_id == MEDIA_CHANNEL
                && *emoji_animated == ADD_EMOJI_ANIMATED
                && *emoji_id == ADD_EMOJI
                && &*emoji_name == ADD_EMOJI_NAME
                && client
                    .guild_member(GUILD, *user_id)
                    .await?
                    .iter()
                    .map(|m| m.roles.iter())
                    .flatten()
                    .any(|r| *r == ADMIN_ROLE) =>
            {
                tokio::spawn(remove_media_msg(
                    user_id.to_string(),
                    *channel_id,
                    *message_id,
                    db.clone(),
                    client.clone(),
                ));
            }

            _ => {}
        }
    }
    Ok(())
}
