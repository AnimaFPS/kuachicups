use comrak;

pub fn markdown_to_html(md: &str) -> String {
    let mut opts: comrak::ComrakOptions = Default::default();
    opts.render.unsafe_ = false;
    opts.render.escape = true;
    opts.extension.table = true;
    opts.extension.autolink = true;
    opts.extension.tasklist = true;
    opts.extension.superscript = true;
    opts.extension.footnotes = true;
    opts.extension.description_lists = true;
    comrak::markdown_to_html(md, &opts)
}
