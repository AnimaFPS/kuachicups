use clap::Clap;
use log::info;
use rand::prelude::random;
use std::path::PathBuf;

#[derive(Clap, Debug, serde::Deserialize, serde::Serialize, Clone)]
#[clap(
    name = "kuachicups",
    author = "Mike Ledger <eleventynine@gmail.com>",
    rename_all = "kebab-case",
    rename_all_env = "screaming-snake"
)]
pub struct Config {
    #[clap(long, env)]
    pub static_dir: PathBuf,

    #[clap(long, env)]
    pub static_enabled: bool,

    #[clap(long, env)]
    pub db_uri: String,

    #[clap(default_value = "0", long, env)]
    pub db_pool_min_size: u32,

    #[clap(default_value = "5", long, env)]
    pub db_pool_max_size: u32,

    #[clap(long, env)]
    pub oauth2_client_id: String,

    #[clap(long, env)]
    pub oauth2_client_secret: String,

    #[clap(long, env)]
    pub discord_bot_token: String,

    #[clap(long, env)]
    pub discord_redirect_url: String,

    #[clap(default_value = "../db", long, env)]
    pub migration_dir: PathBuf,
}

#[derive(Debug)]
pub struct Secrets {
    pub csrf_secret: [u8; 32],
}

impl Secrets {
    pub fn generate() -> Secrets {
        Secrets {
            csrf_secret: random(),
        }
    }
}

#[derive(Debug)]
pub enum Error {
    ClapError(clap::Error),
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:#?}", self)
    }
}

impl std::error::Error for Error {}

impl From<clap::Error> for Error {
    fn from(x: clap::Error) -> Error {
        Error::ClapError(x)
    }
}

impl Config {
    pub fn create() -> Result<Config, Error> {
        dotenv::dotenv().ok();
        let cfg = Config::try_parse()?;
        info!("{}", toml::to_string(&cfg).unwrap());
        Ok(cfg)
    }
}
