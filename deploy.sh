#!/usr/bin/env bash
set -e
docker build . -t quasimal/kuachi.gg:latest
docker push quasimal/kuachi.gg:latest

OPT_REMOTE=true
OPT_FG=false
OPTIND=1

while [[ "$#" -gt 0 ]]; do
  case $1 in
    -detach) OPT_REMOTE=true ;;
    -fg) OPT_FG=true ;;
    -local) OPT_REMOTE=false ;;
    *) echo "Unknown parameter passed: $1"; exit 1 ;;
  esac
  shift
done


if $OPT_REMOTE; then
  export DOCKER_HOST="ssh://mike@kuachi.gg"
  echo Remote deployment to "${DOCKER_HOST}"...
fi

docker-compose pull --no-parallel

if $OPT_FG; then
  docker-compose up --remove-orphans
else
  docker-compose up --remove-orphans -d
fi
